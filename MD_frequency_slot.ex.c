#include "MD_frequency_slot.h"
#include "MD_frequency_index.h"
#include "math.h"
#include "opnet.h"

struct Frequency_Slot {
	Frequency_Index	*lowest_freq_index;
	Frequency_Index	*highest_freq_index;
};



Frequency_Slot* frequency_slot_create(Frequency_Index* lowest_fi_ptr, Frequency_Index* highest_fi_ptr) {
/**
	Creates a new frequency slot given the lowest and higest
	frequency indexes
**/
	Frequency_Slot* slot_ptr;
	FIN( frequency_slot_create(lowest_fi_ptr, highest_fi_ptr) );
	if ( lowest_fi_ptr == PRGC_NIL || highest_fi_ptr == PRGC_NIL ) FRET(PRGC_NIL);
	if ( OPC_TRUE == frequency_index_is_lower(highest_fi_ptr, lowest_fi_ptr) ) {
		op_sim_end("MD_frequency_slot.c","frequency_slot_create","lowest freq higher than highest","");
		FRET(PRGC_NIL);
	}
	slot_ptr = prg_mem_alloc(sizeof(Frequency_Slot));
	slot_ptr->lowest_freq_index = lowest_fi_ptr;
	slot_ptr->highest_freq_index = highest_fi_ptr;
	FRET(slot_ptr);
}


void frequency_slot_destroy(Frequency_Slot* slot_ptr) {
/**
	Destroys the frequency slot
**/
	FIN( frequency_slot_destroy(slot_ptr) );
	if ( PRGC_NIL == slot_ptr ) FOUT;
	frequency_index_destroy(slot_ptr->lowest_freq_index);
	frequency_index_destroy(slot_ptr->highest_freq_index);
	prg_mem_free(slot_ptr);
	FOUT;
}


Frequency_Slot* frequency_slot_copy(Frequency_Slot const* const slot_ptr) {
/**
	Returns a deep copy of the frequency slot.
**/
	Frequency_Slot *copied_slot_ptr;
	FIN( frequency_slot_copy(slot_ptr) );
	if ( PRGC_NIL == slot_ptr ) FRET(PRGC_NIL);
	copied_slot_ptr = prg_mem_alloc(sizeof(Frequency_Slot));
	copied_slot_ptr->lowest_freq_index = frequency_index_copy(slot_ptr->lowest_freq_index);
	copied_slot_ptr->highest_freq_index = frequency_index_copy(slot_ptr->highest_freq_index);
	FRET(copied_slot_ptr);
}


short frequency_slot_resize(Frequency_Slot* slot_ptr, Frequency_Index* lowest_fi_ptr, Frequency_Index* highest_fi_ptr) {
/**
	Resize an existing frequency slot given the lowest and higest
	frequency indexes
**/
	FIN( frequency_slot_create(lowest_fi_ptr, highest_fi_ptr) );
	if ( lowest_fi_ptr == PRGC_NIL || highest_fi_ptr == PRGC_NIL ) FRET(SLOTC_FAIL);
	if ( OPC_TRUE == frequency_index_is_lower(highest_fi_ptr, lowest_fi_ptr) ) {
		op_sim_end("MD_frequency_slot.c","frequency_slot_create","lowest freq higher than highest","");
		FRET(SLOTC_FAIL);
	}
	frequency_index_destroy(slot_ptr->lowest_freq_index);
	frequency_index_destroy(slot_ptr->highest_freq_index);
	slot_ptr->lowest_freq_index = lowest_fi_ptr;
	slot_ptr->highest_freq_index = highest_fi_ptr;
	FRET(SLOTC_SUCCESS);
}


short frequency_slot_lowest_frequency_set(Frequency_Slot* slot_ptr, Frequency_Index* fi_ptr) {
/**
	Set the lowest frequency index of the slot
	with the given one.
**/
	FIN( frequency_slot_lowest_frequency_set( slot_ptr, fi_ptr) );
	if (PRGC_NIL == slot_ptr || PRGC_NIL == fi_ptr) FRET(SLOTC_FAIL);
	if ( OPC_TRUE == frequency_index_is_lower(fi_ptr, slot_ptr->highest_freq_index) ) {
		frequency_index_destroy(slot_ptr->lowest_freq_index);
		slot_ptr->lowest_freq_index = fi_ptr;
		FRET(SLOTC_SUCCESS);
	}
	else {
		op_sim_end("MD_frequency_slot.c","frequency_slot_lowest_frequency_set","lowest freq higher than highest","");
		FRET(SLOTC_FAIL);
	}
}


short frequency_slot_highest_frequency_set(Frequency_Slot* slot_ptr, Frequency_Index* fi_ptr) {
/**
	Set the highest frequency index of the slot
	with the given one.
**/
	FIN( frequency_slot_highest_frequency_set( slot_ptr, fi_ptr) );
	if (PRGC_NIL == slot_ptr || PRGC_NIL == fi_ptr) FRET(SLOTC_FAIL);
	if ( OPC_TRUE == frequency_index_is_higher(fi_ptr, slot_ptr->lowest_freq_index) ) {
		frequency_index_destroy(slot_ptr->highest_freq_index);
		slot_ptr->highest_freq_index = fi_ptr;
		FRET(SLOTC_SUCCESS);
	}
	else {
		op_sim_end("MD_frequency_slot.c","frequency_slot_lowest_frequency_set","highest freq lower than lowest","");
		FRET(SLOTC_FAIL);
	}
}


Frequency_Index const* frequency_slot_highest_frequency_access(Frequency_Slot const* const slot_ptr) {
/**
	Returns a pointer to a constant frequency index
	pointing to the highest frequency index of the slot
**/
	FIN(frequency_slot_highest_frequency_access(slot_ptr));
	if (PRGC_NIL == slot_ptr) FRET(PRGC_NIL);
	FRET(slot_ptr->highest_freq_index);
}


Frequency_Index const* frequency_slot_lowest_frequency_access(Frequency_Slot const* const slot_ptr) {
/**
	Returns a pointer to a constant frequency index
	pointing to the lowest frequency index of the slot
**/
	FIN(frequency_slot_lowest_frequency_access(slot_ptr));
	if (PRGC_NIL == slot_ptr) FRET(PRGC_NIL);
	FRET(slot_ptr->lowest_freq_index);
}


//Frequency_Index* frequency_slot_highest_frequency_get(Frequency_Slot const* const slot_ptr);
//Frequency_Index* frequency_slot_lowest_frequency_get(Frequency_Slot const* const slot_ptr);


short frequency_slot_union(Frequency_Slot* slot_a_ptr,Frequency_Slot const* const slot_b_ptr) {
/**
	Makes the union between two slots.
	They must be INTERSECT or at least ADJACENT
**/
	FIN( frequency_slot_union(slot_a_ptr, slot_b_ptr) );
	if ( PRGC_NIL == slot_a_ptr || PRGC_NIL == slot_b_ptr ) FRET(SLOTC_FAIL);
	switch ( frequency_slot_compare(slot_a_ptr,slot_b_ptr) ) {
		case SLOTC_LOWER_ADJACENT:
		case SLOTC_LOWER_INTERSECT:
			frequency_index_destroy(slot_a_ptr->highest_freq_index);
			slot_a_ptr->highest_freq_index = frequency_index_copy(slot_b_ptr->highest_freq_index);
			FRET(SLOTC_SUCCESS);
			break;
		case SLOTC_HIGHER_ADJACENT:
		case SLOTC_HIGHER_INTERSECT:
			frequency_index_destroy(slot_a_ptr->lowest_freq_index);
			slot_a_ptr->lowest_freq_index = frequency_index_copy(slot_b_ptr->lowest_freq_index);
			FRET(SLOTC_SUCCESS);
			break;
		case SLOTC_IS_INCLUDED:
			frequency_index_destroy(slot_a_ptr->lowest_freq_index);
			frequency_index_destroy(slot_a_ptr->highest_freq_index);
			slot_a_ptr->lowest_freq_index = frequency_index_copy(slot_b_ptr->lowest_freq_index);
			slot_a_ptr->highest_freq_index = frequency_index_copy(slot_b_ptr->highest_freq_index);
			FRET(SLOTC_SUCCESS);
			break;
		case SLOTC_INCLUDE:
		case SLOTC_EQUAL:
			FRET(SLOTC_SUCCESS);
			break;
		default:
			FRET(SLOTC_FAIL);
			break;
	}
	FRET(SLOTC_FAIL);
}


short frequency_slot_subtract(Frequency_Slot* slot_a_ptr,Frequency_Slot const* const slot_b_ptr) {
/**
	Subtract the intersection from slot_a.
	slot_b must not be included in slot_a.
	if slot_a and slot_b are equal or slot_a is included in slot_b,
	slot_a is destroyed and replaced by PRGC_NIL pointer.
**/
	FIN( frequency_slot_subtract(slot_a_ptr, slot_b_ptr) );
	if ( PRGC_NIL == slot_a_ptr || PRGC_NIL == slot_b_ptr ) FRET(SLOTC_FAIL);
	switch ( frequency_slot_compare(slot_a_ptr, slot_b_ptr) ) {
		case SLOTC_HIGHER_INTERSECT:
			frequency_index_destroy(slot_a_ptr->lowest_freq_index);
			slot_a_ptr->lowest_freq_index = frequency_index_copy(slot_b_ptr->highest_freq_index);
			FRET(SLOTC_SUCCESS);
			break;
		case SLOTC_LOWER_INTERSECT:
			frequency_index_destroy(slot_a_ptr->highest_freq_index);
			slot_a_ptr->highest_freq_index = frequency_index_copy(slot_b_ptr->lowest_freq_index);
			FRET(SLOTC_SUCCESS);
			break;
		case SLOTC_EQUAL:
		case SLOTC_IS_INCLUDED:
			frequency_slot_destroy(slot_a_ptr);
			slot_a_ptr = PRGC_NIL;
			FRET(SLOTC_SUCCESS);
			break;
		case SLOTC_INCLUDE:
			FRET(SLOTC_FAIL);
			break;
		default:
			FRET(SLOTC_SUCCESS);
			break;
	}
	FRET(SLOTC_FAIL);
}



short frequency_slot_compare(Frequency_Slot const* const slot_a_ptr,Frequency_Slot const* const slot_b_ptr) {
/**
	Compares the two frequency slots.
	Returns:
	SLOTC_EQUAL				( e.g. slot_a [2,5] slot_b [2,5]  )
	SLOTC_HIGHER			( e.g. slot_a [2,5] slot_b [-1,0] )
	SLOTC_LOWER				( e.g. slot_a [2,5] slot_b [6,10] )
	SLOTC_HIGHER_ADJACENT	( e.g. slot_a [2,5] slot_b [-1,2] )
	SLOTC_LOWER_ADJACENT	( e.g. slot_a [2,5] slot_b [5,10] )
	SLOTC_HIGHER_INTERSECT	( e.g. slot_a [2,5] slot_b [-1,3] )
	SLOTC_LOWER_INTERSECT	( e.g. slot_a [2,5] slot_b [3,10] )
	SLOTC_INCLUDE			( e.g. slot_a [2,5] slot_b [2,4]  )
	SLOTC_IS_INCLUDED		( e.g. slot_a [2,5] slot_b [-1,5] )
**/	
	FIN( frequency_slot_compare(slot_a_ptr, slot_b_ptr) );
	if ( PRGC_NIL == slot_a_ptr || PRGC_NIL == slot_b_ptr ) FRET(SLOTC_FAIL);
	switch ( frequency_index_compare(slot_a_ptr->highest_freq_index, slot_b_ptr->lowest_freq_index) ) {
		case FREQUENCY_INDEXC_LOWER:
			FRET(SLOTC_LOWER);
			break;
		case FREQUENCY_INDEXC_EQUAL:
			FRET(SLOTC_LOWER_ADJACENT);
			break;
		case FREQUENCY_INDEXC_HIGHER:
			switch ( frequency_index_compare(slot_a_ptr->lowest_freq_index ,slot_b_ptr->highest_freq_index) ) {
				case FREQUENCY_INDEXC_HIGHER:
					FRET(SLOTC_HIGHER);
					break;
				case FREQUENCY_INDEXC_EQUAL:
					FRET(SLOTC_HIGHER_ADJACENT);
					break;
				case FREQUENCY_INDEXC_LOWER:
					switch ( frequency_index_compare(slot_a_ptr->highest_freq_index , slot_b_ptr->highest_freq_index) ) {
						case FREQUENCY_INDEXC_EQUAL:
							switch ( frequency_index_compare(slot_a_ptr->lowest_freq_index, slot_b_ptr->lowest_freq_index) ) {
								case FREQUENCY_INDEXC_EQUAL:
									FRET(SLOTC_EQUAL);
									break;
								case FREQUENCY_INDEXC_LOWER:
									FRET(SLOTC_INCLUDE);
									break;
								case FREQUENCY_INDEXC_HIGHER:
									FRET(SLOTC_IS_INCLUDED);
									break;
							}
							break;
						case FREQUENCY_INDEXC_HIGHER:
							switch ( frequency_index_compare(slot_a_ptr->lowest_freq_index, slot_b_ptr->lowest_freq_index) ) {
								case FREQUENCY_INDEXC_EQUAL:
								case FREQUENCY_INDEXC_LOWER:
									FRET(SLOTC_INCLUDE);
									break;
								case FREQUENCY_INDEXC_HIGHER:
									FRET(SLOTC_HIGHER_INTERSECT);
									break;
							}
							break;
						case FREQUENCY_INDEXC_LOWER:
							switch ( frequency_index_compare(slot_a_ptr->lowest_freq_index, slot_b_ptr->lowest_freq_index) ) {
								case FREQUENCY_INDEXC_EQUAL:
								case FREQUENCY_INDEXC_HIGHER:
									FRET(SLOTC_IS_INCLUDED);
									break;
								case FREQUENCY_INDEXC_LOWER:
									FRET(SLOTC_LOWER_INTERSECT);
									break;
							}
							break;
							
					}
					break;
			}
			break;
	}
	FRET(SLOTC_FAIL);
}


short frequency_slot_frequency_index_compare(Frequency_Slot const* const slot_ptr, Frequency_Index const* const fi_ptr) {
/**
	Compares the frequency slot with a frequency index
**/	
	FIN( frequency_slot_frequency_index_compare(slot_ptr, fi_ptr) );
	switch ( frequency_index_compare(fi_ptr,slot_ptr->lowest_freq_index) ) {
		case FREQUENCY_INDEXC_LOWER:
			FRET(SLOTC_HIGHER);
			break;
		case FREQUENCY_INDEXC_HIGHER:
			switch ( frequency_index_compare(fi_ptr,slot_ptr->highest_freq_index) ) {
				case FREQUENCY_INDEXC_HIGHER:
					FRET(SLOTC_LOWER);
					break;
				case FREQUENCY_INDEXC_EQUAL:
					FRET(SLOTC_LOWER_ADJACENT);
					break;
				case FREQUENCY_INDEXC_LOWER:
					FRET(SLOTC_INCLUDE);
					break;
			}
			break;
		case FREQUENCY_INDEXC_EQUAL:
			FRET(SLOTC_HIGHER_ADJACENT);
			break;
	}
	FRET(SLOTC_FAIL);
}





PrgT_List* frequency_slot_bandwidth_allocable_indexes_get(Frequency_Slot const* const slot_ptr, double bandwidth_demand, Frequency_Index const* const fi_sample_ptr) {
/**
	Returns a list of frequency indexes that can be used
	as central frequency for the bandwidth demand.
**/
	PrgT_List	*indexes_lptr;
	Frequency_Index	*frequency_index_ptr;
	double		lowest_freq_hz,highest_freq_hz,spacing,potential_freq,zero_index_freq;
	
	FIN(frequency_slot_bandwidth_allocable_indexes_get(slot_ptr, bandwidth_demand, fi_sample_ptr));
	
	indexes_lptr = prg_list_create();
	
	lowest_freq_hz = frequency_index_hertz_get(slot_ptr->lowest_freq_index) + bandwidth_demand;
	highest_freq_hz = frequency_index_hertz_get(slot_ptr->highest_freq_index) - bandwidth_demand;
	if ( lowest_freq_hz > highest_freq_hz)
		FRET(indexes_lptr);
	if ( PRGC_NIL == fi_sample_ptr ) {
		zero_index_freq = FREQUENCY_INDEXC_ZERO_INDEX_DEFAULT;
		spacing = FREQUENCY_INDEXC_SPACING_DEFAULT;
	}
	else {
		zero_index_freq = frequency_index_zero_frequency_get(fi_sample_ptr);
		spacing = frequency_index_spacing_get(fi_sample_ptr);
	}
	potential_freq = zero_index_freq + spacing * ceil( (lowest_freq_hz-zero_index_freq)/spacing);
	while (potential_freq <= highest_freq_hz) {
		frequency_index_ptr = frequency_index_create(potential_freq, zero_index_freq, spacing);
		prg_list_insert(indexes_lptr, frequency_index_ptr, PRGC_LISTPOS_TAIL);
		prg_list_sorted_set(indexes_lptr);
		potential_freq += spacing;
	}
	FRET(indexes_lptr);
}



int frequency_slot_bandwidth_allocable_indexes_number_get(Frequency_Slot const* const slot_ptr, double bandwidth_demand, Frequency_Index const* const fi_sample_ptr, Boolean non_overlapping) {
/**
	Returns the number of frequency indexes that can accomodate the demand.
	If non_overlapping is TRUE count only the non overlapping indexes.
**/
	double			lowest_freq_hz,highest_freq_hz,spacing,potential_freq,zero_index_freq;
	int				num_indexes;
	
	FIN(frequency_slot_bandwidth_allocable_indexes_number_get(slot_ptr, bandwidth_demand, fi_sample_ptr));
	
	lowest_freq_hz = frequency_index_hertz_get(slot_ptr->lowest_freq_index) + bandwidth_demand;
	highest_freq_hz = frequency_index_hertz_get(slot_ptr->highest_freq_index) - bandwidth_demand;
	if ( lowest_freq_hz > highest_freq_hz)
		FRET(0);
	if ( PRGC_NIL == fi_sample_ptr ) {
		zero_index_freq = FREQUENCY_INDEXC_ZERO_INDEX_DEFAULT;
		spacing = FREQUENCY_INDEXC_SPACING_DEFAULT;
	}
	else {
		zero_index_freq = frequency_index_zero_frequency_get(fi_sample_ptr);
		spacing = frequency_index_spacing_get(fi_sample_ptr);
	}
	potential_freq = zero_index_freq + spacing * ceil( (lowest_freq_hz-zero_index_freq)/spacing );
	if (potential_freq > highest_freq_hz)
		FRET(0);
	if (OPC_FALSE == non_overlapping) {
		num_indexes = 1 + floor( (highest_freq_hz - potential_freq)/spacing );
	}
	else {
		double space = spacing;
		potential_freq += spacing;
		num_indexes = 1;
		while (potential_freq <= highest_freq_hz) {
			if (space >= bandwidth_demand) {
				++num_indexes;
				space = 0;
			}
			potential_freq += spacing;
			space += spacing;
		}
	}
	FRET(num_indexes);
}



double frequency_slot_bandwidth(Frequency_Slot const* const slot_ptr) {
/**
	Returns the bandwidth of the frequency slot
	(expressed in GHz)
**/
	double bw;
	FIN( frequency_slot_bandwidth(slot_ptr) );
	bw = frequency_index_hertz_get(slot_ptr->highest_freq_index) - frequency_index_hertz_get(slot_ptr->lowest_freq_index);
	FRET(bw);
}



void frequency_slot_print(Frequency_Slot const* const slot_ptr) {
/**
	Print the frequency slot ( eg. [-1,7] ).
**/
	FIN( frequency_slot_print(slot_ptr) );
	printf("[");
	frequency_index_print(slot_ptr->lowest_freq_index);
	printf(",");
	frequency_index_print(slot_ptr->highest_freq_index);
	printf("]");
	FOUT;
}
