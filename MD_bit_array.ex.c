#include  <opnet.h>
#include  "MD_bit_array.h"

typedef OpT_uInt32 word_t;

struct BitArray { // indexes goes from 0 to (nbits-1)
	word_t *words;
	int nbits;
};

enum { BITARRAY_WORD_SIZE = sizeof(word_t) * 8 };

/*
static __inline int bindex(int b) { return (b / BITARRAY_WORD_SIZE); }
static __inline int boffset(int b) { return (b % BITARRAY_WORD_SIZE); }
*/

#define bitarray_bindex(b)    (b / BITARRAY_WORD_SIZE)
#define bitarray_boffset(b)   (b % BITARRAY_WORD_SIZE)

void bitarray_destroy(BitArray* bitarray_ptr) {
	FIN(bitarray_destroy());
    prg_mem_free(bitarray_ptr->words);
    prg_mem_free(bitarray_ptr);
	FOUT;
}

BitArray* bitarray_create(int nbits) {
	int nwords;
    BitArray *bitarray_ptr;
	FIN(bitarray_create());
	  bitarray_ptr = prg_mem_alloc(sizeof(BitArray));
      nwords = (nbits / BITARRAY_WORD_SIZE + 1);
	  bitarray_ptr->nbits = nbits;
      bitarray_ptr->words = prg_mem_alloc(sizeof(*bitarray_ptr->words) * nwords);
    FRET(bitarray_ptr);
}


void bitarray_set_bit(BitArray *bitarray_ptr, int bit_index) {
  FIN(bitarray_set_bit());
  if (bit_index>=0 && bit_index<bitarray_ptr->nbits) {
    bitarray_ptr->words[bitarray_bindex(bit_index)] |= 1 << (bitarray_boffset(bit_index));
  }
  FOUT;
}

void bitarray_clear_bit(BitArray *bitarray_ptr, int bit_index) {
  FIN(bitarray_clear_bit());
  if (bit_index>=0 && bit_index<bitarray_ptr->nbits) {
    bitarray_ptr->words[bitarray_bindex(bit_index)] &= ~(1 << (bitarray_boffset(bit_index)));
  }
  FOUT;
}

int bitarray_get_bit(BitArray const* bitarray_ptr, int bit_index) {
  FIN(bitarray_get_bit());
  if (bit_index>=0 && bit_index<bitarray_ptr->nbits) {
    FRET( bitarray_ptr->words[bitarray_bindex(bit_index)] & (1 << (bitarray_boffset(bit_index))) );
  }
  FRET(0);
}

/*
static void word_t_print(word_t const* word_ptr) {
  int c,k;
  FIN(word_t_print());
  c = sizeof(*word_ptr);
  for (; c >= 0; --c) {
    k = (*word_ptr) >> c;
    if (k & 1)
      printf("1");
    else
      printf("0");
  }
  FOUT;
}*/


void bitarray_print(BitArray const* bitarray_ptr) {
  int nwords,i,c,k,printed;
  word_t *word_ptr;
  
  FIN(bitarray_print());
  
  nwords = (bitarray_ptr->nbits / BITARRAY_WORD_SIZE + 1);
  printed = 0;
  for (i=0; i<nwords; ++i) {
    word_ptr = &(bitarray_ptr->words[i]); 
	
	c = sizeof(*word_ptr);
    for (; c >= 0; --c) {
      k = (*word_ptr) >> c;
      if (k & 1)
        printf("1");
      else
        printf("0");
	  
	  ++printed;
	  if (printed == bitarray_ptr->nbits) FOUT;
    }
	
  }
  FOUT;
}


