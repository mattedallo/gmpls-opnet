#include	"MD_pcep_src.h"
#include	"MD_path_computation_src.h"
#include	"MD_rsvp_msg_util.h"


//-------------------------------------OBJECTs HEADER------------------------------------

static Pcep_Object_Header* pcep_object_header_create (int obj_class, int obj_type, Boolean p_flag, Boolean i_flag, int obj_length ) {
// create the common object header
	Pcep_Object_Header	*header_ptr;

	FIN( pcep_object_header_create (obj_class, obj_type, p_flag, i_flag, obj_length) );
	header_ptr = (Pcep_Object_Header*) prg_mem_alloc( sizeof(Pcep_Object_Header) );
	header_ptr->object_class = obj_class;
  	header_ptr->object_type = obj_type;
  	//header_ptr->res_flags = 0; //reserved flags not yet used
  	header_ptr->p_flag = p_flag; //if true the object must be take into account by PCE
  	header_ptr->i_flag = i_flag; //if true means that the PCE has ignored this object
  	header_ptr->object_length = obj_length; //the total lenght of the object in bytes
	FRET(header_ptr);
}
	
static Pcep_Object_Header* pcep_object_header_copy (Pcep_Object_Header *header_ptr) {
// copy an object header
	FIN( pcep_object_header_copy(header_ptr) );
	if (header_ptr == OPC_NIL) FRET(OPC_NIL);
	FRET( prg_mem_copy_create( header_ptr, sizeof(Pcep_Object_Header) ) );
}

static void pcep_object_header_destroy (Pcep_Object_Header *header_ptr) {
// destroy an object header
	FIN( pcep_object_header_destroy(header_ptr) );
	if (header_ptr == OPC_NIL) FOUT;
	prg_mem_free(header_ptr);
	FOUT;
}


//-------------------------------------OPEN OBJECT------------------------------------

Pcep_Open_Object* pcep_open_object_create(short keepalive, short deadtimer, unsigned int sid) {
// create the open object
	Pcep_Open_Object	*open_object_ptr;
	FIN( pcep_open_object_create(keepalive, deadtimer, sid) );
	
	open_object_ptr = (Pcep_Open_Object*) prg_mem_alloc( sizeof(Pcep_Open_Object) );
	// create the common object header (length of the open object is 8 bytes, 4 of the common header+4)
	open_object_ptr->header_ptr = pcep_object_header_create(1,1,OPC_TRUE,OPC_FALSE,(4+4));
	// set the PCEP version
	open_object_ptr->ver = PCEPC_VERSION;
	// set the keepalive time info
	open_object_ptr->keepalive = keepalive;
	// set the deadtimer info
	open_object_ptr->deadtimer = deadtimer;
	// set the session id info
	open_object_ptr->sid = sid;
	
	FRET(open_object_ptr);
}

static Pcep_Open_Object* pcep_open_object_copy(Pcep_Open_Object* open_obj) {
// copy the open object
	Pcep_Open_Object *copied_open_obj;
	
	FIN( pcep_open_object_copy(open_obj) );
	if (open_obj == OPC_NIL) FRET(OPC_NIL);
	copied_open_obj = prg_mem_copy_create(open_obj, sizeof(Pcep_Open_Object) );
	copied_open_obj->header_ptr = pcep_object_header_copy(open_obj->header_ptr);
	FRET(copied_open_obj);
}

void pcep_open_object_destroy(Pcep_Open_Object* open_obj) {
// open object destroy function
	FIN(pcep_open_object_destroy(open_obj) );
	if (open_obj == OPC_NIL) FOUT;
	pcep_object_header_destroy(open_obj->header_ptr);
	prg_mem_free(open_obj);
	FOUT;
}


//--------------------------------CLOSE OBJECT----------------------------------

Pcep_Close_Object* pcep_close_object_create(short reason) {
// create the close object
	Pcep_Close_Object	*close_object_ptr;
	FIN( pcep_close_object_create(reason) );
	
	close_object_ptr = (Pcep_Close_Object*) prg_mem_alloc( sizeof(Pcep_Close_Object) );
	// create the common object header (length of the close object is 8 bytes, 4 of the common header+4)
	close_object_ptr->header_ptr = pcep_object_header_create(15,1,OPC_TRUE,OPC_FALSE,(4+4));
	// set the reason (optional)
	close_object_ptr->reason = reason;	
	FRET(close_object_ptr);
}

static Pcep_Close_Object* pcep_close_object_copy(Pcep_Close_Object* close_obj) {
// copy the close object
	Pcep_Close_Object *copied_close_obj;
	
	FIN( pcep_close_object_copy(close_obj) );
	if (close_obj == OPC_NIL) FRET(OPC_NIL);
	copied_close_obj = prg_mem_copy_create(close_obj, sizeof(Pcep_Close_Object) );
	copied_close_obj->header_ptr = pcep_object_header_copy(close_obj->header_ptr);
	FRET(copied_close_obj);
}

static void pcep_close_object_destroy(Pcep_Close_Object* close_obj) {
// close object destroy function
	FIN(pcep_close_object_destroy(close_obj) );
	if (close_obj == OPC_NIL) FOUT;
	pcep_object_header_destroy(close_obj->header_ptr);
	prg_mem_free(close_obj);
	FOUT;
}


//--------------------------------ERROR OBJECT----------------------------------

Pcep_Error_Object* pcep_error_object_create(short error_type, short error_value) {
// error object create function
	Pcep_Error_Object *error_obj_ptr;
	
	FIN(pcep_error_object_create(error_type, error_value) );
	error_obj_ptr = (Pcep_Error_Object*) prg_mem_alloc( sizeof(Pcep_Error_Object) );
	// create the common object header (length of the open object is 8 bytes, 4 of the common header+4)
	error_obj_ptr->header_ptr = pcep_object_header_create(13,1,OPC_TRUE,OPC_FALSE,(4+4));
	//error_obj_ptr->reserved = 0;
	//error_obj_ptr->flags = 0;
	error_obj_ptr->error_type = error_type;
	error_obj_ptr->error_value = error_value;
	FRET(error_obj_ptr);
}

static Pcep_Error_Object* pcep_error_object_copy(Pcep_Error_Object* error_obj) {
// error object copy function
	Pcep_Error_Object *copied_error_obj;
	
	FIN(pcep_error_object_copy(error_obj) );
	if (error_obj == OPC_NIL) FRET(OPC_NIL);
	copied_error_obj = prg_mem_copy_create( error_obj, sizeof(Pcep_Error_Object) );
	copied_error_obj->header_ptr = pcep_object_header_copy(error_obj->header_ptr);
	FRET(copied_error_obj);
}

static void pcep_error_object_destroy(Pcep_Error_Object* error_obj) {
// error object destroy function
	FIN(pcep_error_object_destroy(error_obj) );
	if (error_obj == OPC_NIL) FOUT;
	pcep_object_header_destroy(error_obj->header_ptr);
	prg_mem_free (error_obj);
	FOUT;
}

//--------------------------------RP OBJECT----------------------------------

Pcep_RP_Object* pcep_rp_object_create( int request_id_number ) {
// RP object create function
	Pcep_RP_Object *rp_obj_ptr;
	
	FIN(pcep_rp_object_create(request_id_number) );
	rp_obj_ptr = (Pcep_RP_Object*) prg_mem_alloc( sizeof(Pcep_RP_Object) );
	// create the common object header (length of the rp object is 12 bytes, 4 of the common header+8)
	rp_obj_ptr->header_ptr = pcep_object_header_create(2,1,OPC_TRUE,OPC_FALSE,(4+8));
	rp_obj_ptr->request_id_number = request_id_number;
	FRET(rp_obj_ptr);
}

static Pcep_RP_Object* pcep_rp_object_copy(Pcep_RP_Object* rp_obj) {
// rp object copy function
	Pcep_RP_Object *copied_obj;
	
	FIN(pcep_rp_object_copy(rp_obj) );
	if (rp_obj == OPC_NIL) FRET(OPC_NIL);
	copied_obj = prg_mem_copy_create( rp_obj, sizeof(Pcep_RP_Object) );
	copied_obj->header_ptr = pcep_object_header_copy(rp_obj->header_ptr);
	FRET(copied_obj);
}

static void pcep_rp_object_destroy(Pcep_RP_Object* rp_obj) {
// rp object destroy function
	FIN( pcep_rp_object_destroy(rp_obj) );
	if (rp_obj == OPC_NIL) FOUT;
	pcep_object_header_destroy(rp_obj->header_ptr);
	prg_mem_free (rp_obj);
	FOUT;
}


//----------------------NO PATH OBJECT---------------------------

Pcep_NoPath_Object* pcep_nopath_object_create( Lsp_event nature_of_issue) {
// NOPath object create function
	Pcep_NoPath_Object *nopath_obj;
	
	FIN(pcep_nopath_object_create() );
	nopath_obj = (Pcep_NoPath_Object*) prg_mem_alloc( sizeof(Pcep_NoPath_Object) );
	nopath_obj->nature_of_issue = nature_of_issue;
	// create the common object header (length of the nopath object is 8 bytes, 4 of the common header+4)
	nopath_obj->header_ptr = pcep_object_header_create(3,1,OPC_TRUE,OPC_FALSE,(4+4));
	FRET(nopath_obj);
}

static Pcep_NoPath_Object* pcep_nopath_object_copy(Pcep_NoPath_Object* nopath_obj) {
// NOPath object copy function
	Pcep_NoPath_Object *copied_obj;
	
	FIN(pcep_rp_object_copy(nopath_obj) );
	if (nopath_obj == OPC_NIL) FRET(OPC_NIL);
	copied_obj = prg_mem_copy_create( nopath_obj, sizeof(Pcep_NoPath_Object) );
	copied_obj->header_ptr = pcep_object_header_copy(nopath_obj->header_ptr);
	FRET(copied_obj);
}

static void pcep_nopath_object_destroy(Pcep_NoPath_Object* nopath_obj) {
// NOPath object destroy function
	FIN( pcep_nopath_object_destroy(nopath_obj) );
	if (nopath_obj == OPC_NIL) FOUT;
	pcep_object_header_destroy(nopath_obj->header_ptr);
	prg_mem_free (nopath_obj);
	FOUT;
}

//-------------------------ENDPOINTS OBJECT------------------------------

Pcep_EndPoints_Object* pcep_endpoints_object_create( InetT_Address source_addr, InetT_Address dest_addr ) {
// endpoints object create function
	Pcep_EndPoints_Object *obj_ptr;
	
	FIN(pcep_endpoints_object_create(source_addr, dest_addr) );
	obj_ptr = (Pcep_EndPoints_Object*) prg_mem_alloc( sizeof(Pcep_EndPoints_Object) );
	// create the common object header (length of the endpoints object is 12 bytes, 4 of the common header+8)
	obj_ptr->header_ptr = pcep_object_header_create(4,1,OPC_TRUE,OPC_FALSE,(4+8));
	obj_ptr->source_addr = inet_address_copy(source_addr);
	obj_ptr->dest_addr = inet_address_copy(dest_addr);
	FRET(obj_ptr);
}

static Pcep_EndPoints_Object* pcep_endpoints_object_copy(Pcep_EndPoints_Object* obj_ptr) {
// endpoints object copy function
	Pcep_EndPoints_Object *copied_obj;
	
	FIN(pcep_endpoints_object_copy(obj_ptr) );
	if (obj_ptr == OPC_NIL) FRET(OPC_NIL);
	copied_obj = prg_mem_copy_create( obj_ptr, sizeof(Pcep_EndPoints_Object) );
	copied_obj->header_ptr = pcep_object_header_copy(obj_ptr->header_ptr);
	FRET(copied_obj);
}

static void pcep_endpoints_object_destroy(Pcep_EndPoints_Object* obj_ptr) {
// endpoints object destroy function
	FIN( pcep_endpoints_object_destroy(obj_ptr) );
	if (obj_ptr == OPC_NIL) FOUT;
	pcep_object_header_destroy(obj_ptr->header_ptr);
	prg_mem_free (obj_ptr);
	FOUT;
}

//-------------------------------------BANDWIDTH OBJECT------------------------------------

Pcep_Bandwidth_Object* pcep_bandwidth_object_create( int capacity ) {
// bandwidth object create function
	Pcep_Bandwidth_Object *obj_ptr;
	
	FIN(pcep_bandwidth_object_create(source_addr, dest_addr) );
	obj_ptr = (Pcep_Bandwidth_Object*) prg_mem_alloc( sizeof(Pcep_Bandwidth_Object) );
	// create the common object header (length of the endpoints object is 8 bytes, 4 of the common header+4)
	obj_ptr->header_ptr = pcep_object_header_create(5,1,OPC_TRUE,OPC_FALSE,(4+4));
	obj_ptr->bandwidth = capacity;
	FRET(obj_ptr);
}

static Pcep_Bandwidth_Object* pcep_bandwidth_object_copy(Pcep_Bandwidth_Object* obj_ptr) {
// bandwidth object copy function
	Pcep_Bandwidth_Object *copied_obj;
	
	FIN(pcep_endpoints_object_copy(obj_ptr) );
	if (obj_ptr == OPC_NIL) FRET(OPC_NIL);
	copied_obj = prg_mem_copy_create( obj_ptr, sizeof(Pcep_Bandwidth_Object) );
	copied_obj->header_ptr = pcep_object_header_copy(obj_ptr->header_ptr);
	FRET(copied_obj);
}

static void pcep_bandwidth_object_destroy(Pcep_Bandwidth_Object* obj_ptr) {
// bandwidth object destroy function
	FIN( pcep_endpoints_object_destroy(obj_ptr) );
	if (obj_ptr == OPC_NIL) FOUT;
	pcep_object_header_destroy(obj_ptr->header_ptr);
	prg_mem_free (obj_ptr);
	FOUT;
}


//-------------------------------------LSP OBJECT------------------------------------

Pcep_LSP_Object* pcep_lsp_object_create(Session_object *session,
																				ERO_object *ero,
																				Sender_tspec_object *sender_tspec,
																				Suggested_label_object* sugg_label_ptr,
																				int lower_freq_indx,
																				int upper_freq_indx,
																				PrgT_Vector *carriers_vptr,
																				int capacity,
																				OpT_uInt32* sugg_ingress_tx_trans_id_ptr,
																				OpT_uInt32* sugg_ingress_rx_trans_id_ptr,
																				OpT_uInt32* sugg_egress_tx_trans_id_ptr,
																				OpT_uInt32* sugg_egress_rx_trans_id_ptr,
																				PrgT_Vector *ingress_tx_carrier_ids_vptr,
                                        PrgT_Vector *ingress_rx_carrier_ids_vptr,
                                        PrgT_Vector *egress_tx_carrier_ids_vptr,
                                        PrgT_Vector *egress_rx_carrier_ids_vptr,
																				LSP_Operational state, Boolean remove) {
	Pcep_LSP_Object* lsp_obj;
	
	FIN( pcep_lsp_object_create(session, ero, sender_tspec, lower_freq_indx, upper_freq_indx, state, remove) );
	lsp_obj = (Pcep_LSP_Object*) prg_mem_alloc( sizeof(Pcep_LSP_Object) );
	lsp_obj->lsp_id = session;
	lsp_obj->ero_ptr = ero;
	lsp_obj->sender_tspec = sender_tspec;
	lsp_obj->state = state;
	lsp_obj->remove = remove;
	lsp_obj->upper_freq_indx = upper_freq_indx;
	lsp_obj->lower_freq_indx = lower_freq_indx;
	lsp_obj->sugg_label_ptr = sugg_label_ptr;
	lsp_obj->capacity = capacity;
	lsp_obj->carriers_vptr = carriers_vptr;
	lsp_obj->sugg_ingress_tx_trans_id_ptr = sugg_ingress_tx_trans_id_ptr;
	lsp_obj->sugg_ingress_rx_trans_id_ptr = sugg_ingress_rx_trans_id_ptr;
	lsp_obj->sugg_egress_tx_trans_id_ptr = sugg_egress_tx_trans_id_ptr;
	lsp_obj->sugg_egress_rx_trans_id_ptr = sugg_egress_rx_trans_id_ptr;

	lsp_obj->ingress_tx_carrier_ids_vptr = ingress_tx_carrier_ids_vptr;
	lsp_obj->ingress_rx_carrier_ids_vptr = ingress_rx_carrier_ids_vptr;
	lsp_obj->egress_tx_carrier_ids_vptr = egress_tx_carrier_ids_vptr;
	lsp_obj->egress_rx_carrier_ids_vptr = egress_rx_carrier_ids_vptr;
	lsp_obj->length= (16+24+8)+ ero->length; //TODO, be more precise
	FRET(lsp_obj);
}

static Pcep_LSP_Object* pcep_lsp_object_copy(Pcep_LSP_Object* lsp_obj) {
// lsp object copy function
	Pcep_LSP_Object *copied_obj;
	
	FIN( pcep_lsp_object_copy(lsp_obj) );
	if (lsp_obj == OPC_NIL) FRET(OPC_NIL);
	copied_obj = prg_mem_copy_create( lsp_obj, sizeof(Pcep_LSP_Object) );
	copied_obj->lsp_id = session_object_copy(lsp_obj->lsp_id);
	copied_obj->ero_ptr = ERO_object_copy(lsp_obj->ero_ptr);
	copied_obj->sender_tspec = sender_tspec_object_copy(lsp_obj->sender_tspec);
	copied_obj->sugg_label_ptr = suggested_label_object_copy(lsp_obj->sugg_label_ptr);
	copied_obj->carriers_vptr = (lsp_obj->carriers_vptr==PRGC_NIL) ? PRGC_NIL : prg_vector_copy(lsp_obj->carriers_vptr);
	copied_obj->sugg_ingress_tx_trans_id_ptr = transponder_id_copy(lsp_obj->sugg_ingress_tx_trans_id_ptr);
	copied_obj->sugg_ingress_rx_trans_id_ptr = transponder_id_copy(lsp_obj->sugg_ingress_rx_trans_id_ptr);
	copied_obj->sugg_egress_tx_trans_id_ptr = transponder_id_copy(lsp_obj->sugg_egress_tx_trans_id_ptr);
	copied_obj->sugg_egress_rx_trans_id_ptr = transponder_id_copy(lsp_obj->sugg_egress_rx_trans_id_ptr);
	
	copied_obj->ingress_tx_carrier_ids_vptr = (lsp_obj->ingress_tx_carrier_ids_vptr==PRGC_NIL) ? PRGC_NIL : prg_vector_copy(lsp_obj->ingress_tx_carrier_ids_vptr);
	copied_obj->ingress_rx_carrier_ids_vptr = (lsp_obj->ingress_rx_carrier_ids_vptr==PRGC_NIL) ? PRGC_NIL : prg_vector_copy(lsp_obj->ingress_rx_carrier_ids_vptr);
	copied_obj->egress_tx_carrier_ids_vptr = (lsp_obj->egress_tx_carrier_ids_vptr==PRGC_NIL) ? PRGC_NIL : prg_vector_copy(lsp_obj->egress_tx_carrier_ids_vptr);
	copied_obj->egress_rx_carrier_ids_vptr = (lsp_obj->egress_rx_carrier_ids_vptr==PRGC_NIL) ? PRGC_NIL : prg_vector_copy(lsp_obj->egress_rx_carrier_ids_vptr);
	FRET(copied_obj);
}

static void pcep_lsp_object_destroy(Pcep_LSP_Object* obj_ptr) {
// lsp object destroy function
	FIN( pcep_lsp_object_destroy(obj_ptr) );
	if (obj_ptr == OPC_NIL) FOUT;
	ERO_object_destroy(obj_ptr->ero_ptr);
	session_object_destroy(obj_ptr->lsp_id);
	sender_tspec_object_destroy(obj_ptr->sender_tspec);
	suggested_label_object_destroy(obj_ptr->sugg_label_ptr);
	if (obj_ptr->carriers_vptr != PRGC_NIL)
		prg_vector_destroy(obj_ptr->carriers_vptr, OPC_TRUE);
	transponder_id_destroy(obj_ptr->sugg_ingress_tx_trans_id_ptr);
	transponder_id_destroy(obj_ptr->sugg_ingress_rx_trans_id_ptr);
	transponder_id_destroy(obj_ptr->sugg_egress_tx_trans_id_ptr);
	transponder_id_destroy(obj_ptr->sugg_egress_rx_trans_id_ptr);
	if (obj_ptr->ingress_tx_carrier_ids_vptr != PRGC_NIL)
		prg_vector_destroy(obj_ptr->ingress_tx_carrier_ids_vptr, OPC_TRUE);
	if (obj_ptr->ingress_rx_carrier_ids_vptr != PRGC_NIL)
		prg_vector_destroy(obj_ptr->ingress_rx_carrier_ids_vptr, OPC_TRUE);
	if (obj_ptr->egress_tx_carrier_ids_vptr != PRGC_NIL)
		prg_vector_destroy(obj_ptr->egress_tx_carrier_ids_vptr, OPC_TRUE);
	if (obj_ptr->egress_rx_carrier_ids_vptr != PRGC_NIL)
		prg_vector_destroy(obj_ptr->egress_rx_carrier_ids_vptr, OPC_TRUE);
	prg_mem_free (obj_ptr);
	FOUT;
}





//########################################### PCEP MESSAGE BODYs ################################################

static Pcep_Open_Msg_Body* pcep_open_msg_body_copy(Pcep_Open_Msg_Body* body_ptr, size_t size) {
// copy the open message body 
	Pcep_Open_Msg_Body* body_cpy_ptr;

	FIN( pcep_open_msg_body_copy(body_ptr) );
	if (body_ptr == OPC_NIL) FRET(OPC_NIL);
	body_cpy_ptr = prg_mem_alloc(sizeof(Pcep_Open_Msg_Body));
	body_cpy_ptr->open_object_ptr = pcep_open_object_copy(body_ptr->open_object_ptr);
	FRET(body_cpy_ptr);
}

static void pcep_open_msg_body_destroy(Pcep_Open_Msg_Body* body_ptr) {
// destroy the open message body
	
	FIN(pcep_open_msg_body_destroy(body_ptr));
	if (body_ptr == OPC_NIL) FOUT;
	pcep_open_object_destroy(body_ptr->open_object_ptr);
	prg_mem_free(body_ptr);
	FOUT;
}



static Pcep_Error_Msg_Body* pcep_error_msg_body_copy(Pcep_Error_Msg_Body* body_ptr, size_t size) {
// copy the error message body 
	Pcep_Error_Msg_Body* body_cpy_ptr;

	FIN( pcep_error_msg_body_copy(body_ptr) );
	if (body_ptr == OPC_NIL) FRET(OPC_NIL);
	body_cpy_ptr = prg_mem_alloc(sizeof(Pcep_Error_Msg_Body));
	body_cpy_ptr->error_objects_vptr = prg_vector_copy(body_ptr->error_objects_vptr);
	FRET(body_cpy_ptr);
}

static void pcep_error_msg_body_destroy(Pcep_Error_Msg_Body* body_ptr) {
// destroy the error message body
	
	FIN(pcep_error_msg_body_destroy(body_ptr));
	if (body_ptr == OPC_NIL) FOUT;
	prg_vector_destroy(body_ptr->error_objects_vptr,PRGC_TRUE);
	prg_mem_free(body_ptr);
	FOUT;
}




static Pcep_Close_Msg_Body* pcep_close_msg_body_copy(Pcep_Close_Msg_Body* body_ptr, size_t size) {
// copy the close message body 
	Pcep_Close_Msg_Body* body_cpy_ptr;

	FIN( pcep_close_msg_body_copy(body_ptr) );
	if (body_ptr == OPC_NIL) FRET(OPC_NIL);
	body_cpy_ptr = prg_mem_alloc(sizeof(Pcep_Close_Msg_Body));
	body_cpy_ptr->close_object_ptr = pcep_close_object_copy(body_ptr->close_object_ptr);
	FRET(body_cpy_ptr);
}

static void pcep_close_msg_body_destroy(Pcep_Close_Msg_Body* body_ptr) {
// destroy the close message body
	
	FIN(pcep_close_msg_body_destroy(body_ptr));
	if (body_ptr == OPC_NIL) FOUT;
	pcep_close_object_destroy(body_ptr->close_object_ptr);
	prg_mem_free(body_ptr);
	FOUT;
}



static Pcep_PcReq_Msg_Body* pcep_pcreq_msg_body_copy(Pcep_PcReq_Msg_Body* body_ptr, size_t size) {
// copy the PCReq message body 
	Pcep_PcReq_Msg_Body* body_cpy_ptr;

	FIN( pcep_pcreq_msg_body_copy(body_ptr) );
	if (body_ptr == OPC_NIL) FRET(OPC_NIL);
	body_cpy_ptr = prg_mem_alloc(sizeof(Pcep_PcReq_Msg_Body));
	body_cpy_ptr->rp_object_ptr = pcep_rp_object_copy(body_ptr->rp_object_ptr);
	body_cpy_ptr->endpoints_object_ptr = pcep_endpoints_object_copy(body_ptr->endpoints_object_ptr);
	body_cpy_ptr->bandwidth_object_ptr = pcep_bandwidth_object_copy(body_ptr->bandwidth_object_ptr);
	body_cpy_ptr->lsp_id = session_object_copy(body_ptr->lsp_id);
	body_cpy_ptr->pc_params = pc_path_comp_params_copy(body_ptr->pc_params);
	FRET(body_cpy_ptr);
}

static void pcep_pcreq_msg_body_destroy(Pcep_PcReq_Msg_Body* body_ptr) {
// destroy the PCReq message body
	
	FIN(pcep_pcreq_msg_body_destroy(body_ptr));
	if (body_ptr == OPC_NIL) FOUT;
	pcep_rp_object_destroy(body_ptr->rp_object_ptr);
	pcep_endpoints_object_destroy(body_ptr->endpoints_object_ptr);
	pcep_bandwidth_object_destroy(body_ptr->bandwidth_object_ptr);
	session_object_destroy(body_ptr->lsp_id);
	pc_path_comp_params_destroy(body_ptr->pc_params);
	prg_mem_free(body_ptr);
	FOUT;
}



static Pcep_PcRep_Msg_Body* pcep_pcrep_msg_body_copy(Pcep_PcRep_Msg_Body* body_ptr, size_t size) {
// copy the pcrep message body 
	Pcep_PcRep_Msg_Body* body_cpy_ptr;

	FIN( pcep_pcrep_msg_body_copy(body_ptr) );
	if (body_ptr == OPC_NIL) FRET(OPC_NIL);
	body_cpy_ptr = prg_mem_alloc(sizeof(Pcep_PcRep_Msg_Body));
	body_cpy_ptr->rp_object_ptr = pcep_rp_object_copy(body_ptr->rp_object_ptr);
	body_cpy_ptr->no_path_object_ptr = pcep_nopath_object_copy(body_ptr->no_path_object_ptr);
	body_cpy_ptr->lsps_vptr = prg_vector_copy(body_ptr->lsps_vptr);
	FRET(body_cpy_ptr);
}

static void pcep_pcrep_msg_body_destroy(Pcep_PcRep_Msg_Body* body_ptr) {
// destroy the pcrep message body
	
	FIN(pcep_pcrep_msg_body_destroy(body_ptr));
	if (body_ptr == OPC_NIL) FOUT;
	pcep_rp_object_destroy(body_ptr->rp_object_ptr);
	pcep_nopath_object_destroy(body_ptr->no_path_object_ptr);
	prg_vector_destroy(body_ptr->lsps_vptr,PRGC_TRUE);
	prg_mem_free(body_ptr);
	FOUT;
}



static Pcep_PcRpt_Msg_Body* pcep_pcrpt_msg_body_copy(Pcep_PcRpt_Msg_Body* body_ptr, size_t size) {
// copy the pcrpt message body 
	Pcep_PcRpt_Msg_Body* body_cpy_ptr;

	FIN( pcep_pcrpt_msg_body_copy(body_ptr) );
	if (body_ptr == OPC_NIL) FRET(OPC_NIL);
	body_cpy_ptr = prg_mem_alloc(sizeof(Pcep_PcRpt_Msg_Body));
	body_cpy_ptr->lsp_object_ptr = pcep_lsp_object_copy(body_ptr->lsp_object_ptr);
	FRET(body_cpy_ptr);
}

static void pcep_pcrpt_msg_body_destroy(Pcep_PcRpt_Msg_Body* body_ptr) {
// destroy the pcrpt message body
	
	FIN(pcep_pcrpt_msg_body_destroy(body_ptr));
	if (body_ptr == OPC_NIL) FOUT;
	pcep_lsp_object_destroy(body_ptr->lsp_object_ptr);
	prg_mem_free(body_ptr);
	FOUT;
}


//################################################## PCEP MESSAGEs #####################################################//

static Packet* pcep_msg_create(Pcep_Message_Type msg_type) {
	Packet	*pcep_pkptr;
	
	FIN(pcep_msg_create(msg_type));
	// create the pcep packet
	pcep_pkptr = op_pk_create_fmt ("MD_PCEP_Packet");
	// set the pcep version
	op_pk_nfd_set_int32(pcep_pkptr, "Version", PCEPC_VERSION);
	// set the message type
	op_pk_nfd_set_int32(pcep_pkptr, "Message-Type", msg_type);
	// return the packet
	FRET(pcep_pkptr);

}
	

Boolean pcep_msg_type_check(Packet* pcep_pkptr, Pcep_Message_Type msg_type) {
// Gets a packet pointer and a message type
// Returns true if the message type of the packet corresponds to the message type provided 
Pcep_Message_Type	pkt_msg_type;
	FIN( pcep_msg_type_check(pcep_pkptr, msg_type) );
	if ( OPC_COMPCODE_FAILURE == op_pk_nfd_access(pcep_pkptr,"Message-Type", &pkt_msg_type) ) {
		printf(" PCEP message type check: packet checked is not a PCEP packet\n");
		FRET(OPC_FALSE);
	}
	if (pkt_msg_type != msg_type) {
		printf(" PCEP message type check: PCEP packet checked does not match\n");
		FRET(OPC_FALSE);
	}
	FRET(OPC_TRUE);
}


// ********************* start KEEPALIVE MESSAGE ************************* //

Packet* pcep_keepalive_msg_create() {
// returns keepalive message
	FIN(pcep_keepalive_msg_create());
	FRET(pcep_msg_create(PCEP_KEEPALIVE_MSG));
}

// ********************* end KEEPALIVE MESSAGE ************************* //

// ********************* start OPEN MESSAGE ************************* //

Packet* pcep_open_msg_create() {
// returns open message without any object//
	FIN(pcep_open_msg_create());
	FRET(pcep_msg_create(PCEP_OPEN_MSG));
}

Packet* pcep_open_msg_create_with_options(short keepalive, short deadtimer, unsigned int sid) {
// returns an open message and the open object within it
	Packet	*pcep_pkptr;
	Pcep_Open_Msg_Body	*msg_body_ptr;
	
	FIN(pcep_open_msg_create_with_options(keepalive, deadtimer, sid));
	// create the pcep open message empty
	pcep_pkptr = pcep_open_msg_create();
	// create the message body
	msg_body_ptr = (Pcep_Open_Msg_Body*) prg_mem_alloc( sizeof(Pcep_Open_Msg_Body) );
	// set the body field of the pcep packet //
	op_pk_nfd_set_ptr(pcep_pkptr, "Body", msg_body_ptr, pcep_open_msg_body_copy, pcep_open_msg_body_destroy, sizeof (Pcep_Open_Msg_Body));
	// create the open object
	msg_body_ptr->open_object_ptr = pcep_open_object_create(keepalive, deadtimer, sid);
	// set the packet bulk size in bit
	op_pk_bulk_size_set (pcep_pkptr, op_pk_bulk_size_get(pcep_pkptr) + 8 * msg_body_ptr->open_object_ptr->header_ptr->object_length);
	// return the open message packet pointer
	FRET(pcep_pkptr); 
}

// ********************* end OPEN MESSAGE ************************* //

// ********************* start CLOSE MESSAGE ************************* //

Packet* pcep_close_msg_create() {
// returns keepalive message
	FIN(pcep_close_msg_create());
	FRET(pcep_msg_create(PCEP_CLOSE_MSG));
}


Packet* pcep_close_msg_create_with_options(short reason) {
// return a close message and the close object within it
	Packet	*pcep_pkptr;
	Pcep_Close_Msg_Body	*msg_body_ptr;
	
	FIN(pcep_close_msg_create_with_options(reason));
	// create the pcep close message
	pcep_pkptr = pcep_close_msg_create();
	// create the message body
	msg_body_ptr = (Pcep_Close_Msg_Body*) prg_mem_alloc( sizeof(Pcep_Close_Msg_Body) );
	// set the body field of the pcep packet //
	op_pk_nfd_set_ptr(pcep_pkptr, "Body", msg_body_ptr, pcep_close_msg_body_copy, pcep_close_msg_body_destroy, sizeof (Pcep_Close_Msg_Body));
	// create the close object
	msg_body_ptr->close_object_ptr = pcep_close_object_create(reason);
	// set the packet bulk size in bit
	op_pk_bulk_size_set (pcep_pkptr, op_pk_bulk_size_get(pcep_pkptr) + 8 * msg_body_ptr->close_object_ptr->header_ptr->object_length);
	// return the close message packet pointer
	FRET(pcep_pkptr); 
}

// ********************* end CLOSE MESSAGE ************************* //

// ********************* start PCREQ MESSAGE ************************* //

Packet* pcep_pcreq_msg_create() {
// returns PCReq message
	FIN(pcep_pcreq_msg_create());
	FRET(pcep_msg_create(PCEP_PCREQ_MSG));
}


Packet* pcep_pcreq_msg_create_with_objects(	Pcep_RP_Object *rp_object_ptr,
											Pcep_EndPoints_Object *endpoints_object_ptr,
											Pcep_Bandwidth_Object *bandwidth_object_ptr,
											Session_object *lsp_id,
											Path_Comp_Params *pc_params) {
// returns an open message and the open object within it
	Packet	*pcep_pkptr;
	Pcep_PcReq_Msg_Body	*msg_body_ptr;
	int total_bytes;
	
	FIN(pcep_pcreq_msg_create_with_objects(rp_object_ptr, endpoints_object_ptr, bandwidth_object_ptr, lsp_id, pc_params));
	// create the pcep pcreq message empty
	pcep_pkptr = pcep_pcreq_msg_create();
	// create the message body
	msg_body_ptr = (Pcep_PcReq_Msg_Body*) prg_mem_alloc( sizeof(Pcep_PcReq_Msg_Body) );
	// set the body field of the pcep packet //
	op_pk_nfd_set_ptr(pcep_pkptr, "Body", msg_body_ptr, pcep_pcreq_msg_body_copy, pcep_pcreq_msg_body_destroy, sizeof (Pcep_PcReq_Msg_Body));
	// assign the objects
	msg_body_ptr->rp_object_ptr = rp_object_ptr;
	msg_body_ptr->endpoints_object_ptr = endpoints_object_ptr;
	msg_body_ptr->bandwidth_object_ptr = bandwidth_object_ptr;
	msg_body_ptr->lsp_id = lsp_id;
	msg_body_ptr->pc_params = pc_params;
	// counts the total bytes
	total_bytes =	msg_body_ptr->rp_object_ptr->header_ptr->object_length +
					msg_body_ptr->endpoints_object_ptr->header_ptr->object_length +
					msg_body_ptr->bandwidth_object_ptr->header_ptr->object_length +
					8 + //consider the params to occupy 8 bytes
					24;  //consider the lsp id to occupy 24 bytes
	// set the packet bulk size in bit
	op_pk_bulk_size_set (pcep_pkptr, op_pk_bulk_size_get(pcep_pkptr) + 8 * total_bytes);
	// return the PCReq message packet pointer
	FRET(pcep_pkptr); 
}

InetT_Address* pcep_pcreq_msg_endpoints_source_addr_access(Packet* pcep_pkptr) {
// Gets a PCEP (PCREQ) packet pointer
// Return the endpoint source address or OPC_NIL if not found
Pcep_PcReq_Msg_Body *pcreq_msg_body_ptr;
	FIN( pcep_pcreq_msg_endpoints_source_addr_access(pcep_pkptr) );
	// check if the packet is a PCREQ message
	if ( OPC_FALSE == pcep_msg_type_check(pcep_pkptr, PCEP_PCREQ_MSG) ) FRET(OPC_NIL);
	// access the body field
	op_pk_nfd_access(pcep_pkptr,"Body", &pcreq_msg_body_ptr);
	// check if the endpoints object is set
	if ( OPC_NIL==pcreq_msg_body_ptr->endpoints_object_ptr ) FRET(OPC_NIL);
	// return the source address
	FRET( &(pcreq_msg_body_ptr->endpoints_object_ptr->source_addr) );
}

InetT_Address* pcep_pcreq_msg_endpoints_destination_addr_access(Packet* pcep_pkptr) {
// Gets a PCEP (PCREQ) packet pointer
// Return the endpoint destination address or OPC_NIL if not found
Pcep_PcReq_Msg_Body *pcreq_msg_body_ptr;
	FIN( pcep_pcreq_msg_endpoints_destination_addr_access(pcep_pkptr) );
	// check if the packet is a PCREQ message
	if ( OPC_FALSE == pcep_msg_type_check(pcep_pkptr, PCEP_PCREQ_MSG) ) FRET(OPC_NIL);
	// access the body field
	op_pk_nfd_access(pcep_pkptr,"Body", &pcreq_msg_body_ptr);
	// check if the endpoints object is set
	if ( OPC_NIL==pcreq_msg_body_ptr->endpoints_object_ptr ) FRET(OPC_NIL);
	// return the destination address
	FRET( &(pcreq_msg_body_ptr->endpoints_object_ptr->dest_addr) );
}

// ********************* end PCREQ MESSAGE ************************* //

// ********************* start ERROR MESSAGE ************************* //

Packet* pcep_error_msg_create() {
// returns an error message
	FIN(pcep_error_msg_create());
	FRET(pcep_msg_create(PCEP_PCERR_MSG));
}

static PrgT_Compcode error_object_vector_destroy(void * elem)	{
	FIN (error_object_vector_destroy(elem));
	pcep_error_object_destroy(elem);
	FRET (PrgC_Compcode_Success);
}

static void* error_object_vector_copy(void * elem)	{
	FIN (error_object_vector_copy(elem));	
	FRET ( pcep_error_object_copy(elem) );
}

void pcep_error_msg_error_object_add(Packet* pcep_pkptr, Pcep_Error_Object *error_obj_ptr) {
	Pcep_Error_Msg_Body	*error_msg_body_ptr;
	
	FIN( pcep_error_msg_error_object_add(pcep_pkptr,error_obj_ptr) );
	op_pk_nfd_access(pcep_pkptr,"Body", &error_msg_body_ptr);
	
	if (error_msg_body_ptr == OPC_NIL) {
		// create the message body
		error_msg_body_ptr = (Pcep_Error_Msg_Body*) prg_mem_alloc( sizeof(Pcep_Error_Msg_Body) );
		// set the body field of the pcep packet //
		op_pk_nfd_set_ptr(pcep_pkptr, "Body", error_msg_body_ptr, pcep_error_msg_body_copy, pcep_error_msg_body_destroy, sizeof (Pcep_Error_Msg_Body));
	}
	// check if the vector of error objects exists
	if (error_msg_body_ptr->error_objects_vptr == OPC_NIL) {
		//create the vector that will contain all the error objects
		error_msg_body_ptr->error_objects_vptr = prg_vector_create(1,error_object_vector_destroy, error_object_vector_copy);
	}
	// add the error object to the vector of error objects
	prg_vector_append (error_msg_body_ptr->error_objects_vptr, error_obj_ptr);
	
	// add the size of the error object to the current bulk size of the packet
	op_pk_bulk_size_set(pcep_pkptr, op_pk_bulk_size_get(pcep_pkptr)+(8*error_obj_ptr->header_ptr->object_length) );
	FOUT;
}

// ********************* end ERROR MESSAGE ************************* //

// ********************* start PCREP ************************* //

Packet* pcep_pcrep_msg_create() {
// returns PCRep message
	Packet	*pkptr;
	Pcep_PcRep_Msg_Body *pcrep_msg_body_ptr;
	FIN(pcep_pcrep_msg_create());
	pkptr = pcep_msg_create(PCEP_PCREP_MSG);
	// create the message body
	pcrep_msg_body_ptr = (Pcep_PcRep_Msg_Body*) prg_mem_alloc( sizeof(Pcep_PcRep_Msg_Body) );
	// set the body field of the pcep packet //
	op_pk_nfd_set_ptr(pkptr, "Body", pcrep_msg_body_ptr, pcep_pcrep_msg_body_copy, pcep_pcrep_msg_body_destroy, sizeof (Pcep_PcRep_Msg_Body));
	pcrep_msg_body_ptr->no_path_object_ptr = OPC_NIL;
	pcrep_msg_body_ptr->lsps_vptr = OPC_NIL;
	pcrep_msg_body_ptr->rp_object_ptr = OPC_NIL;
	FRET(pkptr);
}

static PrgT_Compcode lsp_object_vector_destroy(void * elem)	{
	FIN (lsp_object_vector_destroy(elem));
	pcep_lsp_object_destroy(elem);
	FRET (PrgC_Compcode_Success);
}

static void* lsp_object_vector_copy(void * elem)	{
	FIN (lsp_object_vector_copy(elem));	
	FRET ( pcep_lsp_object_copy(elem) );
}

void pcep_pcrep_msg_lsp_object_add(Packet* pcep_pkptr, Pcep_LSP_Object *lsp_obj_ptr) {
	Pcep_PcRep_Msg_Body	*pcrep_msg_body_ptr;
	FIN( pcep_pcrep_msg_lsp_object_add(pcep_pkptr, lsp_obj_ptr) );
	op_pk_nfd_access(pcep_pkptr,"Body", &pcrep_msg_body_ptr);
	
	if (pcrep_msg_body_ptr == OPC_NIL) {
		// create the message body
		pcrep_msg_body_ptr = (Pcep_PcRep_Msg_Body*) prg_mem_alloc( sizeof(Pcep_PcRep_Msg_Body) );
		// set the body field of the pcep packet //
		op_pk_nfd_set_ptr(pcep_pkptr, "Body", pcrep_msg_body_ptr, pcep_pcrep_msg_body_copy, pcep_pcrep_msg_body_destroy, sizeof (Pcep_PcRep_Msg_Body));
	}
	// check if the vector of lsp objects exists
	if (pcrep_msg_body_ptr->lsps_vptr == OPC_NIL) {
		//create the vector that will contain all the error objects
		pcrep_msg_body_ptr->lsps_vptr = prg_vector_create(1,lsp_object_vector_destroy, lsp_object_vector_copy);
	}
	// add the lsp object to the vector of lsp objects
	prg_vector_append (pcrep_msg_body_ptr->lsps_vptr, lsp_obj_ptr);
	
	// add the size of the lsp object to the current bulk size of the packet
	op_pk_bulk_size_set(pcep_pkptr, op_pk_bulk_size_get(pcep_pkptr)+(8*lsp_obj_ptr->length) );
	FOUT;
}

Pcep_LSP_Object const* pcep_pcrep_msg_lsp_object_access(Packet* pcep_pkptr, long pos_index) {
	Pcep_PcRep_Msg_Body	*pcrep_msg_body_ptr;

	FIN( pcep_pcrep_msg_lsp_object_access(pcep_pkptr, pos_index) );

	if (pcep_pkptr == PRGC_NIL) {
		FRET(PRGC_NIL);
	}

	op_pk_nfd_access(pcep_pkptr,"Body", &pcrep_msg_body_ptr);
	
	if (pcrep_msg_body_ptr == PRGC_NIL ||
		  pcrep_msg_body_ptr->lsps_vptr == PRGC_NIL) {
		FRET(PRGC_NIL);
	}

	// return the lsp object at the specific index (NIL if out of range)
	FRET(prg_vector_access (pcrep_msg_body_ptr->lsps_vptr, pos_index));
}

long pcep_pcrep_msg_lsp_object_count(Packet* pcep_pkptr) {
	Pcep_PcRep_Msg_Body	*pcrep_msg_body_ptr;

	FIN( pcep_pcrep_msg_lsp_object_count(pcep_pkptr) );

	if (pcep_pkptr == PRGC_NIL) {
		FRET(0);
	}

	op_pk_nfd_access(pcep_pkptr,"Body", &pcrep_msg_body_ptr);
	
	if (pcrep_msg_body_ptr == PRGC_NIL ||
		  pcrep_msg_body_ptr->lsps_vptr == PRGC_NIL) {
		FRET(0);
	}
	FRET(prg_vector_size(pcrep_msg_body_ptr->lsps_vptr));
}

void pcep_pcrep_msg_no_path_object_add(Packet* pcep_pkptr, Pcep_NoPath_Object *nopath_obj_ptr) {
// this function appends the nopath object to the pcrep message
	Pcep_PcRep_Msg_Body	*pcrep_msg_body_ptr;
	
	FIN( pcep_pcrep_msg_no_path_object_add(pcep_pkptr, nopath_obj_ptr) );
	op_pk_nfd_access(pcep_pkptr,"Body", &pcrep_msg_body_ptr);
	
	if (pcrep_msg_body_ptr == OPC_NIL) {
		// create the message body
		pcrep_msg_body_ptr = (Pcep_PcRep_Msg_Body*) prg_mem_alloc( sizeof(Pcep_PcRep_Msg_Body) );
		// set the body field of the pcep packet //
		op_pk_nfd_set_ptr(pcep_pkptr, "Body", pcrep_msg_body_ptr, pcep_pcrep_msg_body_copy, pcep_pcrep_msg_body_destroy, sizeof (Pcep_PcRep_Msg_Body));
	}
	// set the nopath object
	pcrep_msg_body_ptr->no_path_object_ptr = nopath_obj_ptr;
	
	// add the size of the nopath object to the current bulk size of the packet
	op_pk_bulk_size_set(pcep_pkptr, op_pk_bulk_size_get(pcep_pkptr)+(8*nopath_obj_ptr->header_ptr->object_length) );
	FOUT;
}

void pcep_pcrep_msg_rp_object_add(Packet* pcep_pkptr, Pcep_RP_Object *rp_obj_ptr) {
// this function appends the RP object to the pcrep message
	Pcep_PcRep_Msg_Body	*pcrep_msg_body_ptr;
	
	FIN( pcep_pcrep_msg_rp_object_add(pcep_pkptr, rp_obj_ptr) );
	op_pk_nfd_access(pcep_pkptr,"Body", &pcrep_msg_body_ptr);
	
	if (pcrep_msg_body_ptr == OPC_NIL) {
		// create the message body
		pcrep_msg_body_ptr = (Pcep_PcRep_Msg_Body*) prg_mem_alloc( sizeof(Pcep_PcRep_Msg_Body) );
		// set the body field of the pcep packet //
		op_pk_nfd_set (pcep_pkptr, "Body", pcrep_msg_body_ptr, pcep_pcrep_msg_body_copy, pcep_pcrep_msg_body_destroy, sizeof (Pcep_PcRep_Msg_Body));
	}
	// set the rp object
	pcrep_msg_body_ptr->rp_object_ptr = rp_obj_ptr;
	
	// add the size of the nopath object to the current bulk size of the packet
	op_pk_bulk_size_set(pcep_pkptr, op_pk_bulk_size_get(pcep_pkptr)+(8*rp_obj_ptr->header_ptr->object_length) );
	FOUT;
}

// ************************ end PCREP ************************* //

// ************************ start PCRPT ************************* //

static Packet* pcep_pcrpt_msg_create() {
// returns an error message
	FIN(pcep_pcrpt_msg_create());
	FRET(pcep_msg_create(PCEP_PCRPT_MSG));
}


Packet* pcep_pcrpt_msg_create_with_objects(Pcep_LSP_Object *lsp_obj_ptr) {
// return a report message
	Packet	*pkptr;
	Pcep_PcRpt_Msg_Body	*msg_body_ptr;
	
	FIN(pcep_pcrpt_msg_create_with_options(reason));
	// create the PCRpt message
	pkptr = pcep_msg_create(PCEP_PCRPT_MSG);
	// create the message body
	msg_body_ptr = (Pcep_PcRpt_Msg_Body*) prg_mem_alloc( sizeof(Pcep_PcRpt_Msg_Body) );
	// set the body field of the pcep packet //
	op_pk_nfd_set_ptr(pkptr, "Body", msg_body_ptr, pcep_pcrpt_msg_body_copy, pcep_pcrpt_msg_body_destroy, sizeof (Pcep_PcRpt_Msg_Body));
	//set the lsp object
	msg_body_ptr->lsp_object_ptr = lsp_obj_ptr;
	// add the size of the lsp object to the current bulk size of the packet
	op_pk_bulk_size_set(pkptr, op_pk_bulk_size_get(pkptr)+(8*lsp_obj_ptr->length) );
	// return the close message packet pointer
	FRET(pkptr); 
}

// ********************* end PCRPT ************************* //

//####################################################### end PCEP MESSAGEs #############################################//
