#ifndef	_MD_FREQUENCY_INDEX_H_INCLUDED_
	#define _MD_FREQUENCY_INDEX_H_INCLUDED_
	
	#include <opnet.h>
	
	#define FREQUENCY_INDEXC_SUCCESS	0
	#define FREQUENCY_INDEXC_FAIL		1
	#define FREQUENCY_INDEXC_HIGHER		2
	#define FREQUENCY_INDEXC_LOWER		3
	#define FREQUENCY_INDEXC_EQUAL		4
	
	#define FREQUENCY_INDEXC_ZERO_INDEX_DEFAULT		193100
	#define FREQUENCY_INDEXC_SPACING_DEFAULT		6.25
	#define FREQUENCY_INDEXC_SLICE_SIZE		12.5
	
	typedef struct Frequency_Index Frequency_Index;

	Frequency_Index* frequency_index_create(double frequency, double zero_freq, double spacing);
	Frequency_Index* frequency_index_copy(Frequency_Index const * const fi_ptr);
	void frequency_index_destroy(Frequency_Index  *fi_ptr);

	Boolean frequency_index_is_equal(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr);
	Boolean frequency_index_is_lower(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr);
	Boolean frequency_index_is_lower_equal(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr);
	Boolean frequency_index_is_higher(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr);
	Boolean frequency_index_is_higher_equal(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr);
	short frequency_index_compare(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr);
	
	double frequency_index_hertz_get(Frequency_Index const * const fi_ptr);
	double frequency_index_zero_frequency_get(Frequency_Index const * const fi_ptr);
	double frequency_index_spacing_get(Frequency_Index const * const fi_ptr);

	void frequency_index_print(Frequency_Index const * const fi_ptr);



	// NOTE: the following functions are not compliant with the previously defined functions
	// Convert the input frequency into the correspondent index
	// using ceil rounding if needed
	int frequency_to_index_ceil(double frequency);

	// Convert the input frequency into the correspondent index
	// using floor rounding if needed
	int frequency_to_index_floor(double frequency);

	//convert the input frequency index into the correspondent frequency
	double index_to_frequency(int frequency_index);
#endif
