#ifndef	_MD_RSVP_CHILD_COMMON_H_INCLUDED_
	#define _MD_RSVP_CHILD_COMMON_H_INCLUDED_

	#include 	<opnet.h>
	#include 	<prg_bin_hash.h>
    #include    <ip_addr_v4.h>
	#include	"MD_rsvp.h"
	#include	"MD_spectrum_assignment_src.h"
    #include	"MD_pcep_api.h"
    #include 	"MD_oxc_api.h"
    #include	"MD_ip_api.h"
    
	
    #define		LINK_FAILURE_DEBUG

    #define NO_DEBUG_OUTPUT
  
	/* Interrupts */
	#define		PATH_MSG_ARRIVED		(msg_arrived==OPC_TRUE && arrived_msg_type==PATH_MSG)
	#define		RESV_MSG_ARRIVED		(msg_arrived==OPC_TRUE && arrived_msg_type==RESV_MSG)
	#define		PATH_ERR_MSG_ARRIVED    (msg_arrived==OPC_TRUE && arrived_msg_type==PATH_ERR_MSG)
  #define		NOTIFY_MSG_ARRIVED    (msg_arrived==OPC_TRUE && arrived_msg_type==NOTIFY_MSG)
  #define   REMOTE_LINK_FAILURE     (obj_list_ptr->if_id_error_spec!=OPC_NIL)
	#define		RESV_ERR_MSG_ARRIVED    (msg_arrived==OPC_TRUE && arrived_msg_type==RESV_ERR_MSG)
	#define		PATH_TEAR_MSG_ARRIVED   (msg_arrived==OPC_TRUE && arrived_msg_type==PATH_TEAR_MSG)
	#define		RESV_TEAR_MSG_ARRIVED   (msg_arrived==OPC_TRUE && arrived_msg_type==RESV_TEAR_MSG)
	#define		RESV_CONF_MSG_ARRIVED   (msg_arrived==OPC_TRUE && arrived_msg_type==RESV_CONF_MSG)
    
	/* Interrupt code values for self interrupt type		*/
	#define		LSP_GENERATE_CODE		1
	#define		STOP_LSP_GEN_CODE		2
	#define		START_PATH_TEAR_CODE	3
    #define		CHILD_DEAD_CODE	        4
  
	
    
	/* the struct of data shared between root and child process */	
	typedef struct Module_Shared_Data {
        Objid                   module_objid;
        Objid                   node_objid;
        Objid                   ospf_te_objid;
        Objid                   ip_objid;
        Objid                   oxc_objid;
        Objid                   failure_process_objid;
        InetT_Address           node_addr;
        Ip_Api_Interface        *ip_intf_ptr;
        //PrgT_Vector* 		        forwarding_table_ptr; 
        //Rsvp_Link**			    rsvp_link_array; //array of Rsvp_Link pointers used by RSVP protocol
        PrgT_Bin_Hash_Table*    rsvp_links_htable_ptr;
        Allocation_policy       allocation_policy;
        //char                    interarrival_distrib[128];
        //double                  mean_interarrival;
        //double                  variance_interarrival;
        //char                    service_distrib[128];
        //double                  mean_service;
        //double                  variance_service;
        // int                      min_number_of_slices;
        // int                      max_number_of_slices;	/* the maximum BW allocation for an LSP request generated by this node */
                                              /* it is described in terms of slices of 12.5GHz accordingly to ITU-T  */
        unsigned long           tot_lsp_request;
        unsigned long           tot_slices_request;
        int                     *capacity_demands_array; /* array containing all the possible capacity demands that an LSP can request */
        int                     capacity_demands_array_size;
        ApiT_Pcep_App_Handle    pcep_handler; //used to communicate with pcep process
        Boolean                 distributed_architecture; //if false, path computation is performed by a centralized PCE
        Ospf_Topology           *ospf_topology; //Topology structure defined in ospf
        Path_Comp_Params        *provisioning_pc_params_ptr;
        Path_Comp_Params        *recovery_pc_params_ptr;
        int                     pcep_api_request_id; //id used to identify a pending path computation request (between RSVP and PCEP modules)
        PrgT_Bin_Hash_Table     *pending_pcreq_htptr; // a table of pending pc request between RSVP and PCEP, it stores the ingress child process
        ApiT_Oxc_Interface_Handle oxc_intf_handle; /* interface to the OXC module */
        PrgT_Bin_Hash_Table     *pending_oxc_processes_htptr; // a table of processes waiting for the OXC to configure the cross connection
        OpT_Int32               oxc_configure_request_id;
        Boolean                 oxc_preconfiguration; // true if pre-configuration during forward signalling is allowed
        PrgT_Bin_Hash_Table     *forwarding_table_ptr; // forwarding table of the OSPF processor //TODO 
        InetT_Address           pce_addr;
        Boolean                 iam_pce;

        //node statistics
        Stathandle      transponders_inuse_sh;
        Stathandle      transponders_avg_usage_sh;
	} Module_Shared_Data;
	
    typedef struct Ingress_To_Subingress {
        Session_object  *session_ptr;
        OpT_uInt32      tx_trans_id;
        OpT_uInt32      rx_trans_id;
        Boolean         bidirect_lsp;
        PrgT_Vector     *ingress_tx_carrier_ids_vptr;
        PrgT_Vector     *ingress_rx_carrier_ids_vptr;
    } Ingress_To_Subingress;
    
    /*typedef struct Traffic_Rate_Distribution_Params {
        double rate;
        double mean_interarrival_time;
        double mean_service_time;
    } Traffic_Rate_Distribution_Params;*/

	/*	Function Prototypes					*/
	Packet* get_received_packet();
	//int find_outstream_from_instream(Objid module_objid,int input_stream_indx,int *neighbor_node_addr);

	Boolean reserve_frequency_slot(Rsvp_Link* rsvp_link, int central_freq_indx, int number_of_slices, Frequency_slot* new_freq_slot);
	Boolean release_resources(Rsvp_Link* rsvp_link, Frequency_slot *  freq_slot);
  
    void trigger_ospf_te_update(Objid ospf_te_objid, PrgT_Bin_Hash_Table* rsvp_links_htable_ptr,int outstream_indx, OpT_Int32 transponder_id);
    
    void rsvp_link_print(Rsvp_Link*	rsvp_link);

    void node_transponders_statistic_update(Ospf_Topology const* topology,
                                            InetT_Address const* address_ptr,
                                            Stathandle *transponders_inuse_sh_ptr,
                                            Stathandle *transponders_avg_usage_sh_ptr);
        
#endif
