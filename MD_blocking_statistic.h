#ifndef	_MD_BLOCKING_STATISTIC_H_INCLUDED_
	#define _MD_BLOCKING_STATISTIC_H_INCLUDED_
    
    typedef struct BlockingStatistic* BlockingStatisticPtr;
    
    BlockingStatisticPtr blocking_statistic_create(const char* const name);
    void blocking_statistic_destroy(BlockingStatisticPtr stat_ptr);
    
    void blocking_statistic_capacity_add(BlockingStatisticPtr stat_ptr, double capacity);
    void blocking_statistic_blocked_capacity_add(BlockingStatisticPtr stat_ptr, double capacity);
    void blocking_statistic_write(BlockingStatisticPtr stat_ptr);
#endif
