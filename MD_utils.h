#ifndef	_MD_UTILS_H_INCLUDED_
	#define _MD_UTILS_H_INCLUDED_

  #include <opnet.h>
  #define MAX(a,b) (a > b ? a : b)
  #define MIN(a,b) (a < b ? a : b)
  /*
  #define MAX(a,b) ({ typeid(a) _a = (a); typeid(b) _b = (b); _a > _b ? _a : _b; })
  #define MIN(a,b) ({ typeid(a) _a = (a); typeid(b) _b = (b); _a < _b ? _a : _b; })
  */
  //  note this example: 
  //  spectrums [1600GHz,1650GHz] [1650GHz,1700GHz] are not considered overlapping
  #define BW_IS_OVERLAPPING(a1,a2,b1,b2) (MAX(a1,b1) < MIN(a2,b2) ? OPC_TRUE : OPC_FALSE)

  typedef enum MD_Compcode {
  	MD_Compcode_Failure = 0,
  	MD_Compcode_Success = 1
  } MD_Compcode;


  PrgT_Compcode prg_vector_mem_free(void* elem_ptr);
  int* int_mem_copy(void* int_ptr);
  double* double_mem_copy(void* double_ptr);
#endif
