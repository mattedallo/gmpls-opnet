#include	"MD_statistics_src.h"
#include <math.h>

#define CONFIDENCE_INTERVAL

// SINGLETON VARS //
	/*Stathandle		*provisioning_routing_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_forward_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_backward_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_total_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_forward_block_bw_lack_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_forward_block_fragmentation_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_forward_block_continuity_lsp_stat_hndl=OPC_NIL;
	*/
	Stathandle		*provisioning_routing_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_forward_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_backward_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_total_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_forward_block_bw_lack_capacity_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_forward_block_fragmentation_capacity_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_forward_block_continuity_capacity_stat_hndl=OPC_NIL;


	Stathandle		*spectrum_utilization_stat_hndl=OPC_NIL;
	Stathandle    	*used_paths_stat_hndl=OPC_NIL;
	double			mu_total_prob = 0;
	double			npts_total_prob = 0;
	double			squared_sum_total_prob = 0;
	double			sum_total_prob = 0;
	/*
	double 			provisioning_generated_lsp=0;
	double 			provisioning_routing_blocked_lsp=0;
	double 			provisioning_forward_blocked_bw_lack_lsp=0;
	double 			provisioning_forward_blocked_fragmentation_lsp=0;
	double 			provisioning_forward_blocked_continuity_lsp=0;
	double 			provisioning_backward_blocked_lsp=0;
*/
	double 			provisioning_generated_capacity=0;
	double 			provisioning_routing_blocked_capacity=0;
	double 			provisioning_forward_blocked_bw_lack_capacity=0;
	double 			provisioning_forward_blocked_fragmentation_capacity=0;
	double 			provisioning_forward_blocked_continuity_capacity=0;
	double 			provisioning_backward_blocked_capacity=0;	
/*
	Stathandle		*recovery_routing_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_backward_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_total_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_bw_lack_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_fragmentation_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_continuity_lsp_stat_hndl=OPC_NIL;
*/
	Stathandle		*recovery_routing_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_backward_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_total_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_bw_lack_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_fragmentation_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_continuity_capacity_stat_hndl=OPC_NIL;

	Stathandle		*recovery_time_stat_hndl=OPC_NIL;
	Stathandle		*total_recovered_percentage_hndl=OPC_NIL;
	Stathandle		*unrecovered_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovered_capacity_stat_hndl=OPC_NIL;

	unsigned short 	received_link_failure_notification = 0; // number of recovery notification received from the nodes attached to failed link
	unsigned int	total_capacity_to_recover = 0; // Gbps
	unsigned int	recovered_capacity = 0;
	unsigned int	unrecovered_capacity = 0;
	unsigned int	total_failed_lsps = 0;
	double			recovery_start_time = 0; //seconds
	double			recovery_end_time = 0; //Seconds
	Boolean			recovery_status = OPC_FALSE;
	/*
	double			recovery_generated_lsp = 0;
	double			recovery_routing_blocked_lsp = 0;
	double			recovery_backward_blocked_lsp = 0;
	double			recovery_forward_blocked_bw_lack_lsp = 0;
	double			recovery_forward_blocked_fragmentation_lsp = 0;
	double			recovery_forward_blocked_continuity_lsp = 0;
	*/
	double			recovery_generated_capacity = 0;
	double			recovery_routing_blocked_capacity = 0;
	double			recovery_backward_blocked_capacity = 0;
	double			recovery_forward_blocked_bw_lack_capacity = 0;
	double			recovery_forward_blocked_fragmentation_capacity = 0;
	double			recovery_forward_blocked_continuity_capacity = 0;	

	Stathandle		*lsps_0G_recovered_stat_hndl=OPC_NIL;
	Stathandle		*lsps_100G_recovered_stat_hndl=OPC_NIL;
	Stathandle		*lsps_200G_recovered_stat_hndl=OPC_NIL;
	Stathandle		*lsps_300G_recovered_stat_hndl=OPC_NIL;
	Stathandle		*lsps_400G_recovered_stat_hndl=OPC_NIL;

	unsigned int	lsps_0G_recovered = 0;
	unsigned int	lsps_100G_recovered = 0;
	unsigned int	lsps_200G_recovered = 0;
	unsigned int	lsps_300G_recovered = 0;
	unsigned int	lsps_400G_recovered = 0;

	
	//Stathandle		*provisioning_egress_tx_trans_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_egress_tx_trans_block_capacity_stat_hndl=OPC_NIL;
	//Stathandle		*recovery_egress_tx_trans_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_egress_tx_trans_block_capacity_stat_hndl=OPC_NIL;

	//double			provisioning_egress_tx_transponder_blocking_lsp = 0;
	double			provisioning_egress_tx_transponder_blocking_capacity = 0;
	//double			recovery_egress_tx_transponder_blocking_lsp = 0;
	double			recovery_egress_tx_transponder_blocking_capacity = 0;

	//Stathandle		*provisioning_egress_rx_trans_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_egress_rx_trans_block_capacity_stat_hndl=OPC_NIL;
	//Stathandle		*recovery_egress_rx_trans_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_egress_rx_trans_block_capacity_stat_hndl=OPC_NIL;

	//double			provisioning_egress_rx_transponder_lack_blocking_lsp = 0;
	double			provisioning_egress_rx_transponder_lack_blocking_capacity = 0;
	//double			recovery_egress_rx_transponder_lack_blocking_lsp = 0;
	double			recovery_egress_rx_transponder_lack_blocking_capacity = 0;

	//Stathandle		*provisioning_ingress_tx_trans_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_ingress_tx_trans_block_capacity_stat_hndl=OPC_NIL;
	//Stathandle		*recovery_ingress_tx_trans_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_ingress_tx_trans_block_capacity_stat_hndl=OPC_NIL;

	//double			provisioning_ingress_tx_transponder_lack_blocking_lsp = 0;
	double			provisioning_ingress_tx_transponder_lack_blocking_capacity = 0;
	//double			recovery_ingress_tx_transponder_blocking_lsp = 0;
	double			recovery_ingress_tx_transponder_lack_blocking_capacity = 0;

	//Stathandle		*provisioning_ingress_rx_trans_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*provisioning_ingress_rx_trans_block_capacity_stat_hndl=OPC_NIL;
	//Stathandle		*recovery_ingress_rx_trans_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_ingress_rx_trans_block_capacity_stat_hndl=OPC_NIL;

	/*
	double			provisioning_ingress_rx_transponder_blocking_lsp = 0;
	double			recovery_ingress_rx_transponder_blocking_lsp = 0;
	*/
	double			provisioning_ingress_rx_transponder_blocking_capacity = 0;
	double			recovery_ingress_rx_transponder_blocking_capacity = 0;


	// ingress TX transponder backward blocking 
	// occurs when the carriers received with RESV msg cannot be allocated
	/*Stathandle		*provisioning_ingress_tx_trans_backward_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_ingress_tx_trans_backward_block_lsp_stat_hndl=OPC_NIL;
	double			provisioning_ingress_tx_transponder_backward_blocking_lsp = 0;
	double			recovery_ingress_tx_transponder_backward_blocking_lsp = 0;
	*/
	Stathandle		*provisioning_ingress_tx_trans_backward_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_ingress_tx_trans_backward_block_capacity_stat_hndl=OPC_NIL;
	double			provisioning_ingress_tx_transponder_backward_blocking_capacity = 0;
	double			recovery_ingress_tx_transponder_backward_blocking_capacity = 0;


	// blocking occuring when a couple of TX RX that match in terms of allowed carriers
	// is not found
	Stathandle		*provisioning_trans_matching_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_trans_matching_block_capacity_stat_hndl=OPC_NIL;
	double			provisioning_trans_matching_blocking_capacity = 0;
	double			recovery_trans_matching_blocking_capacity = 0;


	Stathandle		*provisioning_trans_paths_matching_block_capacity_stat_hndl=OPC_NIL;
	double provisioning_trans_paths_matching_blocking_capacity = 0;
	Stathandle		*recovery_trans_paths_matching_block_capacity_stat_hndl=OPC_NIL;
	double recovery_trans_paths_matching_blocking_capacity = 0;

	Stathandle		*provisioning_path_spectrum_block_capacity_stat_hndl=OPC_NIL;
	double provisioning_path_spectrum_blocking_capacity = 0;
	Stathandle		*recovery_path_spectrum_block_capacity_stat_hndl=OPC_NIL;
	double recovery_path_spectrum_blocking_capacity = 0;



	// confidence interval VARS//
		// number of experiments
		int experiment_count = 0;
		// number of experiments to skip ("burn") during the computation
		int burning_experiment = 10;
		// minimum number of experiment
		int min_rep = 50;
		// the sum of the experiment outcomes
		double experiments_block_prob_sum = 0.0;
		// the sum of the square of all experiment outcomes
		double experiments_block_prob_sum2 = 0.0;
		double prev_experiments_capacity_blocked = 0.0;
		double prev_experiments_capacity_generated = 0.0;


















void register_statistics() {
	static Boolean	init_flag = OPC_FALSE;
	FIN(register_statistics());
	
	if (OPC_TRUE == init_flag) FOUT;
	/*provisioning_routing_block_lsp_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_backward_block_lsp_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_bw_lack_lsp_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_fragmentation_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_continuity_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_total_block_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));*/
	
	provisioning_routing_block_capacity_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_backward_block_capacity_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_bw_lack_capacity_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_fragmentation_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_continuity_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	provisioning_total_block_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	// egress TX transponder lack blocking
	/*provisioning_egress_tx_trans_block_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_egress_tx_trans_block_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Egress TX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	recovery_egress_tx_trans_block_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_egress_tx_trans_block_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Egress TX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	provisioning_egress_tx_trans_block_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_egress_tx_trans_block_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Egress TX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	recovery_egress_tx_trans_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_egress_tx_trans_block_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Egress TX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	//
	
	// ingress TX transponder backward blocking
	/*provisioning_ingress_tx_trans_backward_block_lsp_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_ingress_tx_trans_backward_block_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Ingress TX Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	recovery_ingress_tx_trans_backward_block_lsp_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_ingress_tx_trans_backward_block_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Ingress TX Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	provisioning_ingress_tx_trans_backward_block_capacity_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_ingress_tx_trans_backward_block_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Ingress TX Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	recovery_ingress_tx_trans_backward_block_capacity_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_ingress_tx_trans_backward_block_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Ingress TX Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	//
	
	spectrum_utilization_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	used_paths_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	/*
	*provisioning_routing_block_lsp_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.LSP.Routing Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_backward_block_lsp_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.LSP.Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_bw_lack_lsp_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.LSP.Forward Blocking BW Lack Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_fragmentation_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Forward Blocking Fragmentation Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_continuity_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Forward Blocking Continuity Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Forward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_total_block_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Total Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	*provisioning_routing_block_capacity_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Routing Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_backward_block_capacity_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_bw_lack_capacity_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Forward Blocking BW Lack Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_fragmentation_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Forward Blocking Fragmentation Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_continuity_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Forward Blocking Continuity Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Forward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_total_block_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Total Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*spectrum_utilization_stat_hndl = op_stat_reg ("Spectrum Efficiency.Total Spectrum Utilization", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*used_paths_stat_hndl = op_stat_reg ("Routing.Number Of Used Paths For LSP", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	/*
	recovery_routing_block_lsp_stat_hndl 					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_backward_block_lsp_stat_hndl 					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_bw_lack_lsp_stat_hndl 			= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_fragmentation_lsp_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_continuity_lsp_stat_hndl  		= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_lsp_stat_hndl  					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_total_block_lsp_stat_hndl  					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*/

	recovery_routing_block_capacity_stat_hndl 					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_backward_block_capacity_stat_hndl 					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_bw_lack_capacity_stat_hndl 			= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_fragmentation_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_continuity_capacity_stat_hndl  		= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_capacity_stat_hndl  					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_total_block_capacity_stat_hndl  					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	recovery_time_stat_hndl			=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	total_recovered_percentage_hndl = 	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovered_capacity_stat_hndl 	= 	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	unrecovered_capacity_stat_hndl	= 	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	lsps_0G_recovered_stat_hndl		=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	lsps_100G_recovered_stat_hndl	=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	lsps_200G_recovered_stat_hndl	=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	lsps_300G_recovered_stat_hndl	=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	lsps_400G_recovered_stat_hndl	=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	/*
	*recovery_routing_block_lsp_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.LSP.Routing Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_backward_block_lsp_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.LSP.Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_bw_lack_lsp_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.LSP.Forward Blocking BW Lack Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_fragmentation_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Forward Blocking Fragmentation Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_continuity_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Forward Blocking Continuity Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Forward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_total_block_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Total Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	*recovery_routing_block_capacity_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.Capacity.Routing Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_backward_block_capacity_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.Capacity.Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_bw_lack_capacity_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.Capacity.Forward Blocking BW Lack Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_fragmentation_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Forward Blocking Fragmentation Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_continuity_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Forward Blocking Continuity Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Forward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_total_block_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Total Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*recovery_time_stat_hndl = op_stat_reg ("Recovery.Total Recovery Time", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*total_recovered_percentage_hndl = op_stat_reg ("Recovery.Total Recovered Percentage", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovered_capacity_stat_hndl = op_stat_reg ("Recovery.Recovered Capacity", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*unrecovered_capacity_stat_hndl = op_stat_reg ("Recovery.Unrecovered Capacity", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*lsps_0G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.0Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*lsps_100G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.100Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*lsps_200G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.200Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*lsps_300G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.300Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*lsps_400G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.400Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	/*
	provisioning_egress_rx_trans_block_lsp_stat_hndl  		= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_egress_rx_trans_block_lsp_stat_hndl  		= op_stat_reg ("Provisioning.Blocking Probability.LSP.Egress RX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	provisioning_egress_rx_trans_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_egress_rx_trans_block_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Egress RX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	/*recovery_egress_rx_trans_block_lsp_stat_hndl  			= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_egress_rx_trans_block_lsp_stat_hndl  			= op_stat_reg ("Recovery.Blocking Probability.LSP.Egress RX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	recovery_egress_rx_trans_block_capacity_stat_hndl  		= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_egress_rx_trans_block_capacity_stat_hndl  	= op_stat_reg ("Recovery.Blocking Probability.Capacity.Egress RX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	/*
	provisioning_ingress_rx_trans_block_lsp_stat_hndl  		= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_ingress_rx_trans_block_lsp_stat_hndl  	= op_stat_reg ("Provisioning.Blocking Probability.LSP.Ingress RX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	provisioning_ingress_rx_trans_block_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_ingress_rx_trans_block_capacity_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Ingress RX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	/*
	recovery_ingress_rx_trans_block_lsp_stat_hndl  			= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_ingress_rx_trans_block_lsp_stat_hndl  		= op_stat_reg ("Recovery.Blocking Probability.LSP.Ingress RX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	recovery_ingress_rx_trans_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_ingress_rx_trans_block_capacity_stat_hndl  	= op_stat_reg ("Recovery.Blocking Probability.Capacity.Ingress RX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	/*
	provisioning_ingress_tx_trans_block_lsp_stat_hndl  		= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_ingress_tx_trans_block_lsp_stat_hndl  	= op_stat_reg ("Provisioning.Blocking Probability.LSP.Ingress TX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	provisioning_ingress_tx_trans_block_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_ingress_tx_trans_block_capacity_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Ingress TX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	/*
	recovery_ingress_tx_trans_block_lsp_stat_hndl  			= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_ingress_tx_trans_block_lsp_stat_hndl  		= op_stat_reg ("Recovery.Blocking Probability.LSP.Ingress TX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*/
	recovery_ingress_tx_trans_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_ingress_tx_trans_block_capacity_stat_hndl  	= op_stat_reg ("Recovery.Blocking Probability.Capacity.Ingress TX Lack Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);

	provisioning_trans_matching_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_trans_matching_block_capacity_stat_hndl  	= op_stat_reg ("Provisioning.Blocking Probability.Capacity.Transponder Matching Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);

	recovery_trans_matching_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_trans_matching_block_capacity_stat_hndl  	= op_stat_reg ("Recovery.Blocking Probability.Capacity.Transponder Matching Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	provisioning_trans_paths_matching_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_trans_paths_matching_block_capacity_stat_hndl  	= op_stat_reg ("Provisioning.Blocking Probability.Capacity.Transponders Paths Matching Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);

	recovery_trans_paths_matching_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_trans_paths_matching_block_capacity_stat_hndl  	= op_stat_reg ("Recovery.Blocking Probability.Capacity.Transponders Paths Matching Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);

	provisioning_path_spectrum_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*provisioning_path_spectrum_block_capacity_stat_hndl  	= op_stat_reg ("Provisioning.Blocking Probability.Capacity.Paths Spectrum Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);

	recovery_path_spectrum_block_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	*recovery_path_spectrum_block_capacity_stat_hndl  	= op_stat_reg ("Recovery.Blocking Probability.Capacity.Paths Spectrum Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	init_flag = OPC_TRUE;
	FOUT;
}
/*___________________________________________________UPDATE STATISTICS______________________________________________________________________________________*/
/*------------------------------------UPDATE BLOCKING STATISTICS--------------------------*/
void update_blocking_statistics(Lsp_event event, unsigned int sub_lsp_capacity, unsigned int total_lsp_capacity) {
	double 	forward_blocked;
	double	total_blocked;
	double	routing_block_prob;
	double	backwad_prob;
	double	forwad_bw_lack_prob;
	double	forwad_fragmentation_prob;
	double	forwad_continuity_prob;
	double	forwad_prob;
	double	total_prob;
	//double	lsp_weight;
	double 	egress_tx_trans_prob;
	double 	egress_rx_trans_prob;
	double 	ingress_tx_trans_prob;
	double 	ingress_rx_trans_prob;
	double	ingress_tx_trans_back_prob;
	double	trans_matching_prob;
	double	trans_paths_matching_prob;
	double	path_spectrum_prob;

/*	double  zeta,Relative_CI, computed_ci, ci;
	int 	cl;
	double	sigma_total_prob,mu_total_prob;
*/	
	FIN(update_blocking_statistics(event,sub_lsp_capacity,total_lsp_capacity));
	
	op_prg_mt_global_lock ();
		
		provisioning_generated_capacity += sub_lsp_capacity;
			
		/*lsp_weight = (double)sub_lsp_capacity/total_lsp_capacity;
		provisioning_generated_lsp += lsp_weight;*/
	
		switch (event)	{
			case ROUTING_BLOCKING:					
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													//provisioning_routing_blocked_lsp += lsp_weight;
													break;
			case BACKWARD_BLOCKING:	
													provisioning_backward_blocked_capacity += sub_lsp_capacity;
													//provisioning_backward_blocked_lsp += lsp_weight;
													break;
			case FORWARD_BLOCKING_BW_LACK:	
													provisioning_forward_blocked_bw_lack_capacity += sub_lsp_capacity;
													//provisioning_forward_blocked_bw_lack_lsp += lsp_weight;
													break;
			case FORWARD_BLOCKING_FRAGMENTATION:	
													provisioning_forward_blocked_fragmentation_capacity += sub_lsp_capacity;
													//provisioning_forward_blocked_fragmentation_lsp += lsp_weight;
													break;
			case FORWARD_BLOCKING_CONTINUITY:	
													provisioning_forward_blocked_continuity_capacity += sub_lsp_capacity;
													//provisioning_forward_blocked_continuity_lsp += lsp_weight;
													break;
			case EGRESS_NODE_TX_TRANSPONDER_BLOCKING:	
													provisioning_egress_tx_transponder_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													//provisioning_egress_tx_transponder_blocking_lsp += lsp_weight;
													break;
			case EGRESS_NODE_RX_TRANSPONDER_LACK_BLOCKING:	
													provisioning_egress_rx_transponder_lack_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													//provisioning_egress_rx_transponder_lack_blocking_lsp += lsp_weight;
													break;
			case EGRESS_NODE_RX_TRANSPONDER_BLOCKING:	
													provisioning_egress_rx_transponder_lack_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													//provisioning_egress_rx_transponder_lack_blocking_lsp += lsp_weight;
													break;
			case INGRESS_NODE_TX_TRANSPONDER_LACK_BLOCKING:	
													provisioning_ingress_tx_transponder_lack_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													//provisioning_ingress_tx_transponder_lack_blocking_lsp += lsp_weight;
													break;
			case INGRESS_NODE_TX_TRANSPONDER_BLOCKING:	
													provisioning_ingress_tx_transponder_lack_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													//provisioning_ingress_tx_transponder_lack_blocking_lsp += lsp_weight;
													break;
			case INGRESS_NODE_RX_TRANSPONDER_BLOCKING:	
													provisioning_ingress_rx_transponder_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													//provisioning_ingress_rx_transponder_blocking_lsp += lsp_weight;
													break;
			case TRANSPONDER_MATCHING_BLOCKING:
													provisioning_trans_matching_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													break;
			case TRANSPONDERS_PATHS_MATCHING_BLOCKING:
													provisioning_trans_paths_matching_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													break;
			case PATH_SPECTRUM_BLOCKING:
													provisioning_path_spectrum_blocking_capacity += sub_lsp_capacity;
													provisioning_routing_blocked_capacity += sub_lsp_capacity;
													break;
			case INGRESS_NODE_TX_TRANSPONDER_BACKWARD_BLOCKING:
													provisioning_ingress_tx_transponder_backward_blocking_capacity += sub_lsp_capacity;
													provisioning_backward_blocked_capacity += sub_lsp_capacity;
													/*
													provisioning_ingress_tx_transponder_backward_blocking_lsp += lsp_weight;
													provisioning_backward_blocked_lsp += lsp_weight;*/
													break;
			case ESTABLISHED: 	
													break;
			case RELEASED: 			
													break;
		}
		
		/*
		forward_blocked = provisioning_forward_blocked_bw_lack_lsp + provisioning_forward_blocked_fragmentation_lsp + provisioning_forward_blocked_continuity_lsp;
		total_blocked = provisioning_routing_blocked_lsp +
						forward_blocked + 
						provisioning_backward_blocked_lsp +
						provisioning_egress_tx_transponder_blocking_lsp +
						provisioning_egress_rx_transponder_lack_blocking_lsp +
						provisioning_ingress_tx_transponder_lack_blocking_lsp +
						provisioning_ingress_rx_transponder_blocking_lsp;
		
		routing_block_prob = (double)provisioning_routing_blocked_lsp/provisioning_generated_lsp;
		
		backwad_prob = (double)provisioning_backward_blocked_lsp/provisioning_generated_lsp;
		
		forwad_bw_lack_prob = (double)provisioning_forward_blocked_bw_lack_lsp/provisioning_generated_lsp;
		forwad_fragmentation_prob = (double)provisioning_forward_blocked_fragmentation_lsp/provisioning_generated_lsp;
		forwad_continuity_prob = (double)provisioning_forward_blocked_continuity_lsp/provisioning_generated_lsp;
		forwad_prob = (double)forward_blocked/provisioning_generated_lsp;
		
		egress_tx_trans_prob = (double)provisioning_egress_tx_transponder_blocking_lsp/provisioning_generated_lsp;
		egress_rx_trans_prob = (double)provisioning_egress_rx_transponder_lack_blocking_lsp/provisioning_generated_lsp;
		ingress_tx_trans_prob = (double)provisioning_ingress_tx_transponder_lack_blocking_lsp/provisioning_generated_lsp;
		ingress_rx_trans_prob = (double)provisioning_ingress_rx_transponder_blocking_lsp/provisioning_generated_lsp;
		ingress_tx_trans_back_prob = (double)provisioning_ingress_tx_transponder_backward_blocking_lsp/provisioning_generated_lsp;
		
		total_prob = (double)total_blocked/provisioning_generated_lsp;

		op_stat_write(*provisioning_routing_block_lsp_stat_hndl,routing_block_prob);
		op_stat_write(*provisioning_backward_block_lsp_stat_hndl,backwad_prob);
		op_stat_write(*provisioning_forward_block_bw_lack_lsp_stat_hndl,forwad_bw_lack_prob);
		op_stat_write(*provisioning_forward_block_fragmentation_lsp_stat_hndl,forwad_fragmentation_prob);
		op_stat_write(*provisioning_forward_block_continuity_lsp_stat_hndl,forwad_continuity_prob);
		op_stat_write(*provisioning_forward_block_lsp_stat_hndl,forwad_prob);
		op_stat_write(*provisioning_egress_tx_trans_block_lsp_stat_hndl,egress_tx_trans_prob);
		op_stat_write(*provisioning_egress_rx_trans_block_lsp_stat_hndl,egress_rx_trans_prob);
		op_stat_write(*provisioning_ingress_tx_trans_block_lsp_stat_hndl,ingress_tx_trans_prob);
		op_stat_write(*provisioning_ingress_rx_trans_block_lsp_stat_hndl,ingress_rx_trans_prob);
		op_stat_write(*provisioning_ingress_tx_trans_backward_block_lsp_stat_hndl,ingress_tx_trans_back_prob);
		op_stat_write(*provisioning_total_block_lsp_stat_hndl,total_prob);
		*/
		
		routing_block_prob = (double)provisioning_routing_blocked_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_routing_block_capacity_stat_hndl,routing_block_prob);

		backwad_prob = (double)provisioning_backward_blocked_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_backward_block_capacity_stat_hndl,backwad_prob);

		forwad_bw_lack_prob = (double)provisioning_forward_blocked_bw_lack_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_forward_block_bw_lack_capacity_stat_hndl,forwad_bw_lack_prob);

		forwad_fragmentation_prob = (double)provisioning_forward_blocked_fragmentation_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_forward_block_fragmentation_capacity_stat_hndl,forwad_fragmentation_prob);

		forwad_continuity_prob = (double)provisioning_forward_blocked_continuity_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_forward_block_continuity_capacity_stat_hndl,forwad_continuity_prob);

		forward_blocked = provisioning_forward_blocked_bw_lack_capacity +
											provisioning_forward_blocked_fragmentation_capacity + 
											provisioning_forward_blocked_continuity_capacity;

		forwad_prob = (double)forward_blocked/provisioning_generated_capacity;
		op_stat_write(*provisioning_forward_block_capacity_stat_hndl,forwad_prob);

		egress_tx_trans_prob = (double)provisioning_egress_tx_transponder_blocking_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_egress_tx_trans_block_capacity_stat_hndl,egress_tx_trans_prob);

		egress_rx_trans_prob = (double)provisioning_egress_rx_transponder_lack_blocking_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_egress_rx_trans_block_capacity_stat_hndl,egress_rx_trans_prob);

		ingress_tx_trans_prob = (double)provisioning_ingress_tx_transponder_lack_blocking_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_ingress_tx_trans_block_capacity_stat_hndl,ingress_tx_trans_prob);

		ingress_rx_trans_prob = (double)provisioning_ingress_rx_transponder_blocking_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_ingress_rx_trans_block_capacity_stat_hndl,ingress_rx_trans_prob);

		ingress_tx_trans_back_prob = (double)provisioning_ingress_tx_transponder_backward_blocking_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_ingress_tx_trans_backward_block_capacity_stat_hndl,ingress_tx_trans_back_prob);

		trans_matching_prob = (double)provisioning_trans_matching_blocking_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_trans_matching_block_capacity_stat_hndl,trans_matching_prob);
		
		trans_paths_matching_prob = (double)provisioning_trans_paths_matching_blocking_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_trans_paths_matching_block_capacity_stat_hndl,trans_paths_matching_prob);

		path_spectrum_prob = (double)provisioning_path_spectrum_blocking_capacity/provisioning_generated_capacity;
		op_stat_write(*provisioning_path_spectrum_block_capacity_stat_hndl,path_spectrum_prob);
		
		total_blocked = provisioning_routing_blocked_capacity + 
										forward_blocked + 
										provisioning_backward_blocked_capacity;

		total_prob = (double)total_blocked/provisioning_generated_capacity;
		op_stat_write(*provisioning_total_block_capacity_stat_hndl,total_prob);

	op_prg_mt_global_unlock ();
		
		


	//-----------------------------------------------------
	//
	//           confidence interval computation
	//
	//-----------------------------------------------------
	#ifdef CONFIDENCE_INTERVAL
	{
		int confidence_level = 90;
		double desired_ci = 15;
		double mean_holding_time=3600;

		if (experiment_count < floor(op_sim_time()/(10*mean_holding_time)) ) {
			//a new experiment is terminated
			++experiment_count;
			
			//skip the first experiment to let the network load
			if (experiment_count > burning_experiment) {
				double zeta,mu,sigma2,relative_ci,confidence_interval,experiment_block_prob;
				double experiment_capacity_blocked,experiment_capacity_generated;

				experiment_capacity_blocked = total_blocked - prev_experiments_capacity_blocked;
				experiment_capacity_generated = provisioning_generated_capacity - prev_experiments_capacity_generated;
				experiment_block_prob = (double)experiment_capacity_blocked/experiment_capacity_generated;
			
				experiments_block_prob_sum += experiment_block_prob;

				experiments_block_prob_sum2 += pow(experiment_block_prob,2);
				
				// mu: the average of blocking probability among experiments
				mu=experiments_block_prob_sum/experiment_count;
				
				sigma2 = ((double)experiments_block_prob_sum2 + experiment_count*pow(mu,2) -2.0*mu*experiments_block_prob_sum)/(experiment_count-1);

				switch (confidence_level) {
					case 80:
						zeta=1.28;
						break;
		    	case 85:
						zeta=1.44;
						break;
		    	case 90:
						zeta=1.645;
						break;
		    	case 95:
		        zeta=1.96;
						break;
		    	case 99:
		        zeta=2.575;
						break;
	    	}

				confidence_interval = zeta*sqrt(sigma2/experiment_count);
				printf("confidence_interval=%f\n",confidence_interval);
				if (mu!=0) {
					relative_ci=(2.0*confidence_interval)/mu;
				} else { 
					relative_ci=0;
				}

				printf("mu=%f  relative_ci=%f\n",mu,relative_ci);
				if ((relative_ci*100 < desired_ci) && (experiment_count > min_rep) && (mu!=0) ) {
					op_sim_end("confidence interval reached","","","");
				}
			}

			prev_experiments_capacity_blocked = total_blocked;
			prev_experiments_capacity_generated = provisioning_generated_capacity;
		}

	}
	#endif


	FOUT;
}



/*---------------------------UPDATE NUMBER OF USED PATH STATISTICS--------------------*/
void update_used_path_statistics(unsigned short	number_of_paths){
	FIN(update_used_path_statistics(used_paths_htable_ptr));
		op_prg_mt_global_lock ();
		op_stat_write(*used_paths_stat_hndl,number_of_paths);
		op_prg_mt_global_unlock ();
	FOUT;
}



/*------------------------------------UPDATE LINK STATISTICS--------------------------*/
void update_links_statistics(Lsp_event event, int num_slices) {
	double	spectrum_allocation_ratio;
	FIN(update_links_statistics());
	
	op_prg_mt_global_lock ();
	
	switch (event)	{
			case ESTABLISHED: 	
							gbl_total_busy_slices = gbl_total_busy_slices + num_slices;
							break;
			case RELEASED: 	
							gbl_total_busy_slices = gbl_total_busy_slices - num_slices;
							break;
	}
	spectrum_allocation_ratio = (double)gbl_total_busy_slices*100.0/gbl_total_slices;
	op_stat_write(*spectrum_utilization_stat_hndl,spectrum_allocation_ratio);
	
	op_prg_mt_global_unlock ();
	FOUT;
}

/*------------------------------RECOVERY STATISTICS------------------------------------------*/


void stat_recovery_start() {
FIN(stat_recovery_start());
	if (recovery_status == OPC_FALSE) {
		recovery_status = OPC_TRUE;
		recovery_start_time = op_sim_time ();
		#if defined(LINK_FAILURE_DEBUG)
		printf("recovery starts at %f seconds, capacity to recover %dGbps (up to now)\n",recovery_start_time, total_capacity_to_recover);
		#endif
		op_stat_write(*unrecovered_capacity_stat_hndl,0);
		op_stat_write(*recovered_capacity_stat_hndl,0);
	}
FOUT;
}


void recovery_end() {
double	total_recovery_time,recovered_percentage;
//Objid	failure_node_objid,failure_process_objid;

double 	forward_blocked;
double	total_blocked;
double	routing_block_prob;
double	backwad_prob;
double	forwad_bw_lack_prob;
double	forwad_fragmentation_prob;
double	forwad_continuity_prob;
double	forwad_prob;
double 	egress_tx_trans_prob;
double 	egress_rx_trans_prob;
double 	ingress_tx_trans_prob;
double 	ingress_rx_trans_prob;
double	ingress_tx_trans_back_prob;
double	trans_matching_prob;
double  trans_paths_matching_prob;
double	path_spectrum_prob;
double	total_prob;
unsigned int total_400G_lsps;

FIN(recovery_end());
	recovery_status = OPC_FALSE;
	recovery_end_time = op_sim_time();

	total_recovery_time = recovery_end_time-recovery_start_time;
	op_stat_write(*recovery_time_stat_hndl,total_recovery_time);
			
	recovered_percentage = (double)recovered_capacity*100/(recovered_capacity + unrecovered_capacity);
	op_stat_write(*total_recovered_percentage_hndl,recovered_percentage);
	
	//#if defined(LINK_FAILURE_DEBUG)
	printf("recovery ends at %f seconds, capacity recovered %dGbps, unrecovered %dGbps \n",recovery_end_time,recovered_capacity,unrecovered_capacity);
	//#endif

	/*
	forward_blocked = recovery_forward_blocked_bw_lack_lsp + recovery_forward_blocked_fragmentation_lsp + recovery_forward_blocked_continuity_lsp;
	total_blocked = recovery_routing_blocked_lsp + 
					forward_blocked + 
					recovery_backward_blocked_lsp +
					recovery_egress_tx_transponder_blocking_lsp +
					recovery_egress_rx_transponder_lack_blocking_lsp +
					recovery_ingress_tx_transponder_blocking_lsp +
					recovery_ingress_rx_transponder_blocking_lsp;

	routing_block_prob = (double)recovery_routing_blocked_lsp/recovery_generated_lsp;
	backwad_prob = (double)recovery_backward_blocked_lsp/recovery_generated_lsp;
	forwad_bw_lack_prob = (double)recovery_forward_blocked_bw_lack_lsp/recovery_generated_lsp;
	forwad_fragmentation_prob = (double)recovery_forward_blocked_fragmentation_lsp/recovery_generated_lsp;
	forwad_continuity_prob = (double)recovery_forward_blocked_continuity_lsp/recovery_generated_lsp;
	forwad_prob = (double)forward_blocked/recovery_generated_lsp;
	egress_tx_trans_prob = (double)recovery_egress_tx_transponder_blocking_lsp/recovery_generated_lsp;
	egress_rx_trans_prob = (double)recovery_egress_rx_transponder_lack_blocking_lsp/recovery_generated_lsp;
	ingress_tx_trans_prob = (double)recovery_ingress_tx_transponder_blocking_lsp/recovery_generated_lsp;
	ingress_rx_trans_prob = (double)recovery_ingress_rx_transponder_blocking_lsp/recovery_generated_lsp;
	ingress_tx_trans_back_prob = (double)recovery_ingress_tx_transponder_backward_blocking_lsp/recovery_generated_lsp;
	
	total_prob = (double)total_blocked/recovery_generated_lsp;

	op_stat_write(*recovery_routing_block_lsp_stat_hndl,routing_block_prob);
	op_stat_write(*recovery_backward_block_lsp_stat_hndl,backwad_prob);
	op_stat_write(*recovery_forward_block_bw_lack_lsp_stat_hndl,forwad_bw_lack_prob);
	op_stat_write(*recovery_forward_block_fragmentation_lsp_stat_hndl,forwad_fragmentation_prob);
	op_stat_write(*recovery_forward_block_continuity_lsp_stat_hndl,forwad_continuity_prob);
	op_stat_write(*recovery_forward_block_lsp_stat_hndl,forwad_prob);

	op_stat_write(*recovery_egress_tx_trans_block_lsp_stat_hndl,egress_tx_trans_prob);
	op_stat_write(*recovery_egress_rx_trans_block_lsp_stat_hndl,egress_rx_trans_prob);
	op_stat_write(*recovery_ingress_tx_trans_block_lsp_stat_hndl,ingress_tx_trans_prob);
	op_stat_write(*recovery_ingress_rx_trans_block_lsp_stat_hndl,ingress_rx_trans_prob);
	op_stat_write(*recovery_ingress_tx_trans_backward_block_lsp_stat_hndl,ingress_tx_trans_back_prob);
	
	op_stat_write(*recovery_total_block_lsp_stat_hndl,total_prob);
	*/
	
	forward_blocked = recovery_forward_blocked_bw_lack_capacity + recovery_forward_blocked_fragmentation_capacity + recovery_forward_blocked_continuity_capacity;
	recovery_routing_blocked_capacity += 	recovery_egress_tx_transponder_blocking_capacity +
																				recovery_egress_rx_transponder_lack_blocking_capacity +
																				recovery_ingress_tx_transponder_lack_blocking_capacity +
																				recovery_ingress_rx_transponder_blocking_capacity +
																				recovery_trans_matching_blocking_capacity +
																				recovery_trans_paths_matching_blocking_capacity + 
																				recovery_path_spectrum_blocking_capacity;
	total_blocked = recovery_routing_blocked_capacity +
									forward_blocked + 
									recovery_backward_blocked_capacity;

	routing_block_prob = (double)recovery_routing_blocked_capacity/recovery_generated_capacity;
	backwad_prob = (double)recovery_backward_blocked_capacity/recovery_generated_capacity;
	forwad_bw_lack_prob = (double)recovery_forward_blocked_bw_lack_capacity/recovery_generated_capacity;
	forwad_fragmentation_prob = (double)recovery_forward_blocked_fragmentation_capacity/recovery_generated_capacity;
	forwad_continuity_prob = (double)recovery_forward_blocked_continuity_capacity/recovery_generated_capacity;
	forwad_prob = (double)forward_blocked/recovery_generated_capacity;
	egress_tx_trans_prob = (double)recovery_egress_tx_transponder_blocking_capacity/recovery_generated_capacity;
	egress_rx_trans_prob = (double)recovery_egress_rx_transponder_lack_blocking_capacity/recovery_generated_capacity;
	ingress_tx_trans_prob = (double)recovery_ingress_tx_transponder_lack_blocking_capacity/recovery_generated_capacity;
	ingress_rx_trans_prob = (double)recovery_ingress_rx_transponder_blocking_capacity/recovery_generated_capacity;
	ingress_tx_trans_back_prob = (double)recovery_ingress_tx_transponder_backward_blocking_capacity/recovery_generated_capacity;
	trans_matching_prob = (double)recovery_trans_matching_blocking_capacity/recovery_generated_capacity;
	trans_paths_matching_prob = (double)recovery_trans_paths_matching_blocking_capacity/recovery_generated_capacity;
	path_spectrum_prob = (double)recovery_path_spectrum_blocking_capacity/recovery_generated_capacity;

	total_prob = (double)total_blocked/recovery_generated_capacity;


	op_stat_write(*recovery_routing_block_capacity_stat_hndl,routing_block_prob);
	op_stat_write(*recovery_backward_block_capacity_stat_hndl,backwad_prob);
	op_stat_write(*recovery_forward_block_bw_lack_capacity_stat_hndl,forwad_bw_lack_prob);
	op_stat_write(*recovery_forward_block_fragmentation_capacity_stat_hndl,forwad_fragmentation_prob);
	op_stat_write(*recovery_forward_block_continuity_capacity_stat_hndl,forwad_continuity_prob);
	op_stat_write(*recovery_forward_block_capacity_stat_hndl,forwad_prob);

	op_stat_write(*recovery_egress_tx_trans_block_capacity_stat_hndl,egress_tx_trans_prob);
	op_stat_write(*recovery_egress_rx_trans_block_capacity_stat_hndl,egress_rx_trans_prob);
	op_stat_write(*recovery_ingress_tx_trans_block_capacity_stat_hndl,ingress_tx_trans_prob);
	op_stat_write(*recovery_ingress_rx_trans_block_capacity_stat_hndl,ingress_rx_trans_prob);
	op_stat_write(*recovery_ingress_tx_trans_backward_block_capacity_stat_hndl,ingress_tx_trans_back_prob);
	op_stat_write(*recovery_trans_matching_block_capacity_stat_hndl,trans_matching_prob);
	op_stat_write(*recovery_trans_paths_matching_block_capacity_stat_hndl,trans_paths_matching_prob);
	op_stat_write(*recovery_path_spectrum_block_capacity_stat_hndl,path_spectrum_prob);
	

	op_stat_write(*recovery_total_block_capacity_stat_hndl,total_prob);

	total_400G_lsps = lsps_0G_recovered+lsps_100G_recovered+lsps_200G_recovered+lsps_300G_recovered+lsps_400G_recovered;
	op_stat_write(*lsps_0G_recovered_stat_hndl,(double)100.0*lsps_0G_recovered/total_400G_lsps);
	op_stat_write(*lsps_100G_recovered_stat_hndl,(double)100.0*lsps_100G_recovered/total_400G_lsps);
	op_stat_write(*lsps_200G_recovered_stat_hndl,(double)100.0*lsps_200G_recovered/total_400G_lsps);
	op_stat_write(*lsps_300G_recovered_stat_hndl,(double)100.0*lsps_300G_recovered/total_400G_lsps);
	op_stat_write(*lsps_400G_recovered_stat_hndl,(double)100.0*lsps_400G_recovered/total_400G_lsps);
		
	

	// initialize again the variables //
	received_link_failure_notification = 0;
	total_capacity_to_recover = 0; // Gbps
	recovered_capacity = 0;
	unrecovered_capacity = 0;
	total_failed_lsps = 0;

	/*
	recovery_generated_lsp = 0;
	recovery_routing_blocked_lsp = 0;
	recovery_backward_blocked_lsp = 0;
	recovery_forward_blocked_bw_lack_lsp = 0;
	recovery_forward_blocked_fragmentation_lsp = 0;
	recovery_forward_blocked_continuity_lsp = 0;

	recovery_egress_tx_transponder_blocking_lsp = 0;
	recovery_egress_rx_transponder_lack_blocking_lsp = 0;
	recovery_ingress_tx_transponder_blocking_lsp = 0;
	recovery_ingress_rx_transponder_blocking_lsp = 0;
	recovery_ingress_tx_transponder_backward_blocking_lsp = 0;
	*/

	recovery_generated_capacity = 0;
	recovery_routing_blocked_capacity = 0;
	recovery_backward_blocked_capacity = 0;
	recovery_forward_blocked_bw_lack_capacity = 0;
	recovery_forward_blocked_fragmentation_capacity = 0;
	recovery_forward_blocked_continuity_capacity = 0;

	recovery_egress_tx_transponder_blocking_capacity = 0;
	recovery_egress_rx_transponder_lack_blocking_capacity = 0;
	recovery_ingress_tx_transponder_lack_blocking_capacity = 0;
	recovery_ingress_rx_transponder_blocking_capacity = 0;
	recovery_ingress_tx_transponder_backward_blocking_capacity = 0;
	recovery_trans_matching_blocking_capacity = 0;
	recovery_trans_paths_matching_blocking_capacity = 0;
	recovery_path_spectrum_blocking_capacity = 0;

	lsps_0G_recovered = 0;
	lsps_100G_recovered = 0;
	lsps_200G_recovered = 0;
	lsps_300G_recovered = 0;
	lsps_400G_recovered = 0;

	// send interrupt to restore the link //
	/*failure_node_objid = op_id_from_name (0, OPC_OBJTYPE_NODE_FIX, "Failure Scheduler");
	if (failure_node_objid == OPC_OBJID_INVALID) {
		printf("statistics ERROR: failure scheduler node not found, cannot recover the failed links\n");
	}
	else {
		failure_process_objid = op_id_from_name (failure_node_objid, OPC_OBJTYPE_PROC, "failure process");
		if (failure_process_objid == OPC_OBJID_INVALID) {
			printf("statistics ERROR: failure process not found inside the failure scheduler node, cannot recover the failed links\n");
		}
		else {
			op_intrpt_schedule_remote (op_sim_time (),0,failure_process_objid);
		}
	}
*/
FOUT;
}

void stat_increase_capacity_to_recover(unsigned int capacity) {
FIN(stat_increase_capacity_to_recover(capacity));
	// increment the total capacity to recover //
	total_capacity_to_recover += capacity;
	#if defined(LINK_FAILURE_DEBUG)
	printf("capacity to recover %dGbps (up to now)\n",total_capacity_to_recover);
	#endif

FOUT;
}

void stat_decrease_capacity_to_recover(unsigned int capacity) {
FIN(stat_decrease_capacity_to_recover(capacity));	
	// decrement the total capacity to recover //
	// in case an LSP has been already tear down //
	total_capacity_to_recover -= capacity;
	#if defined(LINK_FAILURE_DEBUG)
	printf("capacity to recover %dGbps (up to now)\n",total_capacity_to_recover);
	#endif
	/*if ( total_capacity_to_recover == 0 && received_link_failure_notification % 2 == 0) {
		recovery_end();
	}*/

FOUT;
}

void stat_notify_failed_lsps (unsigned int failed_lsps) {
FIN(stat_notify_failed_lsps(failed_lsps));
	// increment the number of notification received //
	received_link_failure_notification++;

	total_failed_lsps += failed_lsps;

	/*if ( failed_lsps == 0 
		 && total_capacity_to_recover>0
		 && (recovered_capacity + unrecovered_capacity) >= total_capacity_to_recover 
		 && received_link_failure_notification % 2 == 0) {
	
		recovery_end();
	}*/

FOUT;
}

void update_recovery_blocking_statistics(Lsp_event event,unsigned int sub_lsp_capacity, unsigned int total_lsp_capacity) {
	//double	lsp_weight;
	
	FIN(update_blocking_statistics(event));
	
	op_prg_mt_global_lock ();
		
		recovery_generated_capacity += sub_lsp_capacity;
	
		/*lsp_weight = (double)sub_lsp_capacity/total_lsp_capacity;
		recovery_generated_lsp += lsp_weight;*/
	
		switch (event)	{
			case ROUTING_BLOCKING:					
													recovery_routing_blocked_capacity += sub_lsp_capacity;
													//recovery_routing_blocked_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case BACKWARD_BLOCKING:					
													recovery_backward_blocked_capacity += sub_lsp_capacity;
													//recovery_backward_blocked_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case FORWARD_BLOCKING_BW_LACK:	
													recovery_forward_blocked_bw_lack_capacity += sub_lsp_capacity;
													//recovery_forward_blocked_bw_lack_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case FORWARD_BLOCKING_FRAGMENTATION:	
													recovery_forward_blocked_fragmentation_capacity += sub_lsp_capacity;
													//recovery_forward_blocked_fragmentation_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case FORWARD_BLOCKING_CONTINUITY:	
													recovery_forward_blocked_continuity_capacity += sub_lsp_capacity;
													//recovery_forward_blocked_continuity_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case EGRESS_NODE_TX_TRANSPONDER_BLOCKING:	
													recovery_egress_tx_transponder_blocking_capacity += sub_lsp_capacity;
													//recovery_egress_tx_transponder_blocking_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case EGRESS_NODE_RX_TRANSPONDER_LACK_BLOCKING:	
													recovery_egress_rx_transponder_lack_blocking_capacity += sub_lsp_capacity;
													//recovery_egress_rx_transponder_lack_blocking_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case EGRESS_NODE_RX_TRANSPONDER_BLOCKING:	
													recovery_egress_rx_transponder_lack_blocking_capacity += sub_lsp_capacity;
													//recovery_egress_rx_transponder_lack_blocking_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case INGRESS_NODE_TX_TRANSPONDER_LACK_BLOCKING:	
													recovery_ingress_tx_transponder_lack_blocking_capacity += sub_lsp_capacity;
													//recovery_ingress_tx_transponder_blocking_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case INGRESS_NODE_TX_TRANSPONDER_BLOCKING:	
													recovery_ingress_tx_transponder_lack_blocking_capacity += sub_lsp_capacity;
													//recovery_ingress_tx_transponder_blocking_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case INGRESS_NODE_RX_TRANSPONDER_BLOCKING:	
													recovery_ingress_rx_transponder_blocking_capacity += sub_lsp_capacity;
													//recovery_ingress_rx_transponder_blocking_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case TRANSPONDER_MATCHING_BLOCKING:	
													recovery_trans_matching_blocking_capacity += sub_lsp_capacity;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case TRANSPONDERS_PATHS_MATCHING_BLOCKING:
													recovery_trans_paths_matching_blocking_capacity += sub_lsp_capacity;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case PATH_SPECTRUM_BLOCKING:
													recovery_path_spectrum_blocking_capacity += sub_lsp_capacity;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case ESTABLISHED: 
													recovered_capacity += sub_lsp_capacity;
													op_stat_write(*recovered_capacity_stat_hndl,recovered_capacity);
													break;
			case RELEASED: break;
		}
		
		// check if the recovery interval is concluded //
		/*if ((recovered_capacity + unrecovered_capacity) >= total_capacity_to_recover && received_link_failure_notification % 2 == 0) {
		
			recovery_end();
			
		}*/
		
	op_prg_mt_global_unlock ();
	
	FOUT;
}



/// create histogram for 400G connection recevered///
void update_recovery_400G(int restored_cap) {
	FIN(update_recovery_400G(restored_cap));
	
	switch (restored_cap) {
		case 0:		
					lsps_0G_recovered++;
					break;
		case 100:	
					lsps_100G_recovered++;
					break;
		case 200:
					lsps_200G_recovered++;
					break;
		case 300:
					lsps_300G_recovered++;
					break;
		case 400:	
					lsps_400G_recovered++;
					break;
	}
	
	FOUT;
}


void stat_path_computation_time_write(double pc_time) {
	static Stathandle  *pc_time_stat_hndl=OPC_NIL;
	FIN( stat_path_computation_time_write() );
	if (OPC_NIL == pc_time_stat_hndl) {
		pc_time_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
		*pc_time_stat_hndl = op_stat_reg ("Path Computation Time", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	}
	op_stat_write(*pc_time_stat_hndl,pc_time);
	FOUT;
}

void stat_recovery_time_write(double recovery_time) {
	static Stathandle  *recovery_time_stathndl=OPC_NIL;
	FIN( stat_recovery_time_write() );
	if (OPC_NIL == recovery_time_stathndl) {
		recovery_time_stathndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
		*recovery_time_stathndl = op_stat_reg ("Recovery.Time", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	}
	op_stat_write(*recovery_time_stathndl,recovery_time);
	FOUT;
}

void stat_provisioning_time_write(double provisioning_time) {
	static Stathandle  *provisioning_time_stathndl=OPC_NIL;
	FIN( stat_provisioning_time_write() );
	if (OPC_NIL == provisioning_time_stathndl) {
		provisioning_time_stathndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
		*provisioning_time_stathndl = op_stat_reg ("Provisioning.Time", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	}
	op_stat_write(*provisioning_time_stathndl,provisioning_time);
	FOUT;
}

void stat_path_computation_roundtrip_time_write(double time) {
	//this also includes the time needed to compute the path
	static Stathandle  *pc_roundtrip_time_stathndl=OPC_NIL;
	FIN( stat_path_computation_roundtrip_time_write() );
	if (OPC_NIL == pc_roundtrip_time_stathndl) {
		pc_roundtrip_time_stathndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
		*pc_roundtrip_time_stathndl = op_stat_reg ("Path Computation Round Trip Time", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	}
	op_stat_write(*pc_roundtrip_time_stathndl,time);
	FOUT;
}
