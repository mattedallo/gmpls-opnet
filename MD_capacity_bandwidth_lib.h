#ifndef	_MD_CAPACITY_BANDWIDTH_LIB_H_INCLUDED_
	#define _MD_CAPACITY_BANDWIDTH_LIB_H_INCLUDED_

#include  "MD_utils.h"
#include  "MD_transponder.h"

/* define structure useful during the rouring and spectrum assignment */
typedef struct Capacity_Slices_Assoc {
    int capacity; // expressed in Gigabit per second (Gbps)
    int num_slices; //number of slices requred
} Capacity_Slices_Assoc;

void cb_capacity_to_slices_map_init();
long cb_min_capacity_slices_get(int *capacity, int *slices);
long cb_closer_allowed_capacity_slices_get(int *capacity, int *slices);
long cb_next_lower_capacity_slices_get(long prev_index, int *capacity, int *slices);
int cb_capacity_to_num_carriers(int capacity);

int cb_smaller_capacity_get(int capacity);
int cb_closer_higher_capacity_get(int capacity);

MD_Compcode cb_slices_from_capacity_get(int capacity,
                                        Transponder *transponder_ptr,
                                        int *slices_ptr);
#endif
