#ifndef	_MD_SPECTRUM_H_INCLUDED_
	#define _MD_SPECTRUM_H_INCLUDED_

#include "MD_frequency_index.h"
#include "MD_frequency_slot.h"
#include "MD_utils.h"

#define SPECTRUMC_SUCCESS	0
#define SPECTRUMC_FAIL		1
#define SPECTRUMC_SLOT_BUSY	2

typedef struct Spectrum Spectrum;
typedef struct Spectrum_Map Spectrum_Map;

Spectrum* spectrum_create(Frequency_Index const* const lowest_fi, Frequency_Index const* const highest_fi);
void spectrum_destroy(Spectrum* spectrum_ptr);

Spectrum* spectrum_inversion(Spectrum const* const spectrum_ptr);

void spectrum_union(Spectrum* spectrum_a_ptr,Spectrum const* const spectrum_b_ptr);
void spectrum_intersection(Spectrum* spectrum_a_ptr,Spectrum const* const spectrum_b_ptr);

short spectrum_frequency_slot_allocate(Spectrum *spectrum_ptr, Frequency_Slot const * const slot_ptr);
short spectrum_frequency_slot_exact_release(Spectrum *spectrum_ptr, Frequency_Slot const * const slot_ptr);
short spectrum_frequency_slot_release(Spectrum *spectrum_ptr, Frequency_Slot const * const slot_ptr);
Boolean spectrum_frequency_slot_is_available(Spectrum const* const spectrum_ptr, Frequency_Slot const * const slot_ptr);

double spectrum_available_amount_get(Spectrum const* const spectrum_ptr);//returns bandwidth in GHz
PrgT_List* spectrum_bandwidth_allocable_indexes_get(Spectrum const* const spectrum_ptr, double bandwidth_demand, Frequency_Index const* const fi_sample_ptr);
int spectrum_bandwidth_allocable_indexes_number_get(Spectrum const* const spectrum_ptr, double bandwidth_demand, Frequency_Index const* const fi_sample_ptr, Boolean non_overlapping);

void spectrum_print(Spectrum const* const spectrum_ptr);


Spectrum_Map* spectrum_map_create(int lower_frequency_index, int upper_frequency_index);
Spectrum_Map* spectrum_map_copy(Spectrum_Map const* spectrum_map_ptr);
void spectrum_map_destroy(Spectrum_Map* spectrum_map_ptr);
int spectrum_map_lower_frequency_index_get(Spectrum_Map const* spectrum_map_ptr);
int spectrum_map_upper_frequency_index_get(Spectrum_Map const* spectrum_map_ptr);
void spectrum_map_fully_busy_set(Spectrum_Map* spectrum_map_ptr);
MD_Compcode spectrum_map_busy_range_set(Spectrum_Map* spectrum_map_ptr,int lower_freq_index,int higher_freq_index);
MD_Compcode spectrum_map_free_range_set(Spectrum_Map* spectrum_map_ptr,int lower_freq_index,int higher_freq_index);
MD_Compcode spectrum_map_free_set(Spectrum_Map* spectrum_map_ptr,int freq_index);
MD_Compcode spectrum_map_busy_set(Spectrum_Map* spectrum_map_ptr,int freq_index);
MD_Compcode spectrum_maps_sum(Spectrum_Map** spectrum_map_a_pptr, Spectrum_Map* spectrum_map_b_ptr);
MD_Compcode spectrum_maps_union(Spectrum_Map** spectrum_map_a_pptr, Spectrum_Map* spectrum_map_b_ptr);
MD_Compcode spectrum_map_first_free_frequency_get(Spectrum_Map const* spectrum_map_ptr, int* freq_index_ptr);

// returns the number of free indexes
OpT_uInt32 spectrum_map_free_num(Spectrum_Map const* spectrum_map_ptr);

// returns the number of free indexes spaced at least by min_spacing
unsigned int spectrum_map_free_index_min_spacing_num(Spectrum_Map const* spectrum_map_ptr, unsigned int min_spacing);

Boolean spectrum_is_frequency_index_free(Spectrum_Map const* spectrum_map_ptr,
                                         int freq_index);

void spectrum_map_print(Spectrum_Map const* spectrum_map_ptr);
#endif
