#ifndef	_MD_RSVP_MSG_UTIL_H_INCLUDED_
	#define _MD_RSVP_MSG_UTIL_H_INCLUDED_
	
    #include    <ip_addr_v4.h>
    #include 	<opnet.h>
    #include	"MD_rsvp.h"

    /* Define RSVP protocol structures 		*/
    // RSVP HOP structure that store the IP address 
    // of the node from where the RSVP message comes from
    typedef struct Rsvp_Hop{ 
			InetT_Address  ip_addr; //IP Next/Previous Hop Address
			//LIH Logical Interface Handle
            int length;//12 bytes (4header+8)
	} Rsvp_Hop;
    
	/*structure of Sob-Objects contained into the ERO object*/
	typedef struct ERO_IPv4_subobject{
			int loose;//if loose L=1 else strict
			//int type;
			int length; // 8 bytes
			InetT_Address ipv4_addr;
			int prefix;
			//int padding;
	} ERO_IPv4_subobject;
		
	/*ERO object structure*/
	typedef struct ERO_object{
			int length; //4bytes header + the length of all the subobjects
			//int class_num;
			//int c_type;
			PrgT_List* subobjects;	//list of ERO_IPv4_subobject
	} ERO_object;
		
    typedef struct RRO_IPv4_subobject{
			//int type;
			//int length;
			InetT_Address ipv4_addr;
			int prefix;
			//int flags;
	} RRO_IPv4_subobject;
    
    typedef struct RRO_object{
			//int length;
			//int class_num;
			//int c_type;
			PrgT_List* subobjects;	//list of RRO_IPv4_subobject
	} RRO_object;
    
	typedef struct Session_object{ 
			InetT_Address 	source_addr;
			InetT_Address	destination_addr;
			OpT_uInt32	tunnel_id;
			OpT_uInt32  sub_lsp_id;
            int         length; //(4header+20 = 24 bytes)
	} Session_object;
	
	typedef struct Label_set_object{
			PrgT_Vector* label_vector;
	} Label_set_object;
		
	typedef struct Sender_tspec_object{
			int number_of_slices;			/*	number of slice of BW required for the LSP (flexible grid). */
													/*	the slot width is given by number_of_slices * 12.5 GHz		*/
            int length; //4 bytes+2= 6bytes (not sure)
	} Sender_tspec_object;
		
	typedef struct Generalized_label_object{
			//unsigned short int grid=1; //(ITU-T DWDM)
			//unsigned short int channel_spacing=5; //6.25 GHz
			int central_freq_indx;					/* the central frequency is obtained as 						*/
													/* central_freq=(193.1 + central_freq_indx * 0.00625) THz		*/
	} Generalized_label_object;
	
	typedef struct Suggested_label_object{
            int central_freq_indx;
	} Suggested_label_object;
		
	/* Error specification object, used by the error messages (path err msg, resv err msg) */
	typedef struct IF_ID_Error_spec_object{ //http://tools.ietf.org/html/rfc3473#page-9    http://tools.ietf.org/html/rfc4920#section-6.2
		InetT_Address 	error_node_addr;  /* address of the node who detect the error */
        //Flags
        //Error Code
        //Error Value
        // interface_addr /*IP address of the interface failed*/
        // incoming_addr  /*IP addr of the interface of the other node */
        InetT_Address  prev_hop_addr;  /*IP address of the previous node*/
	} IF_ID_Error_spec_object;
	
    
	/* contains the objects that will be inserted into the RSVP packet */
	typedef struct Object_list{
    Rsvp_Hop  *rsvp_hop;
    ERO_object  *ero;
    RRO_object  *rro;
    Session_object  *session;
    Label_set_object  *label_set;
    Sender_tspec_object  *sender_tspec;
    Generalized_label_object  *generalized_label;
    Suggested_label_object  *suggested_label;
    IF_ID_Error_spec_object *if_id_error_spec;
    OpT_uInt32  *suggested_tx_transponder;
    OpT_uInt32  *suggested_rx_transponder;
    PrgT_Vector *carriers_vptr;
    PrgT_Vector       *egress_tx_carrier_ids_vptr; // list of carrier ids to be used at the TX egress (not in rfc)
    PrgT_Vector       *egress_rx_carrier_ids_vptr; // list of carrier ids to be used at the RX egress (not in rfc)
	} Object_list;
        
        
	/*	Function Prototypes					*/
    Rsvp_Hop*                   rsvp_hop_copy(Rsvp_Hop* rsvp_hop);
    ERO_IPv4_subobject*         rsvp_ero_ipv4_subobject_copy( ERO_IPv4_subobject* ero_subobj);
	ERO_object*                 ERO_object_copy(ERO_object* ero);
    ERO_object*                 rsvp_rro_to_ero(RRO_object* rro_ptr);
    void                        rsvp_ero_object_print(ERO_object const* const ero_ptr);
    RRO_IPv4_subobject*         rsvp_rro_ipv4_subobject_copy( RRO_IPv4_subobject* rro_subobj);
    RRO_object*                 RRO_object_copy(RRO_object* rro);
	Session_object*             session_object_copy(Session_object const * session);
    void                        rsvp_session_object_print(Session_object const *const session_ptr);
	Boolean						session_object_equal(Session_object const *const session1_ptr,Session_object const *const session2_ptr);
	void *                      label_set_vector_copy(void * label_ptr);
	Label_set_object*           label_set_object_copy (Label_set_object* label_set);
	Sender_tspec_object*        sender_tspec_object_copy(Sender_tspec_object* sender_tspec);
	Generalized_label_object*   generalized_label_object_copy (Generalized_label_object* generalized_label);
	Suggested_label_object*     suggested_label_object_copy (Suggested_label_object* suggested_label);
	void 						rsvp_hop_destroy(Rsvp_Hop* rsvp_hop);
    void                        rsvp_ero_ipv4_subobject_destroy( ERO_IPv4_subobject* ero_subobj);
    void 						ERO_object_destroy(ERO_object* ero);
    void                        rsvp_rro_ipv4_subobject_destroy( RRO_IPv4_subobject* rro_subobj);
    void                        RRO_object_destroy(RRO_object* rro);
	void 						session_object_destroy(Session_object* session);
	void 						label_set_object_destroy(Label_set_object* label_set);
    PrgT_Compcode		        label_set_vector_destroy(void* elem);
	void 						sender_tspec_object_destroy(Sender_tspec_object* sender_tspec);
	void 						generalized_label_object_destroy(Generalized_label_object* generalized_label);
	void 						suggested_label_object_destroy(Suggested_label_object* suggested_label);
	void 						rsvp_object_list_destroy (Object_list* obj_list_ptr);
	Object_list* 		        rsvp_object_list_copy (Object_list* obj_list_ptr);
	void						initialize_object_list (Object_list* obj_list_ptr);
  
    void                        rsvp_rro_add_node(RRO_object* rro, InetT_Address addr);
    void                        rsvp_rro_subobject_add(RRO_object* rro, RRO_IPv4_subobject *subobject);
    void                        rsvp_ero_subobject_add(ERO_object* ero, ERO_IPv4_subobject *subobject);
    ERO_object*                 rsvp_ero_object_create();
    ERO_IPv4_subobject*         rsvp_ero_ipv4_subobject_create( InetT_Address  ip_addr);
    
    int                         rsvp_ero_object_compare(ERO_object* ero1, ERO_object* ero2);
	/* RSVP MESSAGES */
    //void                        rsvp_pkt_destroy(Packet* rsvp_pkptr);
	Packet* 					create_rsvp_msg(Object_list* obj_list_ptr, int msg_type);
	Packet* 					create_path_error(Session_object* session_ptr,IF_ID_Error_spec_object* if_id_error_spec_ptr);
	Packet* 					create_resv_error(Session_object* session_ptr);
	Packet* 					create_path_tear(Session_object* session_ptr);
	Packet* 					create_resv_msg(Object_list* obj_list_ptr);
    Packet*                     create_notify_msg(Session_object* session_ptr, IF_ID_Error_spec_object* if_id_error_spec_ptr);
	
  /* GETTER SETTER */
  int rsvp_pkt_type_access(Packet* rsvp_pkptr);
#endif
