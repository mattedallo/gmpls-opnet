#include	"MD_rsvp_msg_util.h"
#include	"MD_rsvp.h"

/*____________________________OBJECT MANIPULATION FUNCTIONS__________________*/
void rsvp_rro_add_node(RRO_object* rro, InetT_Address addr) {
  RRO_IPv4_subobject  *rro_subobj;
  
  FIN (rsvp_rro_add_node(rro,addr));
  if (rro != OPC_NIL) {
    rro_subobj = (RRO_IPv4_subobject*) prg_mem_alloc(sizeof(RRO_IPv4_subobject));
    
    // copy the node address into the rro subobject
    rro_subobj->ipv4_addr = inet_address_copy( addr );
    // consider the netmask prefix always as 32 // //TODO
    rro_subobj->prefix = 32;
    
    if (rro->subobjects == PRGC_NIL) {
        // create the list of nodes (hops/subobjects) //
        rro->subobjects = prg_list_create ();
    }
    
    // insert the subobject into the RRO object //
    prg_list_insert (rro->subobjects, rro_subobj, PRGC_LISTPOS_TAIL);
  }
  FOUT;
}

void rsvp_rro_subobject_add(RRO_object* rro, RRO_IPv4_subobject *subobject) {
// this function append an rro ipv4 subobject to the rro object 
	FIN( rsvp_rro_subobject_add(rro, subobject) );
	//check if the list of ero subobjects exists, if not create
	if ( OPC_NIL == rro->subobjects) rro->subobjects = prg_list_create();
	// insert the subobject into the ERO object //
	prg_list_insert (rro->subobjects, subobject, PRGC_LISTPOS_TAIL);

	FOUT;
}

void rsvp_ero_subobject_add(ERO_object* ero, ERO_IPv4_subobject *subobject) {
// this function append an ipv4 subobject to the ero object 
// and updates the ero object total length
	FIN( rsvp_ero_subobject_add(ero, subobject) );
	//check if the list of ero subobjects exists, if not create
	if ( OPC_NIL == ero->subobjects) 
		ero->subobjects = prg_list_create();
	// insert the subobject into the ERO object //
	prg_list_insert (ero->subobjects, subobject, PRGC_LISTPOS_TAIL);
	//update the total length of the ero object
	ero->length += subobject->length;
	FOUT;
}


int rsvp_ero_object_compare(ERO_object* ero1, ERO_object* ero2) {
// Function used to compare two ERO objects
// Returns -1, if they are different
// Returns  0, if they are equal
	PrgT_List_Cell 		*cell1_ptr,*cell2_ptr;
	int					size;
	ERO_IPv4_subobject	*subobj1_ptr,*subobj2_ptr;
	
	FIN( rsvp_ero_object_compare(ero1, ero2) );
	if (OPC_NIL == ero1 || OPC_NIL == ero2 ||
		OPC_NIL == ero1->subobjects || OPC_NIL == ero2->subobjects ||
		( size = prg_list_size (ero1->subobjects) ) != prg_list_size (ero2->subobjects) )
			FRET(-1);
	
	if ( size == 0 )
			FRET(0);
	
	cell1_ptr = prg_list_head_cell_get (ero1->subobjects);
	cell2_ptr = prg_list_head_cell_get (ero2->subobjects);
	
	while (cell1_ptr != PRGC_NIL) {
		subobj1_ptr = (ERO_IPv4_subobject *) prg_list_cell_data_get (cell1_ptr);
		subobj2_ptr = (ERO_IPv4_subobject *) prg_list_cell_data_get (cell2_ptr);
		// check if the two addresses are equal
		if ( PRGC_FALSE == inet_address_equal(subobj1_ptr->ipv4_addr, subobj1_ptr->ipv4_addr) ) 
			FRET(-1);
		cell1_ptr = prg_list_cell_next_get(cell1_ptr);
		cell2_ptr = prg_list_cell_next_get(cell2_ptr);
	}
	
	FRET(0);
}


void rsvp_ero_object_print(ERO_object const* const ero_ptr) {
	PrgT_List_Cell		*cell_ptr;
	char				node_addr_str[INETC_ADDR_STR_LEN];
	ERO_IPv4_subobject	*ero_subobj_ptr;
	
	FIN( rsvp_ero_object_print(ero_ptr) );
	if (OPC_NIL == ero_ptr) FOUT;
	printf("[");
	cell_ptr = prg_list_head_cell_get(ero_ptr->subobjects);
	while ( PRGC_NIL != cell_ptr ) {
    	ero_subobj_ptr = prg_list_cell_data_get(cell_ptr);
		inet_address_print(node_addr_str, ero_subobj_ptr->ipv4_addr);
		printf("%s,",node_addr_str);
		cell_ptr = prg_list_cell_next_get(cell_ptr);
	}
	printf("]\n");
	FOUT;
}

/*____________________________OBJECT CREATE FUNCTIONS_______________________*/
ERO_IPv4_subobject* rsvp_ero_ipv4_subobject_create( InetT_Address  ip_addr) {
// This function creates a new ipv4 subobject given the ip address
	ERO_IPv4_subobject *ero_subobj;
	
	FIN( rsvp_ero_ipv4_subobject_create(ip_addr) );
	// instantiate IPv4_subobject //
	ero_subobj = (ERO_IPv4_subobject*) prg_mem_alloc(sizeof(ERO_IPv4_subobject));
	// copy the ip address into the subobject//
	ero_subobj->ipv4_addr = inet_address_copy( ip_addr );
	// consider the netmask prefix always as 32 // //TODO
	ero_subobj->prefix = 32;
	// consider the hop as always strict // //TODO
	ero_subobj->loose = 0;
	// set the length of the ipv4 subobject (8 bytes)
	ero_subobj->length = 8;
	FRET(ero_subobj);
}


ERO_object* rsvp_ero_object_create() {
// This function creates a new ero object
	ERO_object *ero_obj;
	
	FIN( rsvp_ero_object_create() );
	// allocate the ERO_object
	ero_obj = (ERO_object*) prg_mem_alloc(sizeof(ERO_object));
	// initialize the list of subobjects to NULL
	ero_obj->subobjects = OPC_NIL;
	// the length of the ero object starts with just the header size (4 bytes)
	ero_obj->length = 4;
	FRET(ero_obj);
}

ERO_object* rsvp_rro_to_ero(RRO_object* rro_ptr) {
	ERO_object	*ero_ptr;
	RRO_IPv4_subobject *rro_subobj_ptr;
	ERO_IPv4_subobject *ero_subobj_ptr;
	PrgT_List_Cell *cell_ptr;
	
	FIN( rsvp_rro_to_ero(rro_ptr) );
	if (OPC_NIL == rro_ptr) FRET(OPC_NIL);
	ero_ptr = rsvp_ero_object_create();

	cell_ptr = prg_list_head_cell_get(rro_ptr->subobjects);
	while ( PRGC_NIL != cell_ptr ) {
    	rro_subobj_ptr = prg_list_cell_data_get(cell_ptr);
		ero_subobj_ptr = rsvp_ero_ipv4_subobject_create(rro_subobj_ptr->ipv4_addr);
		rsvp_ero_subobject_add(ero_ptr,ero_subobj_ptr);
		cell_ptr = prg_list_cell_next_get(cell_ptr);
	}
	FRET(ero_ptr);
}

/*____________________________OBJECT COPY FUNCTIONS__________________________*/

Rsvp_Hop* rsvp_hop_copy(Rsvp_Hop* rsvp_hop) {
	Rsvp_Hop*	rsvp_hop_copy;
	
	FIN (rsvp_hop_copy(rsvp_hop));
	
	if (rsvp_hop == OPC_NIL) FRET(OPC_NIL);
	rsvp_hop_copy = (Rsvp_Hop*) prg_mem_alloc(sizeof(Rsvp_Hop));
	rsvp_hop_copy->length = rsvp_hop->length;
	rsvp_hop_copy->ip_addr = inet_address_copy( rsvp_hop->ip_addr );
	
	FRET (rsvp_hop_copy);
}

ERO_IPv4_subobject* rsvp_ero_ipv4_subobject_copy( ERO_IPv4_subobject* ero_subobj) {
// This function copies the ero ipv4 subobject
	ERO_IPv4_subobject *ero_subobj_copy;
	
	FIN( rsvp_ero_ipv4_subobject_copy(ero_subobj) );
	/*	copy all datatypes that are not pointers	*/
	ero_subobj_copy = prg_mem_copy_create (ero_subobj, sizeof(ERO_IPv4_subobject));
	// copy the ip address into the subobject//
	ero_subobj_copy->ipv4_addr = inet_address_copy( ero_subobj->ipv4_addr );
	FRET(ero_subobj_copy);
}

ERO_object* ERO_object_copy(ERO_object* ero) {
	ERO_object*	ero_copy;
	ERO_IPv4_subobject* ero_subobj_copy;
	PrgT_List_Cell const * cell_ptr;
	
	/** This function is used to create a copy of an ERO object			 	**/
	/** because the op_prg_mem_copy() cannot perform a copy of the 			**/
	/** ERO subobjects, it copies only the reference address				**/
	FIN (ERO_object_copy(ero));
	
	if (ero == OPC_NIL) FRET(OPC_NIL);
	/*	copy all datatypes that are not pointers	*/
	ero_copy= prg_mem_copy_create (ero,sizeof(ERO_object));
	
	ero_copy->subobjects=prg_list_create ();
	
	cell_ptr = prg_list_head_cell_get (ero->subobjects);
	while (cell_ptr != PRGC_NIL) {
		ero_subobj_copy = rsvp_ero_ipv4_subobject_copy(prg_list_cell_data_get(cell_ptr));
		rsvp_ero_subobject_add( ero_copy, ero_subobj_copy);
		cell_ptr = prg_list_cell_next_get(cell_ptr);
	}
	
	FRET (ero_copy);
}


RRO_IPv4_subobject* rsvp_rro_ipv4_subobject_copy( RRO_IPv4_subobject* rro_subobj) {
// This function copies the rro ipv4 subobject
	RRO_IPv4_subobject *rro_subobj_copy;
	
	FIN( rsvp_rro_ipv4_subobject_copy(ip_addr) );
	/*	copy all datatypes that are not pointers	*/
	rro_subobj_copy = prg_mem_copy_create (rro_subobj, sizeof(RRO_IPv4_subobject));
	// copy the ip address into the subobject//
	rro_subobj_copy->ipv4_addr = inet_address_copy( rro_subobj->ipv4_addr );
	FRET(rro_subobj_copy);
}


RRO_object* RRO_object_copy(RRO_object* rro) {
	RRO_object*	rro_copy;
	RRO_IPv4_subobject *rro_subobj_copy;
	PrgT_List_Cell const * cell_ptr;
	
	FIN (RRO_object_copy(rro));
	
	if (rro == OPC_NIL) FRET(OPC_NIL);
	/* instantiate memory for rro_obj */
	rro_copy = (RRO_object*) prg_mem_alloc(sizeof(RRO_object));
	/*	copy all datatypes that are not pointers	*/
	prg_mem_copy (rro, rro_copy, sizeof(RRO_object));

	/* copy all data that are pointers */
	rro_copy->subobjects=prg_list_create ();
	cell_ptr = prg_list_head_cell_get (rro->subobjects);
	while (cell_ptr != PRGC_NIL) {
		rro_subobj_copy = rsvp_rro_ipv4_subobject_copy(prg_list_cell_data_get(cell_ptr));
		rsvp_rro_subobject_add( rro_copy, rro_subobj_copy );
		cell_ptr = prg_list_cell_next_get(cell_ptr);
	}
	
	FRET (rro_copy);
}

Session_object* session_object_copy(Session_object const* session) {
	Session_object*	session_copy;
	
	FIN (session_object_copy(session));
	
	if (session == OPC_NIL) FRET(OPC_NIL);
	session_copy = (Session_object*) prg_mem_alloc(sizeof(Session_object));
	session_copy->source_addr = inet_address_copy(session->source_addr);
	session_copy->destination_addr = inet_address_copy(session->destination_addr);
	session_copy->length = session->length;
	session_copy->tunnel_id = session->tunnel_id;
	session_copy->sub_lsp_id = session->sub_lsp_id;
	FRET (session_copy);
}

void* label_set_vector_copy(void * label_ptr)	{
	int*	new_label_ptr;
	
	FIN (label_set_vector_copy(label_ptr));
	
	if (label_ptr == OPC_NIL) FRET(OPC_NIL);
	new_label_ptr=(int*)prg_mem_alloc(sizeof(int));
	*new_label_ptr=*((int*)label_ptr);
	
	FRET (new_label_ptr);
}

Label_set_object* label_set_object_copy (Label_set_object* label_set) {
	Label_set_object*	label_set_copy;
	
	FIN (label_set_object_copy(label_set));
	
	if (label_set == OPC_NIL) FRET(OPC_NIL);
	label_set_copy = (Label_set_object*) prg_mem_alloc(sizeof(Label_set_object));
	label_set_copy->label_vector=prg_vector_copy (label_set->label_vector);

	FRET (label_set_copy);
}

Sender_tspec_object* sender_tspec_object_copy(Sender_tspec_object* sender_tspec) {
	Sender_tspec_object* sender_tspec_copy;
	
	FIN (sender_tspec_object_copy(sender_tspec));
	
	if (sender_tspec == OPC_NIL) FRET(OPC_NIL);
	sender_tspec_copy = prg_mem_copy_create(sender_tspec,sizeof(Sender_tspec_object));	
	FRET (sender_tspec_copy);
}

Generalized_label_object* generalized_label_object_copy (Generalized_label_object* generalized_label) {
	Generalized_label_object* generalized_label_copy;
	
	FIN (generalized_label_object_copy(generalized_label));
	
	if (generalized_label == OPC_NIL) FRET(OPC_NIL);
	generalized_label_copy = (Generalized_label_object*) prg_mem_alloc(sizeof(Generalized_label_object));
	prg_mem_copy (generalized_label, generalized_label_copy, sizeof(Generalized_label_object));
	
	FRET (generalized_label_copy);
}

Suggested_label_object* suggested_label_object_copy (Suggested_label_object* suggested_label) {
	Suggested_label_object* suggested_label_copy;
	
	FIN (suggested_label_object_copy(suggested_label));
	
	if (suggested_label == OPC_NIL) FRET(OPC_NIL);
	suggested_label_copy = (Suggested_label_object*) prg_mem_alloc(sizeof(Suggested_label_object));
	suggested_label_copy->central_freq_indx = suggested_label->central_freq_indx;
	
	FRET (suggested_label_copy);
}

IF_ID_Error_spec_object* if_id_error_spec_object_copy(IF_ID_Error_spec_object* if_id_error_spec) {
	IF_ID_Error_spec_object* if_id_error_spec_copy;
	
	FIN (if_id_error_spec_object_copy(if_id_error_spec));
	
	if (if_id_error_spec == OPC_NIL) FRET(OPC_NIL);
	if_id_error_spec_copy = (IF_ID_Error_spec_object*) prg_mem_alloc(sizeof(IF_ID_Error_spec_object));
	if_id_error_spec_copy->error_node_addr = inet_address_copy(if_id_error_spec->error_node_addr);
	if_id_error_spec_copy->prev_hop_addr = inet_address_copy(if_id_error_spec->prev_hop_addr);
	
	FRET (if_id_error_spec_copy);
}

/*----------------------------OBJECT DESTROY FUNCTIONS------------------------*/
void rsvp_hop_destroy(Rsvp_Hop* rsvp_hop) {
	FIN (rsvp_hop_destroy(rsvp_hop));
	
	if (rsvp_hop == OPC_NIL) FOUT;
	inet_address_destroy( rsvp_hop->ip_addr );
	prg_mem_free (rsvp_hop);

	FOUT;
}

void rsvp_ero_ipv4_subobject_destroy( ERO_IPv4_subobject* ero_subobj) {
	FIN (rsvp_ero_ipv4_subobject_destroy(ero_subobj));
	if (ero_subobj == OPC_NIL) FOUT;
	inet_address_destroy(ero_subobj->ipv4_addr);
	prg_mem_free(ero_subobj);
	FOUT;
}

void ERO_object_destroy(ERO_object* ero) {
	FIN (ERO_object_destroy(ero));
	
	if (ero == OPC_NIL) FOUT;
	if (ero->subobjects != OPC_NIL) {
		prg_list_clear (ero->subobjects, rsvp_ero_ipv4_subobject_destroy);
		prg_mem_free(ero->subobjects);
	}
	prg_mem_free (ero);

	FOUT;
}

void rsvp_rro_ipv4_subobject_destroy( RRO_IPv4_subobject* rro_subobj) {
	FIN (rsvp_rro_ipv4_subobject_destroy(rro_subobj));
	if (rro_subobj == OPC_NIL) FOUT;
	inet_address_destroy(rro_subobj->ipv4_addr);
	prg_mem_free(rro_subobj);
	FOUT;
}

void RRO_object_destroy(RRO_object* rro) {
	FIN (RRO_object_destroy(rro));
	
	if (rro == OPC_NIL) FOUT;
	if (rro->subobjects != OPC_NIL) {
		prg_list_clear (rro->subobjects, rsvp_rro_ipv4_subobject_destroy);
		prg_mem_free (rro->subobjects);
	}
	prg_mem_free (rro);

	FOUT;
}

void session_object_destroy(Session_object* session) {
	FIN (session_object_destroy(session));
	
	if (session == OPC_NIL) FOUT;
	inet_address_destroy( session->source_addr );
	inet_address_destroy( session->destination_addr );
	prg_mem_free (session);

	FOUT;
}

PrgT_Compcode label_set_vector_destroy(void * elem)	{
	int*	freq_indx;
	
	FIN (label_set_vector_destroy(elem));
	freq_indx = (int *) elem;
	
	prg_mem_free(freq_indx);
	
	FRET (PrgC_Compcode_Success);
}


void label_set_object_destroy(Label_set_object* label_set) {
	FIN (label_set_object_destroy(label_set));
	/* Free label set vector than free label set object memory */
	if (label_set == OPC_NIL) FOUT;
	prg_vector_destroy (label_set->label_vector, OPC_TRUE);
	prg_mem_free(label_set);
	FOUT;
}

void sender_tspec_object_destroy(Sender_tspec_object* sender_tspec) {
	FIN (sender_tspec_object_destroy(sender_tspec));
	
	if (sender_tspec == OPC_NIL) FOUT;
	prg_mem_free (sender_tspec);

	FOUT;
}

void generalized_label_object_destroy(Generalized_label_object* generalized_label) {
	FIN (generalized_label_object_destroy(generalized_label));
	
	if (generalized_label == OPC_NIL) FOUT;
	prg_mem_free (generalized_label);

	FOUT;
}

void suggested_label_object_destroy(Suggested_label_object* suggested_label) {
	FIN (suggested_label_object_destroy(suggested_label));
	
	if (suggested_label == OPC_NIL) FOUT;
	prg_mem_free (suggested_label);

	FOUT;
}

void if_id_error_spec_object_destroy(IF_ID_Error_spec_object* if_id_error_spec) {
	FIN (if_id_error_spec_object_destroy(if_id_error_spec));
	
	if (if_id_error_spec == OPC_NIL) FOUT;
	inet_address_destroy( if_id_error_spec->error_node_addr );
	inet_address_destroy( if_id_error_spec->prev_hop_addr );
	prg_mem_free (if_id_error_spec);

	FOUT;
}


Boolean	session_object_equal(Session_object const *const session1_ptr,Session_object const *const session2_ptr) {
	FIN( session_object_equal( session1_ptr, session2_ptr) );
	if (PRGC_NIL == session1_ptr || PRGC_NIL == session2_ptr) FRET(OPC_FALSE);
	if ( inet_address_equal(session1_ptr->source_addr,session2_ptr->source_addr) &&
		 inet_address_equal(session1_ptr->destination_addr,session2_ptr->destination_addr) &&
		 session1_ptr->tunnel_id == session2_ptr->tunnel_id &&
		 session1_ptr->sub_lsp_id == session2_ptr->sub_lsp_id ) {
			FRET(OPC_TRUE);
	}
	FRET(OPC_FALSE);
}

void rsvp_session_object_print(Session_object const *const session_ptr) {
	char	addr_str1[INETC_ADDR_STR_LEN];
	char	addr_str2[INETC_ADDR_STR_LEN];
	
	FIN(rsvp_session_object_print(session_ptr));
	inet_address_print(addr_str1, session_ptr->source_addr);
	inet_address_print(addr_str2, session_ptr->destination_addr);
	printf("LSP Source:%s, Dest:%s, tunnel:%d, sub-lsp:%d",addr_str1,addr_str2,session_ptr->tunnel_id,session_ptr->sub_lsp_id);
	FOUT;
}

/*--------------------------------------------------------------------------*/
void rsvp_object_list_destroy (Object_list* obj_list_ptr) {
	FIN(rsvp_object_list_destroy (obj_list_ptr));
	
	if (obj_list_ptr == OPC_NIL) FOUT;
	
	if (obj_list_ptr->rsvp_hop!=OPC_NIL) {
		rsvp_hop_destroy (obj_list_ptr->rsvp_hop);
	}
	
	if (obj_list_ptr->ero!=OPC_NIL) {
		ERO_object_destroy (obj_list_ptr->ero);
	}
	
	if (obj_list_ptr->rro!=OPC_NIL) {
		RRO_object_destroy (obj_list_ptr->rro);
	}
	
	if (obj_list_ptr->session!=OPC_NIL) {
		session_object_destroy (obj_list_ptr->session);
	}
	
	if (obj_list_ptr->label_set!=OPC_NIL) {
		label_set_object_destroy (obj_list_ptr->label_set);
	}
	
	if (obj_list_ptr->sender_tspec!=OPC_NIL) {
		sender_tspec_object_destroy (obj_list_ptr->sender_tspec);
	}
	
	if (obj_list_ptr->generalized_label!=OPC_NIL) {
		generalized_label_object_destroy (obj_list_ptr->generalized_label);
	}
	
	if (obj_list_ptr->suggested_label!=OPC_NIL) {
		suggested_label_object_destroy (obj_list_ptr->suggested_label);
	}
	
	if (obj_list_ptr->if_id_error_spec!=OPC_NIL) {
		if_id_error_spec_object_destroy (obj_list_ptr->if_id_error_spec);
	}
	
	if (obj_list_ptr->suggested_tx_transponder!=OPC_NIL) {
		prg_mem_free(obj_list_ptr->suggested_tx_transponder);
	}
	
	if (obj_list_ptr->suggested_rx_transponder!=OPC_NIL) {
		prg_mem_free(obj_list_ptr->suggested_rx_transponder);
	}
	
	if (obj_list_ptr->carriers_vptr!=OPC_NIL) {
		prg_vector_destroy(obj_list_ptr->carriers_vptr,PRGC_TRUE);
	}

	if (obj_list_ptr->egress_tx_carrier_ids_vptr!=OPC_NIL) {
		prg_vector_destroy(obj_list_ptr->egress_tx_carrier_ids_vptr,PRGC_TRUE);
	}

	if (obj_list_ptr->egress_rx_carrier_ids_vptr!=OPC_NIL) {
		prg_vector_destroy(obj_list_ptr->egress_rx_carrier_ids_vptr,PRGC_TRUE);
	}	
	
	prg_mem_free(obj_list_ptr);
	FOUT;
}

/*---------------------------------------------------------------------------*/
Object_list* rsvp_object_list_copy (Object_list* obj_list_ptr) {
	Object_list* new_obj_list_ptr=OPC_NIL;
	
	FIN(rsvp_object_list_copy (obj_list_ptr));
	
	new_obj_list_ptr = (Object_list*)prg_mem_alloc(sizeof(Object_list));
	
	if (obj_list_ptr->rsvp_hop!=OPC_NIL) {
		new_obj_list_ptr->rsvp_hop = rsvp_hop_copy(obj_list_ptr->rsvp_hop);
	}
	else {
		new_obj_list_ptr->rsvp_hop = OPC_NIL;
	}
	
	if (obj_list_ptr->ero!=OPC_NIL) {
		new_obj_list_ptr->ero = ERO_object_copy(obj_list_ptr->ero);
	}
	else {
		new_obj_list_ptr->ero = OPC_NIL;
	}
	
	if (obj_list_ptr->rro!=OPC_NIL) {
		new_obj_list_ptr->rro = RRO_object_copy(obj_list_ptr->rro);
	}
	else {
		new_obj_list_ptr->rro = OPC_NIL;
	}
	
	if (obj_list_ptr->session!=OPC_NIL) {
		new_obj_list_ptr->session = session_object_copy (obj_list_ptr->session);
	}
	else {
		new_obj_list_ptr->session = OPC_NIL;
	}
	
	if (obj_list_ptr->label_set!=OPC_NIL) {
		new_obj_list_ptr->label_set = label_set_object_copy (obj_list_ptr->label_set);
	}
	else {
		new_obj_list_ptr->label_set = OPC_NIL;
	}
	
	if (obj_list_ptr->sender_tspec!=OPC_NIL) {
		new_obj_list_ptr->sender_tspec = sender_tspec_object_copy (obj_list_ptr->sender_tspec);
	}
	else {
		new_obj_list_ptr->sender_tspec = OPC_NIL;
	}
	
	if (obj_list_ptr->generalized_label!=OPC_NIL) {
		new_obj_list_ptr->generalized_label = generalized_label_object_copy (obj_list_ptr->generalized_label);
	}
	else {
		new_obj_list_ptr->generalized_label = OPC_NIL;
	}
	
	if (obj_list_ptr->suggested_label!=OPC_NIL) {
		new_obj_list_ptr->suggested_label = suggested_label_object_copy (obj_list_ptr->suggested_label);
	}
	else {
		new_obj_list_ptr->suggested_label = OPC_NIL;
	}
	
	if (obj_list_ptr->if_id_error_spec!=OPC_NIL) {
		new_obj_list_ptr->if_id_error_spec = if_id_error_spec_object_copy (obj_list_ptr->if_id_error_spec);
	}
	else {
		new_obj_list_ptr->if_id_error_spec = OPC_NIL;
	}
	
	if (obj_list_ptr->suggested_rx_transponder!=OPC_NIL) {
		new_obj_list_ptr->suggested_rx_transponder = prg_mem_copy_create(obj_list_ptr->suggested_rx_transponder,sizeof(OpT_uInt32));
	}
	else {
		new_obj_list_ptr->suggested_rx_transponder = OPC_NIL;
	}
	
	if (obj_list_ptr->suggested_tx_transponder!=OPC_NIL) {
		new_obj_list_ptr->suggested_tx_transponder = prg_mem_copy_create(obj_list_ptr->suggested_tx_transponder,sizeof(OpT_uInt32));
	}
	else {
		new_obj_list_ptr->suggested_tx_transponder = OPC_NIL;
	}
	
	if (obj_list_ptr->carriers_vptr != OPC_NIL) {
		new_obj_list_ptr->carriers_vptr = prg_vector_copy(obj_list_ptr->carriers_vptr);
	}
	else {
		new_obj_list_ptr->carriers_vptr = OPC_NIL;
	}

	if (obj_list_ptr->egress_tx_carrier_ids_vptr!=OPC_NIL) {
		new_obj_list_ptr->egress_tx_carrier_ids_vptr = prg_vector_copy(obj_list_ptr->egress_tx_carrier_ids_vptr);
	} else {
		new_obj_list_ptr->egress_tx_carrier_ids_vptr = OPC_NIL;
	}

	if (obj_list_ptr->egress_rx_carrier_ids_vptr!=OPC_NIL) {
		new_obj_list_ptr->egress_rx_carrier_ids_vptr = prg_vector_copy(obj_list_ptr->egress_rx_carrier_ids_vptr);
	}	else {
		new_obj_list_ptr->egress_rx_carrier_ids_vptr = OPC_NIL;
	}

	FRET(new_obj_list_ptr);
}



/*--------------------------------------------INITIALIZE OBJECT LIST FUNCTION-------------------------------------------*/
void initialize_object_list (Object_list* obj_list_ptr) {
	
	FIN(initialize_object_list (obj_list_ptr));
		
		obj_list_ptr->rsvp_hop = OPC_NIL;
		obj_list_ptr->ero = OPC_NIL;
		obj_list_ptr->rro = OPC_NIL;
		obj_list_ptr->session = OPC_NIL;
		obj_list_ptr->label_set = OPC_NIL;
		obj_list_ptr->sender_tspec = OPC_NIL;
		obj_list_ptr->generalized_label = OPC_NIL;
		obj_list_ptr->suggested_label = OPC_NIL;
		obj_list_ptr->if_id_error_spec = OPC_NIL;
		obj_list_ptr->suggested_rx_transponder = OPC_NIL;
		obj_list_ptr->suggested_tx_transponder = OPC_NIL;
		obj_list_ptr->carriers_vptr = OPC_NIL;
		obj_list_ptr->egress_tx_carrier_ids_vptr = OPC_NIL;
		obj_list_ptr->egress_rx_carrier_ids_vptr = OPC_NIL;
		
	FOUT;
}


/*___________________________________________________________________SEND MESSAGE FUNCTIONS_______________________________________________________________________*/
/*--------------------CREATE RSVP MESSAGE---------------------------*/
Packet* create_rsvp_msg(Object_list* obj_list_ptr, int msg_type) {
	Packet*			rsvp_pkptr;
	
	FIN(create_rsvp_msg(obj_list_ptr, msg_type));
	
	/* create the RSVP packet 											*/
	rsvp_pkptr = op_pk_create_fmt ("MD_RSVP_Packet");
	/* set the message type as PATH ERROR Message						*/
	op_pk_nfd_set (rsvp_pkptr, "Msg Type", msg_type);
	
	/* set the object list field of the packet */
	op_pk_nfd_set (rsvp_pkptr, "Object List", obj_list_ptr, rsvp_object_list_copy,rsvp_object_list_destroy, sizeof (Object_list));
	
	FRET(rsvp_pkptr);
}
	
/*--------------------CREATE PATH ERROR MESSAGE---------------------------*/
Packet* create_path_error(Session_object* session_ptr, IF_ID_Error_spec_object* if_id_error_spec_ptr) {
	Object_list*	obj_list_ptr;
	Packet*			rsvp_pkptr;
	
	FIN(create_path_error(session_ptr));
		
	/* Create and initialize the object list							*/
	obj_list_ptr = (Object_list *) prg_mem_alloc (sizeof (Object_list));
	initialize_object_list( obj_list_ptr );
	
	obj_list_ptr->session = session_ptr;
	obj_list_ptr->if_id_error_spec = if_id_error_spec_ptr;
	
	rsvp_pkptr = create_rsvp_msg(obj_list_ptr, PATH_ERR_MSG);
	
	FRET(rsvp_pkptr);
}

/*--------------------CREATE RESV ERROR MESSAGE---------------------------*/
Packet* create_resv_error(Session_object* session_ptr) {
	Object_list*	obj_list_ptr;
	Packet*			rsvp_pkptr;
	
	FIN(create_resv_error(session_ptr));
		
	/* Create and initialize the object list							*/
	obj_list_ptr = (Object_list *) prg_mem_alloc (sizeof (Object_list));
	initialize_object_list( obj_list_ptr );
	
	obj_list_ptr->session = session_ptr;
	
	rsvp_pkptr = create_rsvp_msg(obj_list_ptr, RESV_ERR_MSG);
	
	FRET(rsvp_pkptr);
}

/*--------------------CREATE PATH TEAR MESSAGE---------------------------*/
Packet* create_path_tear(Session_object* session_ptr) {
	Object_list*	obj_list_ptr;
	Packet*			rsvp_pkptr;
	
	FIN(create_path_tear(session_ptr));
		
	/* Create and initialize the object list							*/
	obj_list_ptr = (Object_list *) prg_mem_alloc (sizeof (Object_list));
	initialize_object_list( obj_list_ptr );
	
	obj_list_ptr->session = session_ptr;

	rsvp_pkptr = create_rsvp_msg(obj_list_ptr, PATH_TEAR_MSG);
	
	FRET(rsvp_pkptr);
}

/*--------------------CREATE RESV MESSAGE---------------------------*/
Packet* create_resv_msg(Object_list* obj_list_ptr) {
	Packet*			rsvp_pkptr;
	
	FIN(create_resv_msg(obj_list_ptr));
		
	rsvp_pkptr = create_rsvp_msg(obj_list_ptr, RESV_MSG);
	
	FRET(rsvp_pkptr);
}

/*-------------------CREATE NOTIFY MESSAGE--------------------------*/
Packet* create_notify_msg(Session_object* session_ptr, IF_ID_Error_spec_object* if_id_error_spec_ptr) {
	Object_list*	obj_list_ptr;
	Packet*			rsvp_pkptr;
	
	FIN(create_notify_msg(session_ptr,if_id_error_spec_ptr));
		
	/* Create and initialize the object list							*/
	obj_list_ptr = (Object_list *) prg_mem_alloc (sizeof (Object_list));
	initialize_object_list( obj_list_ptr );
	
	obj_list_ptr->session = session_ptr;
	obj_list_ptr->if_id_error_spec = if_id_error_spec_ptr;
	
	rsvp_pkptr = create_rsvp_msg(obj_list_ptr, NOTIFY_MSG);
	
	FRET(rsvp_pkptr);
}

/*__________________DESTROY RSVP PACKET______________________*/
/*void rsvp_pkt_destroy(Packet* rsvp_pkptr){
Object_list* obj_list_ptr;
 	FIN(rsvp_pkt_destroy(rsvp_pkptr));
	// strip out the object list //
	if (op_pk_nfd_get (rsvp_pkptr,"Object List", &obj_list_ptr) != OPC_COMPCODE_FAILURE) {
		rsvp_object_list_destroy (obj_list_ptr);
	}
	op_pk_destroy (rsvp_pkptr);
	FOUT;
 }*/


/*------------------RSVP PACKET GETTER--------------------*/
int rsvp_pkt_type_access(Packet* rsvp_pkptr) {
int	msg_type;
	FIN(rsvp_pkt_type_access(Packet* rsvp_pkptr));
	if (op_pk_nfd_access(rsvp_pkptr,"Msg Type", &msg_type)==OPC_COMPCODE_FAILURE) {
		printf("RSVP_MSG_UTIL, rsvp_pkt_type_access: an error occurred on retrieving the mesage type\n");
		FRET(-1)
	}
	FRET(msg_type);
}
