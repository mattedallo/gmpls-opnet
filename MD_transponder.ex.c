#include  <opnet.h>
#include  <prg_vector.h>
#include  "MD_transponder.h"
#include  <math.h>
#include  "MD_spectrum.h"
#include  "MD_frequency_index.h"
#include  "MD_utils.h"


//#define CARRIERS_CAN_OVERLAP

struct Carrier {
  OpT_uInt32 id; // distinguish between carriers on same transponder
  double frequency; // the central frequency in GHz
  double bandwidth; // the bandwidth occupied by the carrier in GHz
  Boolean in_use;
};


static Carrier* carrier_create(OpT_uInt32 id) {
  Carrier *carrier_ptr;
  FIN( carrier_create() );
  carrier_ptr = prg_mem_alloc(sizeof(*carrier_ptr));
  carrier_ptr->frequency = 0.0;
  carrier_ptr->in_use = OPC_FALSE;
  carrier_ptr->bandwidth = 0.0;
  carrier_ptr->id = id;
  FRET(carrier_ptr);
}

static Carrier* carrier_copy(Carrier* carrier_ptr) {
  FIN( carrier_copy(carrier_ptr) );
  FRET(prg_mem_copy_create(carrier_ptr, sizeof(*carrier_ptr)));
}

static void carrier_destroy(Carrier* carrier_ptr) {
  FIN( carrier_destroy(carrier_ptr) );
  prg_mem_free(carrier_ptr);
  FOUT;
}

static MD_Compcode carrier_use(Carrier* carrier_ptr, double frequency, double bandwidth) {
  FIN( carrier_use(carrier_ptr) );
  if (carrier_ptr->in_use) FRET(MD_Compcode_Failure);
  carrier_ptr->in_use = OPC_TRUE;
  carrier_ptr->frequency = frequency;
  carrier_ptr->bandwidth = bandwidth;
  FRET(MD_Compcode_Success);
}

static void carrier_release(Carrier* carrier_ptr) {
  FIN( carrier_release(carrier_ptr) );
  carrier_ptr->in_use = OPC_FALSE;
  FOUT;
}



static Carrier* carriers_vector_find(PrgT_Vector * carriers_vptr, OpT_uInt32 carrier_id) {
  // Return a carrier which has the corresponding id (if it exists)
  unsigned long i,size;
  Carrier *carrier_ptr;
  FIN (carriers_vector_find(carriers_vptr,carrier_id));
  size = prg_vector_size(carriers_vptr);
  for (i=0; i<size; ++i) {
    carrier_ptr = prg_vector_access (carriers_vptr, i);
    if (carrier_ptr->id == carrier_id) {
      FRET(carrier_ptr);
    }
  }
  FRET ( PRGC_NIL );
}

static PrgT_Compcode carriers_vector_destroy(Carrier * carrier_ptr) {
  FIN (carriers_vector_destroy(carrier_ptr));
    carrier_destroy(carrier_ptr);
  FRET (PrgC_Compcode_Success);
}

static void* carriers_vector_copy(Carrier * carrier_ptr)  {
  FIN (carriers_vector_copy(carrier_ptr));  
  FRET ( carrier_copy(carrier_ptr) );
}


PrgT_Compcode carrier_id_destroy(OpT_uInt32 *carrier_id_ptr) {
  FIN( carrier_id_destroy(carrier_id_ptr) );
  prg_mem_free(carrier_id_ptr);
  FRET(PrgC_Compcode_Success);
}

OpT_uInt32* carrier_id_copy(OpT_uInt32 * carrier_id_ptr) {
  FIN(carrier_id_copy());
  FRET(prg_mem_copy_create(carrier_id_ptr, sizeof(*carrier_id_ptr)));
}


PrgT_Vector* carrier_ids_vector_create(unsigned long size) {
  FIN(carrier_ids_vector_create());
  FRET(prg_vector_create(size,carrier_id_destroy,carrier_id_copy));
}


void carrier_ids_vector_print(PrgT_Vector const* carrier_ids_vptr) {
  unsigned long size,i;
  OpT_uInt32 * carrier_id_ptr;
  FIN(carrier_ids_vector_print());
  if (PRGC_NIL == carrier_ids_vptr) FOUT;
  size = prg_vector_size(carrier_ids_vptr);
  printf("[");
  for (i=0; i<size; ++i) {
    carrier_id_ptr = prg_vector_access(carrier_ids_vptr, i);
    printf("%d,",*carrier_id_ptr);
  }
  printf("]");
  FOUT;
}






static int carriers_compare(const void* elem1_ptr,const void* elem2_ptr) {
  Carrier *c1_ptr,*c2_ptr;
  FIN(carriers_compare());
  c1_ptr = (Carrier*)elem1_ptr;
  c2_ptr = (Carrier*)elem2_ptr;
  if (c1_ptr->in_use) {
    if (c2_ptr->in_use) {
      // both carriers are active
      // bring to head of vector the one having lower frequency
      if (c1_ptr->frequency < c2_ptr->frequency) {
        FRET(1);
      } else if (c1_ptr->frequency > c2_ptr->frequency) {
        FRET(-1);
      } else {// FUNDAMENTAL! otherwise invalid access memory in sort algorithm
        FRET(0);
      }
    } else {
      // carrier1 is active, carrier2 is not.
      // bring to the head carrier1.
      FRET(1);
    }
  } else {
    if (c2_ptr->in_use) {
      // carrier1 is not active, carrier2 is active.
      // bring carrier2 closer to the head.
      FRET(-1);
    } else {
      // both carriers are inactive. Order is not important
      FRET(0);
    }
  }
  FRET(0);
}



































//////////////////////////////////////////////////////////////////////////
//                            LASER FUNCTIONS                           //
//////////////////////////////////////////////////////////////////////////

struct Laser {
  OpT_uInt32     id; // distinguish between lasers
  OpT_uInt32     max_num_carriers; //the maximum number of carriers created by the laser
  PrgT_Vector    *carriers_vptr; // vector of carriers
  double         max_spacing; //maximum spacing between carriers (GHz)
  double         min_spacing; //minimum spacing between carriers (GHz)
  double         min_frequency; //minimum frequency supported by the laser
  double         max_frequency; //maximum frequency supported by the laser
  double         lower_frequency; // lower and upper may change depending on the allocated carriers
  double         upper_frequency;
};

static Laser * laser_create( OpT_uInt32 id, int max_num_carriers,
                      double min_spacing, double max_spacing,
                      double min_freq, double max_freq) {
  Laser *laser_ptr;
  FIN( laser_create() );
  laser_ptr = prg_mem_alloc(sizeof(*laser_ptr));
  laser_ptr->carriers_vptr = prg_vector_create(max_num_carriers, carriers_vector_destroy, carriers_vector_copy);
  laser_ptr->id = id;
  laser_ptr->max_num_carriers = max_num_carriers;
  laser_ptr->max_spacing = max_spacing;
  laser_ptr->min_spacing = min_spacing;
  laser_ptr->min_frequency = min_freq;
  laser_ptr->max_frequency = max_freq;
  laser_ptr->lower_frequency = min_freq;
  laser_ptr->upper_frequency = max_freq;
  FRET(laser_ptr);
}

static PrgT_Compcode laser_destroy(Laser * laser_ptr) {
  FIN( laser_destroy(laser_ptr) );
  prg_vector_destroy(laser_ptr->carriers_vptr,PRGC_TRUE);
  prg_mem_free(laser_ptr);
  FRET(PrgC_Compcode_Success);
}

static Laser* laser_copy(Laser const* const laser_ptr) {
  Laser *laser_copy_ptr;
  FIN( laser_copy(laser_ptr) );
  laser_copy_ptr = prg_mem_copy_create(laser_ptr, sizeof(*laser_ptr));
  laser_copy_ptr->carriers_vptr = prg_vector_copy(laser_ptr->carriers_vptr);
  FRET(laser_copy_ptr);
}

static void laser_id_destroy( OpT_uInt32* laser_id_ptr ) {
  FIN( laser_id_destroy() );
  prg_mem_free(laser_id_ptr);
  FOUT;
}



static double laser_minimum_spacing_get(Laser const* const laser_ptr) {
  FIN( laser_minimum_spacing_get(laser_ptr) );
  FRET(laser_ptr->min_spacing);
}


static double laser_maximum_spacing_get(Laser const* const laser_ptr) {
  FIN( laser_maximum_spacing_get(laser_ptr) );
  FRET(laser_ptr->max_spacing);
}



static Boolean laser_is_totally_free(Laser const* const laser_ptr) {
  unsigned long i,size;
  Carrier *carrier_ptr;
  FIN( laser_is_totally_free(laser_ptr) );
  size = prg_vector_size(laser_ptr->carriers_vptr);
  for (i=0; i<size; ++i) {
    carrier_ptr = prg_vector_access (laser_ptr->carriers_vptr, i);
    if (carrier_ptr->in_use) {
      FRET(OPC_FALSE);
    }
  }
  FRET(OPC_TRUE);
}


static void laser_frequency_range_update(Laser * const laser_ptr) {
  // update the maximum spectrum range based on the currently used carriers
  // note: this does not take into account the fillhole carriers
  Carrier *carrier_ptr;
  long size,i;
  int free_carriers;
  FIN(laser_frequency_range_update(laser_ptr));

  size = prg_vector_size(laser_ptr->carriers_vptr);
  
  if (size>0) {
    free_carriers = laser_ptr->max_num_carriers;
    i=0;
    while (i<size) {
      carrier_ptr = prg_vector_access (laser_ptr->carriers_vptr,i);
      ++i;
      if (carrier_ptr->in_use) {
        --free_carriers;
        laser_ptr->lower_frequency = carrier_ptr->frequency;
        laser_ptr->upper_frequency = carrier_ptr->frequency;
        break;
      }
    }

    for (; i<size; ++i) {
      carrier_ptr = prg_vector_access (laser_ptr->carriers_vptr, i);
      if (carrier_ptr->in_use) {
        --free_carriers;
        if (laser_ptr->lower_frequency > carrier_ptr->frequency) {
          laser_ptr->lower_frequency = carrier_ptr->frequency;
        }
        if (laser_ptr->upper_frequency < carrier_ptr->frequency) {
          laser_ptr->upper_frequency = carrier_ptr->frequency;
        }
      }
    }
    
    if (free_carriers == laser_ptr->max_num_carriers) {
      laser_ptr->lower_frequency = laser_ptr->min_frequency;
      laser_ptr->upper_frequency = laser_ptr->max_frequency;
    } else {
      laser_ptr->lower_frequency -= laser_ptr->max_spacing*free_carriers;
      laser_ptr->upper_frequency += laser_ptr->max_spacing*free_carriers;

      if (laser_ptr->lower_frequency < laser_ptr->min_frequency)
        laser_ptr->lower_frequency = laser_ptr->min_frequency;
      if (laser_ptr->upper_frequency > laser_ptr->max_frequency)
        laser_ptr->upper_frequency = laser_ptr->max_frequency;
    }
  } else {
    laser_ptr->lower_frequency = laser_ptr->min_frequency;
    laser_ptr->upper_frequency = laser_ptr->max_frequency;
  }

  FOUT;
}



static int laser_num_free_carriers(Laser const* const laser_ptr) {
  unsigned long i,size;
  int free_carriers;
  Carrier *carrier_ptr;
  
  FIN( laser_num_free_carriers(laser_ptr) );
  free_carriers = laser_ptr->max_num_carriers;
  size = prg_vector_size(laser_ptr->carriers_vptr);
  for (i=0; i<size; ++i) {
    carrier_ptr = prg_vector_access(laser_ptr->carriers_vptr, i);
    if (carrier_ptr->in_use) {
      --free_carriers;
    }
  }
  FRET(free_carriers);
}


#define laser_num_busy_carriers(laser_ptr) (laser_ptr->max_num_carriers - laser_num_free_carriers(laser_ptr))



static double laser_utilization(Laser const* const laser_ptr) {
  // this function returns a value between 0 and 1 corresponding
  // to the utilization of the laser (0 if all laser's carriers are free, 
  // 1 if all carriers are in use).
  int used_carriers;
  FIN( laser_utilization() );
  used_carriers = laser_num_busy_carriers(laser_ptr);
  FRET( (double)used_carriers/(laser_ptr->max_num_carriers) );
}

static int lasers_most_used_first(const void* elem1_ptr,const void* elem2_ptr) {
  Laser *l1_ptr,*l2_ptr;
  double u1,u2;
  FIN(lasers_compare());
  l1_ptr = (Laser*)elem1_ptr;
  l2_ptr = (Laser*)elem2_ptr;
  u1 = laser_utilization(l1_ptr);
  u2 = laser_utilization(l2_ptr);
  if ( u1 > u2 ) {
    FRET(1);
  } else if ( u1 == u2 ) {
    FRET(0);
  } else {
    FRET(-1);
  }
}


static Carrier* laser_free_carrier_get(Laser *const laser_ptr) {
  unsigned long i,size;
  Carrier *carrier_ptr;
  FIN( laser_free_carrier_get() );
  size = prg_vector_size(laser_ptr->carriers_vptr);
  for (i=0; i<size; ++i) {
    carrier_ptr = prg_vector_access (laser_ptr->carriers_vptr, i);
    if (!carrier_ptr->in_use) {
      FRET(carrier_ptr);
    }
  }
  FRET(PRGC_NIL);
}



MD_Compcode laser_free_carrier_use( Laser  * const laser_ptr,
                                    double carrier_frequency,
                                    double bandwidth,
                                    OpT_uInt32 * const o_carrier_id_ptr) {
  int num_busy_carriers, tot_fillhole_carriers, fillhole_carriers,
      fillhole_low_high, free_carriers, not_fillhole_free_carriers;
  unsigned long i,insert_position;
  Carrier   *carrier_ptr, *carrier_a_ptr, 
            *carrier_b_ptr, *high_carrier_ptr, *low_carrier_ptr;

  FIN( laser_free_carrier_use() );

  num_busy_carriers = laser_num_busy_carriers(laser_ptr);
  
  if ( num_busy_carriers == 0) {
    // The very first carrier of the transponder. 
    // Let's just allocate it, no more checks are needed.
    carrier_ptr = laser_free_carrier_get(laser_ptr);
    if (PRGC_NIL == carrier_ptr) {
      FRET(MD_Compcode_Failure);
    }
    carrier_use(carrier_ptr, carrier_frequency, bandwidth);

    laser_frequency_range_update(laser_ptr);
    if (PRGC_NIL != o_carrier_id_ptr) {
      *o_carrier_id_ptr = carrier_ptr->id;
    }
    FRET(MD_Compcode_Success);
  }

  if (num_busy_carriers + 1 > laser_ptr->max_num_carriers) {
    FRET(MD_Compcode_Failure); //maximum number of carriers reached already
  }


  // order the carriers from the lowest to the highest frequency 
  // (unused carriers goes to the bottom)
  prg_vector_sort(laser_ptr->carriers_vptr, carriers_compare);

  if (num_busy_carriers == 1) {
    double lower1, upper1, lower2, upper2;
    
    carrier_ptr = prg_vector_access(laser_ptr->carriers_vptr, 0);
    upper1 = carrier_ptr->frequency + (laser_ptr->max_num_carriers-1)*(laser_ptr->max_spacing);
    lower1 = carrier_ptr->frequency + laser_ptr->min_spacing;
    upper2 = carrier_ptr->frequency - laser_ptr->min_spacing;
    lower2 = carrier_ptr->frequency - (laser_ptr->max_num_carriers-1)*(laser_ptr->max_spacing);

    if ( ( (carrier_frequency <= upper1) && (carrier_frequency >= lower1  ) )  ||
         ( (carrier_frequency <= upper2) && (carrier_frequency >= lower2  ) )    ) {
      carrier_ptr = laser_free_carrier_get(laser_ptr);
      carrier_use(carrier_ptr, carrier_frequency, bandwidth);
      laser_frequency_range_update(laser_ptr);
      if (PRGC_NIL != o_carrier_id_ptr) {
        *o_carrier_id_ptr = carrier_ptr->id;
      }
      FRET(MD_Compcode_Success);
    } else {
      FRET(MD_Compcode_Failure); // the new carrier goes out of boundaries
    }
        
  }
  
  // the fillhole carriers are carriers needed to accomplish the maximum spacing requirement.
  tot_fillhole_carriers = 0;
  high_carrier_ptr = PRGC_NIL;
  low_carrier_ptr = PRGC_NIL;

  // calculate the total fillhole carriers, and saving the ones between the carriers in which the new one falls
  carrier_a_ptr = prg_vector_access (laser_ptr->carriers_vptr, 0);
  for (i=1; i<num_busy_carriers; ++i) {
    carrier_b_ptr = prg_vector_access (laser_ptr->carriers_vptr, i);
    fillhole_carriers = ceil( (double)(carrier_b_ptr->frequency - carrier_a_ptr->frequency)/(double)(laser_ptr->max_spacing) - 1 );
    tot_fillhole_carriers += fillhole_carriers;
    if (carrier_frequency > carrier_a_ptr->frequency &&
      carrier_frequency < carrier_b_ptr->frequency) {
      // save the carriers between which the new carrier falls
      high_carrier_ptr = carrier_b_ptr;
      low_carrier_ptr = carrier_a_ptr;
      fillhole_low_high = fillhole_carriers;
      // save the position in which the new carrier should be inserted
      insert_position = i;
    }
    carrier_a_ptr = carrier_b_ptr;
  }
  
  free_carriers = laser_ptr->max_num_carriers - num_busy_carriers;
  not_fillhole_free_carriers = free_carriers-tot_fillhole_carriers;
  
  if (high_carrier_ptr == PRGC_NIL) {
    // the new carrier is not between already placed carriers.
    // it can be placed only if not needed to fill a hole and only in the limit that
    // all the new fillhole it creates can be in future filled by next carriers.
    
    // load the minimum carrier in carrier_a and the maximum in carrier_b
    carrier_a_ptr = prg_vector_access (laser_ptr->carriers_vptr, 0);
    if (not_fillhole_free_carriers > 0 &&
        ( ( carrier_frequency >= (carrier_a_ptr->frequency - not_fillhole_free_carriers*laser_ptr->max_spacing) &&
          carrier_frequency <= (carrier_a_ptr->frequency - laser_ptr->min_spacing) ) ||
          ( carrier_frequency <= carrier_b_ptr->frequency + not_fillhole_free_carriers*laser_ptr->max_spacing &&
            carrier_frequency >= carrier_b_ptr->frequency + laser_ptr->min_spacing )
        )
    ) {
      carrier_ptr = laser_free_carrier_get(laser_ptr);
      carrier_use(carrier_ptr, carrier_frequency, bandwidth);
      *o_carrier_id_ptr = carrier_ptr->id;
      laser_frequency_range_update(laser_ptr);
      FRET(MD_Compcode_Success);
    } else {
      // the carrier must be used to fill some hole to accomplish the max spacing constraint
      //op_sim_error (OPC_SIM_ERROR_ABORT , "MW_SBVT_VAR", "the carrier must be used to fill some hole to accomplish the max spacing constraint");
      FRET(MD_Compcode_Failure); 
    }
  } else {
    // the new carrier falls between 2 already existing carriers.
    // If the total fillhole carriers needed are less than the free carriers,
    // the new one can be placed whatever. Otherwise it must fill one of the hole.
  
    if (carrier_frequency > high_carrier_ptr->frequency - laser_ptr->min_spacing ||
      carrier_frequency < low_carrier_ptr->frequency + laser_ptr->min_spacing ) {
      // the new carrier does not respect the minimum spacing among carriers
      //op_sim_error (OPC_SIM_ERROR_ABORT , "MW_SBVT_VAR", "the new carrier does not respect the minimum spacing among carriers");
      FRET(MD_Compcode_Failure);
    }
    
    if (not_fillhole_free_carriers > 0) {
      // insert the new carrier without taking into account the fillhole
      carrier_ptr = laser_free_carrier_get(laser_ptr);
      carrier_use(carrier_ptr, carrier_frequency, bandwidth);
      *o_carrier_id_ptr = carrier_ptr->id;
      laser_frequency_range_update(laser_ptr);
      FRET(MD_Compcode_Success);
    } else {
      //the new carrier must fill an hole, if not it cannot be allocated
      if (fillhole_low_high == 0) {
        // no chance to fill a hole in between these two carriers
        //op_sim_error (OPC_SIM_ERROR_ABORT , "MW_SBVT_VAR", "no chance to fill a hole in between these two carriers");
        FRET(MD_Compcode_Failure);
      }
      // count the new fillhole value in case allocating the new carrier
      fillhole_carriers = ceil( (double)(high_carrier_ptr->frequency - carrier_frequency)/(double)(laser_ptr->max_spacing) - 1 );
      fillhole_carriers += ceil( (double)(carrier_frequency - low_carrier_ptr->frequency)/(double)(laser_ptr->max_spacing) - 1 );
      // if the new value is less than the previous it means we have filled an hole, so that's GOOD! Isn't it?
      if (fillhole_carriers < fillhole_low_high) {
        //hole filled! let's allocate the carrier
        carrier_ptr = laser_free_carrier_get(laser_ptr);
        carrier_use(carrier_ptr, carrier_frequency, bandwidth);
        *o_carrier_id_ptr = carrier_ptr->id;
        laser_frequency_range_update(laser_ptr);
        FRET(MD_Compcode_Success);
      } else {
        // hole not filled. reject the carrier
        //op_sim_error (OPC_SIM_ERROR_ABORT , "MW_SBVT_VAR", "hole not filled. reject the carrier");
        FRET(MD_Compcode_Failure);
      }
    }
  }

}


MD_Compcode laser_carrier_release(Laser *const laser_ptr, OpT_uInt32 carrier_id) {
Carrier *carrier_ptr;
  FIN( laser_carrier_release(laser_ptr,carrier_id) );
  carrier_ptr = carriers_vector_find(laser_ptr->carriers_vptr, carrier_id);
  if (carrier_ptr!=PRGC_NIL) {
    if (carrier_ptr->in_use) {
      carrier_ptr->in_use = OPC_FALSE;
      laser_frequency_range_update(laser_ptr);
    } else {
      op_sim_error (OPC_SIM_ERROR_ABORT , "laser_carrier_release","Carrier was already released");
    }
    FRET(MD_Compcode_Success);
  } else {
    FRET(MD_Compcode_Failure);
  }
}

MD_Compcode laser_carriers_release(Laser *const laser_ptr, 
                                   PrgT_Vector const *carrier_ids_vptr) {
  //get a vector of frequency indexes as carriers to be released
  unsigned long   i,size;
  OpT_uInt32  *carrier_id_ptr;
  MD_Compcode result;

  FIN( laser_carriers_release(laser_ptr,carrier_ids_vptr) );
  result = MD_Compcode_Success;
  size = prg_vector_size(carrier_ids_vptr);
  for (i=0; i<size; ++i) {
    carrier_id_ptr = prg_vector_access(carrier_ids_vptr, i);
    if ( MD_Compcode_Failure == laser_carrier_release(laser_ptr,*carrier_id_ptr) ) {
      result = MD_Compcode_Failure;
    }
  }
  FRET(result);
}


static Boolean laser_are_busy_carriers_overlapping(Laser const* const laser_ptr,
                                                   double lower_frequency,
                                                   double upper_frequency) {
  unsigned long size, i;
  Carrier *carrier_ptr;
  FIN(laser_are_busy_carriers_overlapping());

  size = prg_vector_size(laser_ptr->carriers_vptr);
  for (i=0; i<size; ++i) {
    carrier_ptr = prg_vector_access (laser_ptr->carriers_vptr, i);
    if (carrier_ptr->in_use) {
      double carrier_lf,carrier_hf;
      carrier_lf = carrier_ptr->frequency - (carrier_ptr->bandwidth/2);
      carrier_hf = carrier_ptr->frequency + (carrier_ptr->bandwidth/2);
      if (BW_IS_OVERLAPPING(carrier_lf,carrier_hf,lower_frequency,upper_frequency)) {
        FRET(OPC_TRUE);
      }
    }
  }
  FRET(OPC_FALSE);
}




Spectrum_Map* laser_spectrum_map_by_demand_get( Laser const* const laser_ptr,
                                                int num_consecutive_carriers,
                                                double bandwidth,
                                                Boolean carriers_overlap_allowed) {
// This function return a spectrum map of the central frequencies
// that can be used to serve the demand (specified by the number of carriers and the spacing between them)
  int central_freq_index,lower_index,upper_index,i;
  int num_alloc;
  Laser   *laser_cpy_ptr;
  Spectrum_Map  *spectrum_map_ptr;
  unsigned long size;
  double carrier_bw, spacing, offset, carrier_freq;
  Carrier *carrier_ptr;
  OpT_uInt32 carrier_id, *carrier_id_ptr;
  
  FIN( laser_spectrum_map_by_demand_get() );
  
  // assuming equal Bandwidth to each carrier
  carrier_bw = bandwidth / num_consecutive_carriers;
  
  if (num_consecutive_carriers>1) {
    // the spacing is equal to the bandwidth associated to each carrier
    spacing = carrier_bw;
  } else {
    spacing = 0;
  }

  if (num_consecutive_carriers > laser_num_free_carriers(laser_ptr) ||
        ( num_consecutive_carriers>1 && 
          ( spacing > laser_maximum_spacing_get(laser_ptr) ||
            spacing < laser_minimum_spacing_get(laser_ptr) )
        )
  ) {
    FRET(PRGC_NIL);
  }
  
  // bw between the central frequency and the first(/last) carrier (GHz)
  offset = (bandwidth-carrier_bw)/2; 

  lower_index = frequency_to_index_ceil(laser_ptr->lower_frequency + offset);
  upper_index = frequency_to_index_floor(laser_ptr->upper_frequency - offset);
  // create a free spectrum map between for the
  // central frequencies that can be used
  spectrum_map_ptr = spectrum_map_create(lower_index, upper_index);

  if ( laser_is_totally_free(laser_ptr) != OPC_TRUE) {
    spectrum_map_fully_busy_set(spectrum_map_ptr);
    // use a copy of the laser, to try to allocate the carriers sequentially
    laser_cpy_ptr = laser_copy(laser_ptr);

    for (central_freq_index=lower_index; central_freq_index<=upper_index; ++central_freq_index) {
      PrgT_Vector *temp_carrier_ids_vptr;

      carrier_freq = index_to_frequency(central_freq_index) - offset;

      temp_carrier_ids_vptr = carrier_ids_vector_create(num_consecutive_carriers);

      for (num_alloc = 0; num_alloc<num_consecutive_carriers; ++num_alloc) {
        if ( ! laser_free_carrier_use(laser_cpy_ptr,carrier_freq,carrier_bw,&carrier_id) ) {
          break;
        }
        carrier_id_ptr = prg_mem_alloc(sizeof(*carrier_id_ptr));
        *carrier_id_ptr = carrier_id;
        prg_vector_append(temp_carrier_ids_vptr, carrier_id_ptr);
        carrier_freq+=spacing;
      }

      if (num_alloc == num_consecutive_carriers) {
        // all the carriers has been allocated
        // thus set the central frequency as available
        spectrum_map_free_set(spectrum_map_ptr,central_freq_index);
      }

      //deallocate the previous test
      laser_carriers_release(laser_cpy_ptr, temp_carrier_ids_vptr);
      prg_vector_destroy(temp_carrier_ids_vptr,PRGC_TRUE);

    }
    
    // when it is finished to try all the carriers destroy the copy of the laser
    laser_destroy(laser_cpy_ptr);
  }

  if (OPC_TRUE != carriers_overlap_allowed) {
    // Due to the transponder architecture, the carriers are combined, 
    // so this we must exclude bandwidth that brings to overlapping
    size = prg_vector_size(laser_ptr->carriers_vptr);
    for (i=0; i<size; ++i) {
      carrier_ptr = prg_vector_access(laser_ptr->carriers_vptr, i);
      if (carrier_ptr->in_use) {
        // calculate the spectrum range that must be excluded
        // in order to avoid overlap
        lower_index = frequency_to_index_ceil( carrier_ptr->frequency - 
                                                (carrier_ptr->bandwidth/2) -
                                                (bandwidth/2));
        upper_index = frequency_to_index_floor( carrier_ptr->frequency +
                                                (carrier_ptr->bandwidth/2) + 
                                                (bandwidth/2));
        spectrum_map_busy_range_set(spectrum_map_ptr, lower_index, upper_index);
      }
    }
  }

  FRET(spectrum_map_ptr);
}


static void laser_print(Laser const * const laser_ptr) {
/**
  Print the carriers of the transponder.
**/
  unsigned long i,size;
  Carrier *carrier_ptr;
  FIN (laser_print(laser_ptr));
  size = prg_vector_size(laser_ptr->carriers_vptr);
  printf("Laser %d (lf: %f, hf: %f) carriers: [",
          laser_ptr->id,
          laser_ptr->lower_frequency,
          laser_ptr->upper_frequency);
  for (i=0; i<size; ++i) {
    carrier_ptr = prg_vector_access(laser_ptr->carriers_vptr, i);
    printf("%f, ",carrier_ptr->frequency);
  }
  printf("]");
  FOUT;
}






































////////////////////////////////////////////////////////////////////////////////
//                            TRANSPONDER FUNCTIONS                           //
////////////////////////////////////////////////////////////////////////////////

struct Transponder {
  OpT_uInt32      id; // distinguish between transponders
  Transponder_Type  type;
  PrgT_Vector     *tx_lasers_vptr; // vector of lasers for the TX module
  PrgT_Vector     *rx_lasers_vptr; // vector of lasers for the TX module
};


Transponder* transponder_create(OpT_uInt32 id, Transponder_Type type, int max_num_carriers,
                                double min_spacing, double max_spacing, double min_freq, double max_freq) {
  // Note: If MW-SBVT only 1 laser is created containing all the carriers
  //       If ML-SBVT 1 carrier per laser
  Transponder *trans_ptr;
  Carrier *carrier_ptr;
  Laser *tx_laser_ptr, *rx_laser_ptr;
  int num_lasers, num_carriers_per_laser,i,j;
  OpT_uInt32 laser_id,carrier_id;

  FIN( transponder_create(id,type,max_num_carriers,min_spacing,max_spacing,min_freq, max_freq) );

  trans_ptr = prg_mem_alloc(sizeof(*trans_ptr));
  trans_ptr->id = id;
  trans_ptr->type = type;
  switch (type) {
    case SBVT:
      num_lasers = max_num_carriers;
      num_carriers_per_laser = 1;
      max_spacing = max_freq-min_freq;
      min_spacing = 0;
      break;
    case MW_SBVT_VAR:
      num_lasers = 1;
      num_carriers_per_laser = max_num_carriers;
      break;
  }

  trans_ptr->tx_lasers_vptr = prg_vector_create(num_lasers, laser_destroy, laser_copy);
  trans_ptr->rx_lasers_vptr = prg_vector_create(num_lasers, laser_destroy, laser_copy);
  laser_id = 0;
  carrier_id = 0;
  for (i=0; i<num_lasers; ++i) {
    tx_laser_ptr = laser_create(laser_id,
                                num_carriers_per_laser,
                                min_spacing,
                                max_spacing,
                                min_freq,
                                max_freq);
    ++laser_id;
    prg_vector_append (trans_ptr->tx_lasers_vptr, tx_laser_ptr);
    rx_laser_ptr = laser_create(laser_id,
                                num_carriers_per_laser,
                                min_spacing,
                                max_spacing,
                                min_freq,
                                max_freq);
    ++laser_id;
    prg_vector_append (trans_ptr->rx_lasers_vptr, rx_laser_ptr);
    for (j=0; j<num_carriers_per_laser; ++j) {
      carrier_ptr = carrier_create(carrier_id);
      ++carrier_id;
      prg_vector_append (tx_laser_ptr->carriers_vptr, carrier_ptr);
      carrier_ptr = carrier_create(carrier_id);
      ++carrier_id;
      prg_vector_append (rx_laser_ptr->carriers_vptr, carrier_ptr);
    }
  }
  FRET(trans_ptr);
}


Transponder* transponder_copy(Transponder const* const transponder_ptr) {
  Transponder *transponder_copy_ptr;
  FIN( transponder_copy(transponder_ptr) );
  transponder_copy_ptr = prg_mem_copy_create(transponder_ptr, sizeof(*transponder_copy_ptr));
  transponder_copy_ptr->tx_lasers_vptr = prg_vector_copy(transponder_ptr->tx_lasers_vptr);
  transponder_copy_ptr->rx_lasers_vptr = prg_vector_copy(transponder_ptr->rx_lasers_vptr);
  FRET(transponder_copy_ptr);
}


void transponder_destroy(Transponder  *transponder_ptr) {
  FIN( transponder_destroy(transponder_ptr) );
  prg_vector_destroy(transponder_ptr->tx_lasers_vptr,PRGC_TRUE);
  prg_vector_destroy(transponder_ptr->rx_lasers_vptr,PRGC_TRUE);
  prg_mem_free(transponder_ptr);
  FOUT;
}


Transponder_Type transponder_type_get(Transponder const* const transponder_ptr) {
  FIN( transponder_type_get(transponder_ptr));
  if ( transponder_ptr == PRGC_NIL ) FRET(-1);
  FRET(transponder_ptr->type);
}


OpT_uInt32* transponder_id_copy(OpT_uInt32 const * const transponder_id_ptr) {
  OpT_uInt32  *transponder_id_copy_ptr;
  FIN( transponder_id_copy(transponder_id_ptr ) );
  if ( transponder_id_ptr == PRGC_NIL ) FRET(PRGC_NIL);
  transponder_id_copy_ptr = prg_mem_copy_create(transponder_id_ptr, sizeof(*transponder_id_copy_ptr));
  FRET(transponder_id_copy_ptr);
}

void transponder_id_destroy(OpT_uInt32 *transponder_id_ptr) {
  FIN( transponder_id_destroy(transponder_id_ptr) );
  if ( transponder_id_ptr == PRGC_NIL ) FOUT;
  prg_mem_free(transponder_id_ptr);
  FOUT;
}


/*
static Carrier* transponder_free_carrier_get(Transponder const* transponder_ptr) {
  unsigned long size, i;
  Carrier *carrier_ptr;
  FIN(transponder_free_carrier_get());

  size = prg_vector_size(transponder_ptr->carriers_vptr);
  for (i=0; i<size; ++i) {
    carrier_ptr = prg_vector_access (transponder_ptr->carriers_vptr, i);
    if (carrier_ptr->in_use == OPC_FALSE) {
      FRET(carrier_ptr);
    }
  }
  FRET(PRGC_NIL);
}*/


static MD_Compcode laser_vector_carrier_search_and_release(PrgT_Vector *const lasers_vptr, OpT_uInt32 carrier_id) {
  Laser *laser_ptr;
  unsigned long size,i;
  FIN( laser_vector_carrier_search_and_release() );
  //NOTE: Assuming unique carrier id per transponder!!!
  size = prg_vector_size(lasers_vptr);
  for (i=0; i<size; ++i) {
    laser_ptr = prg_vector_access(lasers_vptr, i);
    if ( MD_Compcode_Success == laser_carrier_release(laser_ptr,carrier_id) ) {
      FRET(MD_Compcode_Success);
    }
  }
  FRET(MD_Compcode_Failure);
}

MD_Compcode transponder_rx_carrier_release(Transponder *const transponder_ptr, OpT_uInt32 carrier_id) {
  FIN( transponder_rx_carrier_release(transponder_ptr,carrier_id) );
  //NOTE: Assuming unique carrier id per transponder!!!
  FRET(laser_vector_carrier_search_and_release(transponder_ptr->rx_lasers_vptr,carrier_id));
}

MD_Compcode transponder_tx_carrier_release(Transponder *const transponder_ptr, OpT_uInt32 carrier_id) {
  FIN( transponder_rx_carrier_release(transponder_ptr,carrier_id) );
  //NOTE: Assuming unique carrier id per transponder!!!
  FRET(laser_vector_carrier_search_and_release(transponder_ptr->tx_lasers_vptr,carrier_id));
}

MD_Compcode transponder_tx_carriers_release(Transponder  *transponder_ptr, 
                                            PrgT_Vector const *carrier_ids_vptr) {
  //get a vector of frequency indexes as carriers to be released
  unsigned long   i,size;
  OpT_uInt32  *carrier_id_ptr;

  FIN( transponder_carriers_release(transponder_ptr,carrier_ids_vptr) );
  size = prg_vector_size(carrier_ids_vptr);
  for (i=0; i<size; ++i) {
    carrier_id_ptr = prg_vector_access(carrier_ids_vptr, i);
    transponder_tx_carrier_release(transponder_ptr,*carrier_id_ptr);
  }
  FRET(MD_Compcode_Success);
}

MD_Compcode transponder_rx_carriers_release(Transponder  *transponder_ptr, 
                                            PrgT_Vector const *carrier_ids_vptr) {
  //get a vector of frequency indexes as carriers to be released
  unsigned long   i,size;
  OpT_uInt32  *carrier_id_ptr;

  FIN( transponder_carriers_release(transponder_ptr,carrier_ids_vptr) );
  size = prg_vector_size(carrier_ids_vptr);
  for (i=0; i<size; ++i) {
    carrier_id_ptr = prg_vector_access(carrier_ids_vptr, i);
    transponder_rx_carrier_release(transponder_ptr,*carrier_id_ptr);
  }
  FRET(MD_Compcode_Success);
}




static MD_Compcode laser_vector_free_carrier_search_and_use(PrgT_Vector *const lasers_vptr,
                                                            double carrier_frequency,
                                                            double bandwidth,
                                                            OpT_uInt32 * const o_laser_id_ptr,
                                                            OpT_uInt32 * const o_carrier_id_ptr) {
  long size, i;
  Laser *laser_ptr;

  FIN( laser_vector_free_carrier_search_and_use() );

  // Sort the list of lasers from the most to the least used
  prg_vector_sort(lasers_vptr, lasers_most_used_first);

  size = prg_vector_size(lasers_vptr);

  #ifndef CARRIERS_CAN_OVERLAP
  {// in case carriers CANNOT overlap
    double lower_frequency, upper_frequency;
    lower_frequency = carrier_frequency-(bandwidth/2);
    upper_frequency = carrier_frequency+(bandwidth/2);
    for (i=0; i<size; ++i) {
      laser_ptr = prg_vector_access(lasers_vptr, i);
      if (laser_are_busy_carriers_overlapping(laser_ptr, lower_frequency, upper_frequency)) {
        FRET(MD_Compcode_Failure);
      }
    }
  }
  #endif

  for (i=0; i<size; ++i) {

    laser_ptr = prg_vector_access(lasers_vptr, i);

    if ( MD_Compcode_Success == laser_free_carrier_use(laser_ptr, carrier_frequency, bandwidth, o_carrier_id_ptr) ) {
      if (PRGC_NIL != o_laser_id_ptr) {
        *o_laser_id_ptr = laser_ptr->id;
      }
      FRET(MD_Compcode_Success);
    }

  }

  FRET(MD_Compcode_Failure);
}


MD_Compcode transponder_tx_free_carrier_use( Transponder  *transponder_ptr,
                                            double carrier_frequency,
                                            double bandwidth,
                                            OpT_uInt32 * const o_laser_id_ptr,
                                            OpT_uInt32 * const o_carrier_id_ptr) {
  // Use a free carrier of the transmitting module to generate a carrier frequency with
  // a specific Bandwidth.
  // INPUT: Transponder ptr, the central frequency of the carrier, the bandwidth of the carrier.
  // OUTPUT: The id of the laser generating the carrier, the id of the carrier
  FIN( transponder_tx_free_carrier_use() );
  FRET( laser_vector_free_carrier_search_and_use(transponder_ptr->tx_lasers_vptr, carrier_frequency, bandwidth, o_laser_id_ptr, o_carrier_id_ptr) );
}

MD_Compcode transponder_rx_free_carrier_use( Transponder  *transponder_ptr,
                                            double carrier_frequency,
                                            double bandwidth,
                                            OpT_uInt32 * const o_laser_id_ptr,
                                            OpT_uInt32 * const o_carrier_id_ptr) {
  // Use a free carrier of the receiving module to generate a carrier frequency with
  // a specific Bandwidth.
  // INPUT: Transponder ptr, the central frequency of the carrier, the bandwidth of the carrier.
  // OUTPUT: The id of the laser generating the carrier, the id of the carrier
  FIN( transponder_rx_free_carrier_use() );
  FRET( laser_vector_free_carrier_search_and_use(transponder_ptr->rx_lasers_vptr, carrier_frequency, bandwidth, o_laser_id_ptr, o_carrier_id_ptr) );
}



static MD_Compcode laser_vector_carriers_use( PrgT_Vector *const lasers_vptr,
                                              PrgT_Vector const * const use_carriers_vptr,
                                              double bandwidth,
                                              PrgT_Vector * const o_used_lasers_ids_vptr,
                                              PrgT_Vector * const o_used_carrier_ids_vptr) {
  // This function allocates the frequency given as input into some carriers.
  // INPUT: A pointer to a transponder. A vector of frequencies (double) in GHz.
  // OUTPUT: A Vector of lasers IDs. A vector of carriers IDs.
  //         (note: A carrier in position "i" in the returned vector belongs to 
  //                the the laser at the position "i" in the laser IDs vector)
  long   i,k,size,size2;
  double       *frequency_ptr;
  OpT_uInt32  *carrier_id_ptr;
  OpT_uInt32  *laser_id_ptr = PRGC_NIL;

  FIN( transponder_tx_carriers_use() );

  if (PRGC_NIL == o_used_carrier_ids_vptr) FRET(MD_Compcode_Failure);

  size = prg_vector_size(use_carriers_vptr);
  for (i=0; i<size; ++i) {
    frequency_ptr = prg_vector_access (use_carriers_vptr, i);
    carrier_id_ptr = prg_mem_alloc(sizeof(*carrier_id_ptr));
    prg_vector_append(o_used_carrier_ids_vptr, carrier_id_ptr);
    if (PRGC_NIL != o_used_lasers_ids_vptr) {
      laser_id_ptr = prg_mem_alloc(sizeof(*laser_id_ptr));
      prg_vector_append(o_used_lasers_ids_vptr, laser_id_ptr);
    }
    if ( laser_vector_free_carrier_search_and_use(lasers_vptr,*frequency_ptr, bandwidth, laser_id_ptr, carrier_id_ptr) == MD_Compcode_Failure ) {
      size2 = prg_vector_size(o_used_carrier_ids_vptr);
      //rollback
      for (k=(size2-1); k>=0; --k) {
        carrier_id_ptr = prg_vector_remove(o_used_carrier_ids_vptr, k);
        laser_vector_carrier_search_and_release(lasers_vptr,*carrier_id_ptr);
        carrier_id_destroy(carrier_id_ptr);
      }
      if (PRGC_NIL != o_used_lasers_ids_vptr) {
        size2 = prg_vector_size(o_used_lasers_ids_vptr);
        //rollback
        for (k=(size2-1); k>=0; --k) {
          laser_id_ptr = prg_vector_remove(o_used_lasers_ids_vptr,k);
          laser_id_destroy(laser_id_ptr);
        }
      }
      
      FRET(MD_Compcode_Failure);
    }
    
  }
  FRET(MD_Compcode_Success);
}


MD_Compcode transponder_tx_carriers_use(Transponder  * const transponder_ptr,
                                        PrgT_Vector const * const use_carriers_vptr,
                                        double bandwidth,
                                        PrgT_Vector * const o_used_lasers_ids_vptr,
                                        PrgT_Vector * const o_used_carrier_ids_vptr) {
  FIN( transponder_tx_carriers_use() );
  FRET(laser_vector_carriers_use(transponder_ptr->tx_lasers_vptr,use_carriers_vptr,bandwidth,o_used_lasers_ids_vptr,o_used_carrier_ids_vptr));
}

MD_Compcode transponder_rx_carriers_use(Transponder  * const transponder_ptr,
                                        PrgT_Vector const * const use_carriers_vptr,
                                        double bandwidth,
                                        PrgT_Vector * const o_used_lasers_ids_vptr,
                                        PrgT_Vector * const o_used_carrier_ids_vptr) {
  FIN( transponder_rx_carriers_use() );
  FRET(laser_vector_carriers_use(transponder_ptr->rx_lasers_vptr,use_carriers_vptr,bandwidth,o_used_lasers_ids_vptr,o_used_carrier_ids_vptr));
}









static MD_Compcode laser_vector_carriers_use_by_id( PrgT_Vector  *lasers_vptr,
                                                    PrgT_Vector const *use_carriers_vptr,
                                                    double bandwidth,
                                                    PrgT_Vector const *use_carrier_ids_vptr) {
  unsigned long i,j,size,size2;
  double *frequency_ptr;
  OpT_uInt32 *carrier_id_ptr;
  Carrier *carrier_ptr;
  Laser* laser_ptr;

  FIN(laser_vector_carriers_use_by_id());

  if (PRGC_NIL == use_carrier_ids_vptr) FRET(MD_Compcode_Failure);
  size = prg_vector_size(use_carrier_ids_vptr);
  if (size != prg_vector_size(use_carriers_vptr)) {
    op_sim_error (OPC_SIM_ERROR_ABORT , "transponder_carriers_use_by_id", "different size vector of carriers and IDs");
    FRET(MD_Compcode_Failure);
  }

  size2 = prg_vector_size(lasers_vptr);
  for (i=0; i<size; ++i) {
    frequency_ptr = prg_vector_access (use_carriers_vptr, i);
    carrier_id_ptr = prg_vector_access (use_carrier_ids_vptr, i);
    for (j=0; j<size2; ++j) {
      laser_ptr = prg_vector_access(lasers_vptr, j);
      carrier_ptr = carriers_vector_find(laser_ptr->carriers_vptr, *carrier_id_ptr);
      if (carrier_ptr != PRGC_NIL) {
        carrier_use(carrier_ptr, *frequency_ptr, bandwidth);
        break;
      }
    }
    if (carrier_ptr == PRGC_NIL) {
        op_sim_error (OPC_SIM_ERROR_ABORT , "transponder_carriers_use_by_id", "carrier not found");
        FRET(MD_Compcode_Failure);
    }
  }

  FRET(MD_Compcode_Success);
}



MD_Compcode transponder_tx_carriers_use_by_id(Transponder  *transponder_ptr,
                                              PrgT_Vector const *use_carriers_vptr,
                                              double bandwidth,
                                              PrgT_Vector const *use_carrier_ids_vptr) {
  FIN(transponder_tx_carriers_use_by_id());
  FRET( laser_vector_carriers_use_by_id(transponder_ptr->tx_lasers_vptr,use_carriers_vptr,bandwidth,use_carrier_ids_vptr) );
}

MD_Compcode transponder_rx_carriers_use_by_id(Transponder  *transponder_ptr,
                                              PrgT_Vector const *use_carriers_vptr,
                                              double bandwidth,
                                              PrgT_Vector const *use_carrier_ids_vptr) {
  FIN(transponder_rx_carriers_use_by_id());
  FRET( laser_vector_carriers_use_by_id(transponder_ptr->rx_lasers_vptr,use_carriers_vptr,bandwidth,use_carrier_ids_vptr) );
}






static int laser_vector_num_free_carriers(PrgT_Vector *const lasers_vptr) {
  Laser *laser_ptr;
  int tot_free_carriers;
  unsigned long i,size;

  FIN( transponder_rx_carrier_release(transponder_ptr) );
  tot_free_carriers = 0;
  size = prg_vector_size(lasers_vptr);
  for (i=0; i<size; ++i) {
    laser_ptr = prg_vector_access(lasers_vptr, i);
    tot_free_carriers += laser_num_free_carriers(laser_ptr);
  }
  FRET(tot_free_carriers);
}

int transponder_tx_num_free_carriers(Transponder const* const transponder_ptr) {
  FIN( transponder_tx_num_free_carriers(transponder_ptr) );
  FRET( laser_vector_num_free_carriers(transponder_ptr->tx_lasers_vptr) );
}

int transponder_rx_num_free_carriers(Transponder const* const transponder_ptr) {
  FIN( transponder_rx_num_free_carriers(transponder_ptr) );
  FRET( laser_vector_num_free_carriers(transponder_ptr->rx_lasers_vptr) );
}

int transponder_num_free_carriers(Transponder const* const transponder_ptr) {
  FIN( transponder_num_free_carriers(transponder_ptr) );
  FRET(transponder_tx_num_free_carriers(transponder_ptr)+transponder_rx_num_free_carriers(transponder_ptr));
}





static double laser_vector_avg_utilization(PrgT_Vector *const lasers_vptr) {
  // this function returns a value between 0 and 1 corresponding
  // to the average utilization of all the lasers in the vector given as input.
  Laser *laser_ptr;
  double avg_utilization;
  long size,i;

  FIN( laser_vector_avg_utilization(transponder_ptr) );
  avg_utilization = 0;
  size = prg_vector_size(lasers_vptr);
  for (i=0; i<size; ++i) {
    laser_ptr = prg_vector_access(lasers_vptr, i);
    avg_utilization += laser_utilization(laser_ptr);
  }
  if (avg_utilization != 0) {
    avg_utilization = avg_utilization/size;
  }
  FRET(avg_utilization);
}


double transponder_tx_utilization(Transponder const*const transponder_ptr) {
  // this function returns a value between 0 and TRANSPONDER_C_MAX_UTILIZATION corresponding
  // to the average utilization of all the lasers installed and used to transmit.
  FIN( transponder_tx_utilization(transponder_ptr) );
  FRET( laser_vector_avg_utilization(transponder_ptr->tx_lasers_vptr)*TRANSPONDER_C_MAX_UTILIZATION );
}

double transponder_rx_utilization(Transponder const*const transponder_ptr) {
  // this function returns a value between 0 and TRANSPONDER_C_MAX_UTILIZATION corresponding
  // to the average utilization of all the lasers installed and used to receive.
  FIN( transponder_rx_utilization(transponder_ptr) );
  FRET( laser_vector_avg_utilization(transponder_ptr->rx_lasers_vptr)*TRANSPONDER_C_MAX_UTILIZATION );
}

double transponder_utilization(Transponder const*const transponder_ptr) {
  // this function returns a value between 0 and TRANSPONDER_C_MAX_UTILIZATION corresponding
  // to the average of utilization of TX and RX modules.
  FIN( transponder_utilization(transponder_ptr) );
  FRET( (transponder_tx_utilization(transponder_ptr)+transponder_rx_utilization(transponder_ptr))/2 );
}




static Boolean laser_vector_all_totally_free(PrgT_Vector *const lasers_vptr) {
  Laser *laser_ptr;
  long size,i;

  FIN( laser_vector_all_totally_free() );
  size = prg_vector_size(lasers_vptr);
  for (i=0; i<size; ++i) {
    laser_ptr = prg_vector_access(lasers_vptr, i);
    if ( !laser_is_totally_free(laser_ptr) ) {
      FRET(OPC_FALSE);
    }
  }
  FRET(OPC_TRUE);
}

Boolean transponder_tx_is_totally_free(Transponder const* const transponder_ptr) {
  FIN( transponder_tx_is_totally_free(transponder_ptr) );
  FRET( laser_vector_all_totally_free(transponder_ptr->tx_lasers_vptr) );
}

Boolean transponder_rx_is_totally_free(Transponder const* const transponder_ptr) {
  FIN( transponder_rx_is_totally_free(transponder_ptr) );
  FRET( laser_vector_all_totally_free(transponder_ptr->rx_lasers_vptr) );
}

Boolean transponder_is_totally_free(Transponder const* const transponder_ptr) {
  FIN( transponder_is_totally_free(transponder_ptr) );
  FRET( ( transponder_tx_is_totally_free(transponder_ptr) && transponder_rx_is_totally_free(transponder_ptr) ) );
}




OpT_uInt32 transponder_id_get(Transponder const* const transponder_ptr) {
  FIN( transponder_id_get(transponder_ptr) );
  FRET(transponder_ptr->id);
}




Spectrum_Map* transponder_tx_spectrum_map_by_demand_get(Transponder const* const transponder_ptr,
                                                        int num_consecutive_carriers,
                                                        double bandwidth) {
  Spectrum_Map *spectrum_map_ptr;
  long size,i;
  int num_carriers_per_laser;
  Laser *laser_ptr;

  FIN( transponder_tx_spectrum_map_by_demand_get() );

  if ( transponder_tx_num_free_carriers(transponder_ptr) < num_consecutive_carriers) {
    FRET(PRGC_NIL);
  }

  switch (transponder_ptr->type) {
    case SBVT:
      num_carriers_per_laser = 1;
      break;
    case MW_SBVT_VAR:
      num_carriers_per_laser = num_consecutive_carriers;
      break;
  }

  size = prg_vector_size(transponder_ptr->tx_lasers_vptr);
  if (size>0) {
    laser_ptr = prg_vector_access(transponder_ptr->tx_lasers_vptr, 0);
    spectrum_map_ptr = laser_spectrum_map_by_demand_get( laser_ptr, num_carriers_per_laser, bandwidth, OPC_TRUE);
  }
  for (i=1; i<size; ++i) {
    Spectrum_Map *spectrum_map2_ptr;
    laser_ptr = prg_vector_access(transponder_ptr->tx_lasers_vptr, i);
    spectrum_map2_ptr = laser_spectrum_map_by_demand_get(laser_ptr, num_carriers_per_laser, bandwidth, OPC_TRUE);
    spectrum_maps_union( &spectrum_map_ptr, spectrum_map2_ptr );
    spectrum_map_destroy(spectrum_map2_ptr);
  }

  #ifndef CARRIERS_CAN_OVERLAP
  {
    long j,size1;
    int lower_index,upper_index;
    Carrier *carrier_ptr;
    for (i=0; i<size; ++i) {
      laser_ptr = prg_vector_access(transponder_ptr->tx_lasers_vptr, i);
      size1 = prg_vector_size(laser_ptr->carriers_vptr);
      for (j=0; j<size1; ++j) {
        carrier_ptr = prg_vector_access(laser_ptr->carriers_vptr, j);
        if (carrier_ptr->in_use) {
          // calculate the spectrum range that must be excluded
          // in order to avoid overlap
          lower_index = frequency_to_index_ceil( carrier_ptr->frequency - 
                                                  (carrier_ptr->bandwidth/2) -
                                                  (bandwidth/2));
          upper_index = frequency_to_index_floor( carrier_ptr->frequency +
                                                  (carrier_ptr->bandwidth/2) + 
                                                  (bandwidth/2));
          spectrum_map_busy_range_set(spectrum_map_ptr, lower_index, upper_index);
        }
      }
    }
  }
  #endif

  FRET(spectrum_map_ptr);
}




Spectrum_Map* transponder_rx_spectrum_map_by_demand_get(Transponder const* const transponder_ptr,
                                                        int num_consecutive_carriers,
                                                        double bandwidth) {
  Spectrum_Map *spectrum_map_ptr;
  long size,i;
  int num_carriers_per_laser;
  Laser *laser_ptr;

  FIN( transponder_rx_spectrum_map_by_demand_get() );

  if ( transponder_rx_num_free_carriers(transponder_ptr) < num_consecutive_carriers) {
    FRET(PRGC_NIL);
  }

  switch (transponder_ptr->type) {
    case SBVT:
      num_carriers_per_laser = 1;
      break;
    case MW_SBVT_VAR:
      num_carriers_per_laser = num_consecutive_carriers;
      break;
  }

  size = prg_vector_size(transponder_ptr->rx_lasers_vptr);
  if (size>0) {
    laser_ptr = prg_vector_access(transponder_ptr->rx_lasers_vptr, 0);
    spectrum_map_ptr = laser_spectrum_map_by_demand_get( laser_ptr, num_carriers_per_laser, bandwidth, OPC_TRUE);
  }
  for (i=1; i<size; ++i) {
    Spectrum_Map *spectrum_map2_ptr;
    laser_ptr = prg_vector_access(transponder_ptr->rx_lasers_vptr, i);
    spectrum_map2_ptr = laser_spectrum_map_by_demand_get(laser_ptr, num_carriers_per_laser, bandwidth, OPC_TRUE);
    spectrum_maps_union( &spectrum_map_ptr, spectrum_map2_ptr );
    spectrum_map_destroy(spectrum_map2_ptr);
  }

  #ifndef CARRIERS_CAN_OVERLAP
  {
    long j,size1;
    int lower_index,upper_index;
    Carrier *carrier_ptr;
    for (i=0; i<size; ++i) {
      laser_ptr = prg_vector_access(transponder_ptr->rx_lasers_vptr, i);
      size1 = prg_vector_size(laser_ptr->carriers_vptr);
      for (j=0; j<size1; ++j) {
        carrier_ptr = prg_vector_access(laser_ptr->carriers_vptr, j);
        if (carrier_ptr->in_use) {
          // calculate the spectrum range that must be excluded
          // in order to avoid overlap
          lower_index = frequency_to_index_ceil( carrier_ptr->frequency - 
                                                  (carrier_ptr->bandwidth/2) -
                                                  (bandwidth/2));
          upper_index = frequency_to_index_floor( carrier_ptr->frequency +
                                                  (carrier_ptr->bandwidth/2) + 
                                                  (bandwidth/2));
          spectrum_map_busy_range_set(spectrum_map_ptr, lower_index, upper_index);
        }
      }
    }
  }
  #endif

  FRET(spectrum_map_ptr);
}



void transponder_print(Transponder const * const transponder_ptr) {
/**
  Print the carriers of the transponder.
**/
  unsigned long i,size;
  Laser *laser_ptr;
  FIN (transponder_print(transponder_ptr));
  printf("Transponder %d :",transponder_ptr->id);
  size = prg_vector_size(transponder_ptr->tx_lasers_vptr);
  printf("TX ");
  for (i=0; i<size; ++i) {
    laser_ptr = prg_vector_access (transponder_ptr->tx_lasers_vptr, i);
    printf(",  ");
    laser_print(laser_ptr);
  }
  printf("RX ");
  size = prg_vector_size(transponder_ptr->rx_lasers_vptr);
  for (i=0; i<size; ++i) {
    laser_ptr = prg_vector_access (transponder_ptr->rx_lasers_vptr, i);
    printf(",  ");
    laser_print(laser_ptr);
  }
  printf("\n");
  FOUT;
}


void frequency_vector_print(PrgT_Vector const * const frequencies_vptr) {
/**
  Print the frequencies of the vector
**/
  unsigned long i,size;
  double *freq_ptr;
  FIN (frequency_vector_print(frequencies_vptr));
  size = prg_vector_size(frequencies_vptr);
  printf("Frequencies [");
  for (i=0; i<size; ++i) {
    freq_ptr = prg_vector_access (frequencies_vptr, i);
    printf("%f GHz, ",*freq_ptr);
  }
  printf("]\n");
  FOUT;
}


Transponder* transponders_least_available_transponder_get(PrgT_List const* const transponders_lptr,
                                                          int suggested_available_carriers) {
  Transponder* transponder_ptr, *selected_transponder_ptr;
  double tran_utilization, selected_tran_utilization;
  int selected_tran_num_free_carriers,tran_num_free_carriers;
  PrgT_List_Cell *cell_ptr;

  FIN(transponders_least_available_transponder_get(transponders_lptr,suggested_available_carriers));

  if (prg_list_size(transponders_lptr) == 0) {
    FRET(PRGC_NIL);
  }
  cell_ptr = prg_list_head_cell_get(transponders_lptr);
  selected_transponder_ptr = (Transponder *) prg_list_cell_data_get(cell_ptr);
  selected_tran_utilization = transponder_utilization(selected_transponder_ptr);
  selected_tran_num_free_carriers = transponder_utilization(selected_transponder_ptr);
  cell_ptr = prg_list_cell_next_get(cell_ptr);

  while (PRGC_NIL != cell_ptr) {
    transponder_ptr = (Transponder *) prg_list_cell_data_get(cell_ptr);
    tran_num_free_carriers = transponder_num_free_carriers(transponder_ptr);
    tran_utilization = transponder_utilization(transponder_ptr);
    if ( tran_num_free_carriers >= suggested_available_carriers) {
      // the transponder under considetarion has a number of free carriers
      // greater than the suggested available carriers number.
      // if the selected transponder has a number of free carriers lower than
      // the suggested, or , it is less utilized than the selected one, 
      // select the one under consideration.
      if (selected_tran_num_free_carriers < suggested_available_carriers ||
      tran_utilization > selected_tran_utilization) {
        selected_transponder_ptr = transponder_ptr;
        selected_tran_utilization = tran_utilization;
        selected_tran_num_free_carriers = tran_num_free_carriers;
      }
    } else if (tran_num_free_carriers > selected_tran_num_free_carriers) {
      // the two transponders has a number of free carriers
      // lower than the suggested available carriers number.
      // But the one uder consideration has an higer number. Select it!
      selected_transponder_ptr = transponder_ptr;
      selected_tran_utilization = tran_utilization;
      selected_tran_num_free_carriers = tran_num_free_carriers;
    } else if (tran_num_free_carriers == selected_tran_num_free_carriers) {
      // the two transponders has the same number of free carriers
      // lower than the suggested available carriers number.
      // Select the least available transponder.
      if ( tran_utilization > selected_tran_utilization) {
        selected_transponder_ptr = transponder_ptr;
        selected_tran_utilization = tran_utilization;
        selected_tran_num_free_carriers = tran_num_free_carriers;
      }
    }
    cell_ptr = prg_list_cell_next_get(cell_ptr);
  }

  FRET(selected_transponder_ptr);
}



Transponder* transponder_less_performant( Transponder * transponder1_ptr,
                                          Transponder * transponder2_ptr) {
  FIN( transponder_less_performant(transponder1_ptr, transponder2_ptr) );
  if (transponder1_ptr->type < transponder2_ptr->type) {
    FRET(transponder1_ptr);
  } else {
    FRET(transponder2_ptr);
  }
}

Transponder* transponder_most_performant( Transponder * transponder1_ptr,
                                          Transponder * transponder2_ptr) {
  FIN( transponder_most_performant(transponder1_ptr, transponder2_ptr) );
  if (transponder1_ptr->type < transponder2_ptr->type) {
    FRET(transponder2_ptr);
  } else {
    FRET(transponder1_ptr);
  }
}


short transponder_performance_compare(Transponder * transponder1_ptr,
                                      Transponder * transponder2_ptr) {
  // Returns: 1 if transponder1 is more performant than transponder2
  //         -1 if transponder1 is less performant than transponder2
  //          0 if transponder1 is equal performant than transponder2
  FIN( transponder_performance_compare(transponder1_ptr, transponder2_ptr) );
  if (transponder1_ptr->type > transponder2_ptr->type) {
    FRET(1);
  } else if (transponder1_ptr->type == transponder2_ptr->type) {
    FRET(0);
  } else {
    FRET(-1);
  }
}

