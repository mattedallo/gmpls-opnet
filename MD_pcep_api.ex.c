/** Programmer's Interface to the PCEP protocol**/

#include	<opnet.h>
#include	<oms_pr.h>
#include	"MD_pcep_api.h"
#include	"MD_pcep_src.h"



ApiT_Pcep_App_Handle pcep_app_register (Objid app_module_id) {
	ApiT_Pcep_App_Handle app_pcep_intf_handle;
	List				proc_record_handle_list;
	OmsT_Pr_Handle		process_record_handle;
	Objid				node_objid;
	Objid				pcep_module_id;

	/** The interface handle is used for communication between the 	**/
	/** application and PCEP. The members of the interface handle 	**/
	/** data structure are filled in this function and returned to 	**/
	/** the application. The application will include this handle 	**/
	/** for future calls to the PCEP layer.								**/
	FIN (pcep_app_register (app_module_id));

	/* Make the app module ID as a part of this interface			*/
	app_pcep_intf_handle.app_mod_id = app_module_id; 

	/* Initialize the PCEP module ID to INVALID. It will contain a	*/
	/* valid value after discovering the PCEP module ID.				*/
	app_pcep_intf_handle.pcep_mod_id = OPC_OBJID_INVALID;
	
	/* Node ID from the application module ID. This value is used 	*/
	/* for discovery of the PCEP module.								*/
	node_objid = op_topo_parent (app_module_id);

	/* Use process registry to find the PCEP module ID present in 	*/
	/* the same node as the applciation.							*/
    oms_pr_process_discover (OPC_OBJID_INVALID, &proc_record_handle_list,
                    "node objid",   OMSC_PR_OBJID,   node_objid,
                    "protocol", OMSC_PR_STRING, "pcep", OPC_NIL);

	/* There should be only one PCEP module.							*/
	if (op_prg_list_size (&proc_record_handle_list)) {
		/* Assume that there is only one PCEP module.				*/

		/* Get a handle to the PCEP process handle.					*/
		process_record_handle = (OmsT_Pr_Handle) op_prg_list_remove (&proc_record_handle_list, 
																	 OPC_LISTPOS_HEAD);

		/* Obtain the object ID of the module running PCEP.			*/
		oms_pr_attr_get (process_record_handle, "module objid", OMSC_PR_OBJID, &pcep_module_id);
	}
	else {
		/* There is no PCEP module. Return an invalid PCEP handle.	*/
		FRET (app_pcep_intf_handle);
	}

	/* Hold the value of the PCEP module ID in interface handle.		*/
	app_pcep_intf_handle.pcep_mod_id = pcep_module_id;
	
		
	FRET (app_pcep_intf_handle);
}


void pcep_path_computation_command_send(ApiT_Pcep_App_Handle *pcep_handler, Session_object *lsp_session_ptr,int capacity, Path_Comp_Params *params, int request_id ) {
// Call the PCEP module to issue a PCReq
	ApiT_Pcep_PC_Request *request;
	Ici* ici_ptr;
	
	FIN( pcep_path_computation_command_send(pcep_handler, lsp_session_ptr, capacity, params) );
	// Create the ICI pointer inside the interface hndl
	
	ici_ptr = op_ici_create ("MD_pcep_api_ici");

	request = (ApiT_Pcep_PC_Request*) prg_mem_alloc( sizeof(ApiT_Pcep_PC_Request) );
	request->lsp_id = lsp_session_ptr;
	request->pc_params = params;
	request->capacity = capacity;
	
	if ( op_ici_attr_set (ici_ptr, "ici data", request) == OPC_COMPCODE_FAILURE ||
		 op_ici_attr_set (ici_ptr, "request id", request_id) == OPC_COMPCODE_FAILURE ) {
		op_sim_end("PCEP API","pcep_path_computation_command_send()","cannot set the ICI","");
	}
	
	// Install the ICI and issue the LSP remove command.
    op_ici_install (ici_ptr);
    op_intrpt_force_remote (PCEPC_PATH_COMP_REQ, pcep_handler->pcep_mod_id);
	// Unistall the ici
	op_ici_install (OPC_NIL);
	FOUT;
}


void pcep_lsp_status_changed_notify(ApiT_Pcep_App_Handle *pcep_handler, Session_object *lsp_session_ptr, ERO_object *ero_ptr, Sender_tspec_object *sender_tspec,int upper_freq_indx,int lower_freq_indx, int capacity, LSP_Operational state) {
// informs the PCEP module that an LSP status changed
	ApiT_Pcep_PC_Report *report;
	Ici* ici_ptr;
	FIN( pcep_lsp_status_changed_notify(pcep_handler,lsp_session_ptr, ero_ptr) );
	
	report = (ApiT_Pcep_PC_Report*) prg_mem_alloc( sizeof(ApiT_Pcep_PC_Report) );
	report->lsp_id = lsp_session_ptr;
	report->ero_ptr = ero_ptr;
	report->upper_freq_indx = upper_freq_indx;
	report->lower_freq_indx = lower_freq_indx;
	report->capacity = capacity;
	report->state = state;
	report->sender_tspec = sender_tspec;
	
	
	ici_ptr = op_ici_create ("MD_pcep_api_ici");
	
	if (op_ici_attr_set (ici_ptr, "ici data", report) == OPC_COMPCODE_FAILURE) {
		op_sim_end("PCEP API","pcep_lsp_going_up()","cannot set the ICI","");
	}
	// Install the ICI and issue the LSP remove command.
    op_ici_install (ici_ptr);
    op_intrpt_schedule_remote (op_sim_time(),PCEPC_REPORT, pcep_handler->pcep_mod_id);
	// Unistall the ici
	op_ici_install (OPC_NIL);
	FOUT;
}


void pcep_api_pc_report_destroy(ApiT_Pcep_PC_Report *report) {
// destroy the PC Report
	FIN( pcep_api_pc_report_destroy(report) );
		ERO_object_destroy(report->ero_ptr);
		session_object_destroy(report->lsp_id);
		sender_tspec_object_destroy(report->sender_tspec);
		prg_mem_free (report);
	FOUT;
}


static ApiT_Pcep_LSP* api_lsp_object_copy(ApiT_Pcep_LSP* api_lsp_obj) {
// api lsp object copy function
	ApiT_Pcep_LSP *copied_obj;
	
	FIN( api_lsp_object_copy(api_lsp_obj) );
	copied_obj = prg_mem_copy_create( api_lsp_obj, sizeof(ApiT_Pcep_LSP) );
	copied_obj->lsp_id = session_object_copy(api_lsp_obj->lsp_id);
	copied_obj->ero_object_ptr = ERO_object_copy(api_lsp_obj->ero_object_ptr);
	copied_obj->sender_tspec_ptr = sender_tspec_object_copy(api_lsp_obj->sender_tspec_ptr);
	copied_obj->sugg_label_ptr = suggested_label_object_copy(api_lsp_obj->sugg_label_ptr);
	copied_obj->carriers_vptr =  (PRGC_NIL==api_lsp_obj->carriers_vptr) ? PRGC_NIL : prg_vector_copy(api_lsp_obj->carriers_vptr);
	copied_obj->sugg_ingress_tx_trans_id_ptr = transponder_id_copy(api_lsp_obj->sugg_ingress_tx_trans_id_ptr);
	copied_obj->sugg_ingress_rx_trans_id_ptr = transponder_id_copy(api_lsp_obj->sugg_ingress_rx_trans_id_ptr);
	copied_obj->sugg_egress_tx_trans_id_ptr = transponder_id_copy(api_lsp_obj->sugg_egress_tx_trans_id_ptr);
	copied_obj->sugg_egress_rx_trans_id_ptr = transponder_id_copy(api_lsp_obj->sugg_egress_rx_trans_id_ptr);
	copied_obj->ingress_tx_carrier_ids_vptr =  (PRGC_NIL==api_lsp_obj->ingress_tx_carrier_ids_vptr) ? PRGC_NIL : prg_vector_copy(api_lsp_obj->ingress_tx_carrier_ids_vptr);
	copied_obj->ingress_rx_carrier_ids_vptr =  (PRGC_NIL==api_lsp_obj->ingress_rx_carrier_ids_vptr) ? PRGC_NIL : prg_vector_copy(api_lsp_obj->ingress_rx_carrier_ids_vptr);
	copied_obj->egress_tx_carrier_ids_vptr =  (PRGC_NIL==api_lsp_obj->egress_tx_carrier_ids_vptr) ? PRGC_NIL : prg_vector_copy(api_lsp_obj->egress_tx_carrier_ids_vptr);
	copied_obj->egress_rx_carrier_ids_vptr =  (PRGC_NIL==api_lsp_obj->egress_rx_carrier_ids_vptr) ? PRGC_NIL : prg_vector_copy(api_lsp_obj->egress_rx_carrier_ids_vptr);
	FRET(copied_obj);
}

static void api_lsp_object_destroy(ApiT_Pcep_LSP* api_lsp_obj) {
// api lsp object destroy function
	FIN( api_lsp_object_destroy(api_lsp_obj) );
	ERO_object_destroy(api_lsp_obj->ero_object_ptr);
	session_object_destroy(api_lsp_obj->lsp_id);
	sender_tspec_object_destroy(api_lsp_obj->sender_tspec_ptr);
	suggested_label_object_destroy(api_lsp_obj->sugg_label_ptr);
	if (PRGC_NIL != api_lsp_obj->carriers_vptr)
		prg_vector_destroy(api_lsp_obj->carriers_vptr,PRGC_TRUE);
	transponder_id_destroy(api_lsp_obj->sugg_ingress_tx_trans_id_ptr);
	transponder_id_destroy(api_lsp_obj->sugg_ingress_rx_trans_id_ptr);
	transponder_id_destroy(api_lsp_obj->sugg_egress_tx_trans_id_ptr);
	transponder_id_destroy(api_lsp_obj->sugg_egress_rx_trans_id_ptr);
	if (PRGC_NIL != api_lsp_obj->ingress_tx_carrier_ids_vptr)
		prg_vector_destroy(api_lsp_obj->ingress_tx_carrier_ids_vptr,PRGC_TRUE);
	if (PRGC_NIL != api_lsp_obj->ingress_rx_carrier_ids_vptr)
		prg_vector_destroy(api_lsp_obj->ingress_rx_carrier_ids_vptr,PRGC_TRUE);
	if (PRGC_NIL != api_lsp_obj->egress_tx_carrier_ids_vptr)
		prg_vector_destroy(api_lsp_obj->egress_tx_carrier_ids_vptr,PRGC_TRUE);
	if (PRGC_NIL != api_lsp_obj->egress_rx_carrier_ids_vptr)
		prg_vector_destroy(api_lsp_obj->egress_rx_carrier_ids_vptr,PRGC_TRUE);
	prg_mem_free (api_lsp_obj);
	FOUT;
}

static PrgT_Compcode api_lsp_object_vector_destroy(void * elem)	{
	FIN (api_lsp_object_vector_destroy(elem));
	api_lsp_object_destroy(elem);
	FRET (PrgC_Compcode_Success);
}

static void* api_lsp_object_vector_copy(void * elem)	{
	FIN (api_lsp_object_vector_copy(elem));	
	FRET ( api_lsp_object_copy(elem) );
}


ApiT_Pcep_PC_Reply* api_pc_reply_create(Pcep_PcRep_Msg_Body const* pcrep_msg_body_ptr) {
// This function creates the path computation reply structure
// used to communicate with other modules 
	ApiT_Pcep_PC_Reply	*pc_reply;
	unsigned int vsize,i;
	Pcep_LSP_Object *lsp_obj;
	ApiT_Pcep_LSP	*api_lsp_obj;
	
	FIN( api_pc_reply_create(lsps_vptr) );
	
	pc_reply = (ApiT_Pcep_PC_Reply*) prg_mem_alloc( sizeof(ApiT_Pcep_PC_Reply) );
	
	if ( OPC_NIL != pcrep_msg_body_ptr->no_path_object_ptr) {
		pc_reply->result_code = pcrep_msg_body_ptr->no_path_object_ptr->nature_of_issue;
	}
	else {
		pc_reply->result_code = COMPUTATION_OK;
	}
	
	if ( OPC_NIL == pcrep_msg_body_ptr->lsps_vptr) {
		pc_reply->lsps_vptr = OPC_NIL;
		FRET(pc_reply);
	}
	
	vsize = prg_vector_size(pcrep_msg_body_ptr->lsps_vptr);
	
	// create the vector of LSPs
	pc_reply->lsps_vptr = prg_vector_create(vsize, api_lsp_object_vector_destroy, api_lsp_object_vector_copy);
	
	for (i=0; i<vsize; i++) {
		lsp_obj = prg_vector_access(pcrep_msg_body_ptr->lsps_vptr,i);
		api_lsp_obj = (ApiT_Pcep_LSP*) prg_mem_alloc( sizeof(ApiT_Pcep_LSP) );
		api_lsp_obj->lsp_id = lsp_obj->lsp_id;
		api_lsp_obj->ero_object_ptr = lsp_obj->ero_ptr;
		api_lsp_obj->sender_tspec_ptr = lsp_obj->sender_tspec;
		api_lsp_obj->sugg_label_ptr = lsp_obj->sugg_label_ptr;
		api_lsp_obj->capacity = lsp_obj->capacity;
		api_lsp_obj->carriers_vptr = lsp_obj->carriers_vptr;

		api_lsp_obj->sugg_ingress_tx_trans_id_ptr = lsp_obj->sugg_ingress_tx_trans_id_ptr;
		api_lsp_obj->sugg_ingress_rx_trans_id_ptr = lsp_obj->sugg_ingress_rx_trans_id_ptr;
		api_lsp_obj->sugg_egress_tx_trans_id_ptr = lsp_obj->sugg_egress_tx_trans_id_ptr;
		api_lsp_obj->sugg_egress_rx_trans_id_ptr = lsp_obj->sugg_egress_rx_trans_id_ptr;

		api_lsp_obj->ingress_tx_carrier_ids_vptr = lsp_obj->ingress_tx_carrier_ids_vptr;
		api_lsp_obj->ingress_rx_carrier_ids_vptr = lsp_obj->ingress_rx_carrier_ids_vptr;
		api_lsp_obj->egress_tx_carrier_ids_vptr = lsp_obj->egress_tx_carrier_ids_vptr;
		api_lsp_obj->egress_rx_carrier_ids_vptr = lsp_obj->egress_rx_carrier_ids_vptr;
		// append the new api_lsp_obj to the vector
		prg_vector_append(pc_reply->lsps_vptr,api_lsp_obj);
		
		// remove pointer ownership
		lsp_obj->lsp_id = OPC_NIL;
		lsp_obj->ero_ptr = OPC_NIL;
		lsp_obj->sender_tspec = OPC_NIL;
		lsp_obj->sugg_label_ptr = OPC_NIL;
		lsp_obj->carriers_vptr = OPC_NIL;
		lsp_obj->sugg_ingress_tx_trans_id_ptr = OPC_NIL;
		lsp_obj->sugg_ingress_rx_trans_id_ptr = OPC_NIL;
		lsp_obj->sugg_egress_tx_trans_id_ptr = OPC_NIL;
		lsp_obj->sugg_egress_rx_trans_id_ptr = OPC_NIL;
		
		lsp_obj->ingress_tx_carrier_ids_vptr = OPC_NIL;
		lsp_obj->ingress_rx_carrier_ids_vptr = OPC_NIL;
		lsp_obj->egress_tx_carrier_ids_vptr = OPC_NIL;
		lsp_obj->egress_rx_carrier_ids_vptr = OPC_NIL;
	}
	// destroy the input vector of Pcep_LSP_Object
	prg_vector_destroy(pcrep_msg_body_ptr->lsps_vptr,PRGC_TRUE);
	FRET(pc_reply);
}


void api_pc_reply_destroy(ApiT_Pcep_PC_Reply *pc_reply) {
	FIN( api_pc_reply_destroy(pc_reply) );
	prg_vector_destroy(pc_reply->lsps_vptr,PRGC_TRUE);
	prg_mem_free (pc_reply);
	FOUT;
}


