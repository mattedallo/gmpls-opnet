#ifndef	_MD_LSP_H_INCLUDED_
	#define _MD_LSP_H_INCLUDED_

#include	<opnet.h>
#include	"MD_rsvp_msg_util.h"
#include	"MD_utils.h"


typedef enum Lsp_Status{
		LSP_COMPUTED = 0,
		LSP_ROUTING_BLOCKING,				// generic routing blocking
		LSP_ROUTING_BLOCKING_BW_LACK,
		LSP_ROUTING_BLOCKING_FRAGMENTATION,
		LSP_ROUTING_BLOCKING_MW_TUNABILITY,
		LSP_FORWARD_BLOCKING,				// generic forward blocking
		LSP_FORWARD_BLOCKING_BW_LACK,		/* lack of (total) available bandwith on the link */
		LSP_FORWARD_BLOCKING_FRAGMENTATION, /* enough BW on the link but not enough contiguous slices to satisfy the demand (fragmentation) */
		LSP_FORWARD_BLOCKING_CONTINUITY,	/* enough BW on the link but no spectrum continuity among the links of the path (spectrum continuity constraint)*/
		LSP_BACKWARD_BLOCKING,
		LSP_PROVISIONING,
		LSP_ESTABLISHED,
		LSP_RELEASED,
		LSP_FAILED,
		LSP_SUGGESTED_LABEL_BLOCKING,
		LSP_NONE
} Lsp_Status;


typedef struct LSP {
	Session_object*				session_ptr;
	ERO_object*					ero_ptr;
	Sender_tspec_object*		sender_tspec_ptr;
	Suggested_label_object*		suggested_label_ptr;
	Generalized_label_object*	generalized_label_ptr;
	unsigned int				capacity; // capacity in Gbps allocated for this lsp
	double						born_instant;
	double						computation_time;
	double						forward_signaling_time;
	double						backward_signaling_time;
	double						computation_request_time;
	double						provisioning_time;
	double						failure_notification_time;
	Lsp_Status					status;
} LSP;


LSP* lsp_empty_create();
void lsp_destroy(LSP *lsp, Boolean deep_destroy);
LSP* lsp_copy(LSP *lsp_ptr, Boolean deep_copy);
LSP* lsps_list_remove(PrgT_List *list_ptr, Session_object const * const session_ptr);
OpT_uInt32* lsp_lsp_connection_key_get(LSP* lsp_ptr);

typedef enum LSP_Connection_Status{
	LSP_CONNECTION_STATUS_NONE = 0,
	
} LSP_Connection_Status;


// a group of LSP (sub-LSPs) under the same tunnelID
typedef struct LSP_Connection { 
	InetT_Address			*source_addr_ptr;
	InetT_Address			*dest_addr_ptr;
	OpT_uInt32				tunnel_id;
	OpT_uInt32				capacity_demand;
	OpT_uInt32				capacity_allocated;
	double					born_instant;
	LSP_Connection_Status	status;
	PrgT_List 				*lsps_lptr; //list of LSPs related to the connection
} LSP_Connection;

LSP_Connection*	lsp_connection_create(InetT_Address const* source_addr_ptr, InetT_Address const* dest_addr_ptr, OpT_uInt32 tunnel_id, OpT_uInt32 capacity_demand);
LSP_Connection* lsp_connection_from_lsp_create(LSP* lsp_ptr); //create the connection and insert the lsp pointer
LSP* lsp_connection_lsp_get(LSP_Connection* lsp_connection_ptr, Session_object const * const session_ptr);
MD_Compcode lsp_connection_lsp_insert_unique(LSP_Connection* lsp_connection_ptr, LSP* lsp_ptr);
LSP* lsp_connection_lsp_remove(LSP_Connection* lsp_connection_ptr, Session_object const * const session_ptr);

#endif
