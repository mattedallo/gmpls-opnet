#include  <math.h>
#include  <prg_graph.h>
#include  <prg_bin_hash.h>
#include  "MD_path_computation_src.h"
#include  "MD_TED.h"
#include  "MD_spectrum.h"


PrgT_Graph_Vertex* topology_vertex_by_id_get(Ospf_Topology const*topology_ptr,InetT_Address const* address_ptr) {
  PrgT_Graph_Vertex *vertex_ptr;
  FIN( topology_vertex_by_id_get(topology,address_ptr) );
  vertex_ptr = (PrgT_Graph_Vertex *)prg_bin_hash_table_item_get(topology_ptr->vertices_htable_ptr, inet_address_addr_ptr_get_const(address_ptr));
  FRET(vertex_ptr);
}



PrgT_Graph_Edge* topology_edge_between_addresses_get(Ospf_Topology const* const topology_ptr, InetT_Address const* const from_addr_ptr, InetT_Address const* const to_addr_ptr) {
  PrgT_Graph_Vertex *vertex_a_ptr, *vertex_b_ptr;
  PrgT_Graph_Edge   *edge_ab_ptr;
  
  FIN(topology_edge_between_addresses_get(topology_ptr));
    vertex_a_ptr = (PrgT_Graph_Vertex *)prg_bin_hash_table_item_get(topology_ptr->vertices_htable_ptr, inet_address_addr_ptr_get_const(from_addr_ptr));
    if (vertex_a_ptr == PRGC_NIL) {
      FRET(PRGC_NIL);
    }
    vertex_b_ptr = (PrgT_Graph_Vertex *)prg_bin_hash_table_item_get(topology_ptr->vertices_htable_ptr,inet_address_addr_ptr_get_const(to_addr_ptr));
    if (vertex_b_ptr == PRGC_NIL) {
      FRET(PRGC_NIL);
    }
    edge_ab_ptr = prg_graph_edge_exists (topology_ptr->graph_ptr, vertex_a_ptr, vertex_b_ptr, PrgC_Graph_Edge_Simplex);
  FRET(edge_ab_ptr);  
}

// Return a pointer to the TED state
// of the given vertex
TE_vertex_state* te_vertex_state_get(Ospf_Topology *topology_ptr,
                                     PrgT_Graph_Vertex *vertex_ptr) {
  FIN( te_vertex_state_get(topology_ptr, vertex_ptr) );
  FRET(prg_vertex_client_state_get (vertex_ptr,
                                    topology_ptr->graph_state_handler));
}


InetT_Address* ted_vertex_address(Ospf_Topology *topology_ptr,
                                  PrgT_Graph_Vertex *vertex_ptr) {
  TE_vertex_state* vertex_state_ptr;
  FIN(ted_vertex_address(topology_ptr, vertex_ptr));
  vertex_state_ptr = te_vertex_state_get(topology_ptr,vertex_ptr);
  FRET(&(vertex_state_ptr->vertex_node_addr));
}

MD_Compcode ted_edge_frequency_slot_release(Ospf_Topology const* const topology_ptr, PrgT_Graph_Edge* edge_ptr, int lower_index, int upper_index) {
  unsigned long     i,size;
  Ospf_Label_Range  *free_slot_ptr,*pre_free_slot_ptr,*new_free_slot_ptr;
  TE_edge_state   *edge_state_ptr;
  
  FIN( ted_edge_frequency_slot_release(topology_ptr, edge_ptr, lower_index, upper_index) );
  // get the TE informations on the edge //
  edge_state_ptr = prg_edge_client_state_get (edge_ptr, topology_ptr->graph_state_handler);
  if (PRGC_NIL == edge_state_ptr) FRET(MD_Compcode_Failure);
  size = prg_vector_size(edge_state_ptr->available_slots_vptr);
  if (0 == size) {
    new_free_slot_ptr = (Ospf_Label_Range*) prg_mem_alloc(sizeof(Ospf_Label_Range));
    new_free_slot_ptr->lower_freq_indx = lower_index;
    new_free_slot_ptr->upper_freq_indx = upper_index;
    prg_vector_append (edge_state_ptr->available_slots_vptr, new_free_slot_ptr);
    edge_state_ptr->tot_available_freq_slices += (upper_index - lower_index)/2;
    FRET(MD_Compcode_Success);
  }
  
  pre_free_slot_ptr = PRGC_NIL;
  
  /*{//DEBUG
    printf("prima: ");
    for (i=0; i<prg_vector_size(edge_state_ptr->available_slots_vptr); ++i) {
      free_slot_ptr = (Ospf_Label_Range *)prg_vector_access (edge_state_ptr->available_slots_vptr, i);
      printf("[%d,%d]",free_slot_ptr->lower_freq_indx,free_slot_ptr->upper_freq_indx);
    }
    printf("\n");
  }*/
  for (i=0; i<size; ++i) {
    free_slot_ptr = (Ospf_Label_Range *)prg_vector_access (edge_state_ptr->available_slots_vptr, i);
    if (upper_index <= free_slot_ptr->lower_freq_indx) {
      if (upper_index == free_slot_ptr->lower_freq_indx) {
        free_slot_ptr->lower_freq_indx = lower_index;
        if ( pre_free_slot_ptr != PRGC_NIL) {
          if ( pre_free_slot_ptr->upper_freq_indx > lower_index) {
            op_sim_error(OPC_SIM_ERROR_ABORT, "overlapping", "");
          }
          if ( pre_free_slot_ptr->upper_freq_indx == lower_index) {
            pre_free_slot_ptr->upper_freq_indx = free_slot_ptr->upper_freq_indx;
            prg_vector_remove (edge_state_ptr->available_slots_vptr, i);
            prg_mem_free(free_slot_ptr);
          }
        }
      }
      else {
        if ( pre_free_slot_ptr != PRGC_NIL) {
          if ( pre_free_slot_ptr->upper_freq_indx > lower_index) {
            op_sim_error(OPC_SIM_ERROR_ABORT, "overlapping", "");
          }
          if ( pre_free_slot_ptr->upper_freq_indx == lower_index) {
            pre_free_slot_ptr->upper_freq_indx = upper_index;
          }
          else{
            //new free slot needed
            new_free_slot_ptr = (Ospf_Label_Range*) prg_mem_alloc(sizeof(Ospf_Label_Range));
            new_free_slot_ptr->lower_freq_indx = lower_index;
            new_free_slot_ptr->upper_freq_indx = upper_index;
            prg_vector_insert (edge_state_ptr->available_slots_vptr, new_free_slot_ptr, i);
          }
        }
        else{
          //new free slot needed
          new_free_slot_ptr = (Ospf_Label_Range*) prg_mem_alloc(sizeof(Ospf_Label_Range));
          new_free_slot_ptr->lower_freq_indx = lower_index;
          new_free_slot_ptr->upper_freq_indx = upper_index;
          prg_vector_insert (edge_state_ptr->available_slots_vptr, new_free_slot_ptr, i);
          
        }
      }
      
      /*{//DEBUG
        printf("dopo: ");
        for (i=0; i<prg_vector_size(edge_state_ptr->available_slots_vptr); ++i) {
          free_slot_ptr = (Ospf_Label_Range *)prg_vector_access (edge_state_ptr->available_slots_vptr, i);
          printf("[%d,%d]",free_slot_ptr->lower_freq_indx,free_slot_ptr->upper_freq_indx);
        }
        printf("\n");
      }*/
      edge_state_ptr->tot_available_freq_slices += (upper_index - lower_index)/2;
      FRET(MD_Compcode_Success);
    }
    else {
      pre_free_slot_ptr = free_slot_ptr;
    }
  }
  
  if (pre_free_slot_ptr != PRGC_NIL) {
    if ( pre_free_slot_ptr->upper_freq_indx < lower_index) {
      new_free_slot_ptr = (Ospf_Label_Range*) prg_mem_alloc(sizeof(Ospf_Label_Range));
      new_free_slot_ptr->lower_freq_indx = lower_index;
      new_free_slot_ptr->upper_freq_indx = upper_index;
      prg_vector_append (edge_state_ptr->available_slots_vptr, new_free_slot_ptr);
      
      /*{//DEBUG
        printf("dopo: ");
        for (i=0; i<prg_vector_size(edge_state_ptr->available_slots_vptr); ++i) {
          free_slot_ptr = (Ospf_Label_Range *)prg_vector_access (edge_state_ptr->available_slots_vptr, i);
          printf("[%d,%d]",free_slot_ptr->lower_freq_indx,free_slot_ptr->upper_freq_indx);
        }
        printf("\n");
      }*/
      
      edge_state_ptr->tot_available_freq_slices += (upper_index - lower_index)/2;
      FRET(MD_Compcode_Success);
    }
    else if ( pre_free_slot_ptr->upper_freq_indx == lower_index) {
      pre_free_slot_ptr->upper_freq_indx = upper_index;
      
      /*{//DEBUG
        printf("dopo: ");
        for (i=0; i<prg_vector_size(edge_state_ptr->available_slots_vptr); ++i) {
          free_slot_ptr = (Ospf_Label_Range *)prg_vector_access (edge_state_ptr->available_slots_vptr, i);
          printf("[%d,%d]",free_slot_ptr->lower_freq_indx,free_slot_ptr->upper_freq_indx);
        }
        printf("\n");
      }*/
      
      edge_state_ptr->tot_available_freq_slices += (upper_index - lower_index)/2;
      FRET(MD_Compcode_Success);
    }
  }
  
  op_sim_error(OPC_SIM_ERROR_ABORT, "slot to release not found", "");
  FRET(MD_Compcode_Failure);
}


/*
static void paths_htable_tuple_destroy(Paths_htable_tuple *tuple) {
  FIN (paths_htable_tuple_destroy());
  if (tuple == OPC_NIL) FOUT;
  if (tuple->te_paths_vptr != OPC_NIL) {
    prg_vector_destroy(tuple->te_paths_vptr,PRGC_TRUE);
  }
  prg_mem_free(tuple);
  FOUT;
}*/


static void precomputed_paths_destroy(PrgT_Vector *routes_vptr) {
  FIN(precomputed_paths_destroy(routes_vptr));
  prg_vector_destroy (routes_vptr, PRGC_TRUE);
  FOUT;
}

// destroy the stored precomputed paths
void topology_computed_paths_destroy(Ospf_Topology *topology) {
  FIN(topology_computed_paths_destroy(topology));
  if ( topology == OPC_NIL) FOUT;
  if (topology->paths_htable_ptr != OPC_NIL) {
    //prg_bin_hash_table_destroy (topology->paths_htable_ptr,paths_htable_tuple_destroy);
    prg_bin_hash_table_destroy (topology->paths_htable_ptr,precomputed_paths_destroy);
    topology->paths_htable_ptr = OPC_NIL;
  }
  FOUT;
}

PrgT_Bin_Hash_Table * ted_node_transponders_get(Ospf_Topology *topology, InetT_Address const*address_ptr) {
  PrgT_Graph_Vertex *vertex_ptr;
  TE_vertex_state *vertex_state_ptr;
  FIN(ted_node_transponders_get(topology,address_ptr));
  if (PRGC_NIL == address_ptr || PRGC_NIL==topology) FRET(PRGC_NIL);
  vertex_ptr = prg_bin_hash_table_item_get(topology->vertices_htable_ptr,inet_address_addr_ptr_get_const(address_ptr));
  if (PRGC_NIL == vertex_ptr) FRET(PRGC_NIL);
  vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get(vertex_ptr, topology->graph_state_handler);
  if (PRGC_NIL == vertex_state_ptr) FRET(PRGC_NIL);
  FRET(vertex_state_ptr->transponders_htptr);
}

OpT_uInt32 ted_node_free_carriers_num_get(Ospf_Topology *topology,
                                          InetT_Address const* address_ptr) {
  PrgT_Bin_Hash_Table *transponders_htptr;
  PrgT_List     *transponders_lptr;
  PrgT_List_Cell  *cell_ptr;
  Transponder   *transponder_ptr;
  int   num_free_carriers;
  
  FIN(ted_node_free_carriers_num_get(topology,address_ptr));
  num_free_carriers = 0;
  transponders_htptr = ted_node_transponders_get(topology,address_ptr);
  transponders_lptr = prg_bin_hash_table_item_list_get(transponders_htptr);
  cell_ptr = prg_list_head_cell_get (transponders_lptr);
  while (cell_ptr != PRGC_NIL) {
    transponder_ptr = prg_list_cell_data_get (cell_ptr);
    num_free_carriers += transponder_num_free_carriers(transponder_ptr);
    cell_ptr = prg_list_cell_next_get (cell_ptr);
  }
  prg_list_destroy(transponders_lptr,PRGC_FALSE);
  FRET(num_free_carriers);
}



Transponder* ted_node_transponder_by_id_get(Ospf_Topology *topology,
                                            InetT_Address *address_ptr,
                                            OpT_uInt32 id) {
  PrgT_Bin_Hash_Table *node_transponders_htptr;
  FIN( ted_node_transponder_by_id_get(topology, address_ptr, id) );
  node_transponders_htptr = ted_node_transponders_get(topology,address_ptr);
  if ( node_transponders_htptr == PRGC_NIL) FRET(PRGC_NIL);
  FRET(prg_bin_hash_table_item_get(node_transponders_htptr, &id));
}




OpT_uInt32 ted_vertex_transponders_number(Ospf_Topology *topology_ptr,
                                          PrgT_Graph_Vertex *vertex_ptr) {
  TE_vertex_state   *vertex_state_ptr;
  FIN(ted_vertex_transponders_number(topology_ptr, vertex_ptr));
  vertex_state_ptr = te_vertex_state_get(topology_ptr,vertex_ptr);
  FRET(prg_bin_hash_table_num_items_get(vertex_state_ptr->transponders_htptr));
}


InetT_Address* topology_random_network_address_get(Ospf_Topology *topology, const InetT_Address *excluded) {
  Distribution  *uniform_int_dist;
  InetT_Address *destination_addr;
  int       number_of_nodes,indx;
  
  FIN( random_network_address_get(excluded) );
  
  if (topology->node_address_vptr == PRGC_NIL) {
    op_sim_error(OPC_SIM_ERROR_ABORT,"vector of addresses is empty","");
  }
  number_of_nodes = prg_vector_size(topology->node_address_vptr);
  
  // Load the uniform integer distribution used in extracting the node//
  uniform_int_dist = op_dist_load("uniform_int", 0,  number_of_nodes - 1 );
  
  // extract randomly a node address  //
  indx = op_dist_outcome (uniform_int_dist);
  destination_addr = (InetT_Address*) prg_vector_access (topology->node_address_vptr, indx);
  while ( inet_address_ptr_equal(destination_addr,excluded) && number_of_nodes > 1) {
    indx = op_dist_outcome (uniform_int_dist);
    destination_addr =  prg_vector_access (topology->node_address_vptr, indx);
  }
  
  destination_addr = inet_address_copy_dynamic(destination_addr);
  op_dist_unload (uniform_int_dist);
  
  FRET(destination_addr);
}



void te_vertex_state_destroy(TE_vertex_state *vertex_state_ptr) {
  FIN( te_vertex_state_destroy(vertex_state_ptr) );
  if (vertex_state_ptr == PRGC_NIL) FOUT;
  // destroy the table but not the transponder inside
  if (vertex_state_ptr->transponders_htptr != PRGC_NIL)
    prg_bin_hash_table_destroy (vertex_state_ptr->transponders_htptr , PRGC_NIL);
  inet_address_destroy(vertex_state_ptr->vertex_node_addr);
  prg_mem_free(vertex_state_ptr);
  FOUT;
}

PrgT_Graph_Vertex* topology_new_fake_transponder_vertex(Ospf_Topology *topology, Transponder *transponder_ptr) {
  PrgT_Graph_Vertex   *vertex_ptr;
  OpT_uInt32        transponder_id;
  TE_vertex_state     *vertex_state_ptr;
  
  FIN ( topology_new_fake_transponder_vertex(topology,transponder_ptr) );
    
    vertex_ptr = prg_graph_vertex_insert (topology->graph_ptr);
    
    vertex_state_ptr = (TE_vertex_state*) prg_mem_alloc( sizeof(TE_vertex_state));
    vertex_state_ptr->vertex_node_addr = INETC_ADDRESS_INVALID;
    vertex_state_ptr->transponders_htptr = prg_bin_hash_table_create(0, sizeof(OpT_uInt32)); // just 1(2^0) transponder will be stored 
    prg_vertex_client_state_set(vertex_ptr, topology->graph_state_handler, vertex_state_ptr);
    
    transponder_id = transponder_id_get(transponder_ptr);
    // add the transponder to the hash table using the transponder id as key
    prg_bin_hash_table_item_insert(vertex_state_ptr->transponders_htptr, &transponder_id, transponder_ptr, PRGC_NIL);
      
  FRET (vertex_ptr);
}

Transponder* topology_fake_transponder_vertex_transponder_get(Ospf_Topology *topology_ptr,
                                                              PrgT_Graph_Vertex *vertex_ptr) {
  TE_vertex_state   *vertex_state_ptr;
  PrgT_List       *transponders_lptr;
  Transponder     *transponder_ptr;
  FIN( topology_fake_transponder_vertex_transponder_get(topology_ptr,vertex_ptr) );
  vertex_state_ptr = prg_vertex_client_state_get (vertex_ptr, topology_ptr->graph_state_handler);
  transponders_lptr = prg_bin_hash_table_item_list_get (vertex_state_ptr->transponders_htptr);
  transponder_ptr = prg_list_access (transponders_lptr, PRGC_LISTPOS_HEAD);
  prg_list_destroy (transponders_lptr, PRGC_FALSE);
  FRET(transponder_ptr);
}
  

static PrgT_Bin_Hash_Table* fake_vertices_table_create() {
  PrgT_Bin_Hash_Table *htptr;
  unsigned int num_nodes;
  FIN(fake_vertices_table_create());
  num_nodes = op_topo_object_count (OPC_OBJTYPE_NODE_FIX);
  htptr = prg_bin_hash_table_create(log(num_nodes+1)/log(2), sizeof(OpT_uInt32));
  FRET(htptr);
}


static void fake_vertices_table_insert(Ospf_Topology * topology_ptr,
                                       PrgT_Graph_Vertex * vertex_ptr,
                                       InetT_Address const* node_addr_ptr) {

  FIN( fake_vertices_table_insert(topology_ptr,vertex_ptr,node_addr_ptr));
  if (PRGC_NIL == topology_ptr->fake_vertices_htptr) {
      topology_ptr->fake_vertices_htptr = fake_vertices_table_create();
  }
  // save the fake vertex into the hash table of fake vertices
  prg_bin_hash_table_item_insert(topology_ptr->fake_vertices_htptr,
                                 inet_address_addr_ptr_get_const(node_addr_ptr),
                                 vertex_ptr, PRGC_NIL);
  FOUT;

}




// Create a fake vertex related to a node address
// and keep track of it in a fake_vertices_table
PrgT_Graph_Vertex* topology_fake_vertex_create(Ospf_Topology *topology_ptr,
                                               InetT_Address const* node_addr_ptr) {
  PrgT_Graph_Vertex   *vertex_ptr;
  TE_vertex_state     *vertex_state_ptr;
  FIN ( topology_fake_vertex_create(topology_ptr,node_addr_ptr) );
    vertex_ptr = prg_graph_vertex_insert (topology_ptr->graph_ptr);
    vertex_state_ptr = (TE_vertex_state*) prg_mem_alloc( sizeof(TE_vertex_state));
    vertex_state_ptr->vertex_node_addr = INETC_ADDRESS_INVALID;
    vertex_state_ptr->transponders_htptr = PRGC_NIL;
    prg_vertex_client_state_set(vertex_ptr, topology_ptr->graph_state_handler, vertex_state_ptr);
    fake_vertices_table_insert(topology_ptr, vertex_ptr, node_addr_ptr);
  FRET (vertex_ptr);
}


// get a fake vertex from the table
// or create and store it if not present
PrgT_Graph_Vertex* topology_fake_vertex_get(Ospf_Topology * topology_ptr,
                                            InetT_Address const* node_addr_ptr) {
  FIN(topology_fake_vertex_get(topology_ptr,node_addr_ptr));
  if (topology_ptr->fake_vertices_htptr == PRGC_NIL) FRET(PRGC_NIL);
  FRET(prg_bin_hash_table_item_get(topology_ptr->fake_vertices_htptr,
                                   inet_address_addr_ptr_get_const(node_addr_ptr))); 
}


// get a fake vertex from the table
// or create and store it if not present
PrgT_Graph_Vertex* topology_fake_vertex_get_or_create(Ospf_Topology * topology_ptr,
                                                      InetT_Address const* node_addr_ptr) {
  PrgT_Graph_Vertex* vertex_ptr;
  FIN(topology_fake_vertex_get_or_create(topology_ptr,node_addr_ptr));
  vertex_ptr = topology_fake_vertex_get(topology_ptr,node_addr_ptr);
  if (PRGC_NIL==vertex_ptr) {
    vertex_ptr = topology_fake_vertex_create(topology_ptr,node_addr_ptr);
  }
  FRET(vertex_ptr);
}



TE_edge_state * te_edge_state_create(double metric) {
  TE_edge_state *te_edge_state_ptr;
  FIN( te_edge_state_create(te_edge_state_ptr) );
  te_edge_state_ptr = (TE_edge_state*) prg_mem_alloc( sizeof(TE_edge_state));
  te_edge_state_ptr->available_slots_vptr = PRGC_NIL;
  te_edge_state_ptr->spectrum_map_ptr = PRGC_NIL;
  te_edge_state_ptr->tot_available_freq_slices = 0;
  te_edge_state_ptr->metric = metric;
  FRET(te_edge_state_ptr);
}

void te_edge_state_destroy(TE_edge_state *te_edge_state_ptr) {
  FIN( te_edge_state_destroy(te_edge_state_ptr) );
  if (PRGC_NIL == te_edge_state_ptr ) FOUT;
  if (PRGC_NIL != te_edge_state_ptr->available_slots_vptr)
    prg_vector_destroy(te_edge_state_ptr->available_slots_vptr, PRGC_TRUE);
  if (PRGC_NIL != te_edge_state_ptr->spectrum_map_ptr)
    spectrum_map_destroy(te_edge_state_ptr->spectrum_map_ptr);
  prg_mem_free(te_edge_state_ptr);
  FOUT;
}


void te_edge_spectrum_map_set(Ospf_Topology *topology, PrgT_Graph_Edge *edge_ptr, Spectrum_Map* spectrum_map_ptr) {
  TE_edge_state *te_edge_state_ptr;
  FIN( te_edge_spectrum_map_set(topology, edge_ptr, spectrum_map_ptr) );
  te_edge_state_ptr = prg_edge_client_state_get (edge_ptr, topology->graph_state_handler);
  if (PRGC_NIL == te_edge_state_ptr) {
    /*te_edge_state_ptr = te_edge_state_create();
    prg_edge_client_state_set (edge_ptr, topology->graph_state_handler, te_edge_state_ptr);*/
	op_sim_error(OPC_SIM_ERROR_ABORT, "edge state does not exists", "");
  } else {
    spectrum_map_destroy(te_edge_state_ptr->spectrum_map_ptr);
  }
  te_edge_state_ptr->spectrum_map_ptr = spectrum_map_ptr;
  FOUT;
}
  

void te_edge_spectrum_map_compute(Ospf_Topology *topology_ptr, PrgT_Graph_Edge *edge_ptr,int required_slices) {
  // this function creates the spectrum map of the TE edge, 
  // based on the required number of slices.
  // In particular, in the spectrum map, 1 corresponds to the frequency indexes 
  // that can be used as central freq.
  TE_edge_state *te_edge_state_ptr;
  Ospf_Label_Range *free_slot_ptr;
  long j,size,last_av_slot,first_av_slot;
  int upper_index,lower_index;
  FIN( te_edge_spectrum_map_compute(te_edge_state_ptr) );
  
  te_edge_state_ptr = prg_edge_client_state_get (edge_ptr, topology_ptr->graph_state_handler);
  
  // destroy the previous spectrum map
  spectrum_map_destroy(te_edge_state_ptr->spectrum_map_ptr);
  
  if (PRGC_NIL == te_edge_state_ptr->available_slots_vptr) {
    te_edge_state_ptr->spectrum_map_ptr = PRGC_NIL;
    FOUT;
  }
  
  size = prg_vector_size(te_edge_state_ptr->available_slots_vptr);
  if (0 == size) {
    te_edge_state_ptr->spectrum_map_ptr = PRGC_NIL;
    FOUT;
  }
  
  // get the lower and upper indexes of the spectrum map, 
  // considering the lowest index of the first available slot (that can support the required_slices)
  // and the highest index of the last available slot of the edge
  for (j=0; j < size; ++j) {
    free_slot_ptr = prg_vector_access(te_edge_state_ptr->available_slots_vptr, j);
    if ((required_slices*2) <= (free_slot_ptr->upper_freq_indx - free_slot_ptr->lower_freq_indx)) {
     lower_index = free_slot_ptr->lower_freq_indx+required_slices;
     first_av_slot=j;
     break;
    }
  }
  for (j=size-1; j >= 0; --j) {
    free_slot_ptr = prg_vector_access(te_edge_state_ptr->available_slots_vptr, j);
    if ((required_slices*2) <= (free_slot_ptr->upper_freq_indx - free_slot_ptr->lower_freq_indx)) {
     upper_index = free_slot_ptr->upper_freq_indx-required_slices;
     last_av_slot=j;
     break;
    }
  }
  if (j<0) {
    te_edge_state_ptr->spectrum_map_ptr = PRGC_NIL;
    FOUT;
  }
  
  te_edge_state_ptr->spectrum_map_ptr = spectrum_map_create(lower_index,upper_index);

  if (te_edge_state_ptr->spectrum_map_ptr == PRGC_NIL) FOUT;
  
  spectrum_map_fully_busy_set(te_edge_state_ptr->spectrum_map_ptr);
  
  for (j=first_av_slot; j <= last_av_slot; ++j) {
    free_slot_ptr = (Ospf_Label_Range *) prg_vector_access (te_edge_state_ptr->available_slots_vptr, j);
    if ((required_slices*2) > (free_slot_ptr->upper_freq_indx - free_slot_ptr->lower_freq_indx)) continue;
    spectrum_map_free_range_set(te_edge_state_ptr->spectrum_map_ptr, 
                  free_slot_ptr->lower_freq_indx + required_slices, 
                  free_slot_ptr->upper_freq_indx - required_slices);
  }
  FOUT;
}

Spectrum_Map* te_edges_spectrum_sum(Ospf_Topology *topology, PrgT_Vector *edges_vptr) {
  // This function returns the sum of the spectrum
  // of all the edges inside the input vector.
  long i,size;
  TE_edge_state   *te_edge_state_ptr;
  PrgT_Graph_Edge *edge_ptr;
  Spectrum_Map  *spectrum_map_ptr;
  
  FIN( te_edges_spectrum_sum(edges_vptr) );
  edge_ptr = prg_vector_access(edges_vptr,0);
  te_edge_state_ptr = prg_edge_client_state_get (edge_ptr, topology->graph_state_handler);
  spectrum_map_ptr = spectrum_map_copy(te_edge_state_ptr->spectrum_map_ptr);
  //spectrum_map_print(spectrum_map_ptr);
  size = prg_vector_size(edges_vptr);
  for (i=1; i<size; ++i) {
    edge_ptr = prg_vector_access(edges_vptr,i);
    te_edge_state_ptr = prg_edge_client_state_get (edge_ptr, topology->graph_state_handler);
    spectrum_maps_sum(&spectrum_map_ptr,te_edge_state_ptr->spectrum_map_ptr);
    //spectrum_map_print(spectrum_map_ptr);
  }
  FRET(spectrum_map_ptr);
}


void topology_vertex_remove_and_destroy(Ospf_Topology *topology, PrgT_Graph_Vertex* vertex_ptr) {
  // This function remove and destroy a vertex from a graph
  // and also all the attached edges
  TE_vertex_state *vertex_state_ptr;
  TE_edge_state   *te_edge_state_ptr;
  PrgT_Graph_Edge *edge_ptr;
  PrgT_Vector   *edges_vptr;
  long      i;
  
  FIN ( topology_vertex_remove_and_destroy(topology,vertex_ptr) );
    vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get (vertex_ptr, topology->graph_state_handler);
    te_vertex_state_destroy(vertex_state_ptr);
    // get all the attached edges
    edges_vptr = prg_vertex_edges_get (vertex_ptr, PrgC_Graph_Element_Set_All);
    for (i=0; i<prg_vector_size(edges_vptr); ++i) {
      edge_ptr = prg_vector_access(edges_vptr,i);
      te_edge_state_ptr = prg_edge_client_state_set(edge_ptr, topology->graph_state_handler,PRGC_NIL);
      te_edge_state_destroy(te_edge_state_ptr);
      prg_graph_edge_remove (topology->graph_ptr, edge_ptr, PRGC_TRUE);
    }
    prg_vector_destroy(edges_vptr,PRGC_FALSE);
    // when calling remove on a vertex also all the attached edges will be automatically removed
    prg_graph_vertex_remove (topology->graph_ptr, vertex_ptr, PRGC_TRUE);
  FOUT;
}


void topology_vertex_address_print(Ospf_Topology const * const topology_ptr,
                                   PrgT_Graph_Vertex const * const vertex_ptr) {
  TE_vertex_state *vertex_state_ptr;
  char addr_str[INETC_ADDR_STR_LEN];

  FIN( topology_vertex_address_print(topology_ptr,vertex_ptr) );
    vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get (vertex_ptr, topology_ptr->graph_state_handler);
    inet_address_print(addr_str, vertex_state_ptr->vertex_node_addr);
    printf("%s",addr_str);
  FOUT;
}


void topology_edge_addresses_print(Ospf_Topology const * const topology_ptr,
                                   PrgT_Graph_Edge const * const edge_ptr) {
  PrgT_Graph_Vertex * vertex_ptr;

  FIN( topology_edge_addresses_print(topology_ptr,edge_ptr) );
    vertex_ptr = prg_edge_vertex_a_get(edge_ptr);
    topology_vertex_address_print(topology_ptr,vertex_ptr);
    printf(" -> ");
    vertex_ptr = prg_edge_vertex_b_get(edge_ptr);
    topology_vertex_address_print(topology_ptr,vertex_ptr);
  FOUT;
}


void topology_edge_print(Ospf_Topology const * const topology_ptr,
                         PrgT_Graph_Edge const * const edge_ptr,
                         int *tot_free_slot_ptr,
                         double *percent_ptr) {
  // print the spectrum of an edge in terms of free slots
  unsigned long size,i;
  Ospf_Label_Range *free_range_ptr;
  TE_edge_state   *te_edge_state_ptr;
  double percent;
  int tot_free_slot,dif,tot_free_ge4slot;

  FIN( topology_edge_print(topology_ptr,edge_ptr) );
    topology_edge_addresses_print(topology_ptr,edge_ptr);
    printf(" ");
    te_edge_state_ptr = prg_edge_client_state_get(edge_ptr, topology_ptr->graph_state_handler);
    size = prg_vector_size(te_edge_state_ptr->available_slots_vptr);
    tot_free_slot=0;
    tot_free_ge4slot=0;
    for (i=0; i<size; ++i) {
      free_range_ptr = prg_vector_access(te_edge_state_ptr->available_slots_vptr,i);
      printf("[%d,%d]",free_range_ptr->lower_freq_indx,free_range_ptr->upper_freq_indx);

      dif = free_range_ptr->upper_freq_indx - free_range_ptr->lower_freq_indx;
      tot_free_slot += dif;
      if (dif>=18) tot_free_ge4slot += dif;
    }
  	
    if (tot_free_slot>0) {
      percent = 100*((double)tot_free_ge4slot/tot_free_slot);
	} else {
	  percent = 0.0;
	}
    tot_free_slot = tot_free_slot/2;

    printf("\n");
    printf("total free slots = %d, %f%% of which can accomodate 400Gbps \n", tot_free_slot, percent);
    if (OPC_NIL != percent_ptr)
      *percent_ptr = percent;
    if (OPC_NIL != tot_free_slot_ptr)
      *tot_free_slot_ptr = tot_free_slot;
  FOUT;
}


void topology_links_print(Ospf_Topology const * const topology_ptr) {
  PrgT_Vector *edges_vptr;
  PrgT_Graph_Edge *edge_ptr;
  int size,i;
  int tot_free_slot,link_free_slot;
  double tot_percent,link_percent;

  FIN ( topology_links_print(topology_ptr) );
    edges_vptr = prg_graph_edge_all_get (topology_ptr->graph_ptr);
    size = prg_vector_size(edges_vptr);
    tot_free_slot = 0;
    tot_percent = 0;
    for (i=0; i<size; ++i) {
      edge_ptr = prg_vector_access(edges_vptr,i);
      topology_edge_print(topology_ptr,edge_ptr,
                          &link_free_slot,
                          &link_percent);

      tot_free_slot += link_free_slot;
      tot_percent += link_percent;
    }

    tot_free_slot = tot_free_slot/size;
    tot_percent = tot_percent/size;
    printf("\n\nNetwork average free slots per link= %d, %f%% of which can accomodate 400Gbps \n", tot_free_slot, tot_percent);
  FOUT;
}
