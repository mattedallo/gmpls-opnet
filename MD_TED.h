#ifndef	_MD_TED_H_INCLUDED_
	#define _MD_TED_H_INCLUDED_

#include 	<opnet.h>
#include    <ip_addr_v4.h>
#include	"MD_ospf_src.h"
#include	"MD_utils.h"
#include	"MD_spectrum.h"

PrgT_Graph_Edge* topology_edge_between_addresses_get(Ospf_Topology const* const topology_ptr,
                                                     InetT_Address const* const from_addr_ptr,
                                                     InetT_Address const* const to_addr_ptr);

MD_Compcode ted_edge_frequency_slot_release(Ospf_Topology const* const topology_ptr,
                                            PrgT_Graph_Edge* edge_ptr,
                                            int lower_index, int upper_index);

void topology_computed_paths_destroy(Ospf_Topology *topology);

PrgT_Bin_Hash_Table * ted_node_transponders_get(Ospf_Topology *topology,
                                                InetT_Address const *address_ptr);

OpT_uInt32 ted_node_free_carriers_num_get(Ospf_Topology *topology, 
                                          InetT_Address const* address_ptr);

InetT_Address* topology_random_network_address_get(Ospf_Topology *topology,
                                                   const InetT_Address *excluded);

// get the address associated to a vertex
InetT_Address* ted_vertex_address(Ospf_Topology *topology_ptr,
                                  PrgT_Graph_Vertex *vertex_ptr);

Transponder* ted_node_transponder_by_id_get(Ospf_Topology *topology,
                                            InetT_Address *address_ptr,
                                            OpT_uInt32 id);

OpT_uInt32 ted_vertex_transponders_number(Ospf_Topology *topology_ptr,
                                          PrgT_Graph_Vertex * vertex_ptr);

PrgT_Graph_Vertex* topology_new_fake_transponder_vertex(Ospf_Topology *topology,
                                                        Transponder *transponder_ptr);

PrgT_Graph_Vertex* topology_new_fake_vertex(Ospf_Topology *topology);

// get a fake vertex from the table
PrgT_Graph_Vertex* topology_fake_vertex_get(Ospf_Topology * topology_ptr,
                                            InetT_Address const* node_addr_ptr);

// get a fake vertex from the table
// or create and store it if not present
PrgT_Graph_Vertex* topology_fake_vertex_get_or_create(Ospf_Topology * topology_ptr,
                                                      InetT_Address const* node_addr_ptr);

void te_vertex_state_destroy(TE_vertex_state *vertex_state_ptr);

void topology_vertex_destroy(Ospf_Topology *topology,
                             PrgT_Graph_Vertex* vertex_ptr);

void te_edge_spectrum_map_set(Ospf_Topology *topology,
                              PrgT_Graph_Edge *edge_ptr,
                              Spectrum_Map* spectrum_map_ptr);

void te_edge_state_destroy(TE_edge_state *te_edge_state_ptr);

TE_edge_state * te_edge_state_create(double metric);

TE_vertex_state* te_vertex_state_get(Ospf_Topology *topology_ptr,
                                     PrgT_Graph_Vertex * vertex_ptr);

Spectrum_Map* te_edges_spectrum_sum(Ospf_Topology *topology,
                                    PrgT_Vector *edges_vptr);

void topology_vertex_remove_and_destroy(Ospf_Topology *topology,
                                        PrgT_Graph_Vertex* vertex_ptr);

PrgT_Graph_Vertex* topology_vertex_by_id_get(Ospf_Topology const*topology_ptr,
                                             InetT_Address const* address_ptr);

Transponder* topology_fake_transponder_vertex_transponder_get(Ospf_Topology *topology,
                                                              PrgT_Graph_Vertex *vertex_ptr);

void te_edge_spectrum_map_compute(Ospf_Topology *topology_ptr,
                                  PrgT_Graph_Edge *edge_ptr,
                                  int required_slices);


void topology_edge_addresses_print(Ospf_Topology const * const topology_ptr,
                                   PrgT_Graph_Edge const * const edge_ptr);

void topology_edge_print(Ospf_Topology const * const topology_ptr,
                         PrgT_Graph_Edge const * const edge_ptr,
                         int *tot_free_slot_ptr,
                         double *percent_ptr);

void topology_links_print(Ospf_Topology const * const topology_ptr);

#endif
