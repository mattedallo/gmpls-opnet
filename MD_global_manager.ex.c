#include	<opnet.h>
#include	<prg_bin_hash.h>
#include	"MD_global_manager.h"
#include	"MD_utils.h"
#include	"MD_statistics_src.h"

/**
	recovery: indicates whether the network is under 
			  recovery after a failure.
	connections_hptr: An hash table of connections (composed by 1 or more LSPs)
**/
struct Global_Manager {
	Boolean	failure;
	double failure_begin;
	double failure_end;
	//PrgT_Bin_Hash_Table	*connections_hptr;
	PrgT_Bin_Hash_Table	*lsps_timestamps_hptr;
	//PrgT_List 			*failed_lsps_lptr;
	int					failure_notification_sent;
	int					failure_notification_elaborated;
	unsigned int		failed_capacity; //in Gbps
	unsigned int		recovered_capacity; //in Gbps
	unsigned int		unrecovered_capacity; //in Gbps
};

typedef struct Lsp_Timestamps {
	double pc_request_instant;
	double pc_response_instant;
	double provisioning_start_instant;
	double provisioning_end_instant;
} Lsp_Timestamps;

Global_Manager* global_manager_get() {
	static Global_Manager	*gbl_manager = PRGC_NIL;
	
	FIN(global_manager_get());
	if (gbl_manager == PRGC_NIL) {
		gbl_manager = prg_mem_alloc(sizeof(Global_Manager));
		//gbl_manager->connections_hptr = prg_bin_hash_table_create (30, 3*sizeof(OpT_uInt32));
		gbl_manager->lsps_timestamps_hptr = prg_bin_hash_table_create (30, 4*sizeof(OpT_uInt32));
		//gbl_manager->failed_lsps_lptr = prg_list_create();
		gbl_manager->failure = OPC_FALSE;
		gbl_manager->failure_notification_sent = 0;
		gbl_manager->failure_notification_elaborated = 0;
		gbl_manager->failed_capacity = 0;
		gbl_manager->recovered_capacity = 0;
	}
	FRET(gbl_manager);
}


double global_manager_failure_begin_get(Global_Manager* gbl_manager_ptr){	
	FIN(global_manager_failure_begin_get(gbl_manager_ptr));
	FRET(gbl_manager_ptr->failure_begin);
}

static OpT_uInt32* connection_hash_key_get( Session_object const * const session_ptr ) {
	static OpT_uInt32	hash_key[3];
	
	FIN( connection_hash_key_get(session_ptr) );
	hash_key[0] = *( (OpT_uInt32 *) inet_address_addr_ptr_get_const(&(session_ptr->source_addr)) );
	hash_key[1] = *( (OpT_uInt32 *) inet_address_addr_ptr_get_const(&(session_ptr->destination_addr)) );
	hash_key[2] = session_ptr->tunnel_id;
	FRET(hash_key);
}

static void check_failure_recovery_end(Global_Manager* gbl_manager_ptr) {
	FIN( check_failure_recovery_end() );
	if (gbl_manager_ptr->failure_notification_elaborated == gbl_manager_ptr->failure_notification_sent &&
		gbl_manager_ptr->failed_capacity <= (gbl_manager_ptr->recovered_capacity + gbl_manager_ptr->unrecovered_capacity) ) {
		Objid	failure_node_objid,failure_process_objid;
		
		gbl_manager_ptr->failure_end = op_sim_time();
		
		printf("failure recovery took %f seconds\n", (gbl_manager_ptr->failure_end - gbl_manager_ptr->failure_begin) );
		
		// send interrupt to restore the link //
		failure_node_objid = op_id_from_name (0, OPC_OBJTYPE_NODE_FIX, "Failure Scheduler");
		if (failure_node_objid == OPC_OBJID_INVALID) {
			printf("statistics ERROR: failure scheduler node not found, cannot recover the failed links\n");
		}
		else {
			failure_process_objid = op_id_from_name (failure_node_objid, OPC_OBJTYPE_PROC, "failure process");
			if (failure_process_objid == OPC_OBJID_INVALID) {
				printf("statistics ERROR: failure process not found inside the failure scheduler node, cannot recover the failed links\n");
			}
			else {
				op_intrpt_schedule_remote (op_sim_time(),0,failure_process_objid);
			}
		}
		
		recovery_end();
		
		//reset some vars
		gbl_manager_ptr->failure_notification_elaborated = 0;
		gbl_manager_ptr->failure_notification_sent = 0;
		gbl_manager_ptr->failure_begin = 0;
		gbl_manager_ptr->failure_end = 0;
		gbl_manager_ptr->unrecovered_capacity = 0;
		gbl_manager_ptr->recovered_capacity = 0;
		gbl_manager_ptr->failed_capacity = 0;
	}
	FOUT;
}

void global_manager_notification_sent(Global_Manager* gbl_manager_ptr) {
	FIN( global_manager_notification_sent(gbl_manager_ptr) );
	
	if (gbl_manager_ptr->failure_notification_sent == 0) {
		gbl_manager_ptr->failure_begin = op_sim_time();
		printf("start failure at %f\n",gbl_manager_ptr->failure_begin);
	}
	++(gbl_manager_ptr->failure_notification_sent);
	//printf ("notifiche mandate  %d\n",gbl_manager_ptr->failure_notification_sent);
	FOUT;
}


void global_manager_increase_capacity_to_recover(Global_Manager* gbl_manager_ptr, unsigned int capacity) {
	FIN( global_manager_increase_failed_capacity(capacity) );
	gbl_manager_ptr->failed_capacity += capacity;
	//printf ("capacit� fallita  %d\n",gbl_manager_ptr->failed_capacity);
	FOUT;
}

void global_manager_increase_recovered_capacity(Global_Manager* gbl_manager_ptr, unsigned int capacity) {
	FIN( global_manager_increase_recovered_capacity(capacity) );
	
	gbl_manager_ptr->recovered_capacity += capacity;
	//printf ("capacit� recuperata  %d\n",gbl_manager_ptr->recovered_capacity);
	check_failure_recovery_end(gbl_manager_ptr);
	
	FOUT;
}

void global_manager_increase_unrecovered_capacity(Global_Manager* gbl_manager_ptr, unsigned int capacity) {
	FIN( global_manager_increase_unrecovered_capacity(capacity) );
	
	gbl_manager_ptr->unrecovered_capacity += capacity;
	//printf ("capacit� NON recuperata  %d\n",gbl_manager_ptr->unrecovered_capacity);
	check_failure_recovery_end(gbl_manager_ptr);
	
	FOUT;
}



void global_manager_notifications_elaborated(Global_Manager* gbl_manager_ptr, int number) {
	
	FIN( global_manager_notifications_elaborated(gbl_manager_ptr, number) );
	
	gbl_manager_ptr->failure_notification_elaborated += number;
	
	//printf ("notifiche elaborate  %d\n",gbl_manager_ptr->failure_notification_elaborated);
	
	if (gbl_manager_ptr->failure_notification_elaborated > gbl_manager_ptr->failure_notification_sent)
		op_sim_error(OPC_SIM_ERROR_ABORT, "more notification received than sent","");
	
	check_failure_recovery_end(gbl_manager_ptr);
	
	FOUT;
}



static OpT_uInt32* lsp_hash_key_get(Session_object const * const session_ptr ) {
	static OpT_uInt32	hash_key[4];
	
	FIN( lsp_hash_key_get(session_ptr) );
	hash_key[0] = *( (OpT_uInt32 *) inet_address_addr_ptr_get_const(&(session_ptr->source_addr)) );
	hash_key[1] = *( (OpT_uInt32 *) inet_address_addr_ptr_get_const(&(session_ptr->destination_addr)) );
	hash_key[2] = session_ptr->tunnel_id;
	hash_key[3] = session_ptr->sub_lsp_id;
	FRET(hash_key);
}

static Lsp_Timestamps* lsp_timestamp_get_or_create(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr) {
	Lsp_Timestamps	*lsp_timestamps_ptr;
	OpT_uInt32		*hash_key;
	FIN( lsp_timestamp_get_or_create() );
	hash_key = lsp_hash_key_get(session_ptr);
	lsp_timestamps_ptr = prg_bin_hash_table_item_get(gbl_manager_ptr->lsps_timestamps_hptr, hash_key);
	if (lsp_timestamps_ptr == PRGC_NIL) {
		lsp_timestamps_ptr = prg_mem_alloc(sizeof(Lsp_Timestamps));
		prg_bin_hash_table_item_insert(gbl_manager_ptr->lsps_timestamps_hptr, hash_key, lsp_timestamps_ptr,OPC_NIL);
	}
	FRET(lsp_timestamps_ptr);
}

void global_manager_lsp_pc_request_instant_set(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr, double instant) {
	
	Lsp_Timestamps	*lsp_timestamps_ptr;
	
	FIN( global_manager_lsp_pc_request_instant_set() );
	lsp_timestamps_ptr = lsp_timestamp_get_or_create(gbl_manager_ptr, session_ptr);
	lsp_timestamps_ptr->pc_request_instant = instant;
	FOUT;
}

void global_manager_lsp_pc_response_instant_set(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr, double instant) {
	
	Lsp_Timestamps	*lsp_timestamps_ptr;
	
	FIN( global_manager_lsp_pc_response_instant_set() );
	lsp_timestamps_ptr = lsp_timestamp_get_or_create(gbl_manager_ptr, session_ptr);
	lsp_timestamps_ptr->pc_response_instant = instant;
	FOUT;
}

void global_manager_lsp_provisioning_start(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr) {
	Lsp_Timestamps	*lsp_timestamps_ptr;
	
	FIN( global_manager_lsp_provisioning_start() );
	lsp_timestamps_ptr = lsp_timestamp_get_or_create(gbl_manager_ptr, session_ptr);
	lsp_timestamps_ptr->provisioning_start_instant = op_sim_time();
	FOUT;
}

void global_manager_lsp_provisioning_end(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr) {
	OpT_uInt32		*hash_key;
	Lsp_Timestamps	*lsp_timestamps_ptr;
	
	FIN( global_manager_lsp_provisioning_end() );
	hash_key = lsp_hash_key_get(session_ptr);
	lsp_timestamps_ptr = prg_bin_hash_table_item_get(gbl_manager_ptr->lsps_timestamps_hptr, hash_key);
	if (lsp_timestamps_ptr == PRGC_NIL) {
		FOUT;
	}
	lsp_timestamps_ptr->provisioning_end_instant = op_sim_time();
	
	stat_provisioning_time_write(lsp_timestamps_ptr->provisioning_end_instant-lsp_timestamps_ptr->provisioning_start_instant);
	
	FOUT;
}


void global_manager_lsp_life_end(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr) {
	OpT_uInt32		*hash_key;
	Lsp_Timestamps	*lsp_timestamps_ptr;
	
	FIN( global_manager_lsp_provisioning_end() );
	hash_key = lsp_hash_key_get(session_ptr);
	lsp_timestamps_ptr = prg_bin_hash_table_item_remove(gbl_manager_ptr->lsps_timestamps_hptr, hash_key);
	if (lsp_timestamps_ptr == PRGC_NIL) {
		FOUT;
	}
	prg_mem_free(lsp_timestamps_ptr);
	FOUT;
}



/*
MD_Compcode global_manager_lsp_insert(Global_Manager* gbl_manager_ptr, LSP* lsp_ptr) {
/**
	Insert the LSP pointer (not a copy) inside the global manager LSP list
**//*
	LSP_Connection	*lsp_connection_ptr;
	OpT_uInt32		*hash_key;
	//LSP				*lsp_copy_ptr;
	
	FIN( global_manager_lsp_add(gbl_manager_ptr, lsp) );
	//lsp_copy_ptr = lsp_copy(lsp_ptr, OPC_TRUE);
	hash_key = lsp_lsp_connection_key_get(lsp_ptr);
	lsp_connection_ptr = prg_bin_hash_table_item_get(gbl_manager_ptr->connections_hptr, hash_key);
	if (PRGC_NIL == lsp_connection_ptr) {
		//create a new lsp connection with the lsp
		lsp_connection_ptr = lsp_connection_from_lsp_create(lsp_ptr);
		// insert the new connection inside the hash table of connections
		prg_bin_hash_table_item_insert(gbl_manager_ptr->connections_hptr, hash_key, lsp_connection_ptr,OPC_NIL);
	}
	else {
		// insert the lsp inside the connection
		if ( MD_Compcode_Failure == lsp_connection_lsp_insert_unique(lsp_connection_ptr, lsp_ptr) ) {
			//lsp_destroy(lsp_copy_ptr, OPC_TRUE);
			FRET(MD_Compcode_Failure);
		}
	}
	
	FRET(MD_Compcode_Success);
}


/*
LSP* global_manager_lsp_get(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr) {
/**
	Returns an LSP pointer if present on the global manager.
**//*
	OpT_uInt32		*hash_key;
	LSP				*lsp_ptr;
	LSP_Connection	*lsp_connection_ptr;
	
	FIN( global_manager_lsp_get(gbl_manager_ptr, session_ptr) );
	hash_key = connection_hash_key_get(session_ptr);
	lsp_connection_ptr = prg_bin_hash_table_item_get(gbl_manager_ptr->connections_hptr, hash_key);
	if (PRGC_NIL == lsp_connection_ptr) {
		FRET(PRGC_NIL);
	}
	
	lsp_ptr = lsp_connection_lsp_get(lsp_connection_ptr,session_ptr);
	FRET(lsp_ptr);
}

LSP* global_manager_lsp_remove(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr) {
/**
	Returns an LSP pointer if present on the global manager.
**//*
	OpT_uInt32		*hash_key;
	LSP				*lsp_ptr;
	LSP_Connection	*lsp_connection_ptr;
	
	FIN( global_manager_lsp_remove(gbl_manager_ptr, session_ptr) );
	hash_key = connection_hash_key_get(session_ptr);
	lsp_connection_ptr = prg_bin_hash_table_item_remove (gbl_manager_ptr->connections_hptr, hash_key);
	if (PRGC_NIL == lsp_connection_ptr) {
		FRET(PRGC_NIL);
	}
	//if ( prg_list_size(gbl_manager_ptr->failed_lsps_lptr) > 0 ) {
	//	lsps_list_remove(gbl_manager_ptr->failed_lsps_lptr, session_ptr);
	//}
	lsp_ptr = lsp_connection_lsp_remove(lsp_connection_ptr,session_ptr);
	FRET(lsp_ptr);
}

MD_Compcode global_manager_lsp_update(Global_Manager* gbl_manager_ptr, LSP const* lsp_ptr) {
	OpT_uInt32		*hash_key;
	LSP* old_lsp_ptr;
	
	FIN( global_manager_lsp_update(gbl_manager_ptr, lsp_ptr) );
	hash_key = lsp_connection_key_get(lsp_copy_ptr);
	lsp_connection_ptr = prg_bin_hash_table_item_get(gbl_manager_ptr->connections_hptr, hash_key);
	if (PRGC_NIL == lsp_connection_ptr) FRET(MD_Compcode_Failure);
	old_lsp_ptr = lsp_connection_lsp_get(lsp_connection_ptr, lsp_ptr->session_ptr);
	
	if ( old_lsp_ptr->status != LSP_FAILED && lsp_ptr->status == LSP_FAILED ) {
		
	}
	
	if ( old_lsp_ptr->status == LSP_FAILED && lsp_ptr->status != LSP_FAILED ) {
		
	}
	
	old_lsp_ptr->status = lsp_ptr->status;
	
	FOUT;
}*/
