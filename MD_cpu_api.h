#ifndef	_MD_CPU_API_H_INCLUDED_
	#define _MD_CPU_API_H_INCLUDED_

#include <opnet.h>

/* Interrupt codes */
#define CPU_COMPUTATION_DONE_CODE			0
#define CPU_COMPUTATION_REQUEST_CODE		1


typedef struct Api_Cpu_Interface_Handle {
	Objid	client_objid;
	Objid	cpu_objid;
} Api_Cpu_Interface_Handle;


// INTERFACE
Api_Cpu_Interface_Handle cpu_client_register (Objid client_objid);
Objid cpu_objid_get(Api_Cpu_Interface_Handle const * const cpu_intf_hndlptr);
void cpu_computation_request(Api_Cpu_Interface_Handle const * const cpu_intf_hndlptr, OpT_Int32 request_id);
OpT_Int32 cpu_interrupt_request_id_get(Api_Cpu_Interface_Handle const * const cpu_intf_hndlptr);




#endif
