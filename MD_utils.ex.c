#include "MD_utils.h"

PrgT_Compcode prg_vector_mem_free(void* elem_ptr) {
	FIN( prg_vector_mem_free(elem_ptr) );
	prg_mem_free(elem_ptr);
	FRET(PrgC_Compcode_Success);
}

int* int_mem_copy(void* int_ptr) {
	FIN( int_mem_copy(int_ptr) );
	FRET( prg_mem_copy_create(int_ptr, sizeof(int)) );
}

double* double_mem_copy(void* double_ptr) {
  FIN( double_mem_copy(double_ptr) );
  FRET( prg_mem_copy_create(double_ptr, sizeof(double)) );
}
