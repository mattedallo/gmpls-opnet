#include	"MD_lsp.h"
#include	"MD_rsvp_msg_util.h"


LSP* lsp_empty_create() {
	LSP* lsp_ptr;
	FIN( lsp_empty_create() );
	lsp_ptr = prg_mem_alloc( sizeof(LSP) );
	lsp_ptr->ero_ptr = PRGC_NIL;
	lsp_ptr->sender_tspec_ptr = PRGC_NIL;
	lsp_ptr->suggested_label_ptr = PRGC_NIL;
	lsp_ptr->session_ptr = PRGC_NIL;
	lsp_ptr->generalized_label_ptr = PRGC_NIL;
	lsp_ptr->capacity = 0;
	lsp_ptr->status = LSP_NONE;
	FRET(lsp_ptr);
}

void lsp_destroy(LSP *lsp_ptr, Boolean deep_destroy) {
	FIN( lsp_destroy(LSP *lsp_ptr, Boolean deep_destroy) );
	if (OPC_TRUE == deep_destroy) {
		ERO_object_destroy (lsp_ptr->ero_ptr);
		sender_tspec_object_destroy(lsp_ptr->sender_tspec_ptr);
		suggested_label_object_destroy(lsp_ptr->suggested_label_ptr);
		session_object_destroy(lsp_ptr->session_ptr);
		generalized_label_object_destroy(lsp_ptr->generalized_label_ptr);
	}
	prg_mem_free (lsp_ptr);
	FOUT;
}


LSP* lsp_copy(LSP *lsp_ptr, Boolean deep_copy) {
	LSP* lsp_copy_ptr;
	FIN( lsp_copy(LSP *lsp_ptr, Boolean deep_copy) );
	lsp_copy_ptr = prg_mem_copy_create(lsp_ptr, sizeof(LSP));
	if (OPC_TRUE == deep_copy) {
		lsp_copy_ptr->ero_ptr = ERO_object_copy (lsp_ptr->ero_ptr);
		lsp_copy_ptr->sender_tspec_ptr = sender_tspec_object_copy(lsp_ptr->sender_tspec_ptr);
		lsp_copy_ptr->suggested_label_ptr = suggested_label_object_copy(lsp_ptr->suggested_label_ptr);
		lsp_copy_ptr->session_ptr = session_object_copy(lsp_ptr->session_ptr);
		lsp_copy_ptr->generalized_label_ptr = generalized_label_object_copy(lsp_ptr->generalized_label_ptr);
	}
	FRET(lsp_copy_ptr);
}

OpT_uInt32* lsp_lsp_connection_key_get(LSP* lsp_ptr) {
	static OpT_uInt32	hash_key[3];
	
	FIN( lsp_lsp_connection_key_get(lsp_ptr) );
	hash_key[0] = *( (OpT_uInt32 *) inet_address_addr_ptr_get_const(&(lsp_ptr->session_ptr->source_addr)) );
	hash_key[1] = *( (OpT_uInt32 *) inet_address_addr_ptr_get_const(&(lsp_ptr->session_ptr->destination_addr)) );
	hash_key[2] = lsp_ptr->session_ptr->tunnel_id;
	FRET(hash_key);
}


LSP* lsps_list_remove(PrgT_List *list_ptr, Session_object const * const session_ptr) {
	LSP				*lsp_ptr;
	PrgT_List_Cell 	*cell_ptr;
	
	FIN( lsps_list_remove(list_ptr, session_ptr) );
	
	cell_ptr = prg_list_head_cell_get (list_ptr);
	while (cell_ptr!= PRGC_NIL) {
		lsp_ptr = prg_list_cell_data_get(cell_ptr);
		if ( session_object_equal(session_ptr,lsp_ptr->session_ptr) == OPC_TRUE ) {
			prg_list_cell_remove(list_ptr, cell_ptr);
			FRET(lsp_ptr);
		}
		cell_ptr = prg_list_cell_next_get(cell_ptr);
	}
	
	FRET(PRGC_NIL);
}

LSP_Connection*	lsp_connection_create(InetT_Address const* source_addr_ptr, InetT_Address const* dest_addr_ptr, OpT_uInt32 tunnel_id, OpT_uInt32 capacity_demand) {
	LSP_Connection	* lsp_conn;
	FIN( lsp_connection_create() );
	lsp_conn = prg_mem_alloc(sizeof(LSP_Connection));
	lsp_conn->source_addr_ptr=inet_address_create_dynamic(*source_addr_ptr);
	lsp_conn->dest_addr_ptr=inet_address_create_dynamic(*dest_addr_ptr);
	lsp_conn->tunnel_id=tunnel_id;
	lsp_conn->capacity_demand=capacity_demand;
	lsp_conn->capacity_allocated=0;
	lsp_conn->status=LSP_CONNECTION_STATUS_NONE;
	lsp_conn->lsps_lptr=prg_list_create();
	FRET(lsp_conn);
}


LSP_Connection* lsp_connection_from_lsp_create(LSP* lsp_ptr) {
/** 
	Creates an LSP connection from an LSP and append it into the lsp list.
**/
	LSP_Connection	* lsp_conn;
	FIN( lsp_connection_from_lsp_create(lsp_ptr) );
	lsp_conn = prg_mem_alloc(sizeof(LSP_Connection));
	lsp_conn->source_addr_ptr=inet_address_create_dynamic(lsp_ptr->session_ptr->source_addr);
	lsp_conn->dest_addr_ptr=inet_address_create_dynamic(lsp_ptr->session_ptr->destination_addr);
	lsp_conn->tunnel_id=lsp_ptr->session_ptr->tunnel_id;
	lsp_conn->capacity_demand=lsp_ptr->capacity;
	lsp_conn->capacity_allocated=0;
	lsp_conn->status=LSP_CONNECTION_STATUS_NONE;
	lsp_conn->lsps_lptr=prg_list_create();
	prg_list_insert (lsp_conn->lsps_lptr, lsp_ptr, PRGC_LISTPOS_TAIL);
	FRET(lsp_conn);
}


MD_Compcode lsp_connection_lsp_insert_unique(LSP_Connection* lsp_connection_ptr, LSP* lsp_ptr) {
/** 
	Insert an LSP inside the connection uniquely.
**/
	FIN( lsp_connection_lsp_insert_unique() );
	if (lsp_connection_ptr == PRGC_NIL || lsp_ptr == PRGC_NIL) FRET(MD_Compcode_Failure);
	
	if (OPC_FALSE == inet_address_ptr_equal(lsp_connection_ptr->source_addr_ptr, &(lsp_ptr->session_ptr->source_addr)) ||
		OPC_FALSE == inet_address_ptr_equal(lsp_connection_ptr->dest_addr_ptr,&(lsp_ptr->session_ptr->destination_addr)) ||
		lsp_connection_ptr->tunnel_id != lsp_ptr->session_ptr->tunnel_id) {
		FRET(MD_Compcode_Failure);
	}
	
	prg_list_insert (lsp_connection_ptr->lsps_lptr, lsp_ptr, PRGC_LISTPOS_TAIL);
	FRET(MD_Compcode_Success);
}

