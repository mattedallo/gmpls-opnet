#ifndef	_MD_BIT_ARRAY_H_INCLUDED_
	#define _MD_BIT_ARRAY_H_INCLUDED_

typedef struct BitArray BitArray;

BitArray* bitarray_create(int nbits);

void bitarray_destroy(BitArray *bitarray_ptr);

void bitarray_set_bit(BitArray *bitarray_ptr, int bit_index);

void bitarray_clear_bit(BitArray *bitarray_ptr, int bit_index);

int bitarray_get_bit(BitArray const* bitarray_ptr, int bit_index);

void bitarray_print(BitArray const*bitarray_ptr);

#endif
