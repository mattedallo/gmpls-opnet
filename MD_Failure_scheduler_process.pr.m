MIL_3_Tfile_Hdr_ 171A 171A modeler 9 520D21F7 550060FA 5F labeffa matteo 0 0 none none 0 0 none 97FA7436 3DD9 0 0 0 0 0 0 2b1a 1                                                                                                                                                                                                                                                                                                                                                                                                ��g�      D  k  o  7  ;  �  ;�  ;�  ;�  ;�  ;�  ;�  ;�  �  �      Link Failure Distribution   ���   MD_distribution_attributes�Z             #Link Failure Mean Interarrival Time   �������      secs      ��         infinite              ����              ����         infinite   ��      ����       �Z             "Link Failure Interarrival Variance   �������      secs               ����              ����              ����           �Z             Link Failure Start Time   �������      secs               ����              ����              ����           �Z                 	   begsim intrpt         
   ����   
   doc file            	nd_module      endsim intrpt         
    ����   
   failure intrpts            disabled      intrpt interval         ԲI�%��}����      priority              ����      recovery intrpts            disabled      subqueue                     count    ���   
   ����   
      list   	���   
          
      super priority             ����          
   +/* number of duplex link in the topology */   int	\num_links;       'OmsT_Dist_Handle	\link_failure_distrib;       $/* list of failed link to recover */   PrgT_List*	\failed_links_lptr;       int	\no_lsp;          double link_failure_mean;      #include	<oms_dist_support.h>   #include	<ip_addr_v4.h>   #include	"MD_lmp_src.h"       #include	"MD_rsvp.h"   '// include support for process registry   #include 	"oms_pr.h"       3#define 	END_FAIL_PROCESS 		(link_failure_mean < 0)   h#define		LINK_FAILURE_INTRPT		(op_intrpt_type()==OPC_INTRPT_SELF && op_intrpt_code()==LINK_FAILURE_CODE)   Z#define		LINK_RESTORE_INTRPT		(op_intrpt_type()==OPC_INTRPT_REMOTE && op_intrpt_code()==0)   _#define		NO_LSP_TO_RESTORE_INTRPT		(op_intrpt_type()==OPC_INTRPT_REMOTE && op_intrpt_code()==1)       #define LINK_FAILURE_DEBUG                                                  Z   Z          
   init   
       
      $char	link_failure_distrib_name[128];   double	link_failure_variance;   double	first_failure;       bop_ima_obj_attr_get_dbl(op_id_self (), "Link Failure Mean Interarrival Time", &link_failure_mean);       if (link_failure_mean < 0) {   >// link failure is disabled, no need of a failure scheduler //   	   }   else {       eop_ima_obj_attr_get_dbl(op_id_self (), "Link Failure Interarrival Variance", &link_failure_variance);   `op_ima_obj_attr_get_str(op_id_self(),"Link Failure Distribution",128,link_failure_distrib_name);   hlink_failure_distrib = oms_dist_load(link_failure_distrib_name,link_failure_mean,link_failure_variance);       &/* count the number of duplex links */   4num_links = op_topo_object_count(OPC_OBJTYPE_LKDUP);       9//num_nodes = op_topo_object_count(OPC_OBJTYPE_NODE_FIX);       %/* schedule the first link failure */   Rop_ima_obj_attr_get_dbl(op_id_self (), "Link Failure Start Time", &first_failure);       Jop_intrpt_schedule_self (op_sim_time ()+first_failure, LINK_FAILURE_CODE);       #// create a list of failed links //   'failed_links_lptr = prg_list_create ();   		   }   
       
      no_lsp = 0;   
       
   ����   
          pr_state        �   Z          
   failure   
       
   �   $int 			num_tx, num_rx,k,i,rndm_indx;   \Objid			stream_objid,rx_objid,tx_objid,link_objid,rx_rsvp_objid,rx_node_objid,rx_ospf_objid;   Objid			*fl;   OpT_uInt32		stream_indx;   Ici				*iciptr;   Distribution	*unif_int_dist;   double			next_failure;    List 			proc_record_handle_list;   'OmsT_Pr_Handle 		process_record_handle;   #if defined(LINK_FAILURE_DEBUG)   	Objid		ip_objid;   $	char		addr_str[INETC_ADDR_STR_LEN];   #endif           /*   1)extract a link to fail   +2)obtain the two nodes attached to the link   �3)schedule a forced remote interrupt to the two nodes passing the source and destination addresses with the ICI to identify the link   */       E// Obtain random link object ID and determine if already disabled. //   ;unif_int_dist = op_dist_load ("uniform_int",0,num_links-1);   +rndm_indx = op_dist_outcome(unif_int_dist);   op_dist_unload(unif_int_dist);                   /*   {//////////   +fl = (Objid *)prg_mem_alloc(sizeof(Objid));       3for (rndm_indx=4;rndm_indx<num_links;++rndm_indx) {       ;link_objid = op_topo_object (OPC_OBJMTYPE_LINK, rndm_indx);       *fl = link_objid;   Nnum_rx = op_topo_assoc_count(link_objid,OPC_TOPO_ASSOC_OUT, OPC_OBJTYPE_PTRX);       #if defined(LINK_FAILURE_DEBUG)   ]	printf("\n@@@@@@@@@@@@ LINK FAILURE @@@@@@@@@@@@\nLink (id:%d) failed between nodes: ",*fl);   #endif   for (i=0; i<num_rx; i++) {   	   M	rx_objid = op_topo_assoc(link_objid,OPC_TOPO_ASSOC_OUT, OPC_OBJTYPE_PTRX,i);   	   D	// get the object ID of the node acting as receiver for the link //   *	rx_node_objid = op_topo_parent(rx_objid);   	    	#if defined(LINK_FAILURE_DEBUG)   E		ip_objid = op_id_from_name (rx_node_objid, OPC_OBJTYPE_PROC, "IP");   Q		op_ima_obj_attr_get_str(ip_objid,"node address", INETC_ADDR_STR_LEN, addr_str);   		printf("%s ",addr_str);   	#endif   }   }   op_sim_end("","","","");   }/////////////   */           rndm_indx=12;       ;link_objid = op_topo_object (OPC_OBJMTYPE_LINK, rndm_indx);       +fl = (Objid *)prg_mem_alloc(sizeof(Objid));   *fl = link_objid;   ,// insert the new failed link in the list //   ;prg_list_insert (failed_links_lptr, fl, PRGC_LISTPOS_HEAD);   @// since in reality each link is half duplex (unidirectional) //   +// fail both links between the two nodes //   Nnum_rx = op_topo_assoc_count(link_objid,OPC_TOPO_ASSOC_OUT, OPC_OBJTYPE_PTRX);       #if defined(LINK_FAILURE_DEBUG)   ]	printf("\n@@@@@@@@@@@@ LINK FAILURE @@@@@@@@@@@@\nLink (id:%d) failed between nodes: ",*fl);   #endif   for (i=0; i<num_rx; i++) {   	   M	rx_objid = op_topo_assoc(link_objid,OPC_TOPO_ASSOC_OUT, OPC_OBJTYPE_PTRX,i);   	   D	// get the object ID of the node acting as receiver for the link //   *	rx_node_objid = op_topo_parent(rx_objid);   	    	#if defined(LINK_FAILURE_DEBUG)   E		ip_objid = op_id_from_name (rx_node_objid, OPC_OBJTYPE_PROC, "IP");   Q		op_ima_obj_attr_get_str(ip_objid,"node address", INETC_ADDR_STR_LEN, addr_str);   		printf("%s ",addr_str);   	#endif   	   N	num_tx = op_topo_assoc_count(link_objid,OPC_TOPO_ASSOC_IN, OPC_OBJTYPE_PTTX);   	for (k=0; k<num_tx; k++) {   M		tx_objid = op_topo_assoc(link_objid,OPC_TOPO_ASSOC_IN, OPC_OBJTYPE_PTTX,k);   7		if (op_topo_parent(tx_objid) == rx_node_objid) break;   	}   	   &	// get the output stream object id //   P	stream_objid = op_topo_assoc (tx_objid, OPC_TOPO_ASSOC_IN,OPC_OBJTYPE_STRM, 0);   			   m	// get the stream index of the transmitter on the failed link, it is used as identifier for the interface //   ?	op_ima_obj_attr_get (stream_objid,"src stream", &stream_indx);   	   ?	// get the objectID of the rsvp-te processor of the rx node //   P	//rx_rsvp_objid = op_id_from_name (rx_node_objid, OPC_OBJTYPE_PROC, "RSVP-TE");   J	if (oms_pr_process_discover (OPC_OBJID_INVALID, &proc_record_handle_list,   1		"node objid",   OMSC_PR_OBJID,  	rx_node_objid,   +		"protocol", 	OMSC_PR_STRING, 	"rsvp-te",    %		OPC_NIL) == OPC_COMPCODE_SUCCESS) {   	   J		// There should be only one RSVP-TE module per node. If more, flag error   8		if (op_prg_list_size (&proc_record_handle_list) > 1) {   �			op_sim_end ("Failure scheduler process","More than one RSVP-TE process has been identified in the selected node",OPC_NIL, OPC_NIL);   		}   		   "		// Get handle to OSPF-TE process   k		process_record_handle = (OmsT_Pr_Handle) op_prg_list_remove (&proc_record_handle_list, OPC_LISTPOS_HEAD);   		   !		// get the ObjectId of the ospf   [		oms_pr_attr_get (process_record_handle, "module objid",	OMSC_PR_OBJID, &(rx_rsvp_objid));   	}   	else {   z		op_sim_end ("Failure scheduler process","There is no RSVP-TE process configured in the selected node.",OPC_NIL,OPC_NIL);   	}   	   5	// remove all the element from the list of process//   4	prg_list_clear (&proc_record_handle_list,PRGC_NIL);   	   ?	// get the objectID of the ospf-te processor of the rx node //   P	//rx_ospf_objid = op_id_from_name (rx_node_objid, OPC_OBJTYPE_PROC, "OSPF-TE");   J	if (oms_pr_process_discover (OPC_OBJID_INVALID, &proc_record_handle_list,   1		"node objid",   OMSC_PR_OBJID,  	rx_node_objid,   +		"protocol", 	OMSC_PR_STRING, 	"ospf-te",    %		OPC_NIL) == OPC_COMPCODE_SUCCESS) {   	   J		// There should be only one OFPF-TE module per node. If more, flag error   8		if (op_prg_list_size (&proc_record_handle_list) > 1) {   �			op_sim_end ("Failure scheduler process","More than one OSPF-TE process has been identified in the selected node",OPC_NIL, OPC_NIL);   		}   		   "		// Get handle to OSPF-TE process   k		process_record_handle = (OmsT_Pr_Handle) op_prg_list_remove (&proc_record_handle_list, OPC_LISTPOS_HEAD);   		   !		// get the ObjectId of the ospf   [		oms_pr_attr_get (process_record_handle, "module objid",	OMSC_PR_OBJID, &(rx_ospf_objid));   	}   	else {   z		op_sim_end ("Failure scheduler process","There is no OSPF-TE process configured in the selected node.",OPC_NIL,OPC_NIL);   	}       E	// create an ICI to send informations together with the interrupt //   0	iciptr = op_ici_create ("MD_link_failure_ici");   	   3	// place the object id of the link into the ICI //   ?	op_ici_attr_set_int32(iciptr, "interface index", stream_indx);   	   -	// Call the interrupt coupled with the ICI//   	op_ici_install (iciptr);   	   c	// schedule an interrupt to the downlink node because is the one that first discover the failure//   S	// in particular schedule an interrupt to the RSVP-TE and the OSPF-TE processes //   L	op_intrpt_schedule_remote (op_sim_time (),LINK_FAILURE_CODE,rx_ospf_objid);   	   :	// create another identical ICI to send to the RSVP-TE //   0	iciptr = op_ici_create ("MD_link_failure_ici");   	   3	// place the object id of the link into the ICI //   ?	op_ici_attr_set_int32(iciptr, "interface index", stream_indx);   	   -	// Call the interrupt coupled with the ICI//   	op_ici_install (iciptr);       L	op_intrpt_schedule_remote (op_sim_time (),LINK_FAILURE_CODE,rx_rsvp_objid);       }       #if defined(LINK_FAILURE_DEBUG)   		printf("\n");   #endif   		   $/* schedule the next link failure */   7next_failure = oms_dist_outcome (link_failure_distrib);       Iop_intrpt_schedule_self (op_sim_time ()+next_failure, LINK_FAILURE_CODE);   
       
       
       
   ����   
          pr_state         Z            
   	terminate   
       
       
                         ����             pr_state         Z  �          
   st_3   
       
          //stampa_memoria();       
                         ����             pr_state          J          
   restore   
       
   8   Objid	*link_objid;   PrgT_List_Cell *cell_ptr;   int 		num_tx, num_rx,k,i;   BObjid		stream_objid,rx_objid,tx_objid,rx_node_objid,rx_ospf_objid;   OpT_uInt32	stream_indx;   Ici			*iciptr;       6cell_ptr = prg_list_head_cell_get (failed_links_lptr);       while (cell_ptr != PRGC_NIL) {   9	link_objid = (Objid*) prg_list_cell_data_get (cell_ptr);   r	printf("RESTORED link %d @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n",*link_objid);   	   P	num_rx = op_topo_assoc_count(*link_objid,OPC_TOPO_ASSOC_OUT, OPC_OBJTYPE_PTRX);       	for (i=0; i<num_rx; i++) {   	   O		rx_objid = op_topo_assoc(*link_objid,OPC_TOPO_ASSOC_OUT, OPC_OBJTYPE_PTRX,i);   	   E		// get the object ID of the node acting as receiver for the link //   +		rx_node_objid = op_topo_parent(rx_objid);   	   P		num_tx = op_topo_assoc_count(*link_objid,OPC_TOPO_ASSOC_IN, OPC_OBJTYPE_PTTX);   		for (k=0; k<num_tx; k++) {   O			tx_objid = op_topo_assoc(*link_objid,OPC_TOPO_ASSOC_IN, OPC_OBJTYPE_PTTX,k);   8			if (op_topo_parent(tx_objid) == rx_node_objid) break;   		}   	   '		// get the output stream object id //   Q		stream_objid = op_topo_assoc (tx_objid, OPC_TOPO_ASSOC_IN,OPC_OBJTYPE_STRM, 0);   			   n		// get the stream index of the transmitter on the failed link, it is used as identifier for the interface //   @		op_ima_obj_attr_get (stream_objid,"src stream", &stream_indx);   	   	   @		// get the objectID of the ospf-te processor of the rx node //   O		rx_ospf_objid = op_id_from_name (rx_node_objid, OPC_OBJTYPE_PROC, "OSPF-TE");       F		// create an ICI to send informations together with the interrupt //   1		iciptr = op_ici_create ("MD_link_failure_ici");   	   4		// place the object id of the link into the ICI //   @		op_ici_attr_set_int32(iciptr, "interface index", stream_indx);   	   .		// Call the interrupt coupled with the ICI//   		op_ici_install (iciptr);       I		op_intrpt_schedule_remote (op_sim_time (),NEW_LINK_CODE,rx_ospf_objid);       	}   		   ,	cell_ptr =prg_list_cell_next_get(cell_ptr);   	   }       "prg_list_free (failed_links_lptr);   
       
      no_lsp = 0;   
       
   ����   
          pr_state         �   Z          
   idle   
       
      X//printf("type %d code %d self=%d\n",op_intrpt_type(),op_intrpt_code(),OPC_INTRPT_SELF);   
       
       
           ����             pr_state        �   �          
   no lsp   
       
      	++no_lsp;   
                     
   ����   
          pr_state          
             Y   �      Z   Z   X            
   tr_2   
       
   END_FAIL_PROCESS   
       ����          
    ����   
          ����                       pr_transition            ����  �      \     Z  �          
   tr_3   
       ����          ����          
    ����   
          ����                       pr_transition                �   E      Z   Y   �   [          
   tr_4   
       
   default   
       ����          
    ����   
          ����                       pr_transition              )   0      �   Y  (   ;  �   Y          
   tr_5   
       
   LINK_FAILURE_INTRPT   
       ����          
    ����   
          ����                       pr_transition              X   j     �   Z  ,   y   �   Y          
   tr_6   
       ����          ����          
    ����   
          ����                       pr_transition               �   �      �   Y   �   �    L          
   tr_7   
       
   LINK_RESTORE_INTRPT   
       ����          
    ����   
          ����                       pr_transition               �  +       I     �   �   W          
   tr_8   
       ����          ����          
    ����   
          ����                       pr_transition      	        �   �      �   g  �   �          
   tr_9   
       
   NO_LSP_TO_RESTORE_INTRPT   
       ����          
    ����   
          ����                       pr_transition      
        h   �     �   �   �   e          
   tr_10   
       
   	no_lsp==1   
       ����          
    ����   
          ����                       pr_transition              v  '     �   �    F          
   tr_11   
       
   	no_lsp==2   
       ����          
    ����   
          ����                       pr_transition                           oms_dist_support                    