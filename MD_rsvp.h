#ifndef _MD_RSVP_H_INCLUDED_
  #define _MD_RSVP_H_INCLUDED_
  //#include  <oms_dist_support.h>
  #include  <prg_vector.h>
  //#include  <prg_bin_hash.h>
    //#include    "MD_pce_src.h"
    
  /* RSVP Msg Type Field: 8 bits    */
  #define   PATH_MSG    1
  #define   RESV_MSG    2
  #define   PATH_ERR_MSG  3
  #define   RESV_ERR_MSG  4
  #define   PATH_TEAR_MSG 5
  #define   RESV_TEAR_MSG 6
  #define   RESV_CONF_MSG 7
  #define   NOTIFY_MSG      21
  
  //#define NO_DEBUG_OUTPUT

  /* Define OPNET useful structures */
        
  typedef struct Frequency_slot {
    int       upper_freq_indx;
    int       lower_freq_indx;
  } Frequency_slot;
  
  typedef enum Node_role {
    INGRESS_NODE,
    INTERMEDIATE_NODE,
    EGRESS_NODE,
    UNKNOWN_NODE
  } Node_role;
  
  typedef enum Lsp_event {
    ROUTING_BLOCKING = 1,
    FORWARD_BLOCKING_BW_LACK,   /* lack of (total) available bandwith on the link */
    FORWARD_BLOCKING_FRAGMENTATION, /* enough BW on the link but not enough contiguous slices to satisfy the demand (fragmentation) */
    FORWARD_BLOCKING_CONTINUITY,  /* enough BW on the link but no spectrum continuity among the links of the path (spectrum continuity constraint)*/
    BACKWARD_BLOCKING,
    REMOTE_BLOCKING,
    PROVISIONING,
    RESERVING,
    ESTABLISHED,
    RELEASED,
    LOCAL_FAILURE,
    REMOTE_FAILURE,
    SUGGESTED_LABEL_MISMATCH,
    INGRESS_NODE_TX_TRANSPONDER_LACK_BLOCKING,
    INGRESS_NODE_RX_TRANSPONDER_LACK_BLOCKING,
    EGRESS_NODE_TX_TRANSPONDER_LACK_BLOCKING,
    EGRESS_NODE_RX_TRANSPONDER_LACK_BLOCKING,
    EGRESS_NODE_TX_TRANSPONDER_BLOCKING,
    INGRESS_NODE_TX_TRANSPONDER_BLOCKING,
    EGRESS_NODE_RX_TRANSPONDER_BLOCKING,
    INGRESS_NODE_RX_TRANSPONDER_BLOCKING,
    INGRESS_NODE_TX_TRANSPONDER_BACKWARD_BLOCKING,
    TRANSPONDER_MATCHING_BLOCKING,
    PATH_SPECTRUM_BLOCKING,
    TRANSPONDERS_PATHS_MATCHING_BLOCKING,
    COMPUTATION_OK,
    UNREACHABLE_DESTINATION,
    NONE
  } Lsp_event;
  
  typedef struct Rsvp_Link {
    int       tot_freq_slices;
    int       tot_available_freq_slices;
    PrgT_Vector*  allocated_freq_slots; //a vector of Frequency_slot
    int       min_frequency_indx; /* number of slices (of 12.5GHz) allocable are given by (max_frequency_indx - min_frequency_indx)/2*/
    int       max_frequency_indx; // TODO this information shuold be associated to each TX/RX
    int       outstream_indx; //the index ot the output stream connected to the link
  } Rsvp_Link;
  
      
  // TO DELETEEEEEEEEEEEEEEE
  //#define prg_mem_free(_addr)
  //#define prg_vector_destroy(_vec,_b)
  //#define prg_list_destroy(list_ptr, dealloc_elements) 
  //#define inet_address_destroy_dynamic(_addr)
  //#define op_pk_destroy(a)
#endif
