#include    "MD_blocking_statistic.h"

BlockingStatisticPtr    provisioning_routing_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_routing_bandwidth_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_routing_fragmentation_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_routing_comb_tunability_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_forward_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_backward_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_total_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_forward_bandwidth_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_forward_fragmentation_blocking_statptr = PRGC_NIL;
BlockingStatisticPtr    provisioning_forward_continuity_blocking_statptr = PRGC_NIL;

void register_blocking_statistics() {

	FIN(register_blocking_statistics());
	
    if (provisioning_routing_blocking_statptr == PRGC_NIL)
        provisioning_routing_blocking_statptr = blocking_statistic_create("Provisioning.Blocking Probability.Capacity.Routing Blocking Probability");
    if (provisioning_routing_bandwidth_blocking_statptr == PRGC_NIL)
        provisioning_routing_bandwidth_blocking_statptr = blocking_statistic_create("");
    if (provisioning_routing_fragmentation_blocking_statptr == PRGC_NIL)
        provisioning_routing_fragmentation_blocking_statptr = blocking_statistic_create("");
    if (provisioning_routing_comb_tunability_blocking_statptr == PRGC_NIL)
        provisioning_routing_comb_tunability_blocking_statptr = blocking_statistic_create("");
    if (provisioning_forward_blocking_statptr == PRGC_NIL)
        provisioning_forward_blocking_statptr = blocking_statistic_create("Provisioning.Blocking Probability.Capacity.Forward Blocking Probability");
    if (provisioning_backward_blocking_statptr == PRGC_NIL)
        provisioning_backward_blocking_statptr = blocking_statistic_create("Provisioning.Blocking Probability.Capacity.Backward Blocking Probability");
    if (provisioning_total_blocking_statptr == PRGC_NIL)
        provisioning_total_blocking_statptr = blocking_statistic_create("Provisioning.Blocking Probability.Capacity.Total Blocking Probability");
    if (provisioning_forward_bandwidth_blocking_statptr == PRGC_NIL)
        provisioning_forward_bandwidth_blocking_statptr = blocking_statistic_create("Provisioning.Blocking Probability.Capacity.Forward Blocking BW Lack Probability");
    if (provisioning_forward_fragmentation_blocking_statptr == PRGC_NIL)
        provisioning_forward_fragmentation_blocking_statptr = blocking_statistic_create("Provisioning.Blocking Probability.Capacity.Forward Blocking Fragmentation Probability");
    if (provisioning_forward_continuity_blocking_statptr == PRGC_NIL)
        provisioning_forward_continuity_blocking_statptr = blocking_statistic_create("Provisioning.Blocking Probability.Capacity.Forward Blocking Continuity Probability");
	
	FOUT;
}


/*------------------------------------UPDATE BLOCKING STATISTICS--------------------------*/
void update_blocking_statistics(Lsp_event event, double capacity) {
	double 	forward_blocked;
	double	total_blocked;
	double	routing_block_prob;
	double	backwad_prob;
	double	forwad_bw_lack_prob;
	double	forwad_fragmentation_prob;
	double	forwad_continuity_prob;
	double	forwad_prob;
	double	total_prob;
	double	lsp_weight;
/*	double  zeta,Relative_CI, computed_ci, ci;
	int 	cl;
	double	sigma_total_prob,mu_total_prob;
*/	
	FIN(update_blocking_statistics(event,sub_lsp_capacity,total_lsp_capacity));
	
	op_prg_mt_global_lock ();
		
		blocking_statistic_capacity_add(provisioning_routing_blocking_statptr,capacity);
        blocking_statistic_capacity_add(provisioning_backward_blocking_statptr,capacity);
        blocking_statistic_capacity_add(provisioning_forward_blocking_statptr,capacity);
        blocking_statistic_capacity_add(provisioning_total_blocking_statptr,capacity);
        blocking_statistic_capacity_add(provisioning_forward_bandwidth_blocking_statptr,capacity);
        blocking_statistic_capacity_add(provisioning_forward_fragmentation_blocking_stat,capacity);
        blocking_statistic_capacity_add(provisioning_forward_continuity_blocking_statptr,capacity);

		switch (event)	{
			case ROUTING_BLOCKING:					
                    blocking_statistic_blocked_capacity_add(provisioning_routing_blocking_statptr,capacity);
                    blocking_statistic_blocked_capacity_add(provisioning_total_blocking_statptr,capacity);
                    break;
			case BACKWARD_BLOCKING:	
                    blocking_statistic_blocked_capacity_add(provisioning_backward_blocking_statptr,capacity);
                    blocking_statistic_blocked_capacity_add(provisioning_total_blocking_statptr,capacity);
                    break;
			case FORWARD_BLOCKING_BW_LACK:	
                    blocking_statistic_blocked_capacity_add(provisioning_forward_bandwidth_blocking_statptr,capacity);
                    blocking_statistic_blocked_capacity_add(provisioning_forward_blocking_statptr,capacity);
                    blocking_statistic_blocked_capacity_add(provisioning_total_blocking_statptr,capacity);
                    break;
			case FORWARD_BLOCKING_FRAGMENTATION:	
                    blocking_statistic_blocked_capacity_add(provisioning_forward_fragmentation_blocking_stat,capacity);
                    blocking_statistic_blocked_capacity_add(provisioning_forward_blocking_statptr,capacity);
                    blocking_statistic_blocked_capacity_add(provisioning_total_blocking_statptr,capacity);
                    break;
			case FORWARD_BLOCKING_CONTINUITY:	
                    blocking_statistic_blocked_capacity_add(provisioning_forward_continuity_blocking_statptr,capacity);
                    blocking_statistic_blocked_capacity_add(provisioning_forward_blocking_statptr,capacity);
                    blocking_statistic_blocked_capacity_add(provisioning_total_blocking_statptr,capacity);
                    break;
			default: break;
		}
		
        
        blocking_statistic_write(provisioning_routing_blocking_statptr);
        blocking_statistic_write(provisioning_backward_blocking_statptr);
        blocking_statistic_write(provisioning_forward_blocking_statptr);
        blocking_statistic_write(provisioning_total_blocking_statptr);
        blocking_statistic_write(provisioning_forward_bandwidth_blocking_statptr);
        blocking_statistic_write(provisioning_forward_fragmentation_blocking_stat);
        blocking_statistic_write(provisioning_forward_continuity_blocking_statptr);
		
	op_prg_mt_global_unlock ();
		
		
		
	// confidence interval //
/*	npts_total_prob++;
	squared_sum_total_prob += pow(total_prob,2);
	sum_total_prob += total_prob;
		
	if (npts_total_prob > 100) {
		ci = 80;
		cl= 90;
		switch (cl) {
		case 80:
			zeta=1.28;
			break;
    	case 85:
			zeta=1.44;
			break;
    	case 90:
			zeta=1.645;
			break;
    	case 95:
        	zeta=1.96;
			break;
    	case 99:
        	zeta=2.575;
			break;
    	}
		
		mu_total_prob = sum_total_prob/npts_total_prob;
		sigma_total_prob = (squared_sum_total_prob - 2*mu_total_prob*sum_total_prob + pow(mu_total_prob,2))/(npts_total_prob-1);
		
		computed_ci=zeta*sqrt(sigma_total_prob/npts_total_prob);
		
		if (mu_total_prob !=0) Relative_CI=(2*computed_ci)/mu_total_prob;
		else Relative_CI=0;
		
		
		if (Relative_CI*100 > ci) {
			op_sim_end("confidence interval reached","","","");
		}
	}
	*/
	FOUT;
}









// SINGLETON VARS //

	Stathandle		*spectrum_utilization_stat_hndl=OPC_NIL;
	Stathandle    	*used_paths_stat_hndl=OPC_NIL;
	double			mu_total_prob = 0;
	double			npts_total_prob = 0;
	double			squared_sum_total_prob = 0;
	double			sum_total_prob = 0;

	Stathandle		*recovery_routing_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_backward_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_total_block_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_bw_lack_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_fragmentation_lsp_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_continuity_lsp_stat_hndl=OPC_NIL;

	Stathandle		*recovery_routing_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_backward_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_total_block_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_bw_lack_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_fragmentation_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovery_forward_block_continuity_capacity_stat_hndl=OPC_NIL;

	Stathandle		*recovery_time_stat_hndl=OPC_NIL;
	Stathandle		*total_recovered_percentage_hndl=OPC_NIL;
	Stathandle		*unrecovered_capacity_stat_hndl=OPC_NIL;
	Stathandle		*recovered_capacity_stat_hndl=OPC_NIL;

	unsigned short 	received_link_failure_notification = 0; // number of recovery notification received from the nodes attached to failed link
	unsigned int	total_capacity_to_recover = 0; // Gbps
	unsigned int	recovered_capacity = 0;
	unsigned int	unrecovered_capacity = 0;
	unsigned int	total_failed_lsps = 0;
	double			recovery_start_time = 0; //seconds
	double			recovery_end_time = 0; //Seconds
	Boolean			recovery_status = OPC_FALSE;

	double			recovery_generated_lsp = 0;
	double			recovery_routing_blocked_lsp = 0;
	double			recovery_backward_blocked_lsp = 0;
	double			recovery_forward_blocked_bw_lack_lsp = 0;
	double			recovery_forward_blocked_fragmentation_lsp = 0;
	double			recovery_forward_blocked_continuity_lsp = 0;
	
	double			recovery_generated_capacity = 0;
	double			recovery_routing_blocked_capacity = 0;
	double			recovery_backward_blocked_capacity = 0;
	double			recovery_forward_blocked_bw_lack_capacity = 0;
	double			recovery_forward_blocked_fragmentation_capacity = 0;
	double			recovery_forward_blocked_continuity_capacity = 0;	

	Stathandle		*lsps_0G_recovered_stat_hndl=OPC_NIL;
	Stathandle		*lsps_100G_recovered_stat_hndl=OPC_NIL;
	Stathandle		*lsps_200G_recovered_stat_hndl=OPC_NIL;
	Stathandle		*lsps_300G_recovered_stat_hndl=OPC_NIL;
	Stathandle		*lsps_400G_recovered_stat_hndl=OPC_NIL;

	unsigned int	lsps_0G_recovered = 0;
	unsigned int	lsps_100G_recovered = 0;
	unsigned int	lsps_200G_recovered = 0;
	unsigned int	lsps_300G_recovered = 0;
	unsigned int	lsps_400G_recovered = 0;


void register_statistics() {

	FIN(register_statistics());
	
	provisioning_routing_block_lsp_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_backward_block_lsp_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_bw_lack_lsp_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_fragmentation_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_continuity_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_total_block_lsp_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	provisioning_routing_block_capacity_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_backward_block_capacity_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_bw_lack_capacity_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_fragmentation_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_continuity_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_forward_block_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	provisioning_total_block_capacity_stat_hndl  = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	spectrum_utilization_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	used_paths_stat_hndl = (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	
	*provisioning_routing_block_lsp_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.LSP.Routing Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_backward_block_lsp_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.LSP.Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_bw_lack_lsp_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.LSP.Forward Blocking BW Lack Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_fragmentation_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Forward Blocking Fragmentation Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_continuity_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Forward Blocking Continuity Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Forward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_total_block_lsp_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.LSP.Total Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*provisioning_routing_block_capacity_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Routing Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_backward_block_capacity_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_bw_lack_capacity_stat_hndl = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Forward Blocking BW Lack Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_fragmentation_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Forward Blocking Fragmentation Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_continuity_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Forward Blocking Continuity Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_forward_block_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Forward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*provisioning_total_block_capacity_stat_hndl  = op_stat_reg ("Provisioning.Blocking Probability.Capacity.Total Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*spectrum_utilization_stat_hndl = op_stat_reg ("Spectrum Efficiency.Total Spectrum Utilization", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*used_paths_stat_hndl = op_stat_reg ("Routing.Number Of Used Paths For LSP", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	// RECOVERY STAT //
	recovery_routing_block_lsp_stat_hndl 					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_backward_block_lsp_stat_hndl 					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_bw_lack_lsp_stat_hndl 			= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_fragmentation_lsp_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_continuity_lsp_stat_hndl  		= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_lsp_stat_hndl  					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_total_block_lsp_stat_hndl  					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	recovery_routing_block_capacity_stat_hndl 					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_backward_block_capacity_stat_hndl 					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_bw_lack_capacity_stat_hndl 			= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_fragmentation_capacity_stat_hndl  	= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_continuity_capacity_stat_hndl  		= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_forward_block_capacity_stat_hndl  					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovery_total_block_capacity_stat_hndl  					= (Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	recovery_time_stat_hndl			=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	total_recovered_percentage_hndl = 	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	recovered_capacity_stat_hndl 	= 	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	unrecovered_capacity_stat_hndl	= 	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	lsps_0G_recovered_stat_hndl		=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	lsps_100G_recovered_stat_hndl	=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	lsps_200G_recovered_stat_hndl	=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	lsps_300G_recovered_stat_hndl	=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	lsps_400G_recovered_stat_hndl	=	(Stathandle*)prg_mem_alloc(sizeof(Stathandle));
	
	
	*recovery_routing_block_lsp_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.LSP.Routing Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_backward_block_lsp_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.LSP.Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_bw_lack_lsp_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.LSP.Forward Blocking BW Lack Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_fragmentation_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Forward Blocking Fragmentation Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_continuity_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Forward Blocking Continuity Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Forward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_total_block_lsp_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.LSP.Total Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*recovery_routing_block_capacity_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.Capacity.Routing Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_backward_block_capacity_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.Capacity.Backward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_bw_lack_capacity_stat_hndl = op_stat_reg ("Recovery.Blocking Probability.Capacity.Forward Blocking BW Lack Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_fragmentation_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Forward Blocking Fragmentation Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_continuity_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Forward Blocking Continuity Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_forward_block_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Forward Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovery_total_block_capacity_stat_hndl  = op_stat_reg ("Recovery.Blocking Probability.Capacity.Total Blocking Probability", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*recovery_time_stat_hndl = op_stat_reg ("Recovery.Total Recovery Time", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*total_recovered_percentage_hndl = op_stat_reg ("Recovery.Total Recovered Percentage", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*recovered_capacity_stat_hndl = op_stat_reg ("Recovery.Recovered Capacity", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*unrecovered_capacity_stat_hndl = op_stat_reg ("Recovery.Unrecovered Capacity", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	*lsps_0G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.0Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*lsps_100G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.100Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*lsps_200G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.200Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*lsps_300G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.300Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	*lsps_400G_recovered_stat_hndl = op_stat_reg ("Recovery.400G Components.400Gbps", OPC_STAT_INDEX_NONE, OPC_STAT_GLOBAL);
	
	FOUT;
}
/*___________________________________________________UPDATE STATISTICS______________________________________________________________________________________*/




/*---------------------------UPDATE NUMBER OF USED PATH STATISTICS--------------------*/
void update_used_path_statistics(unsigned short	number_of_paths){
	FIN(update_used_path_statistics(used_paths_htable_ptr));
		op_prg_mt_global_lock ();
		op_stat_write(*used_paths_stat_hndl,number_of_paths);
		op_prg_mt_global_unlock ();
	FOUT;
}



/*------------------------------------UPDATE LINK STATISTICS--------------------------*/
void update_links_statistics(Lsp_event event, int num_slices) {
	double	spectrum_allocation_ratio;
	FIN(update_links_statistics());
	
	op_prg_mt_global_lock ();
	
	switch (event)	{
			case ESTABLISHED: 	
							gbl_total_busy_slices = gbl_total_busy_slices + num_slices;
							break;
			case RELEASED: 	
							gbl_total_busy_slices = gbl_total_busy_slices - num_slices;
							break;
	}
	spectrum_allocation_ratio = (double)gbl_total_busy_slices*100.0/gbl_total_slices;
	op_stat_write(*spectrum_utilization_stat_hndl,spectrum_allocation_ratio);
	
	op_prg_mt_global_unlock ();
	FOUT;
}

/*------------------------------RECOVERY STATISTICS------------------------------------------*/


void stat_recovery_start() {
FIN(stat_recovery_start());
	if (recovery_status == OPC_FALSE) {
		recovery_status = OPC_TRUE;
		recovery_start_time = op_sim_time ();
		#if defined(LINK_FAILURE_DEBUG)
		printf("recovery starts at %f seconds, capacity to recover %dGbps (up to now)\n",recovery_start_time, total_capacity_to_recover);
		#endif
		op_stat_write(*unrecovered_capacity_stat_hndl,0);
		op_stat_write(*recovered_capacity_stat_hndl,0);
	}
FOUT;
}


static void recovery_end() {
double	total_recovery_time,recovered_percentage;
Objid	failure_node_objid,failure_process_objid;

double 	forward_blocked;
double	total_blocked;
double	routing_block_prob;
double	backwad_prob;
double	forwad_bw_lack_prob;
double	forwad_fragmentation_prob;
double	forwad_continuity_prob;
double	forwad_prob;
double	total_prob;
unsigned int total_400G_lsps;

FIN(recovery_end());
	recovery_status = OPC_FALSE;
	recovery_end_time = op_sim_time ();

	total_recovery_time = recovery_end_time-recovery_start_time;
	op_stat_write(*recovery_time_stat_hndl,total_recovery_time);
			
	recovered_percentage = (double)recovered_capacity*100/(recovered_capacity + unrecovered_capacity);
	op_stat_write(*total_recovered_percentage_hndl,recovered_percentage);
	
	//#if defined(LINK_FAILURE_DEBUG)
	printf("recovery ends at %f seconds, capacity recovered %dGbps, unrecovered %dGbps \n",recovery_end_time,recovered_capacity,unrecovered_capacity);
	//#endif

	forward_blocked = recovery_forward_blocked_bw_lack_lsp + recovery_forward_blocked_fragmentation_lsp + recovery_forward_blocked_continuity_lsp;
	total_blocked = recovery_routing_blocked_lsp + forward_blocked + recovery_backward_blocked_lsp;

	routing_block_prob = (double)recovery_routing_blocked_lsp/recovery_generated_lsp;
	backwad_prob = (double)recovery_backward_blocked_lsp/recovery_generated_lsp;
	forwad_bw_lack_prob = (double)recovery_forward_blocked_bw_lack_lsp/recovery_generated_lsp;
	forwad_fragmentation_prob = (double)recovery_forward_blocked_fragmentation_lsp/recovery_generated_lsp;
	forwad_continuity_prob = (double)recovery_forward_blocked_continuity_lsp/recovery_generated_lsp;
	forwad_prob = (double)forward_blocked/recovery_generated_lsp;
	total_prob = (double)total_blocked/recovery_generated_lsp;

	op_stat_write(*recovery_routing_block_lsp_stat_hndl,routing_block_prob);
	op_stat_write(*recovery_backward_block_lsp_stat_hndl,backwad_prob);
	op_stat_write(*recovery_forward_block_bw_lack_lsp_stat_hndl,forwad_bw_lack_prob);
	op_stat_write(*recovery_forward_block_fragmentation_lsp_stat_hndl,forwad_fragmentation_prob);
	op_stat_write(*recovery_forward_block_continuity_lsp_stat_hndl,forwad_continuity_prob);
	op_stat_write(*recovery_forward_block_lsp_stat_hndl,forwad_prob);
	op_stat_write(*recovery_total_block_lsp_stat_hndl,total_prob);
	
	
	forward_blocked = recovery_forward_blocked_bw_lack_capacity + recovery_forward_blocked_fragmentation_capacity + recovery_forward_blocked_continuity_capacity;
	total_blocked = recovery_routing_blocked_capacity + forward_blocked + recovery_backward_blocked_capacity;

	routing_block_prob = (double)recovery_routing_blocked_capacity/recovery_generated_capacity;
	backwad_prob = (double)recovery_backward_blocked_capacity/recovery_generated_capacity;
	forwad_bw_lack_prob = (double)recovery_forward_blocked_bw_lack_capacity/recovery_generated_capacity;
	forwad_fragmentation_prob = (double)recovery_forward_blocked_fragmentation_capacity/recovery_generated_capacity;
	forwad_continuity_prob = (double)recovery_forward_blocked_continuity_capacity/recovery_generated_capacity;
	forwad_prob = (double)forward_blocked/recovery_generated_capacity;
	total_prob = (double)total_blocked/recovery_generated_capacity;

	op_stat_write(*recovery_routing_block_capacity_stat_hndl,routing_block_prob);
	op_stat_write(*recovery_backward_block_capacity_stat_hndl,backwad_prob);
	op_stat_write(*recovery_forward_block_bw_lack_capacity_stat_hndl,forwad_bw_lack_prob);
	op_stat_write(*recovery_forward_block_fragmentation_capacity_stat_hndl,forwad_fragmentation_prob);
	op_stat_write(*recovery_forward_block_continuity_capacity_stat_hndl,forwad_continuity_prob);
	op_stat_write(*recovery_forward_block_capacity_stat_hndl,forwad_prob);
	op_stat_write(*recovery_total_block_capacity_stat_hndl,total_prob);


	total_400G_lsps = lsps_0G_recovered+lsps_100G_recovered+lsps_200G_recovered+lsps_300G_recovered+lsps_400G_recovered;
	op_stat_write(*lsps_0G_recovered_stat_hndl,(double)100.0*lsps_0G_recovered/total_400G_lsps);
	op_stat_write(*lsps_100G_recovered_stat_hndl,(double)100.0*lsps_100G_recovered/total_400G_lsps);
	op_stat_write(*lsps_200G_recovered_stat_hndl,(double)100.0*lsps_200G_recovered/total_400G_lsps);
	op_stat_write(*lsps_300G_recovered_stat_hndl,(double)100.0*lsps_300G_recovered/total_400G_lsps);
	op_stat_write(*lsps_400G_recovered_stat_hndl,(double)100.0*lsps_400G_recovered/total_400G_lsps);
		
	

	// initialize again the variables //
	received_link_failure_notification = 0;
	total_capacity_to_recover = 0; // Gbps
	recovered_capacity = 0;
	unrecovered_capacity = 0;
	total_failed_lsps = 0;

	recovery_generated_lsp = 0;
	recovery_routing_blocked_lsp = 0;
	recovery_backward_blocked_lsp = 0;
	recovery_forward_blocked_bw_lack_lsp = 0;
	recovery_forward_blocked_fragmentation_lsp = 0;
	recovery_forward_blocked_continuity_lsp = 0;

	recovery_generated_capacity = 0;
	recovery_routing_blocked_capacity = 0;
	recovery_backward_blocked_capacity = 0;
	recovery_forward_blocked_bw_lack_capacity = 0;
	recovery_forward_blocked_fragmentation_capacity = 0;
	recovery_forward_blocked_continuity_capacity = 0;


	lsps_0G_recovered = 0;
	lsps_100G_recovered = 0;
	lsps_200G_recovered = 0;
	lsps_300G_recovered = 0;
	lsps_400G_recovered = 0;

	// send interrupt to restore the link //
	failure_node_objid = op_id_from_name (0, OPC_OBJTYPE_NODE_FIX, "Failure Scheduler");
	if (failure_node_objid == OPC_OBJID_INVALID) {
		printf("statistics ERROR: failure scheduler node not found, cannot recover the failed links\n");
	}
	else {
		failure_process_objid = op_id_from_name (failure_node_objid, OPC_OBJTYPE_PROC, "failure process");
		if (failure_process_objid == OPC_OBJID_INVALID) {
			printf("statistics ERROR: failure process not found inside the failure scheduler node, cannot recover the failed links\n");
		}
		else {
			op_intrpt_schedule_remote (op_sim_time (),0,failure_process_objid);
		}
	}

FOUT;
}

void stat_increase_capacity_to_recover(unsigned int capacity) {
FIN(stat_increase_capacity_to_recover(capacity));
	// increment the total capacity to recover //
	total_capacity_to_recover += capacity;
	#if defined(LINK_FAILURE_DEBUG)
	printf("capacity to recover %dGbps (up to now)\n",total_capacity_to_recover);
	#endif

FOUT;
}

void stat_decrease_capacity_to_recover(unsigned int capacity) {
FIN(stat_decrease_capacity_to_recover(capacity));	
	// decrement the total capacity to recover //
	// in case an LSP has been already tear down //
	total_capacity_to_recover -= capacity;
	#if defined(LINK_FAILURE_DEBUG)
	printf("capacity to recover %dGbps (up to now)\n",total_capacity_to_recover);
	#endif
	if ( total_capacity_to_recover == 0 && received_link_failure_notification % 2 == 0) {
		recovery_end();
	}

FOUT;
}

void stat_notify_failed_lsps (unsigned int failed_lsps) {
FIN(stat_notify_failed_lsps(failed_lsps));
	// increment the number of notification received //
	received_link_failure_notification++;

	total_failed_lsps += failed_lsps;

	if ( failed_lsps == 0 
		 && total_capacity_to_recover>0
		 && (recovered_capacity + unrecovered_capacity) >= total_capacity_to_recover 
		 && received_link_failure_notification % 2 == 0) {
	
		recovery_end();
	}

FOUT;
}

void update_recovery_blocking_statistics(Lsp_event event,unsigned int sub_lsp_capacity, unsigned int total_lsp_capacity) {
	double	lsp_weight;
	
	FIN(update_blocking_statistics(event));
	
	op_prg_mt_global_lock ();
		
		recovery_generated_capacity += sub_lsp_capacity;
	
		lsp_weight = (double)sub_lsp_capacity/total_lsp_capacity;
		recovery_generated_lsp += lsp_weight;
	
		switch (event)	{
			case ROUTING_BLOCKING:					
													recovery_routing_blocked_capacity += sub_lsp_capacity;
													recovery_routing_blocked_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case BACKWARD_BLOCKING:					
													recovery_backward_blocked_capacity += sub_lsp_capacity;
													recovery_backward_blocked_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case FORWARD_BLOCKING_BW_LACK:	
													recovery_forward_blocked_bw_lack_capacity += sub_lsp_capacity;
													recovery_forward_blocked_bw_lack_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case FORWARD_BLOCKING_FRAGMENTATION:	
													recovery_forward_blocked_fragmentation_capacity += sub_lsp_capacity;
													recovery_forward_blocked_fragmentation_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case FORWARD_BLOCKING_CONTINUITY:	
													recovery_forward_blocked_continuity_capacity += sub_lsp_capacity;
													recovery_forward_blocked_continuity_lsp += lsp_weight;
													unrecovered_capacity += sub_lsp_capacity;
													op_stat_write(*unrecovered_capacity_stat_hndl,unrecovered_capacity);
													break;
			case ESTABLISHED: 
													recovered_capacity += sub_lsp_capacity;
													op_stat_write(*recovered_capacity_stat_hndl,recovered_capacity);
													break;
			case RELEASED: break;
		}
		
		// check if the recovery interval is concluded //
		if ((recovered_capacity + unrecovered_capacity) >= total_capacity_to_recover && received_link_failure_notification % 2 == 0) {
		
			recovery_end();
			
		}
		
	op_prg_mt_global_unlock ();
	
	FOUT;
}



/// create histogram for 400G connection recevered///
void update_recovery_400G(int restored_cap) {
	FIN(update_recovery_400G(restored_cap));
	
	switch (restored_cap) {
		case 0:		
					lsps_0G_recovered++;
					break;
		case 100:	
					lsps_100G_recovered++;
					break;
		case 200:
					lsps_200G_recovered++;
					break;
		case 300:
					lsps_300G_recovered++;
					break;
		case 400:	
					lsps_400G_recovered++;
					break;
	}
	
	FOUT;
}


