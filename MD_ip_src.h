#ifndef	_MD_IP_H_INCLUDED_
	#define _MD_IP_H_INCLUDED_
	
    #include 	<opnet.h>
    #include 	<prg_bin_hash.h>
    #include 	<ip_addr_v4.h>
  
  #define   FW_TABLE_UPDATE_CODE    99
  
  #define NO_DEBUG_OUTPUT
    
  // multicast addresses //
    #define IP_ADDRESS_ALLSPFROUTERS    "224.0.0.5"
	#define IP_ADDRESS_ALLDROUTERS      "224.0.0.6"
  
  typedef enum  Protocol_Numbers{
    TCP =6,
		UDP = 17,
    RSVP = 46,
    OSPFIGP = 89
	} Protocol_Numbers;

	/* define an entry structure for the forwarding table */	
	typedef struct Forwarding_Entry {
			InetT_Address  addr;
			PrgT_Vector *interfaces_vptr; // vector of interfaces (in case of multicast addresses)
	} Forwarding_Entry;
		
	/*	Function Prototypes					*/
    //InetT_Address* forwarding_table_reverse_lookup(PrgT_Bin_Hash_Table *forwarding_table_ptr, int outstream_indx);
    PrgT_Vector* forwarding_table_lookup(PrgT_Bin_Hash_Table *forwarding_table_ptr, const InetT_Address* addr_ptr);
    OpT_uInt32* forwarding_table_first_entry_lookup(PrgT_Bin_Hash_Table *forwarding_table_ptr, const InetT_Address* addr_ptr);
    //void pkt_send_to_ip_layer (Packet* rsvp_pkptr, const InetT_Address* dest_addr);
#endif
