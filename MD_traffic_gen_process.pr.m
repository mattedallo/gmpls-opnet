MIL_3_Tfile_Hdr_ 171A 171A modeler 9 54BE3203 54C1053D 25 labeffa matteo 0 0 none none 0 0 none 8C397B34 232B 0 0 0 0 0 0 2b1a 1                                                                                                                                                                                                                                                                                                                                                                                                ��g�      D  �  �  n  r  �   �   �   �   �  !  !#  !'  �  �      traffic file   �������   ����       ����      ����      ����             Ethe name of the .csv file containing the list of connection requests.    it must be formatted as follows:   FSource;Destination;Capacity (Gbps);Arrival Time Instant (sec);Duration   1.1.1.1;2.2.2.2;100;0;inf   "12.12.12.12;31.31.31.31;400;50;888�Z                 	   begsim intrpt         
   ����   
   doc file            	nd_module      endsim intrpt             ����      failure intrpts            disabled      intrpt interval         ԲI�%��}����      priority              ����      recovery intrpts            disabled      subqueue                     count    ���   
   ����   
      list   	���   
          
      super priority             ����             K/* hash table of all GMPLS nodes in the network. indexed with IP address */   $PrgT_Bin_Hash_Table *	\nodes_ht_ptr;       #/* file name of the traffic file */   char	\traffic_file_str[128];          Boolean  end = OPC_FALSE;      #include 	<math.h>   #include 	<oms_pr.h>   #include	<ip_addr_v4.h>   #include	<prg_bin_hash.h>   #include 	<stdlib.h>   #include 	<csv_parser.h>    #include 	"MD_connection_desc.h"       typedef struct Node_Des {     Objid node_objid;     Objid	ip_objid;     Objid rsvp_objid;   } Node_Des;                                                  Z   Z          
   init   
       
      -// let other processes to register themselves       %// save the ip address of the node //   Kop_ima_obj_attr_get_str(op_id_self(), "traffic file",128,traffic_file_str);       'if (0 == strcmp(traffic_file_str,"")) {    end = OPC_TRUE;   }   
                     
   ����   
          pr_state        J   Z          
   schedule traffic   
       
   S   CsvParser *csvparser;    //CsvRow *header;   CsvRow *row;   4char full_path[_MAX_PATH],directory[1024],drive[16];       \prg_tfile_path_get("MD_traffic_gen_process", PrgC_Tfile_Type_Process, full_path, _MAX_PATH);       <_splitpath( full_path, drive, directory, OPC_NIL, OPC_NIL );   ?sprintf(full_path, "%s%s%s", drive,directory,traffic_file_str);       -csvparser = CsvParser_new(full_path, ";", 1);   /*   (header = CsvParser_getHeader(csvparser);   if (header == NULL) {   9    printf("%s\n", CsvParser_getErrorMessage(csvparser));       return 1;   }   2char **headerFields = CsvParser_getFields(header);   8for (i = 0 ; i < CsvParser_getNumFields(header) ; i++) {   +    printf("TITLE: %s\n", headerFields[i]);   }   CsvParser_destroy_row(header);   */       /while ( (row = CsvParser_getRow(csvparser)) ) {       char **rowFields;   	Ici	 *iciptr;   &	Connection_Desc *connection_desc_ptr;   ,	double arrival_instant, capacity, duration;   	InetT_Address node_addr;   	Node_Des *node_des_ptr;   	char *src_addr_str;   	char *dst_addr_str;       %	if (CsvParser_getNumFields(row)<5) {   O	 op_sim_error(OPC_SIM_ERROR_ABORT,"Traffic Generator","Bad traffic csv file");   	 CsvParser_destroy_row(row);   	 continue;   	}   	   &	rowFields = CsvParser_getFields(row);   	   	src_addr_str = rowFields[0];   	dst_addr_str = rowFields[1];    	capacity =  atoi(rowFields[2]);   &	arrival_instant = atof(rowFields[3]);   	duration = atof(rowFields[4]);   	if (0.0 == duration) {   -		// set duration to -1 (considered infinite)   		duration = -1;   	}   		   -	// get the source node to send the interrupt   E	node_addr = inet_address_create(src_addr_str, InetC_Addr_Family_v4);   	   s	node_des_ptr = (Node_Des*)prg_bin_hash_table_item_get(nodes_ht_ptr, inet_address_addr_ptr_get_const(&node_addr) );   	   !	inet_address_destroy(node_addr);   	    	if (PRGC_NIL == node_des_ptr) {   	  CsvParser_destroy_row(row);   	  continue;   	}   	   ;	connection_desc_ptr = connection_desc_create(src_addr_str,   >                                                 dst_addr_str,   :                                                 capacity,   A                                                 arrival_instant,   ;                                                 duration);   	   E	// create an ICI to send informations together with the interrupt //   +	iciptr = op_ici_create ("MD_generic_ici");   :	op_ici_attr_set_ptr(iciptr, "data", connection_desc_ptr);   	op_ici_install(iciptr);   	   $	//schedule remote interrupt to RSVP   X	op_intrpt_schedule_remote (op_sim_time()+arrival_instant, 8, node_des_ptr->rsvp_objid);   	   	CsvParser_destroy_row(row);          }       CsvParser_destroy(csvparser);   
                     
   ����   
          pr_state         �   Z          
   init0   
       
   G   {  //   5   // CREATE AN HASH TABLE CONTAINING ALL GMPLS NODES      //       int number_of_nodes,i;   :// number of fixed nodes (not all of them are GMPLS nodes)   =number_of_nodes = op_topo_object_count(OPC_OBJTYPE_NODE_FIX);       [nodes_ht_ptr = prg_bin_hash_table_create (log(number_of_nodes)/log(2), sizeof(OpT_uInt32));       #for (i=0; i<number_of_nodes; i++) {   	Objid node_objid;   "	List 				proc_record_handle_list;   (	OmsT_Pr_Handle 		process_record_handle;   	   	   6	node_objid = op_topo_object(OPC_OBJTYPE_NODE_FIX, i);   	   J	if (oms_pr_process_discover (OPC_OBJID_INVALID, &proc_record_handle_list,   .		"node objid",   OMSC_PR_OBJID,  	node_objid,   (		"protocol", 	OMSC_PR_STRING, 	"MD_ip",   %		OPC_NIL) == OPC_COMPCODE_SUCCESS) {   		   $		char	addr_str[INETC_ADDR_STR_LEN];   		InetT_Address node_addr;   		Node_Des *node_des_ptr;   	   =		node_des_ptr = (Node_Des*) prg_mem_alloc(sizeof(Node_Des));   (		node_des_ptr->node_objid = node_objid;   		   1		// There should be only one IP module per node.   8		if (op_prg_list_size (&proc_record_handle_list) > 1) {   u			op_sim_end ("Traffic generator","More than one IP process has been identified in a single node",OPC_NIL, OPC_NIL);   		}   		   		// Get handle of IP process   k		process_record_handle = (OmsT_Pr_Handle) op_prg_list_remove (&proc_record_handle_list, OPC_LISTPOS_HEAD);   		   &		// get the ObjectId of the IP Module   d		oms_pr_attr_get (process_record_handle, "module objid",	OMSC_PR_OBJID, &(node_des_ptr->ip_objid));   		   %		// get the address of the IP Module   m		if (oms_pr_attr_get(process_record_handle, "address",	OMSC_PR_STRING, &addr_str) != OPC_COMPCODE_SUCCESS) {   V			op_sim_end ("Traffic generator","ip module doesn't have address",OPC_NIL, OPC_NIL);   		}   		   B		node_addr = inet_address_create(addr_str, InetC_Addr_Family_v4);   		   q		prg_bin_hash_table_item_insert(nodes_ht_ptr, inet_address_addr_ptr_get_const(&node_addr),node_des_ptr,OPC_NIL);   		   "		inet_address_destroy(node_addr);   		   		// DISCOVER THE RSVP module   K		if (oms_pr_process_discover (OPC_OBJID_INVALID, &proc_record_handle_list,   /			"node objid",   OMSC_PR_OBJID,  	node_objid,   ,			"protocol", 	OMSC_PR_STRING, 	"rsvp-te",    &			OPC_NIL) == OPC_COMPCODE_SUCCESS) {   		   #			// Get handle to RSVP-TE process   l			process_record_handle = (OmsT_Pr_Handle) op_prg_list_remove (&proc_record_handle_list, OPC_LISTPOS_HEAD);   		   ,			// get the ObjectId of the RSVP-TE module   g			oms_pr_attr_get (process_record_handle, "module objid",	OMSC_PR_OBJID, &(node_des_ptr->rsvp_objid));   		}   		else {   k			op_sim_end ("PCEP root process","There is no RSVP-TE process configured in this node.",OPC_NIL,OPC_NIL);   		}   	}   }       }   
                     
   ����   
          pr_state         �   �          
   end   
                                       ����             pr_state                        �   M      W   Y   �   Z          
   tr_0   
       
   !end   
       ����          
    ����   
          ����                       pr_transition              J   Z      �   Y  I   Z          
   tr_1   
       ����          ����          
    ����   
          ����                       pr_transition                �   �      b   j   �   �          
   tr_3   
       
   end   
       ����          
    ����   
          ����                       pr_transition                 �     I   [   �   �          
   tr_4   
       ����          ����          
    ����   
          ����                       pr_transition                           
csv_parser   MD_connection_desc   oms_pr                    