#include	"MD_spectrum_assignment_src.h"

/*______________________________________________OPERATION WITH LABEL (SET)_____________________________________________________________________________________________________________________________*/

/*-----------------------------------------LEABEL SELECTION FUNCTION--------------------------------------------------*/
int* sa_select_label_from_set (Label_set_object const* label_set_ptr, Allocation_policy a_policy,int requested_slices, PrgT_List *excluded_freq_ranges_lptr /*, unsigned long tot_lsp_request, unsigned long tot_slices_request*/) {
	int 			selected_label,*selected_label_ptr,free_slot_width,larger_slot_width,prev_freq_indx,first_freq_slot_indx;
	int				label_vector_size,extracted_index,freq_indx,i;
	//double			avg_slot_req;
	Distribution*	uniform_dist;
	Boolean			valid_selected_label;
	PrgT_List_Cell 	*cell_ptr;
	Excluded_Frequency_Range *excluded_freq_range;
	
	FIN(sa_select_label_from_set(label_set_ptr,a_policy,requested_slices,excluded_freq_ranges_lptr));

	switch (a_policy) {
	
		case FIRST_FIT:
						if (excluded_freq_ranges_lptr == PRGC_NIL) {
						// if we dont have a range of frequency that cannot be used we simply pick the first label from the vector //
							selected_label=*((int*)prg_vector_access(label_set_ptr->label_vector,0));
							valid_selected_label = OPC_TRUE;
						}
						else {
							label_vector_size = prg_vector_size(label_set_ptr->label_vector);
							for (i=0;i<label_vector_size;i++) {
								// select the label from the label vector //
								selected_label=*((int*)prg_vector_access(label_set_ptr->label_vector,i));
								// assume the label as valid //
								valid_selected_label = OPC_TRUE;
								// iterate the list of excluded frequency range to check if the spectrum that will be occupied //
								// with the considered selected label will not overlap with the excluded freq range//
								cell_ptr = prg_list_head_cell_get (excluded_freq_ranges_lptr);
								while (cell_ptr!=PRGC_NIL ) {
									excluded_freq_range = (Excluded_Frequency_Range *) prg_list_cell_data_get (cell_ptr);
									
									if ( (selected_label+requested_slices)< excluded_freq_range->excluded_lower_freq_indx  ) {
										// get the next excluded frequency range//
										cell_ptr = prg_list_cell_next_get (cell_ptr);
										continue;
									}
									else {
										if ( (selected_label-requested_slices)> excluded_freq_range->excluded_upper_freq_indx  ) {
											// get the next excluded frequency range//
											cell_ptr = prg_list_cell_next_get (cell_ptr);
											continue;
										}
										else {
											valid_selected_label = OPC_FALSE;
											break;
										}
									}
								}
								
								if (valid_selected_label == OPC_TRUE) {
									// we found a valid label //
									break;
								}
							}
						}
						break;
						
						
						
		case LAST_FIT:
						label_vector_size=prg_vector_size(label_set_ptr->label_vector);
						if (excluded_freq_ranges_lptr == PRGC_NIL) {
							selected_label=*((int*)prg_vector_access(label_set_ptr->label_vector, label_vector_size-1));
							valid_selected_label = OPC_TRUE;
						}
						else {
							for (i=label_vector_size-1;i>=0;i--) {
								// select the label from the label vector //
								selected_label=*((int*)prg_vector_access(label_set_ptr->label_vector,i));
								// assume the label as valid //
								valid_selected_label = OPC_TRUE;
								// iterate the list of excluded frequency range to check if the spectrum that will be occupied //
								// with the considered selected label will not overlap with the excluded freq range//
								cell_ptr = prg_list_head_cell_get (excluded_freq_ranges_lptr);
								while (cell_ptr!=PRGC_NIL ) {
									excluded_freq_range = (Excluded_Frequency_Range *) prg_list_cell_data_get (cell_ptr);
									
									if ( (selected_label+requested_slices)< excluded_freq_range->excluded_lower_freq_indx  ) {
										// get the next excluded frequency range//
										cell_ptr = prg_list_cell_next_get (cell_ptr);
										continue;
									}
									else {
										if ( (selected_label-requested_slices)> excluded_freq_range->excluded_upper_freq_indx  ) {
											// get the next excluded frequency range//
											cell_ptr = prg_list_cell_next_get (cell_ptr);
											continue;
										}
										else {
											valid_selected_label = OPC_FALSE;
											break;
										}
									}
								}
								
								if (valid_selected_label == OPC_TRUE) {
									// we found a valid label //
									break;
								}
							}
						}
						break;
						
						
						
		case RANDOM:	
						label_vector_size=prg_vector_size(label_set_ptr->label_vector);
						if (excluded_freq_ranges_lptr == PRGC_NIL) {
								uniform_dist = op_dist_load("uniform_int", 0, label_vector_size-1);
								extracted_index = op_dist_outcome(uniform_dist);
								op_dist_unload(uniform_dist);
								selected_label=*((int*)prg_vector_access(label_set_ptr->label_vector, extracted_index));
								valid_selected_label = OPC_TRUE;
						}
						else {
							while (label_vector_size > 0) { 
								uniform_dist = op_dist_load("uniform_int", 0, label_vector_size-1);
								extracted_index = op_dist_outcome(uniform_dist);
								op_dist_unload(uniform_dist);
								selected_label=*((int*)prg_vector_remove(label_set_ptr->label_vector, extracted_index));
								label_vector_size--;
								// assume the label as valid //
								valid_selected_label = OPC_TRUE;
								// iterate the list of excluded frequency range to check if the spectrum that will be occupied //
								// with the considered selected label will not overlap with the excluded freq range//
								cell_ptr = prg_list_head_cell_get (excluded_freq_ranges_lptr);
								while (cell_ptr!=PRGC_NIL ) {
									excluded_freq_range = (Excluded_Frequency_Range *) prg_list_cell_data_get (cell_ptr);
									
									if ( (selected_label+requested_slices)< excluded_freq_range->excluded_lower_freq_indx  ) {
										// get the next excluded frequency range//
										cell_ptr = prg_list_cell_next_get (cell_ptr);
										continue;
									}
									else {
										if ( (selected_label-requested_slices)> excluded_freq_range->excluded_upper_freq_indx  ) {
											// get the next excluded frequency range//
											cell_ptr = prg_list_cell_next_get (cell_ptr);
											continue;
										}
										else {
											valid_selected_label = OPC_FALSE;
											break;
										}
									}
								}
								
								if (valid_selected_label == OPC_TRUE) {
									// we found a valid label //
									break;
								}
							}
						}
						
						break;
		
						
						
						
						
		case EXACT_FIT:	
						// whatever will be the selected label it is valid because the exact fit will never be used //
						// by the PCE for the suggested label selection //
						valid_selected_label = OPC_TRUE;
						
						free_slot_width = 1;
						larger_slot_width = 1;
						label_vector_size=prg_vector_size(label_set_ptr->label_vector);
						prev_freq_indx=*((int*)prg_vector_access(label_set_ptr->label_vector, 0));
						first_freq_slot_indx = prev_freq_indx;
						selected_label = prev_freq_indx;
						
						for (i=1; i<label_vector_size; i++) {
							freq_indx=*((int*)prg_vector_access(label_set_ptr->label_vector, i));
							
							/*count the contiguous frequency that belongs to the same free slot*/
							if ((prev_freq_indx+2) >= freq_indx) {
								free_slot_width++;
							}
							else {
								if (free_slot_width == 1) { 
								/* in this case the slot exact fit the demand*/
									selected_label=first_freq_slot_indx;
									break;
								}
								if (free_slot_width > larger_slot_width) {
									larger_slot_width = free_slot_width;
									selected_label = first_freq_slot_indx;
								}
								
								first_freq_slot_indx = freq_indx;
								free_slot_width = 1;
							}
							
							prev_freq_indx=freq_indx;
						}
						
						if (i==label_vector_size) {
							if (free_slot_width == 1) {
								selected_label=first_freq_slot_indx;
							}
							else 
								if (free_slot_width > larger_slot_width) {
									selected_label = first_freq_slot_indx;
								}
						}
						
						break;
		/*				
		case FIRST_PROPER_FIT:
								free_slot_width = 1;
								larger_slot_width = 1;
								avg_slot_req = (double) tot_slices_request/tot_lsp_request;
								label_vector_size=prg_vector_size(label_set_ptr->label_vector);
								prev_freq_indx=*((int*)prg_vector_access(label_set_ptr->label_vector, 0));
								first_freq_slot_indx = prev_freq_indx;
								selected_label = prev_freq_indx;
								
								for (i=1; i<label_vector_size; i++) {
									freq_indx=*((int*)prg_vector_access(label_set_ptr->label_vector, i));
									
									/*count the contiguous frequency that belongs to the same free slot*/
		/*							if ((prev_freq_indx+2) >= freq_indx) {
										free_slot_width++;
									}
									else {
										if ((free_slot_width == 1) || (free_slot_width - 1 >= avg_slot_req)) { 
										/* in this case the slot exact fit the demand*/
		/*									selected_label=first_freq_slot_indx;
											break;
										}
										if (free_slot_width > larger_slot_width) {
											larger_slot_width = free_slot_width;
											selected_label = first_freq_slot_indx;
										}
										
										first_freq_slot_indx = freq_indx;
										free_slot_width = 1;
									}
										
									prev_freq_indx=freq_indx;
								}
						
								if (i==label_vector_size) {
									if ((free_slot_width == 1) || (free_slot_width - 1 >= avg_slot_req)) {
										selected_label=first_freq_slot_indx;
									}
									else 
										if (free_slot_width > larger_slot_width) {
											selected_label = first_freq_slot_indx;
										}
								}
						
								
								break;
		*/
		default: break;
	}
	
	if (valid_selected_label == OPC_TRUE) {
		selected_label_ptr = (int*) prg_mem_alloc(sizeof(int));
		*selected_label_ptr = selected_label;
		FRET(selected_label_ptr);
	}
	else {
		FRET(OPC_NIL);
	}
}
