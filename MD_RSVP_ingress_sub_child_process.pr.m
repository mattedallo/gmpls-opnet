MIL_3_Tfile_Hdr_ 171A 171A modeler 9 5203A70F 5526ABE3 152 labeffa matteo 0 0 none none 0 0 none 6FAFD10F A632 0 0 0 0 0 0 2b1a 1                                                                                                                                                                                                                                                                                                                                                                                               ŤÍg      D   H   L      "  Łe  Łi  Łm  Łq  ¤&  ¤*  ¤.  "  "           	   begsim intrpt             ˙˙˙˙      doc file            	nd_module      endsim intrpt             ˙˙˙˙      failure intrpts            disabled      intrpt interval         Ô˛I­%Ă}˙˙˙˙      priority              ˙˙˙˙      recovery intrpts            disabled      subqueue                     count    ˙˙˙   
   ˙˙˙˙   
      list   	˙˙˙   
          
      super priority             ˙˙˙˙          F   9/* Shared information that all children need to access */   "Module_Shared_Data *	\shared_data;       5/* the output interface index for the data packets */   "OpT_uInt32	\output_interface_indx;       4/* specify the role of node for this specific LSP */   Node_role	\node_role;       /* id of the child process */   int	\own_pro_id;       ,/* Save the Session related to this child */   $Session_object*	\session_object_ptr;       F/* the frequency slot reserved for the lsp associated to this child */   Frequency_slot	\lsp_freq_slot;       4/* save the number of hops, useful for statistics */   int	\lsp_number_of_hops;       U/* useful for statistics purposes, to keep track of the number of slices requested */   .Sender_tspec_object*	\sender_tspec_object_ptr;       >/* next node ip address, useful when create the opaque_link */   InetT_Address	\next_node_addr;       1/* the state of the LSP returned to the parent */   Lsp_event*	\ret_lsp_state;       unsigned int	\lsp_capacity;       /* the ero object pointer */   ERO_object*	\ero_object_ptr;       (char	\node_addr_str[INETC_ADDR_STR_LEN];       $Ip_Api_Interface*	\ip_interface_ptr;       $Global_Manager*	\global_manager_ptr;       LSP*	\global_lsp_ptr;       #double	\start_provisioning_instant;       !double	\end_provisioning_instant;       Boolean	\lsp_established;       P/* the central frequency of the lsp (may not be the one of the carriers used) */   int	\central_frequency_indx;       !Transponder*	\tx_transponder_ptr;       /* NOT USED up to now */   !Transponder*	\rx_transponder_ptr;       Boolean	\bidirectional_lsp;       OpT_uInt32	\tx_transponder_id;       C/* A vector of carrier IDs of the TX transponder used by the LSP */   (PrgT_Vector *	\used_tx_carrier_ids_vptr;       5/* A vector of carrier frequencies used by the LSP */   "PrgT_Vector *	\used_carriers_vptr;       /* NOT USED up to now */   (PrgT_Vector *	\used_rx_carrier_ids_vptr;          (/* used to distinguish the interrupts */   int			arrived_msg_type;   Boolean		msg_arrived=OPC_FALSE;   Packet *	received_pkptr;           +/* Variables passed through forced states*/   Object_list*		obj_list_ptr;   $Label_set_object*	new_label_set_ptr;   Lsp_event			lsp_status;       Boolean				backward_blocking;   Rsvp_Link*			rsvp_link;   Ici 		*rx_iciptr;   '   //#include	"MD_rsvp.h"   #include	"MD_rsvp_msg_util.h"   #include	"MD_statistics_src.h"   //#include	<oms_dist_support.h>   //#include    <ip_addr_v4.h>   //#include	"MD_ospf_src.h"   #include 	"MD_ip_src.h"   #include	"MD_ip_api.h"   #include	"MD_lmp_src.h"   #include	"MD_pcep_api.h"   %#include	"MD_rsvp_child_common_src.h"   #include	"MD_lsp.h"   #include	"MD_global_manager.h"   #include	"MD_TED.h"   #include	"MD_Transponder.h"       /* interrupst*/   n#define		SEND_PATH_TEAR_INTRPT   (op_intrpt_type()==OPC_INTRPT_SELF && op_intrpt_code()==START_PATH_TEAR_CODE)   k#define		LINK_FAILURE_INTRPT		(op_intrpt_type()==OPC_INTRPT_PROCESS && op_intrpt_code()==LINK_FAILURE_CODE)       /* dispatch conditions */   7#define		LABEL_SET_EMPTY			(new_label_set_ptr==OPC_NIL)   .#define		LABEL_SET_NOT_EMPTY		!LABEL_SET_EMPTY   Y#define		FAILURE					((NOTIFY_MSG_ARRIVED && REMOTE_LINK_FAILURE) || LINK_FAILURE_INTRPT)       /* Debug messages ON				*/   #ifndef NO_DEBUG_OUTPUT   	#define		DEBUG   	#define		RSVP_TE_DEBUG   #endif               // TO DELETEEEEEEEEEEEEEEE   -//#define trigger_ospf_te_update(_a,_b,_c,_d)   -//#define inet_address_destroy_dynamic(_addr)   !//#define prg_vector_destroy(a,b)   //#define prg_mem_free(a)   //#define prg_list_destroy(a,b)   ]       É/*______________________________________________OPERATION WITH LABEL (SET)_____________________________________________________________________________________________________________________________*/       /*----------------------------------------------CREATE LABEL SET--------------------------------------------------------------*/   tLsp_event create_label_set_object(Label_set_object** new_label_set_ptr, int out_stream_indx, int number_of_slices) {   	PrgT_Vector* 		label_vector;   	Frequency_slot*		slot;   *	int					allocated_freq_slots_vector_size;   	int					i,j;   3	int					prev_upper_freq_indx,next_lower_freq_indx;   	int					guard_band;   	int*				new_label;   	Rsvp_Link*			rsvp_link;       V	FIN (create_label_set_object (new_label_set_ptr, out_stream_indx, number_of_slices));   	   <	/* get the spectrum allocation vector for the output link*/   k	rsvp_link = (Rsvp_Link*) prg_bin_hash_table_item_get(shared_data->rsvp_links_htable_ptr,&out_stream_indx);   G	if (PRGC_NIL == rsvp_link) op_sim_end("rsvp link not found","","","");   =	//rsvp_link = shared_data->rsvp_link_array[out_stream_indx];   	   S	/* check if there is enough free spectrum that can accomodate the BW requirement*/   ?	if (rsvp_link->tot_available_freq_slices < number_of_slices) {   /		/*FORWARD BLOCKING due to LACK of BANDWIDTH*/   		*new_label_set_ptr = OPC_NIL;   !		FRET(FORWARD_BLOCKING_BW_LACK);   	}   	   		   U	/* create the vector that will contain the central frequencies of the label set 		*/   	label_vector = prg_vector_create (rsvp_link->max_frequency_indx - rsvp_link->min_frequency_indx, label_set_vector_destroy, label_set_vector_copy);   	   [	/* In this case there is no previous label set, all the available frequencies satisfing	*/   L	/* the BW requirements can be choosen and feed the label set object.					*/   		   N	guard_band = 0;//TODO depending on the requirements the guard band can change        1    /* save all the available frequency indexes*/   F    prev_upper_freq_indx = rsvp_link->min_frequency_indx - guard_band;          .	/* get the number of slot already allocated*/   U	allocated_freq_slots_vector_size = prg_vector_size(rsvp_link->allocated_freq_slots);       4	for(j=0; j<allocated_freq_slots_vector_size; j++) {        X          slot = (Frequency_slot*) prg_vector_access(rsvp_link->allocated_freq_slots,j);   
             7          next_lower_freq_indx = slot->lower_freq_indx;   
                       for(i=(prev_upper_freq_indx + number_of_slices + guard_band); i <= (next_lower_freq_indx - number_of_slices - guard_band); ++i) {   ;              new_label = (int*)prg_mem_alloc(sizeof(int));                 *new_label = i;   9              prg_vector_append (label_vector,new_label);             }   	            7          prev_upper_freq_indx = slot->upper_freq_indx;        }   	   z     /* do not forget to consider also the frequency indexes between the last allocated slot and the MAX FREQUENCY INDEX*/   @     if (prev_upper_freq_indx < rsvp_link->max_frequency_indx) {             for(i=(prev_upper_freq_indx + number_of_slices + guard_band); i <= (rsvp_link->max_frequency_indx - number_of_slices); ++i) {   ?                  new_label = (int*)prg_mem_alloc(sizeof(int));   !                  *new_label = i;   =                  prg_vector_append (label_vector,new_label);             }         }                )	if (prg_vector_size(label_vector) > 0) {   %	 	/* return the updated label set */   S	 	*new_label_set_ptr = (Label_set_object*)prg_mem_alloc(sizeof(Label_set_object));   4		(*new_label_set_ptr)->label_vector = label_vector;   
             		FRET(ESTABLISHED);   	}   	else {   ,		prg_vector_destroy(label_vector,OPC_TRUE);   "    	//prg_mem_free(label_vector);   j		/* if here means that there was enough BW but not continuous to accomodate the request (fragmentation)*/   		*new_label_set_ptr = OPC_NIL;   '		FRET(FORWARD_BLOCKING_FRAGMENTATION);   	}   		   }       static void state_none() {   	FIN(state_none());   	printf("status none\n");   	*ret_lsp_state = NONE;   	FOUT;   }              (/*release state variables of the child*/   +session_object_destroy(session_object_ptr);   5sender_tspec_object_destroy(sender_tspec_object_ptr);   #ERO_object_destroy(ero_object_ptr);   +ip_api_interface_destroy(ip_interface_ptr);   1prg_vector_destroy(used_carriers_vptr,PRGC_TRUE);   7prg_vector_destroy(used_tx_carrier_ids_vptr,PRGC_TRUE);   7prg_vector_destroy(used_rx_carrier_ids_vptr,PRGC_TRUE);                                      Z             
   init   
       
   $   ./* specify the role of the node for this LSP*/   node_role = INGRESS_NODE;       A/* access the shared data among roots and all childs processes	*/   =shared_data = (Module_Shared_Data *) op_pro_modmem_access ();       /*{// DEBUG TO DELETE   	Module_Shared_Data *asd;   6	asd = (Module_Shared_Data *) op_pro_modmem_access ();   printf("pippo1");   9ip_api_interface_copy(asd->ip_intf_ptr);printf("pippo2");   }//*/       $// save the node address as a string   :inet_address_print(node_addr_str, shared_data->node_addr);       -// copy the ip interface from the shared data   &// useful to communicate with ip layer   Cip_interface_ptr = ip_api_interface_copy(shared_data->ip_intf_ptr);       ./* access the parent to child shared memory */   C/* it is used to advertise the parent about the state of the LSP */   5ret_lsp_state = (Lsp_event *) op_pro_parmem_access();               S/* save the id of the child process (useful to identify the process in the debug)*/   &own_pro_id = op_pro_id(op_pro_self());       ,/* Register the global statistics									*/   //register_statistics();       // Var Initialization   lsp_established = OPC_FALSE;   lsp_number_of_hops = 0;   session_object_ptr = PRGC_NIL;   
       
   +   */* Variables used in the "init" state.		*/   void*	arg_mem_ptr;       Q/* Obtain the address of the argument memory block passed by calling process.  */   F/* in the first invoke the session object is passed to the child				*/   %arg_mem_ptr = op_pro_argmem_access();   if (arg_mem_ptr != OPC_NIL)	{   2	Ingress_To_Subingress	*ingress_to_subingress_ptr;   	OpT_uInt32	rx_transponder_id;   J		/* save on the state variable of the child the related session object */   B		ingress_to_subingress_ptr = (Ingress_To_Subingress*)arg_mem_ptr;   >		session_object_ptr = ingress_to_subingress_ptr->session_ptr;   		   =		tx_transponder_id = ingress_to_subingress_ptr->tx_trans_id;   =		rx_transponder_id = ingress_to_subingress_ptr->rx_trans_id;   		   @		// When PCE is used, a list of carrier identifiers is received   Q		// indicating which carrier generator module should be used in the transponder.   T		used_tx_carrier_ids_vptr = ingress_to_subingress_ptr->ingress_tx_carrier_ids_vptr;   T		used_rx_carrier_ids_vptr = ingress_to_subingress_ptr->ingress_rx_carrier_ids_vptr;   	   >		bidirectional_lsp = ingress_to_subingress_ptr->bidirect_lsp;   		   ~		tx_transponder_ptr = ted_node_transponder_by_id_get(shared_data->ospf_topology,&(shared_data->node_addr),tx_transponder_id);   	   		if (bidirectional_lsp) {   			rx_transponder_ptr = ted_node_transponder_by_id_get(shared_data->ospf_topology,&(shared_data->node_addr),rx_transponder_id);   		}   		else {   !			rx_transponder_ptr = PRGC_NIL;   		}   		   *		prg_mem_free(ingress_to_subingress_ptr);   }   else {   		printf("ERROR [node %s (sub-child %d)]: No session object is passed in the creation of the child \n",node_addr_str,own_pro_id);   }           // get the global lsp   *global_manager_ptr = global_manager_get();   Q//global_lsp_ptr = global_manager_lsp_get(global_manager_ptr,session_object_ptr);       
       
   ˙˙˙˙   
          pr_state                     
   idle   
                     
      msg_arrived = OPC_TRUE;       //* get the packet passed by the root process */   'received_pkptr = get_received_packet();       -// read the message type and the object list    ^if ( op_pk_nfd_access(received_pkptr,"Msg Type", &arrived_msg_type) == OPC_COMPCODE_FAILURE ||   [	 op_pk_nfd_access(received_pkptr,"Object List", &obj_list_ptr) == OPC_COMPCODE_FAILURE ) {   r			op_sim_end("RSVP ingress sub child process","idle state","cannot read the fields of received message",OPC_NIL);   }           #ifdef RSVP_TE_DEBUG   l	printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d sub_id:%d ): INGRESS NODE",node_addr_str,own_pro_id,   		obj_list_ptr->session->source_addr,obj_list_ptr->session->destination_addr,obj_list_ptr->session->tunnel_id,obj_list_ptr->session->sub_lsp_id);   #endif       		   4// get the ICI associated to the received packet  //   *rx_iciptr = op_pk_ici_get(received_pkptr);       if (OPC_NIL != rx_iciptr) {   0	// read the capacity of the lsp from the ici //   <	op_ici_attr_get (rx_iciptr, "Lsp Capacity", &lsp_capacity);   }       if (!PATH_MSG_ARRIVED) {   	op_pk_destroy(received_pkptr);   }   
           ˙˙˙˙             pr_state                     
   new label set   
       
   2   	// VARS//    int						number_of_slices, *app;   #ERO_IPv4_subobject*		next_node_ptr;           9// save a copy of the Sender Tspec object on the child //   Qsender_tspec_object_ptr = sender_tspec_object_copy( obj_list_ptr->sender_tspec );    // save a copy of the ERO object   4ero_object_ptr = ERO_object_copy(obj_list_ptr->ero);       ;// get the number of Spectrum slices (12.5 GHz) required //   @number_of_slices = obj_list_ptr->sender_tspec->number_of_slices;       B// in case of centralized architecture (or suggested label active)   0if ( obj_list_ptr->suggested_label != OPC_NIL) {   e	lsp_freq_slot.upper_freq_indx = obj_list_ptr->suggested_label->central_freq_indx + number_of_slices;   e	lsp_freq_slot.lower_freq_indx = obj_list_ptr->suggested_label->central_freq_indx - number_of_slices;   }       <// increase the total slices request and total lsp request//   @shared_data->tot_lsp_request = shared_data->tot_lsp_request + 1;   Ushared_data->tot_slices_request = shared_data->tot_slices_request + number_of_slices;           X// get the ERO suboject (node) at the top of the subobject list inside the ERO object //   gnext_node_ptr = (ERO_IPv4_subobject*)prg_list_access (obj_list_ptr->ero->subobjects, OPC_LISTPOS_HEAD);       // save the next node address//   =next_node_addr = inet_address_copy(next_node_ptr->ipv4_addr);   	   !// Find the output stream index//   ^app = forwarding_table_first_entry_lookup(shared_data->forwarding_table_ptr, &next_node_addr);   if (app == OPC_NIL)	{   	printf("\nRSVP ERROR node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): No output stream found while processing ERO \n",node_addr_str,own_pro_id,   		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);   }   else {   	output_interface_indx = *app;   	#ifdef RSVP_TE_DEBUG   b		printf(" ,next node is %d (output stream %d)\n",next_node_ptr->ipv4_addr,output_interface_indx);   	#endif   }           1// initialize the new label set to NULL (empty)//   new_label_set_ptr = OPC_NIL;       D// Create the New label set object for the specified output stream//   ~lsp_status = create_label_set_object(&new_label_set_ptr, output_interface_indx, obj_list_ptr->sender_tspec->number_of_slices);       
       
       
       
   ˙˙˙˙   
          pr_state                    
   reservation   
       J   L   %int			number_of_slices, num_carriers;   double		carrier_bw;       #ifdef RSVP_TE_DEBUG   w	printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): RESV MESSAGE RECEIVED\n",node_addr_str,own_pro_id,   		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);   #endif   	   8/* get the number of slices (12.5 GHz) of BW required */   @number_of_slices = obj_list_ptr->sender_tspec->number_of_slices;       4/* get the selected central frequency for the LSP */   Lcentral_frequency_indx = obj_list_ptr->generalized_label->central_freq_indx;       ,/* get the the link that will be allocated*/   prsvp_link = (Rsvp_Link*) prg_bin_hash_table_item_get(shared_data->rsvp_links_htable_ptr,&output_interface_indx);   B//rsvp_link = shared_data->rsvp_link_array[output_interface_indx];       sif (central_frequency_indx<rsvp_link->min_frequency_indx || central_frequency_indx>rsvp_link->max_frequency_indx) {   >	printf("frequency out of range %d\n",central_frequency_indx);   	op_sim_end("","","","");   }           Bused_carriers_vptr = prg_vector_copy(obj_list_ptr->carriers_vptr);       3num_carriers = prg_vector_size(used_carriers_vptr);       J// assume the carrier BW as the total BW divided by the number of carriers   Icarrier_bw = (number_of_slices*FREQUENCY_INDEXC_SLICE_SIZE)/num_carriers;       1// try to reserve the carriers on the transponder   Gif ( !shared_data->distributed_architecture && shared_data->iam_pce ) {   O	// IN case we are in a centralized PCE architecture, and this node is the PCE,   e	// the transponder carriers should not be reserved, because already reserved after path computation.          C	// try to reserve the frequency slot (backward blocking can occur)   o	backward_blocking = reserve_frequency_slot(rsvp_link,central_frequency_indx,number_of_slices, &lsp_freq_slot);   	if (backward_blocking) {   I		// if the slot cannot be reserved, release also the transponder carrier   O		//transponder_carriers_release(tx_transponder_ptr, used_tx_carrier_ids_vptr);   5		// set the state to be returned to the parent child   %		*ret_lsp_state = BACKWARD_BLOCKING;   	}   }   else {      MD_Compcode comp_code;   	   B	// In case a list of carriers IDs is given, allocate the carriers   K	// to the corresponding carrier generator module (identified with the ID).   U	// Otherwise allocate the carriers in the first available carrier generator modules    )	// and save the IDs of the used modules.   ,	if (used_tx_carrier_ids_vptr != PRGC_NIL) {   ~		comp_code = transponder_tx_carriers_use_by_id(tx_transponder_ptr, used_carriers_vptr, carrier_bw, used_tx_carrier_ids_vptr);   		} else {   E		used_tx_carrier_ids_vptr = carrier_ids_vector_create(num_carriers);   		comp_code = transponder_tx_carriers_use(tx_transponder_ptr, used_carriers_vptr, carrier_bw, PRGC_NIL, used_tx_carrier_ids_vptr);   	}   	   )	if (MD_Compcode_Failure == comp_code ) {   		backward_blocking = OPC_TRUE;   5		// set the state to be returned to the parent child   A		*ret_lsp_state = INGRESS_NODE_TX_TRANSPONDER_BACKWARD_BLOCKING;   		} else {   2	  // if the transponder carrier is reserved, then   E	  // try to reserve the frequency slot (backward blocking can occur)   q	  backward_blocking = reserve_frequency_slot(rsvp_link,central_frequency_indx,number_of_slices, &lsp_freq_slot);   	  if (backward_blocking) {   I		// if the slot cannot be reserved, release also the transponder carrier   P		transponder_tx_carriers_release(tx_transponder_ptr, used_tx_carrier_ids_vptr);   5		// set the state to be returned to the parent child   %		*ret_lsp_state = BACKWARD_BLOCKING;   	  }   	}   	   }   J                     
   ˙˙˙˙   
          pr_state                     
   remote blocking   
       
      //* forward blocking or link failure occurred */   #ifdef RSVP_TE_DEBUG   	printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): PATH ERROR MESSAGE received during provisioning\n",node_addr_str,own_pro_id,   		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);   #endif       ,/* destroy the path error message received*/   op_pk_destroy(received_pkptr);       /*   ,if (shared_data->stateful_pce == OPC_TRUE) {   <	// a stateful pce is present, update of link information //   *	// is made by ingress node through PCP //   <	pce_lsp_update_status(session_object_ptr, REMOTE_BLOCKING);   }   */   
       
      5/* set the state to be returned to the parent child*/   !*ret_lsp_state = REMOTE_BLOCKING;       8// remove the lsp from the global manager and destroy it   M/*global_manager_lsp_remove(global_manager_ptr, global_lsp_ptr->session_ptr);   &lsp_destroy(global_lsp_ptr, OPC_TRUE);   global_lsp_ptr = PRGC_NIL;   */   Dglobal_manager_lsp_life_end(global_manager_ptr, session_object_ptr);           9if (shared_data->distributed_architecture == OPC_FALSE) {   A// in this case we have the PCE, therefore inform the PCEP module   6// that the LSP is down (It will send a PCRep message)   .	// use the pcep api to inform the PCEP module   1	pcep_lsp_down_notify(&shared_data->pcep_handler,   /						 session_object_copy(session_object_ptr),   '						 ERO_object_copy(ero_object_ptr),   9						 sender_tspec_object_copy(sender_tspec_object_ptr),   %						 lsp_freq_slot.upper_freq_indx,   %						 lsp_freq_slot.lower_freq_indx,   						 lsp_capacity);   }       
       
   ˙˙˙˙   
          pr_state      
              
   wait provisioning   
       
       
       
      if (!LINK_FAILURE_INTRPT) {   	msg_arrived = OPC_TRUE;       /	/* get the packet passed by the root process*/   (	received_pkptr = get_received_packet();       `	/*read the message type and put in arrived_msg_type that will determine the next forced state*/   A	op_pk_nfd_access(received_pkptr,"Msg Type", &arrived_msg_type);	       $	/* access to the list of objects */   ?	op_pk_nfd_access(received_pkptr,"Object List", &obj_list_ptr);   	   }   else {   	msg_arrived = OPC_FALSE;   }   
           ˙˙˙˙             pr_state        ţ  ţ          
   established   
       
      lsp_established = OPC_TRUE;   
       
      3if (SEND_PATH_TEAR_INTRPT || LINK_FAILURE_INTRPT) {   	msg_arrived = OPC_FALSE;   }    else {   	msg_arrived = OPC_TRUE;   	   /	/* get the packet passed by the root process*/   (	received_pkptr = get_received_packet();       `	/*read the message type and put in arrived_msg_type that will determine the next forced state*/   A	op_pk_nfd_access(received_pkptr,"Msg Type", &arrived_msg_type);	       $	/* access to the list of objects */   ?	op_pk_nfd_access(received_pkptr,"Object List", &obj_list_ptr);   }   
           ˙˙˙˙             pr_state        J  *          
   	terminate   
       
      #ifdef RSVP_DEBUG   S	printf("node %s [sub-child %d]: SUB-CHILD TERMINATED\n",node_addr_str,own_pro_id);   #endif       /* this child destroy itself */    op_pro_destroy (op_pro_self ());   
                         ˙˙˙˙             pr_state        v  v          
   failure   
       
      if (NOTIFY_MSG_ARRIVED) {   :	#if defined(RSVP_TE_DEBUG) || defined(LINK_FAILURE_DEBUG)   		printf("Ingress Node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): Received a message notification for a LINK FAILURE\n",node_addr_str,own_pro_id,   			session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);   	#endif   	/* destroy the packet*/   	op_pk_destroy(received_pkptr);   }           9#if defined(RSVP_TE_DEBUG) || defined(LINK_FAILURE_DEBUG)   if (LINK_FAILURE_INTRPT) {   		printf("Ingress Node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): Involved in a Local LINK FAILURE\n",node_addr_str,own_pro_id,   			session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);   }   #endif       
       
   	       5/* set the state to be returned to the parent child*/   if (LINK_FAILURE_INTRPT) {    	*ret_lsp_state = LOCAL_FAILURE;   }   else {   !	*ret_lsp_state = REMOTE_FAILURE;   }       
       
   ˙˙˙˙   
          pr_state                      
   forward blocking   
       
   -   /* FORWARD BLOCKING!! */        /* Free Label Set Memory */   ,label_set_object_destroy(new_label_set_ptr);       =/* update statistics based on the type of forward blocking */   switch (lsp_status) {   "    case FORWARD_BLOCKING_BW_LACK:   ,                        #ifdef RSVP_TE_DEBUG                             printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): FORWARD BLOCKING due to BW LACK!!!!\n",node_addr_str,own_pro_id,                                session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);                           #endif   O                        //update_blocking_statistics(FORWARD_BLOCKING_BW_LACK);                           break;   (    case FORWARD_BLOCKING_FRAGMENTATION:   ,                        #ifdef RSVP_TE_DEBUG   Ľ                          printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): FORWARD BLOCKING due to FRAGMENTATION!!!!\n",node_addr_str,own_pro_id,                                session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);                           #endif   U                        //update_blocking_statistics(FORWARD_BLOCKING_FRAGMENTATION);                           break;   %    case FORWARD_BLOCKING_CONTINUITY:   ,                        #ifdef RSVP_TE_DEBUG   ­                          printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): FORWARD BLOCKING due to CONTINUITY CONSTRAINT!!!!\n",node_addr_str,own_pro_id,                                session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);                           #endif   R                        //update_blocking_statistics(FORWARD_BLOCKING_CONTINUITY);                           break;   B	default: op_sim_end("RSVP ingress subchild","ciooooooooo","","");   }       Y/* destroy the packet. Since this is the ingress node we DON'T need to send PATH ERROR */   op_pk_destroy(received_pkptr);       // destroy the ici //   op_ici_destroy(rx_iciptr);           /*   ,if (shared_data->stateful_pce == OPC_TRUE) {   <	// a stateful pce is present, update of link information //   *	// is made by ingress node through PCP //   7	pce_lsp_update_status(session_object_ptr, lsp_status);   }   */   
       
      5/* set the state to be returned to the parent child*/   *ret_lsp_state = lsp_status;           8// remove the lsp from the global manager and destroy it   M/*global_manager_lsp_remove(global_manager_ptr, global_lsp_ptr->session_ptr);   &lsp_destroy(global_lsp_ptr, OPC_TRUE);   global_lsp_ptr = PRGC_NIL;   */       9if (shared_data->distributed_architecture == OPC_FALSE) {   A// in this case we have the PCE, therefore inform the PCEP module   6// that the LSP is down (It will send a PCRpt message)   .	// use the pcep api to inform the PCEP module   1	pcep_lsp_down_notify(&shared_data->pcep_handler,   /						 session_object_copy(session_object_ptr),   '						 ERO_object_copy(ero_object_ptr),   9						 sender_tspec_object_copy(sender_tspec_object_ptr),   %						 lsp_freq_slot.upper_freq_indx,   %						 lsp_freq_slot.lower_freq_indx,   						 lsp_capacity);   }       
       
   ˙˙˙˙   
          pr_state                     
   backward blocking   
       
      Packet*			resv_err_pkptr;       #ifdef RSVP_TE_DEBUG   	printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d. sub_id:%d): BACKWARD BLOCKING!!!! Send RESV ERROR\n",node_addr_str,own_pro_id,   		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);   #endif   		   4/* INGRESS node has to send only the RESV_ERR msg */   Lresv_err_pkptr = create_resv_error(session_object_copy(session_object_ptr));       ,// send the RESV error towards the egress //   Bip_api_pk_send(ip_interface_ptr, resv_err_pkptr, &next_node_addr);       /* update statistics */   0//update_blocking_statistics(BACKWARD_BLOCKING);       #// destroy the received RSVP packet   op_pk_destroy(received_pkptr);       /*   ,if (shared_data->stateful_pce == OPC_TRUE) {   <	// a stateful pce is present, update of link information //   *	// is made by ingress node through PCP //   >	pce_lsp_update_status(session_object_ptr, BACKWARD_BLOCKING);   }   */   
       
          8// remove the lsp from the global manager and destroy it   M/*global_manager_lsp_remove(global_manager_ptr, global_lsp_ptr->session_ptr);   &lsp_destroy(global_lsp_ptr, OPC_TRUE);   global_lsp_ptr = PRGC_NIL;   */           9if (shared_data->distributed_architecture == OPC_FALSE) {   A// in this case we have the PCE, therefore inform the PCEP module   6// that the LSP is down (It will send a PCRep message)   .	// use the pcep api to inform the PCEP module   1	pcep_lsp_down_notify(&shared_data->pcep_handler,   /						 session_object_copy(session_object_ptr),   '						 ERO_object_copy(ero_object_ptr),   9						 sender_tspec_object_copy(sender_tspec_object_ptr),   %						 lsp_freq_slot.upper_freq_indx,   %						 lsp_freq_slot.lower_freq_indx,   						 lsp_capacity);   }       
       
   ˙˙˙˙   
          pr_state                     
   forward   
       
   	       Y// save the number of hops in a state variable, useful for statistics of BW utilization//   Blsp_number_of_hops = prg_list_size(obj_list_ptr->ero->subobjects);          !// insert the Label Set Object //   ,obj_list_ptr->label_set = new_label_set_ptr;       &// forward the packet to the next node   Bip_api_pk_send(ip_interface_ptr, received_pkptr, &next_node_addr);   
       
      5/* set the state to be returned to the parent child*/   *ret_lsp_state = PROVISIONING;       +start_provisioning_instant = op_sim_time();       Nglobal_manager_lsp_provisioning_start(global_manager_ptr, session_object_ptr);       /*   9if (shared_data->distributed_architecture == OPC_FALSE) {   A// in this case we have the PCE, therefore inform the PCEP module   @// about the LSP under signalling (It will send a PCRep message)   .	// use the pcep api to inform the PCEP module   5	pcep_lsp_going_up_notify(&shared_data->pcep_handler,   0							 session_object_copy(session_object_ptr),   (							 ERO_object_copy(ero_object_ptr),   :							 sender_tspec_object_copy(sender_tspec_object_ptr),   &							 lsp_freq_slot.upper_freq_indx,   &							 lsp_freq_slot.lower_freq_indx,   							 lsp_capacity);   }   */   
       
   ˙˙˙˙   
          pr_state        ţ            
   succeed   
       
   "   int					number_of_slices;       #//----RESERVATION SUCCEED--------//   #ifdef RSVP_TE_DEBUG   s	printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): Reserved frequency slot [%d,%d] in link %d\n",   		node_addr_str,own_pro_id,   		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id,   U		lsp_freq_slot.lower_freq_indx,lsp_freq_slot.upper_freq_indx,output_interface_indx);   #endif       K// update the statistics related to all slot occupied along the lightpath//   Vnumber_of_slices = (lsp_freq_slot.upper_freq_indx - lsp_freq_slot.lower_freq_indx )/2;   Iupdate_links_statistics(ESTABLISHED,number_of_slices*lsp_number_of_hops);   								   // update statistics //   *//update_blocking_statistics(ESTABLISHED);           //destroy the resv message//   op_pk_destroy(received_pkptr);               /*   -if (shared_data->stateful_pce == OPC_FALSE) {   B	// schedule the ospf opaque lsa by invoking the ospf-te module //   n	trigger_ospf_te_update(shared_data->ospf_te_objid,shared_data->rsvp_links_htable_ptr, output_interface_indx);   }   else {   ;// a stateful pce is present, update of link information //   )// is made by ingress node through PCP //   8	pce_lsp_update_status(session_object_ptr, ESTABLISHED);   }   */   
       
       5/* set the state to be returned to the parent child*/   *ret_lsp_state = ESTABLISHED;       )end_provisioning_instant = op_sim_time();       Lglobal_manager_lsp_provisioning_end(global_manager_ptr, session_object_ptr);       8if (shared_data->distributed_architecture == OPC_TRUE) {   9		// trigger the OSPF module to send opaque and node LSAs   		trigger_ospf_te_update(shared_data->ospf_te_objid, shared_data->rsvp_links_htable_ptr, output_interface_indx,tx_transponder_id);   }       /*   9if (shared_data->distributed_architecture == OPC_FALSE) {   A// in this case we have the PCE, therefore inform the PCEP module   6// about the active LSP (It will send a PCRep message)   .	// use the pcep api to inform the PCEP module   4	pcep_lsp_active_notify(	&shared_data->pcep_handler,   /							session_object_copy(session_object_ptr),   '							ERO_object_copy(ero_object_ptr),   9							sender_tspec_object_copy(sender_tspec_object_ptr),   %							lsp_freq_slot.upper_freq_indx,   %							lsp_freq_slot.lower_freq_indx,   							lsp_capacity );   }*/           .//update statistics of transponder utilization   >node_transponders_statistic_update(shared_data->ospf_topology,   =                                   &(shared_data->node_addr),   I                                   &(shared_data->transponders_inuse_sh),   N                                   &(shared_data->transponders_avg_usage_sh));   
       
   ˙˙˙˙   
          pr_state        ţ  v          
   release   
       J   0   Rsvp_Link* 		rsvp_link;       // get the allocated link //   B//rsvp_link = shared_data->rsvp_link_array[output_interface_indx];   prsvp_link = (Rsvp_Link*) prg_bin_hash_table_item_get(shared_data->rsvp_links_htable_ptr,&output_interface_indx);       2// release the reserved freq slot from the link //   ?if (release_resources(rsvp_link, &lsp_freq_slot) == OPC_TRUE) {   	#ifdef RSVP_TE_DEBUG   z		printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): Frequency slot released\n",node_addr_str,own_pro_id,   			session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);   	#endif   (	//lsp_freq_slot = OPC_NIL; //!important   }   else {   	printf("ERROR node %s [sub-child %d]: Fail on removing a frequency slot on link %d\n",node_addr_str,own_pro_id,output_interface_indx);   	op_sim_end("","","","");   }       !//release the transponder carrier   -if (shared_data->distributed_architecture ||    D(!shared_data->distributed_architecture && !shared_data->iam_pce)) {   m	if ( transponder_tx_carriers_release(tx_transponder_ptr, used_tx_carrier_ids_vptr) == MD_Compcode_Failure) {   P		op_sim_error(OPC_SIM_ERROR_ABORT,"cannot release the transponder carrier","");   	}   }   	   S//update the statistics related to the capacity utilization of the entire network//   bupdate_links_statistics(RELEASED, sender_tspec_object_ptr->number_of_slices * lsp_number_of_hops);       /*   8if (shared_data->stateful_pce == OPC_TRUE && !FAILURE) {   <	// a stateful pce is present, update of link information //   *	// is made by ingress node through PCP //   5	pce_lsp_update_status(session_object_ptr, RELEASED);   }   else {   B	// schedule the ospf opaque lsa by invoking the ospf-te module //   n	trigger_ospf_te_update(shared_data->ospf_te_objid,shared_data->rsvp_links_htable_ptr, output_interface_indx);   }*/               .//update statistics of transponder utilization   >node_transponders_statistic_update(shared_data->ospf_topology,   =                                   &(shared_data->node_addr),   I                                   &(shared_data->transponders_inuse_sh),   N                                   &(shared_data->transponders_avg_usage_sh));   J       
      5/* set the state to be returned to the parent child*/   *ret_lsp_state = RELEASED;       8if (shared_data->distributed_architecture == OPC_TRUE) {   B	// schedule the ospf opaque lsa by invoking the ospf-te module //   	trigger_ospf_te_update(shared_data->ospf_te_objid,shared_data->rsvp_links_htable_ptr, output_interface_indx,tx_transponder_id);   }   
       
   ˙˙˙˙   
          pr_state        ţ  î          
   tear   
       
      Packet*			path_tear_pkptr;       Npath_tear_pkptr = create_path_tear( session_object_copy(session_object_ptr) );       0// send the PATH TEAR towards the egress node //   Cip_api_pk_send(ip_interface_ptr, path_tear_pkptr, &next_node_addr);       #ifdef RSVP_TE_DEBUG   t	printf("node %s [sub-child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): Send PATH TEAR MSG\n",node_addr_str,own_pro_id,   			session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id);   #endif       
       
              9if (shared_data->distributed_architecture == OPC_FALSE) {   A// in this case we have the PCE, therefore inform the PCEP module   ?// that the LSP is under release (It will send a PCRep message)   .	// use the pcep api to inform the PCEP module   	if (lsp_established) {   ;		/*pcep_lsp_going_down_notify(	&shared_data->pcep_handler,   1									session_object_copy(session_object_ptr),   )									ERO_object_copy(ero_object_ptr),   ;									sender_tspec_object_copy(sender_tspec_object_ptr),   '									lsp_freq_slot.upper_freq_indx,   '									lsp_freq_slot.lower_freq_indx,   									lsp_capacity );*/   	}   	else {   3		pcep_lsp_down_notify(	&shared_data->pcep_handler,   0								session_object_copy(session_object_ptr),   (								ERO_object_copy(ero_object_ptr),   :								sender_tspec_object_copy(sender_tspec_object_ptr),   &								lsp_freq_slot.upper_freq_indx,   &								lsp_freq_slot.lower_freq_indx,   								lsp_capacity);   	}   =	//pce_lsp_update_status(session_object_ptr, *ret_lsp_state);   }       
       
   ˙˙˙˙   
          pr_state                        ľ˙˙˙c      e      ű             
   tr_0   
       ˙˙˙˙          ˙˙˙˙          
    ˙˙˙˙   
          ˙˙˙˙                       pr_transition         
     E  H                     
   tr_21   
       
   RESV_MSG_ARRIVED   
       ˙˙˙˙          
    ˙˙˙˙   
          ˙˙˙˙                       pr_transition         
        ˙                      
   tr_23   
       
   PATH_ERR_MSG_ARRIVED   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition               -             *     0    L            
   tr_24   
       ˙˙˙˙          ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition                 X        /               
   tr_26   
       
   PATH_MSG_ARRIVED   
       ˙˙˙˙          
    ˙˙˙˙   
          ˙˙˙˙                       pr_transition               ž   q                        
   tr_31   
       
   LABEL_SET_EMPTY   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition                -  Ú            )      0     O  !          
   tr_32   
       ˙˙˙˙          ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      #         @   a           )      0  "  L  !          
   tr_35   
       
   default   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      $          }                      
   tr_36   
       
   backward_blocking   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      &        n   p                       
   tr_38   
       
   LABEL_SET_NOT_EMPTY   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      '      
                           
   tr_39   
       ˙˙˙˙          ˙˙˙˙          
    ˙˙˙˙   
          ˙˙˙˙                       pr_transition      *          J     ý    ţ  ˙          
   tr_42   
       ˙˙˙˙          ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      -        Ă  }         ę            
   tr_45   
       
   default   
       ˙˙˙˙          
    ˙˙˙˙   
          ˙˙˙˙                       pr_transition      0          >     ţ     ü  v          
   tr_48   
       
    SEND_PATH_TEAR_INTRPT || FAILURE   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      2        Ą  š     ţ  x  ú  í          
   tr_50   
       
   SEND_PATH_TEAR_INTRPT   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      4        ą        ű  í  ü    Q             
   tr_52   
       
˙˙˙˙   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      6   
     4           o    p  s          
   tr_54   
       
   FAILURE   
       ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition      7        ;  i     ţ  s  r  v          
   tr_55   
       
   FAILURE   
       ˙˙˙˙          
    ˙˙˙˙   
          ˙˙˙˙                       pr_transition      8        ý  :     t  v  ý  í          
   tr_56   
       ˙˙˙˙          ˙˙˙˙          
    ˙˙˙˙   
          ˙˙˙˙                       pr_transition      %         .  P           +     1    K  !          
   tr_37   
       ˙˙˙˙          ˙˙˙˙          
    ˙˙˙˙   
       
    ˙˙˙˙   
                    pr_transition         9               
   MD_global_manager   	MD_ip_api   	MD_ip_src   MD_lsp   MD_pcep_api   MD_rsvp_child_common_src   MD_rsvp_msg_util   MD_statistics_src   MD_TED   MD_transponder                    