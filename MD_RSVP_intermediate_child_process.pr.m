MIL_3_Tfile_Hdr_ 171A 171A modeler 9 51F64E95 55003F94 113 labeffa matteo 0 0 none none 0 0 none 38D02E4 A776 0 0 0 0 0 0 2b1a 1                                                                                                                                                                                                                                                                                                                                                                                                «Ķg      D   H   L      0*  ¤Ó  ¤×  ¤Ū  ¤ß  „j  „n  „r  0  0&           	   begsim intrpt             ’’’’      doc file            	nd_module      endsim intrpt             ’’’’      failure intrpts            disabled      intrpt interval         Ō²I­%Ć}’’’’      priority              ’’’’      recovery intrpts            disabled      subqueue                     count    ’’’   
   ’’’’   
      list   	’’’   
          
      super priority             ’’’’          =   9/* Shared information that all children need to access */   "Module_Shared_Data *	\shared_data;       4/* specify the role of node for this specific LSP */   Node_role	\node_role;       /* id of the child process */   int	\own_pro_id;       ,/* Save the Session related to this child */   $Session_object*	\session_object_ptr;       F/* the frequency slot reserved for the lsp associated to this child */   Frequency_slot	\lsp_freq_slot;       U/* useful for statistics purposes, to keep track of the number of slices requested */   0Sender_tspec_object*	\child_sender_tspec_object;       >/* next node ip address, useful when create the opaque_link */   InetT_Address	\next_node_addr;       InetT_Address	\prev_node_addr;       2/* the interface from which the data comes from */   !OpT_uInt32	\input_interface_indx;       -/* the output interface index for the data */   "OpT_uInt32	\output_interface_indx;       &/* the capacity carried by this lsp */   unsigned int	\lsp_capacity;       */* TRUE if the LSP has been established */   Boolean	\lsp_established;       Packet *	\resv_pkptr;       ;/* true if the oxc cross connect is configured and ready */   Boolean	\oxc_ready;       H/* true if the process is waiting the oxc to set the cross connection */   Boolean	\waiting_oxc;       #/* a copy of the suggested label */   4Suggested_label_object*	\suggested_label_object_ptr;       6/* the request id of the last configuration request */   6/* issued by this process                           */   OpT_Int32	\conf_oxc_request_id;       (char	\node_addr_str[INETC_ADDR_STR_LEN];       $Ip_Api_Interface*	\ip_interface_ptr;       $Global_Manager*	\global_manager_ptr;       LSP*	\global_lsp_ptr;       K/* TRUE when the resources are reserved (resv message not yet forwarded) */   Boolean	\lsp_reserved;          (/* used to distinguish the interrupts */   int			arrived_msg_type;   Boolean		msg_arrived=OPC_FALSE;   Packet *	received_pkptr;       +/* Variables passed through forced states*/   Object_list*		obj_list_ptr;   Lsp_event			lsp_status;       Boolean				backward_blocking;       Rsvp_Link*			rsvp_link;   		   Ici					*rx_iciptr;       #include	"MD_rsvp.h"   #include	"MD_rsvp_msg_util.h"   #include	"MD_statistics_src.h"   #include	"MD_ospf_src.h"   #include 	"MD_ip_src.h"   #include	"MD_ip_api.h"   #include	"MD_lmp_src.h"   %#include	"MD_rsvp_child_common_src.h"   #include	"MD_lsp.h"   #include	"MD_global_manager.h"       /* dispatch conditions */   =#define		LABEL_SET_EMPTY			(obj_list_ptr->label_set==OPC_NIL)   .#define		LABEL_SET_NOT_EMPTY		!LABEL_SET_EMPTY   k#define		LINK_FAILURE_INTRPT		(op_intrpt_type()==OPC_INTRPT_PROCESS && op_intrpt_code()==LINK_FAILURE_CODE)   Y#define		FAILURE					((NOTIFY_MSG_ARRIVED && REMOTE_LINK_FAILURE) || LINK_FAILURE_INTRPT)   #define		OXC_CONFIGURED			(op_intrpt_type()==OPC_INTRPT_REMOTE && op_intrpt_code()==OXC_CONFIGURED_CODE && op_intrpt_source()==shared_data->oxc_objid)           ;#define		OXC_PRECONF				(shared_data->oxc_preconfiguration)       /* Debug messages ON				*/   #ifndef NO_DEBUG_OUTPUT   	#define		DEBUG   	#define		RSVP_TE_DEBUG   #endif               // TO DELETEEEEEEEEEEEEEEE   -//#define trigger_ospf_te_update(_a,_b,_c,_d)   -//#define inet_address_destroy_dynamic(_addr)   Ą       É/*______________________________________________OPERATION WITH LABEL (SET)_____________________________________________________________________________________________________________________________*/       /*----------------------------------------------UPDATE THE LABEL SET--------------------------------------------------------------*/   tLsp_event update_label_set_object(Label_set_object** rx_label_set_pptr, int out_stream_indx, int number_of_slices) {   	Frequency_slot*		slot;   *	int					allocated_freq_slots_vector_size;   	int					rx_label_vector_size;   	int					i,j;   	int*				freq_indx;   	int					prev_upper_freq_indx;   	int					guard_band;   	Rsvp_Link*			rsvp_link;       	   V	FIN (update_label_set_object (rx_label_set_pptr, out_stream_indx, number_of_slices));   	   <	/* get the spectrum allocation vector for the output link*/   =	//rsvp_link = shared_data->rsvp_link_array[out_stream_indx];   k	rsvp_link = (Rsvp_Link*) prg_bin_hash_table_item_get(shared_data->rsvp_links_htable_ptr,&out_stream_indx);   L	if ( PRGC_NIL == rsvp_link || OPC_NIL == rsvp_link->allocated_freq_slots) {   		printf("ERROR RSVP Node %s [child %d](LSP S:%d, D:%d, T_id:%d): Cannot find the rsvp link or frequency vector is not allocated\n",node_addr_str,own_pro_id,                               session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   :		op_sim_end("function update_label_set_object","","","");   	}   	   S	/* check if there is enough free spectrum that can accomodate the BW requirement*/   ?	if (rsvp_link->tot_available_freq_slices < number_of_slices) {   /		/*FORWARD BLOCKING due to LACK of BANDWIDTH*/   +			/* release the memory of the label set*/   0			label_set_object_destroy(*rx_label_set_pptr);   			   U			/* set the label set has empty by setting the pointer of the object list to NULL*/    			*rx_label_set_pptr = OPC_NIL;   			   )			/* return the forward blocking state*/   "			FRET(FORWARD_BLOCKING_BW_LACK);   	}   	   H	/* From the received label set we dicard the unavailable frequencies */       .	/* get the number of slot already allocated*/   U	allocated_freq_slots_vector_size = prg_vector_size(rsvp_link->allocated_freq_slots);   	   	   V	/* first of all check if exists enough continuos free slices to satisfy the demand */   ,	if (allocated_freq_slots_vector_size > 0) {   8			prev_upper_freq_indx = rsvp_link->min_frequency_indx;   7			for (j=0; j<allocated_freq_slots_vector_size; j++) {   R				slot = (Frequency_slot*) prg_vector_access(rsvp_link->allocated_freq_slots,j);   U				if ((slot->lower_freq_indx - prev_upper_freq_indx) >= ((number_of_slices) * 2)) {   V					/* we found at least some free continuous slices that can accomodate the demand*/   					break;   				}   /				prev_upper_freq_indx=slot->upper_freq_indx;   			}   			   /			if (j == allocated_freq_slots_vector_size) {   ?				/* in the case we have not yet found enough continuous BW*/   ^				/* check the space between the last allocated slot and the end of the allocable spectrum*/   ]				if ((rsvp_link->max_frequency_indx - slot->upper_freq_indx) < ((number_of_slices) * 2)) {   M					/* in this case we have FORWARD BLOCKING due to SPECTRUM FRAGMENTATION*/   .						/* release the memory of the label set*/   3						label_set_object_destroy(*rx_label_set_pptr);   			   '						/* set the label set has empty */   #						*rx_label_set_pptr = OPC_NIL;   						   ,						/* return the forward blocking state*/   +						FRET(FORWARD_BLOCKING_FRAGMENTATION);   				}   			}   	}    		   L	rx_label_vector_size = prg_vector_size((*rx_label_set_pptr)->label_vector);   		   X	/* Check if the frequencies in the received label set are in the range of feasible			*/   >	/* frequencies for the transponder of the node	 											*/   "	while (rx_label_vector_size!=0) {   K			freq_indx=(int*)prg_vector_access((*rx_label_set_pptr)->label_vector,0);   K			if ( *freq_indx < (rsvp_link->min_frequency_indx + number_of_slices) ) {   V				/*remove the specific frequency from the set because it is out of possible range*/   >				prg_vector_remove ((*rx_label_set_pptr)->label_vector, 0);   %				prg_mem_free(freq_indx); //!!!!!    				--rx_label_vector_size;   			}   				else {   				break; //break while loop   			}   	}   		   	i=rx_label_vector_size-1;   	while (i>=0) {   K			freq_indx=(int*)prg_vector_access((*rx_label_set_pptr)->label_vector,i);   I			if ( (*freq_indx+number_of_slices) > rsvp_link->max_frequency_indx ) {   V				/*remove the specific frequency from the set because it is out of possible range*/   >				prg_vector_remove ((*rx_label_set_pptr)->label_vector, i);   %				prg_mem_free(freq_indx); //!!!!!    				--rx_label_vector_size;   				--i;   			}   				else {   				break; //break while loop   			}   	}   		   H	if (allocated_freq_slots_vector_size > 0 && rx_label_vector_size > 0) {   \	/* Not all frequencies are available on the output link, we need to update the label set */   				   P				guard_band=0; //TODO depending on the requirements the guard band can change   				i=0; j=0;   N				freq_indx = (int*)prg_vector_access((*rx_label_set_pptr)->label_vector,i);   R				slot = (Frequency_slot*) prg_vector_access(rsvp_link->allocated_freq_slots,j);       S				while ((j<allocated_freq_slots_vector_size) && (i<rx_label_vector_size)) {					   O					if (*freq_indx >= slot->upper_freq_indx + number_of_slices + guard_band) {   S						/* the considered frequency slot does not affect the considered frequency. */   4						/* So let's move to the next slot											*/   
						j++;   5						if (j==allocated_freq_slots_vector_size) break;   T						slot = (Frequency_slot*) prg_vector_access(rsvp_link->allocated_freq_slots,j);   					}   					else {   O						if (*freq_indx > slot->lower_freq_indx - number_of_slices - guard_band) {   O							/* this frequency cannot be used anymore because the BW required will */   8							/* overlap with the considered slot. 								  */   ?							/* So let's remove the frequency from the set 						  */   A							prg_vector_remove ((*rx_label_set_pptr)->label_vector, i);   '							prg_mem_free(freq_indx); //!!!!!   							rx_label_vector_size--;   O							/* don't need to increase the counter because we have lost an element */   						}   						else {   7							/* increment the counter to read the next freq*/   							i++;   						}   						   )						if (i==rx_label_vector_size) break;   6						/* read the next frequency from the label set */   N						freq_indx=(int*)prg_vector_access((*rx_label_set_pptr)->label_vector,i);   					}   				}   				   	}   		   	if (rx_label_vector_size>0) {   4	/* the label set still contains some freq. index */   			   !			/* return the succeed state */   			FRET(ESTABLISHED);   	}   	else {   /	/* no frequencies survived in the label set */   b	/* in this case we have FORWARD BLOCKING due to CONTINUITY CONSTRAINT not respected among nodes*/   			   +			/* release the memory of the label set*/   2			label_set_object_destroy((*rx_label_set_pptr));   			   $			/* set the label set has empty */    			*rx_label_set_pptr = OPC_NIL;   						   )			/* return the forward blocking state*/   %			FRET(FORWARD_BLOCKING_CONTINUITY);   	}   		   }           static void set_oxc_ready() {   	FIN(set_oxc_ready());   "	// set the cross connect as ready   	oxc_ready = OPC_TRUE;   .	// set waiting for oxc configuration to false   	waiting_oxc = OPC_FALSE;   	FOUT;   }       static void cciao(){   FIN(cciao());   ^	printf("cciaocciao interrupt type %d interrupt code %d\n",op_intrpt_type(),op_intrpt_code());   ¦	printf("OPC_INTRPT_PROCESS %d, OPC_INTRPT_REMOTE %d, OPC_INTRPT_SELF %d, OPC_INTRPT_STRM %d\n",OPC_INTRPT_PROCESS,OPC_INTRPT_REMOTE,OPC_INTRPT_SELF,OPC_INTRPT_STRM);   )	if (op_intrpt_type()==OPC_INTRPT_STRM) {   		Packet* pkptr;   		int		msg_type;    		pkptr = get_received_packet();   1		op_pk_nfd_access(pkptr,"Msg Type", &msg_type);	   B		printf("message received in CIAO CIAO, msg type=%d\n",msg_type);   	}   	   	FOUT;   }   @//printf("node %d:fin qui ci siamo 2\n",shared_data->node_addr);          (/*release state variables of the child*/   +session_object_destroy(session_object_ptr);   7sender_tspec_object_destroy(child_sender_tspec_object);   +ip_api_interface_destroy(ip_interface_ptr);   ;suggested_label_object_destroy(suggested_label_object_ptr);                                      Z             
   init   
       
      ./* specify the role of the node for this LSP*/   node_role = INTERMEDIATE_NODE;       >/* access the shared data between root and childs processes	*/   =shared_data = (Module_Shared_Data *) op_pro_modmem_access ();       -// copy the ip interface from the shared data   &// useful to communicate with ip layer   Cip_interface_ptr = ip_api_interface_copy(shared_data->ip_intf_ptr);       $// save the node address as a string   :inet_address_print(node_addr_str, shared_data->node_addr);       S/* save the id of the child process (useful to identify the process in the debug)*/   &own_pro_id = op_pro_id(op_pro_self());       ,/* Register the global statistics									*/   //register_statistics();       // LSP not yet reserved   lsp_reserved = OPC_FALSE;   // LSP not yet established   lsp_established = OPC_FALSE;       
       
      */* Variables used in the "init" state.		*/   void*	arg_mem_ptr;       Q/* Obtain the address of the argument memory block passed by calling process.  */   F/* in the first invoke the session object is passed to the child				*/   &arg_mem_ptr = op_pro_argmem_access ();   if (arg_mem_ptr != OPC_NIL)	{   J		/* save on the state variable of the child the related session object */   4		session_object_ptr = (Session_object*)arg_mem_ptr;   }   else {   		printf("ERROR [node %d (child %d)]: No session object is passed in the creation of the child \n",shared_data->node_addr,own_pro_id);   }       	   // get the global lsp   *global_manager_ptr = global_manager_get();   Q//global_lsp_ptr = global_manager_lsp_get(global_manager_ptr,session_object_ptr);   
       
   ’’’’   
          pr_state                     
   idle   
                     
   !   msg_arrived = OPC_TRUE;       /// get the packet passed by the root process //   'received_pkptr = get_received_packet();       _//read the message type and put in arrived_msg_type that will determine the next forced state//   @op_pk_nfd_access(received_pkptr,"Msg Type", &arrived_msg_type);	       #// access to the list of objects //   >op_pk_nfd_access(received_pkptr,"Object List", &obj_list_ptr);       #ifdef RSVP_TE_DEBUG   k	printf("node %d [child %d](LSP S:%d, D:%d, T_id:%d): INTERMEDIATE NODE",shared_data->node_addr,own_pro_id,   o		obj_list_ptr->session->source_addr,obj_list_ptr->session->destination_addr,obj_list_ptr->session->tunnel_id);   #endif           4// get the ICI associated to the received packet  //   *rx_iciptr = op_pk_ici_get(received_pkptr);   /// read the capacity of the lsp from the ici //   ;op_ici_attr_get (rx_iciptr, "Lsp Capacity", &lsp_capacity);       1/*//initialize the address of the previous node//   prev_node_addr = -1;       ?// find and store the output stream towards the ingress node //   :// (useful in the future to forward the resv message)		 //   wtowards_ingress_stream_indx = find_outstream_from_instream(shared_data->module_objid,op_intrpt_strm(),&prev_node_addr);       #ifdef RSVP_TE_DEBUG   		printf("RSVP_TE_DEBUG [node %d (child %d)]: stream towards ingress %d \n",shared_data->node_addr,own_pro_id,towards_ingress_stream_indx);   #endif   */   
           ’’’’             pr_state                     
   update label set   
       
   @   	/* VARS*/   int						number_of_slices,*app;   5ERO_IPv4_subobject		*current_node_ptr,*next_node_ptr;       9/* save a copy of the Sender Tspec object on the child */   Schild_sender_tspec_object = sender_tspec_object_copy( obj_list_ptr->sender_tspec );       %// save a copy of the suggested label   Xsuggested_label_object_ptr = suggested_label_object_copy(obj_list_ptr->suggested_label);       ;/* get the number of Spectrum slices (12.5 GHz) required */   @number_of_slices = obj_list_ptr->sender_tspec->number_of_slices;       </* increase the total slices request and total lsp request*/   @shared_data->tot_lsp_request = shared_data->tot_lsp_request + 1;   Ushared_data->tot_slices_request = shared_data->tot_slices_request + number_of_slices;       C/* remove the current node from the ERO list and free its memory */   hcurrent_node_ptr = (ERO_IPv4_subobject*)prg_list_remove(obj_list_ptr->ero->subobjects,OPC_LISTPOS_HEAD);   prg_mem_free(current_node_ptr);       H/* read the next hop from the list (now it is at the top of the list) */   gnext_node_ptr = (ERO_IPv4_subobject*)prg_list_access (obj_list_ptr->ero->subobjects, OPC_LISTPOS_HEAD);       <// if Record Route Object is present add the current node //   #if (obj_list_ptr->rro != OPC_NIL) {   >	rsvp_rro_add_node(obj_list_ptr->rro, shared_data->node_addr);   }       /* save the next node address*/   =next_node_addr = inet_address_copy(next_node_ptr->ipv4_addr);       5// save the previous hop address (towards ingress) //   Dprev_node_addr = inet_address_copy(obj_list_ptr->rsvp_hop->ip_addr);       4// get the input interface index of the data plane//   ^app = forwarding_table_first_entry_lookup(shared_data->forwarding_table_ptr, &prev_node_addr);   if (app == OPC_NIL)	{   	printf("\nERROR node %s [child %d](LSP S:%d, D:%d, T_id:%d): No input stream found while processing ERO \n",node_addr_str,own_pro_id,   f		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   }   else {   	input_interface_indx = *app;   }           E//update the rsvp hop address with the address of the current node //   9obj_list_ptr->rsvp_hop->ip_addr = shared_data->node_addr;       N/* Find the output stream index used to identify the link (output interface)*/   ]app = forwarding_table_first_entry_lookup(shared_data->forwarding_table_ptr,&next_node_addr);   if (app == OPC_NIL)	{   	printf("\nERROR node %s [child %d](LSP S:%d, D:%d, T_id:%d): No output stream found while processing ERO \n",node_addr_str,own_pro_id,   f		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   }   else {   	output_interface_indx = *app;   	#ifdef RSVP_TE_DEBUG   b		printf(" ,next node is %d (output stream %d)\n",next_node_ptr->ipv4_addr,output_interface_indx);   	#endif   }       c/* Update the received label set object according to the spectrum availability in the output link*/   lsp_status = update_label_set_object(&(obj_list_ptr->label_set), output_interface_indx, obj_list_ptr->sender_tspec->number_of_slices);   
       
       
       
   ’’’’   
          pr_state                    
   reservation   
       
   "   )int			number_of_slices,central_freq_indx;       resv_pkptr = received_pkptr;       #ifdef RSVP_TE_DEBUG   q	printf("node %d [child %d](LSP S:%d, D:%d, T_id:%d): RESV MESSAGE RECEIVED\n",shared_data->node_addr,own_pro_id,   f		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   #endif   	   8/* get the number of slices (12.5 GHz) of BW required */   @number_of_slices = obj_list_ptr->sender_tspec->number_of_slices;       4/* get the selected central frequency for the LSP */   Gcentral_freq_indx = obj_list_ptr->generalized_label->central_freq_indx;   	   -// get the the link that will be allocated //   prsvp_link = (Rsvp_Link*) prg_bin_hash_table_item_get(shared_data->rsvp_links_htable_ptr,&output_interface_indx);       E/* try to reserve the frequency slot (backward blocking can occur) */   ibackward_blocking = reserve_frequency_slot(rsvp_link,central_freq_indx,number_of_slices, &lsp_freq_slot);       if (!backward_blocking) {   	// LSP resources reserved   	lsp_reserved = OPC_TRUE;   }       .if ( OPC_NIL != suggested_label_object_ptr &&    F	suggested_label_object_ptr->central_freq_indx != central_freq_indx) {   J	// in this case even if the oxc was pre-configured on the suggested label   G	// it must be re-configured because the central frequency is different   	oxc_ready = OPC_FALSE;   K	// set waiting oxc to false because a new oxc configuration must be issued   	waiting_oxc = OPC_FALSE;   }   
                     
   ’’’’   
          pr_state                     
   remote fblocking   
       
   	   `// NOTE: thi state should not be accessed. Path error messages are sent directly to ingress node   #ifdef RSVP_TE_DEBUG   	printf("node %d [child %d](LSP S:%d, D:%d, T_id:%d, sub_id:%d): PATH ERROR MESSAGE received during provisioning\n",shared_data->node_addr,own_pro_id,   f		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   #endif       </* forward the PATH ERROR message towards the ingress node*/   =//TODO path error do not need to strictly follow the lsp path   Bip_api_pk_send(ip_interface_ptr, received_pkptr, &prev_node_addr);   
                     
   ’’’’   
          pr_state      	    ²          
   release   
       
   %   Rsvp_Link* 		rsvp_link;       #ifdef RSVP_TE_DEBUG   Z	printf("node %d [child %d](LSP S:%d, D:%d, T_id:%d): ",shared_data->node_addr,own_pro_id,   f		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   	   	if (RESV_ERR_MSG_ARRIVED) {   *		printf("RESV ERROR MESSAGE received, ");   	}   	else {   )		printf("PATH TEAR MESSAGE received, ");   	}   #endif	        if (OPC_TRUE == lsp_reserved) {    	#ifdef RSVP_TE_DEBUG	   		printf("release frequency slot [%d,%d] in link %d\n",lsp_freq_slot.lower_freq_indx,lsp_freq_slot.upper_freq_indx,output_interface_indx);   	#endif   		   		   	// get the allocated link //   C	//rsvp_link = shared_data->rsvp_link_array[output_interface_indx];   q	rsvp_link = (Rsvp_Link*) prg_bin_hash_table_item_get(shared_data->rsvp_links_htable_ptr,&output_interface_indx);   	   3	// release the reserved freq slot from the link //   A	if (release_resources(rsvp_link, &lsp_freq_slot) == OPC_FALSE) {   t		printf("ERROR node %d [child %d]: Fail on removing a frequency slot on link\n",shared_data->node_addr,own_pro_id);   	}   	   X	if (OPC_TRUE == lsp_established && OPC_TRUE == shared_data->distributed_architecture) {   C		// schedule the ospf opaque lsa by invoking the ospf-te module //   r		trigger_ospf_te_update(shared_data->ospf_te_objid,shared_data->rsvp_links_htable_ptr, output_interface_indx,-1);   	}   }       E// forward the RESV ERROR/PATH TEAR message towards the egress node//   Bip_api_pk_send(ip_interface_ptr, received_pkptr, &next_node_addr);   
                     
   ’’’’   
          pr_state      
              
   wait provisioning   
       
       
       
      (if (op_intrpt_type()==OPC_INTRPT_STRM) {   	msg_arrived = OPC_TRUE;       /	/* get the packet passed by the root process*/   (	received_pkptr = get_received_packet();       `	/*read the message type and put in arrived_msg_type that will determine the next forced state*/   A	op_pk_nfd_access(received_pkptr,"Msg Type", &arrived_msg_type);	       $	/* access to the list of objects */   ?	op_pk_nfd_access(received_pkptr,"Object List", &obj_list_ptr);   	   	/*if (PATH_TEAR_MSG_ARRIVED) {   <		// forward the PATH TEAR message towards the egress node//   7		pkt_send_to_ip_layer(received_pkptr, next_node_addr);   	}*/   }   else {   	msg_arrived = OPC_FALSE;   }   
           ’’’’             pr_state          :          
   established   
       
      lsp_established = OPC_TRUE;   
       
      (if (op_intrpt_type()==OPC_INTRPT_STRM) {   	msg_arrived = OPC_TRUE;       /	/* get the packet passed by the root process*/   (	received_pkptr = get_received_packet();       `	/*read the message type and put in arrived_msg_type that will determine the next forced state*/   A	op_pk_nfd_access(received_pkptr,"Msg Type", &arrived_msg_type);	       $	/* access to the list of objects */   ?	op_pk_nfd_access(received_pkptr,"Object List", &obj_list_ptr);   	   }   else {   	msg_arrived = OPC_FALSE;   }   
           ’’’’             pr_state          ī          
   	terminate   
       
      /* this child destroy itself */    op_pro_destroy (op_pro_self ());   
                         ’’’’             pr_state        :  w          
   failure   
       J   3   *IF_ID_Error_spec_object	*if_id_error_spec;   Packet					*notify_msg_pkptr;   Ici						*iciptr;       /*   0if (NOTIFY_MSG_ARRIVED && REMOTE_LINK_FAILURE) {   ;//We should not enter in this block because notify message    ,//is sent directly towards the ingress node.   6	// Received a NOTIFY message due to a link failure //   3	// Forward the message towards the ingress node //   :	#if defined(RSVP_TE_DEBUG) || defined(LINK_FAILURE_DEBUG)   ¹		printf("RSVP Intermediate Node %d [child %d](LSP S:%d, D:%d, T_id:%d): PATH ERROR MESSAGE received due to a link failure, forward to ingress node\n",shared_data->node_addr,own_pro_id,   g			session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   	#endif   		   :	// forward the NOTIFY message towards the ingress node //   J	pkt_send_to_ip_layer(received_pkptr, &(session_object_ptr->source_addr));   }   else */   if (LINK_FAILURE_INTRPT) {   N	//global_manager_lsp_failed(global_manager_ptr, global_lsp_ptr->session_ptr);   	   :	#if defined(RSVP_TE_DEBUG) || defined(LINK_FAILURE_DEBUG)   	{   _	    char ip_str[INETC_ADDR_STR_LEN], ip_str2[INETC_ADDR_STR_LEN], ip_str3[INETC_ADDR_STR_LEN];   		printf("RSVP Intermediate Node %s [sub-child %d](LSP S:%s, D:%s, T_id:%d, sub_id:%d): LINK coming from node %s FAILED, send notification message\n",   ¦			node_addr_str,own_pro_id,inet_address_ptr_print(ip_str,&(session_object_ptr->source_addr)),inet_address_ptr_print(ip_str2,&(session_object_ptr->destination_addr)),   t			session_object_ptr->tunnel_id,session_object_ptr->sub_lsp_id, inet_address_ptr_print(ip_str3,&(prev_node_addr)));   	}   	#endif   	   #	// create the error spec object //   ^	if_id_error_spec = (IF_ID_Error_spec_object*) prg_mem_alloc(sizeof(IF_ID_Error_spec_object));   O	if_id_error_spec->error_node_addr = inet_address_copy(shared_data->node_addr);   E	if_id_error_spec->prev_hop_addr = inet_address_copy(prev_node_addr);   	   H	// create the NOTIFY message with the information of the failed link //   `	notify_msg_pkptr = create_notify_msg(session_object_copy(session_object_ptr),if_id_error_spec);   	   P	// Attach ICI information useful for the ingress node to plot the statistics //   ,	iciptr = op_ici_create ("MD_lsp_info_ici");   =	op_ici_attr_set_int32(iciptr, "Lsp Status", REMOTE_FAILURE);   =	op_ici_attr_set_int32(iciptr, "Lsp Capacity", lsp_capacity);   -	// associate the ici to a specific packet //   *	op_pk_ici_set (notify_msg_pkptr, iciptr);   	   6	global_manager_notification_sent(global_manager_ptr);   	   2	// send the path error towards the ingress node//   X	ip_api_pk_send(ip_interface_ptr, notify_msg_pkptr, &(session_object_ptr->source_addr));   }   J       
      (if (op_intrpt_type()==OPC_INTRPT_STRM) {   	msg_arrived = OPC_TRUE;       /	/* get the packet passed by the root process*/   (	received_pkptr = get_received_packet();       `	/*read the message type and put in arrived_msg_type that will determine the next forced state*/   A	op_pk_nfd_access(received_pkptr,"Msg Type", &arrived_msg_type);	       $	/* access to the list of objects */   ?	op_pk_nfd_access(received_pkptr,"Object List", &obj_list_ptr);   	   }   else {   	msg_arrived = OPC_FALSE;   }       
       
    ’’’’   
          pr_state                      
   forward blocking   
       
   7   Ici				*iciptr;   Packet*			path_err_pkptr;       /* FORWARD BLOCKING!! */       =/* update statistics based on the type of forward blocking */   switch (lsp_status) {   "    case FORWARD_BLOCKING_BW_LACK:   ,                        #ifdef RSVP_TE_DEBUG                             printf("node %s [child %d](LSP S:%d, D:%d, T_id:%d): FORWARD BLOCKING due to BW LACK!!!!\n",shared_data->node_addr_str,own_pro_id,                               session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);                           #endif   O                        //update_blocking_statistics(FORWARD_BLOCKING_BW_LACK);                           break;   (    case FORWARD_BLOCKING_FRAGMENTATION:   ,                        #ifdef RSVP_TE_DEBUG   £                          printf("node %s [child %d](LSP S:%d, D:%d, T_id:%d): FORWARD BLOCKING due to FRAGMENTATION!!!!\n",shared_data->node_addr_str,own_pro_id,                               session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);                           #endif   U                        //update_blocking_statistics(FORWARD_BLOCKING_FRAGMENTATION);                           break;   %    case FORWARD_BLOCKING_CONTINUITY:   ,                        #ifdef RSVP_TE_DEBUG   «                          printf("node %s [child %d](LSP S:%d, D:%d, T_id:%d): FORWARD BLOCKING due to CONTINUITY CONSTRAINT!!!!\n",shared_data->node_addr_str,own_pro_id,                               session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);                           #endif   R                       // update_blocking_statistics(FORWARD_BLOCKING_CONTINUITY);                           break;   }       // destroy the received ici //   op_ici_destroy(rx_iciptr);       D/* create the PATH ERROR MESSAGE and send towards the ingress node*/   Tpath_err_pkptr = create_path_error(session_object_copy(session_object_ptr),OPC_NIL);       O// Attach ICI information useful for the ingress node to plot the statistics //   +iciptr = op_ici_create ("MD_lsp_info_ici");   8op_ici_attr_set_int32(iciptr, "Lsp Status", lsp_status);   ,// associate the ici to a specific packet //   'op_pk_ici_set (path_err_pkptr, iciptr);       Bip_api_pk_send(ip_interface_ptr, path_err_pkptr, &prev_node_addr);               #ifdef RSVP_TE_DEBUG   s	printf("node %s [child %d](LSP S:%d, D:%d, T_id:%d): Send PATH ERROR MSG\n",shared_data->node_addr_str,own_pro_id,   f		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   #endif       /* destroy the packet*/   op_pk_destroy(received_pkptr);           
                     
   ’’’’   
          pr_state                     
   backward blocking   
       
   "   Packet*			path_err_pkptr;   Packet*			resv_err_pkptr;   Ici				*iciptr;       #ifdef RSVP_TE_DEBUG   	printf("node %d [child %d](LSP S:%d, D:%d, T_id:%d): BACKWARD BLOCKING!!!! Send RESV ERROR and PATH ERROR MSGs\n",shared_data->node_addr,own_pro_id,   f		session_object_ptr->source_addr,session_object_ptr->destination_addr,session_object_ptr->tunnel_id);   #endif   		   F/* INTERMEDIATE node has to send both PATH_ERR msg and RESV_ERR msg */   Tpath_err_pkptr = create_path_error(session_object_copy(session_object_ptr),OPC_NIL);   Lresv_err_pkptr = create_resv_error(session_object_copy(session_object_ptr));       h// send the path error towards the ingress node and the resv error to the next hop towards the egress //   O// Attach ICI information useful for the ingress node to plot the statistics //   +iciptr = op_ici_create ("MD_lsp_info_ici");   ?op_ici_attr_set_int32(iciptr, "Lsp Status", BACKWARD_BLOCKING);   ,// associate the ici to a specific packet //   'op_pk_ici_set (path_err_pkptr, iciptr);       Bip_api_pk_send(ip_interface_ptr, path_err_pkptr, &prev_node_addr);   Bip_api_pk_send(ip_interface_ptr, resv_err_pkptr, &next_node_addr);           ./* ingress node will update the statistics  */   0//update_blocking_statistics(BACKWARD_BLOCKING);       G// destroy the Ici coming from the egress node into the resv message //   'iciptr = op_pk_ici_get(received_pkptr);   op_ici_destroy(iciptr);       k/*destroy the resv message (be careful to not destroy the packet before making a copy of the object list)*/   op_pk_destroy(received_pkptr);       
                     
   ’’’’   
          pr_state                     
   forward   
       
      )// set the cross connect as not yet ready   oxc_ready = OPC_FALSE;   -// set waiting for oxc configuration to false   waiting_oxc = OPC_FALSE;       7/* forward the PATH message towards the ingress node */   Bip_api_pk_send(ip_interface_ptr, received_pkptr, &next_node_addr);   
       
       
       
   ’’’’   
          pr_state        ž  ż          
   wait oxc   
       
      if (! waiting_oxc) {   2// oxc has not yet triggered for the configuration   	Prohandle *prohandle_ptr;   	   	//increment the request id   +	++(shared_data->oxc_configure_request_id);   J	// save a copy of the request id of the last configuration request issued   =	conf_oxc_request_id = shared_data->oxc_configure_request_id;   	// trigger the configuration   b	oxc_configuration_request(&(shared_data->oxc_intf_handle),shared_data->oxc_configure_request_id);   	waiting_oxc = OPC_TRUE;   	   O	// insert this process inside the hash table of pending configuration requests   B	prohandle_ptr = (Prohandle *) prg_mem_alloc (sizeof (Prohandle));    	*prohandle_ptr = op_pro_self();   u	prg_bin_hash_table_item_insert(shared_data->pending_oxc_processes_htptr,&conf_oxc_request_id,prohandle_ptr,OPC_NIL);   }   
       
   #   (if (op_intrpt_type()==OPC_INTRPT_STRM) {   	msg_arrived = OPC_TRUE;       /	/* get the packet passed by the root process*/   (	received_pkptr = get_received_packet();       `	/*read the message type and put in arrived_msg_type that will determine the next forced state*/   A	op_pk_nfd_access(received_pkptr,"Msg Type", &arrived_msg_type);	       $	/* access to the list of objects */   ?	op_pk_nfd_access(received_pkptr,"Object List", &obj_list_ptr);   	   	/*if (PATH_TEAR_MSG_ARRIVED) {   <		// forward the PATH TEAR message towards the egress node//   7		pkt_send_to_ip_layer(received_pkptr, next_node_addr);   	}*/   }   else {   	msg_arrived = OPC_FALSE;   	   	if (OXC_CONFIGURED) {   		OpT_uInt32 	*request_id_ptr;   		// read the request id   *		request_id_ptr = op_pro_argmem_access();   4		// check if the request id correspond to the last    )		// configuration issued by this process   /		if (*request_id_ptr == conf_oxc_request_id) {   			// set the oxc as ready   			set_oxc_ready();   		}   	}   	   }           
           ’’’’             pr_state          ž          
   forward resv   
       
   
   8if (shared_data->distributed_architecture == OPC_TRUE) {   B	// schedule the ospf opaque lsa by invoking the ospf-te module //   q	trigger_ospf_te_update(shared_data->ospf_te_objid,shared_data->rsvp_links_htable_ptr, output_interface_indx,-1);   }       6// forward the RESV message towards the ingress node//   >ip_api_pk_send(ip_interface_ptr, resv_pkptr, &prev_node_addr);       2// the packet is not owned by this process anymore   resv_pkptr = OPC_NIL;   
                     
   ’’’’   
          pr_state        :             
   preconf oxc   
       
      ,if (OPC_NIL != suggested_label_object_ptr) {   	Prohandle *prohandle_ptr;   	//increment the request id   +	++(shared_data->oxc_configure_request_id);   J	// save a copy of the request id of the last configuration request issued   =	conf_oxc_request_id = shared_data->oxc_configure_request_id;   	// trigger the configuration   b	oxc_configuration_request(&(shared_data->oxc_intf_handle),shared_data->oxc_configure_request_id);   	waiting_oxc = OPC_TRUE;   	   O	// insert this process inside the hash table of pending configuration requests   B	prohandle_ptr = (Prohandle *) prg_mem_alloc (sizeof (Prohandle));    	*prohandle_ptr = op_pro_self();   u	prg_bin_hash_table_item_insert(shared_data->pending_oxc_processes_htptr,&conf_oxc_request_id,prohandle_ptr,OPC_NIL);   }   
                     
   ’’’’   
          pr_state                        µ’’’c      e      ū             
   tr_0   
       ’’’’          ’’’’          
    ’’’’   
          ’’’’                       pr_transition         
     5  M                     
   tr_21   
       
   RESV_MSG_ARRIVED   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition         
        ų                      
   tr_23   
       
   PATH_ERR_MSG_ARRIVED   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition               /             /  
   /  ¦    ć          
   tr_24   
       ’’’’          ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition                 V        /               
   tr_26   
       
   PATH_MSG_ARRIVED   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition              ü  B       :  ;  v          
   tr_27   
       
   FAILURE   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition               ¹   r                        
   tr_31   
       
   LABEL_SET_EMPTY   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition                /  Ś            .      /  Ø    ć          
   tr_32   
       ’’’’          ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      #         K   `           /      /  ©    ä          
   tr_35   
       
   default   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      $          x                      
   tr_36   
       
   backward_blocking   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      %         /  R           .     /  §    ā          
   tr_37   
       ’’’’          ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      &        p   q                       
   tr_38   
       
   LABEL_SET_NOT_EMPTY   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      '      
  E   ×                      
   tr_39   
       
   ! OXC_PRECONF   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition      .      	  9         7    Æ          
   tr_46   
       
   -PATH_TEAR_MSG_ARRIVED || RESV_ERR_MSG_ARRIVED   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition      0      	  8       9  w    ±          
   tr_48   
       
   -PATH_TEAR_MSG_ARRIVED || RESV_ERR_MSG_ARRIVED   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition      2   
       D         9    8  u          
   tr_50   
       
   FAILURE   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      4   	      Ņ  w       °    ļ          
   tr_52   
       ’’’’          ’’’’          
    ’’’’   
          ’’’’                       pr_transition      8   
   	  *           q    q  ©  j  ©          
   tr_56   
       
   PATH_TEAR_MSG_ARRIVED    
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      ;         ó  É           ś          
   tr_59   
       
   !backward_blocking && oxc_ready   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition      <        ū  ¢            ž          
   tr_60   
       
    !backward_blocking && !oxc_ready   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition      =          ā     ł  ė    ė          
   tr_61   
       
   OXC_CONFIGURED && oxc_ready   
       
’’’’   
       
    ’’’’   
       
    ’’’’   
                    pr_transition      >         ń              ;          
   tr_62   
       ’’’’          ’’’’          
    ’’’’   
          ’’’’                       pr_transition      ?        Ż        ~     :             
   tr_63   
       
   OXC_PRECONF   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition      @      
  ķ   ¢     %     ¼   „     Ó     ū          
   tr_64   
       ’’’’          ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      A   
   
     ģ        ’  ō   ł     ž          
   tr_65   
       
   OXC_CONFIGURED   
       
   set_oxc_ready()   
       
    ’’’’   
          ’’’’                       pr_transition      B        ü  A     ’  ü  7  1  8  d          
   tr_66   
       
   FAILURE   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      C      	  +       ż  ś  f  ż  q  
  q  ©    ©          
   tr_67   
       
   PATH_TEAR_MSG_ARRIVED    
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      D          Ė     9  v  ī  ½  5  z          
   tr_68   
       
   OXC_CONFIGURED   
       ’’’’          
    ’’’’   
          ’’’’                       pr_transition      E        E  ¼     ž  ķ    Č  ’  ī          
   tr_69   
       
   OXC_CONFIGURED && !oxc_ready   
       ’’’’          
    ’’’’   
       
    ’’’’   
                    pr_transition      F          Ō     F      É  A  ~          
   tr_70   
       
   default   
       
   cciao()   
       
    ’’’’   
          ’’’’                       pr_transition         G                  MD_global_manager   	MD_ip_api   MD_lsp   
MD_oxc_api   MD_rsvp_child_common_src   MD_rsvp_msg_util   MD_statistics_src                    