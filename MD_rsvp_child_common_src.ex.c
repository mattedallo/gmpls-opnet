#include 	"MD_rsvp_child_common_src.h"
#include	"MD_ospf_src.h"
#include  "MD_TED.h"

/*------------------------------------GET RECEIVED PACKET--------------------------*/
Packet* get_received_packet() {
Packet*		received_pkptr;
void*		arg_mem_ptr;

FIN(get_received_packet());

	/* Obtain the address of the argument memory block passed by calling process. */
	arg_mem_ptr = op_pro_argmem_access ();

	if (arg_mem_ptr == OPC_NIL) {

			#ifdef DEBUG
				printf("ERROR node %d [child %d]: No packet passed to child process \n",shared_data->node_addr,own_pro_id);
			#endif
			
			FRET(OPC_NIL);
		  
	}
	else {
			received_pkptr = (Packet*)arg_mem_ptr;
			FRET(received_pkptr);
	}

}


/*___________________________________________________FIND OUTPUT STREAM (LINK)_______________________________________________________________________________*/
/*
int find_outstream_from_instream(Objid module_objid,int input_stream_indx, int *neighbor_node_addr) {
	Objid	input_stream_objid,rx_objid,tx_objid,link_objid,output_stream_objid,node_objid,neighbor_tx_objid,neighbor_node_objid;
	int		num_transmitters,i;
	int 	output_stream_index=-1;
	
	FIN (find_outstream_from_instream (module_objid,input_stream_indx));
	
	node_objid = op_topo_parent(module_objid);
	input_stream_objid = op_topo_assoc (module_objid, OPC_TOPO_ASSOC_IN,OPC_OBJTYPE_STRM,input_stream_indx );
	//printf("node %d:input_stream_objid %d\n",shared_data->node_addr,input_stream_objid);
	rx_objid = op_topo_assoc(input_stream_objid,OPC_TOPO_ASSOC_IN, OPC_OBJTYPE_PTRX,0);	
	//printf("node %d:rx_objid %d\n",shared_data->node_addr,rx_objid);
	link_objid = op_topo_assoc(rx_objid,OPC_TOPO_ASSOC_IN, OPC_OBJTYPE_LKDUP,0);
	//printf("node %d:link_objid %d\n",shared_data->node_addr,link_objid);
	num_transmitters = op_topo_assoc_count (link_objid, OPC_TOPO_ASSOC_IN,OPC_OBJTYPE_PTTX);
	for (i = num_transmitters-1; i >= 0; i--) {
		tx_objid= op_topo_assoc(link_objid,OPC_TOPO_ASSOC_IN, OPC_OBJTYPE_PTTX,i);
		if (op_topo_parent(tx_objid) == node_objid) {
			//printf("node %d:tx_objid %d\n",shared_data->node_addr,tx_objid);
		 	output_stream_objid=op_topo_assoc (tx_objid, OPC_TOPO_ASSOC_IN,OPC_OBJTYPE_STRM,0);
			//printf("node %d:output_stream_objid %d\n",shared_data->node_addr,output_stream_objid);
			op_ima_obj_attr_get (output_stream_objid, "src stream",&output_stream_index);
			//printf("node %d:output_stream_index %d\n",shared_data->node_addr,output_stream_index);
			break;
		}
	}
	
	// get the object ID of the transmitter at the other side//
	neighbor_tx_objid = op_topo_assoc(rx_objid,OPC_TOPO_ASSOC_IN, OPC_OBJTYPE_PTTX,0);
	// get the object ID of the neighbor node //
	neighbor_node_objid=op_topo_parent(neighbor_tx_objid);
	// get the IP address of the remote node //
	op_ima_obj_attr_get (neighbor_node_objid, "node_addr",neighbor_node_addr);
	
	FRET(output_stream_index);
}
*/

/*____________________________________________________ RESERVE FREQUENCY SLOT _______________________________________________________________*/
Boolean reserve_frequency_slot(Rsvp_Link* rsvp_link, int central_freq_indx, int number_of_slices, Frequency_slot* new_freq_slot_ptr) {
int					lower_freq_indx_to_reserve,upper_freq_indx_to_reserve; 
int					allocated_freq_slots_vector_size;
int					index_order;
int					prev_upper_freq_indx;
int					i;
Frequency_slot*		freq_slot;
Boolean				back_blocking;

FIN(reserve_frequency_slot(rsvp_link,central_freq_indx, number_of_slices,new_freq_slot_ptr));

	allocated_freq_slots_vector_size = prg_vector_size(rsvp_link->allocated_freq_slots);
	
	lower_freq_indx_to_reserve = central_freq_indx - number_of_slices;
	upper_freq_indx_to_reserve = central_freq_indx + number_of_slices;
	
	/* check if the bandwidth is still available and fill it (otherwise backward blocking!) */
	back_blocking = OPC_FALSE;
	prev_upper_freq_indx = rsvp_link->min_frequency_indx;
	
	for (i=0; i<allocated_freq_slots_vector_size ;i++) {
			freq_slot = (Frequency_slot*) prg_vector_access(rsvp_link->allocated_freq_slots,i);
			if (central_freq_indx <= freq_slot->lower_freq_indx) {
				if ( (  lower_freq_indx_to_reserve >= prev_upper_freq_indx ) && ( upper_freq_indx_to_reserve <= freq_slot->lower_freq_indx ) ) {
					back_blocking = OPC_FALSE;
					index_order = i;
				}
				else {
					/* backward blocking! */
					back_blocking = OPC_TRUE;
				}
				
				break; //break for loop
			}
			
			prev_upper_freq_indx = freq_slot->upper_freq_indx;
	}
	
	if (i == allocated_freq_slots_vector_size) {
		if ( ( lower_freq_indx_to_reserve >= prev_upper_freq_indx ) && ( upper_freq_indx_to_reserve <= rsvp_link->max_frequency_indx ) ) {
			/* no blocking */
			back_blocking = OPC_FALSE;
			index_order = PRGC_VECTOR_INSERT_POS_TAIL;
		}
		else {
			/* backward blocking! */
			back_blocking = OPC_TRUE;
		}
	}
	
	
	if (back_blocking == OPC_FALSE) {
		/*----RESERVATION SUCCEED--------*/
	
		/* create the reserved frequency slot structure associated to this Lightpath (/child) */
		new_freq_slot_ptr->lower_freq_indx = lower_freq_indx_to_reserve;
		new_freq_slot_ptr->upper_freq_indx = upper_freq_indx_to_reserve;
		
		/* insert the new frequency slot in the vector (respecting the order)*/
		prg_vector_insert (rsvp_link->allocated_freq_slots, new_freq_slot_ptr, index_order);
			
		/* decrease the counter of available frequency slices*/
		rsvp_link->tot_available_freq_slices = rsvp_link->tot_available_freq_slices - number_of_slices;

		//printf("reserve [%d,%d]  tot_av_slice=%d\n",new_freq_slot_ptr->lower_freq_indx,new_freq_slot_ptr->upper_freq_indx,rsvp_link->tot_available_freq_slices);
	}
	
	FRET(back_blocking);
}


/*___________________________________________________ RELEASE ALLOCATED RESOURCES (FREQUENCY SLOT) ___________________________________________________________*/
Boolean release_resources(Rsvp_Link* rsvp_link, Frequency_slot * freq_slot) {
	int		number_of_slices;
	
	FIN(release_resources());
	
	/* find and remove the reserved frequency slot from the vector of slots  */
	/* associated to the output link of the lsp related to the current child */
	if ( prg_vector_find_and_remove (rsvp_link->allocated_freq_slots, freq_slot) == PrgC_Compcode_Failure ) {
		FRET(OPC_FALSE);
	}
	
	/* increment back the number of available frequency slots*/
	number_of_slices = (freq_slot->upper_freq_indx - freq_slot->lower_freq_indx )/2;
	rsvp_link->tot_available_freq_slices = rsvp_link->tot_available_freq_slices + number_of_slices;
	
	FRET(OPC_TRUE);
}

/*_________________________________________TRIGGER OSPF-TE LINK UPDATE_____________________________________________________________________*/
void trigger_ospf_te_update(Objid ospf_te_objid, PrgT_Bin_Hash_Table* rsvp_links_htable_ptr,int outstream_indx, OpT_Int32 transponder_id) {
	Ici		*iciptr;
	
	FIN(trigger_ospf_te_opaque_lsa(ospf_te_objid,rsvp_link_array,outstream_indx));
	
	// create an ICI to send informations together with the interrupt //
	iciptr = op_ici_create ("MD_ospf_te_update");
	
	// attach the output stream connected to the link inside the ICI //
	op_ici_attr_set_int32(iciptr, "output stream index", outstream_indx);
	// attach the pointer to the hash table of the rsvp-te links inside the ICI //
	op_ici_attr_set_ptr(iciptr, "rsvp-te links", rsvp_links_htable_ptr);
	
	op_ici_attr_set_int32(iciptr, "transponder ID", transponder_id);
	
	// Schedule the interrupt coupled with the ICI//
	op_ici_install (iciptr);
	op_intrpt_schedule_remote (op_sim_time(),LINK_STATUS_CHANGE_CODE,ospf_te_objid);
	
	op_ici_install (OPC_NIL);
	FOUT;
}



// DEBUG FUNCTIONS
void rsvp_link_print(Rsvp_Link*	rsvp_link) {
//PRINT LINK SPECTRUM, for debug purpose
	Frequency_slot* slot;
	int allocated_freq_slots_vector_size,j;
	
	FIN( rsvp_link_print(rsvp_link) );
	if (OPC_NIL == rsvp_link) FOUT;
	allocated_freq_slots_vector_size = prg_vector_size(rsvp_link->allocated_freq_slots);
	for(j=0; j<allocated_freq_slots_vector_size; j++) {
          slot = (Frequency_slot*) prg_vector_access(rsvp_link->allocated_freq_slots,j);
		  printf("[%d,%d]",slot->lower_freq_indx, slot->upper_freq_indx);
     }
	printf("\n");
	FOUT;
}



void node_transponders_statistic_update(Ospf_Topology const* topology,
                                        InetT_Address const* address_ptr,
                                        Stathandle *transponders_inuse_sh_ptr,
                                        Stathandle *transponders_avg_usage_sh_ptr) {
	PrgT_List *transponders_lptr;
	PrgT_List_Cell  *cell_ptr;
	Transponder  *transponder_ptr;
	double tot_utilization,avg_transponder_utilization;
	int t_inuse_count,tot_t;
	PrgT_Bin_Hash_Table *node_transponders_htptr;
	FIN(node_transponders_statistic_update());

	// avoid computation if statistics are not enabled
	if ((op_stat_valid(*transponders_inuse_sh_ptr) == OPC_FALSE) && 
	(op_stat_valid(*transponders_avg_usage_sh_ptr) == OPC_FALSE) ) FOUT;

  node_transponders_htptr = ted_node_transponders_get(topology,address_ptr);
  if ( node_transponders_htptr == PRGC_NIL) FOUT;
  t_inuse_count = 0;
  tot_utilization = 0.0;
  transponders_lptr = prg_bin_hash_table_item_list_get(node_transponders_htptr);
  tot_t = prg_list_size(transponders_lptr);
  cell_ptr = prg_list_head_cell_get (transponders_lptr);
  while (cell_ptr != PRGC_NIL) {
    transponder_ptr = prg_list_cell_data_get (cell_ptr);
    if (transponder_is_used(transponder_ptr)) {
    	++t_inuse_count;
    	tot_utilization += transponder_utilization_percentage(transponder_ptr);
    }
    cell_ptr = prg_list_cell_next_get (cell_ptr);
  }
  
  prg_list_destroy (transponders_lptr, PRGC_FALSE);
  
  op_stat_write(*transponders_inuse_sh_ptr,t_inuse_count);

  if (t_inuse_count>0) {
  	avg_transponder_utilization = tot_utilization/t_inuse_count;
  	op_stat_write(*transponders_avg_usage_sh_ptr,avg_transponder_utilization);
  }
  
	FOUT;
}
