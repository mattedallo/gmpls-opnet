#include 	<opnet.h>
#include 	<stdlib.h>
#include	"MD_capacity_bandwidth_lib.h"

//___________ SILNGLETON VARS ____________//

// vector used to map the capacity demand into the number of slices required to satisfy the demand
PrgT_Vector		*capacity_slices_map_vptr = OPC_NIL;



//___________ FUNCTIONS __________________//

static int capacity_compare_proc(const void* elem1_ptr,const void* elem2_ptr) {
// function used to sort the vector of capacity to slices mapping
Capacity_Slices_Assoc	*c1,*c2;
	FIN(capacity_compare_proc(elem1_ptr,elem2_ptr));
	c1 = (Capacity_Slices_Assoc*) elem1_ptr;
	c2 = (Capacity_Slices_Assoc*) elem2_ptr;
	if (c1->capacity >= c2->capacity) {
		if (c1->capacity == c2->capacity) FRET(0);
		FRET (-1); // element 1 will be closer to the end of the list than element 2
	}
	else {
		FRET (1);
	}
}



void cb_capacity_to_slices_map_init() {
//Initializes the singleton vector containing the mapping between capacity and number of slices.
//The mapping is saved inside a .gdf file
char							key[16], value[16],full_path[_MAX_PATH],directory[1024],drive[16];
PrgT_File_Parse					*file_parse_ptr;
PrgT_File_Parse_Block_Handle	block_handle;
int								num_entries,i,k;
Capacity_Slices_Assoc			*capacity_slices_assoc;

	FIN( capacity_to_slices_map_get() );
	// create the mapping between the capacity demand and the number of slices required //
	if (capacity_slices_map_vptr == PRGC_NIL) {
		
		// get the directory of the project //
		/*if( _fullpath( full_path, "MD_capacity_slices_mapping_file.gdf", _MAX_PATH ) == NULL )
			op_sim_end("Capacity Bandwidth Lib"," Configuration file with capacity to slices mapping NOT FOUND in",full_path,"");
		*/
	
		if (prg_tfile_path_rediscover ("MD_PCEP_root_process", PrgC_Tfile_Type_Process, full_path, 2048) == PrgC_Compcode_Failure) {
			op_sim_end("PCE SIMULATION ERROR","MD_RSVP_process directory not found","","");
		}
		
		_splitpath( full_path, drive, directory, OPC_NIL, OPC_NIL );
		
		sprintf (full_path, "%s%sMD_capacity_slices_mapping_file.gdf", drive,directory);
		
		file_parse_ptr = prg_file_parse (full_path);
		if (file_parse_ptr == PRGC_NIL)
			op_sim_end("Capacity Bandwidth Lib","Configuration file with capacity to slices mapping NOT FOUND","","");
		else
			printf("Capacity Bandwidth Lib : Found configuration file at %s\n",full_path);
		
		block_handle = prg_file_parse_block_handle_get (file_parse_ptr,"capacity_to_slice_mapping", 0);
		
		num_entries = prg_file_parse_block_entry_count_get (block_handle);
		
		if (num_entries < 1)
			op_sim_end("Capacity Bandwidth Lib"," No mapping between the capacity demand and the number of slices found","","");
		
		// create the vector that will contain the mapping between capacity demand and slices requires //
		
		capacity_slices_map_vptr = prg_vector_create (num_entries, PRGC_NIL,PRGC_NIL);
		
		#ifdef PCE_DEBUG
			printf("Capacity Bandwidth Lib : Found the following capacity->slices mapping,",value);
		#endif
		k=0;
		for (i=0; i<num_entries; i++) {
			if (prg_file_parse_block_entry_get (block_handle, i, key, value) != PrgC_Compcode_Failure) {	
				#ifdef PCE_DEBUG
					printf(" %sGbps->%sslices",key,value);
				#endif
				capacity_slices_assoc = (Capacity_Slices_Assoc*) prg_mem_alloc(sizeof(Capacity_Slices_Assoc));
				capacity_slices_assoc->capacity = atoi(key);
				capacity_slices_assoc->num_slices = atoi(value);
				// insert the capacity to slices association into the vector //
				prg_vector_insert (capacity_slices_map_vptr, capacity_slices_assoc, k);
				
				k++;
			}
		}
		
		#ifdef PCE_DEBUG
			printf("\n");
		#endif
		if (k==0)
    		op_sim_end("Capacity Bandwidth Lib"," No mapping between the capacity demand and the number of slices required","","");
		else
    		// the vector is sorted from the lower capacity to the higher in order to speed up the slice-ability algorithm //
    		prg_vector_sort (capacity_slices_map_vptr, capacity_compare_proc);	
		
		prg_file_parse_destroy(file_parse_ptr);
	}
	
	FOUT;
}


long cb_min_capacity_slices_get(int *capacity, int *slices) {
//Returns the index of the entry of the minimum capacity/slices association
//or -1 if not found. Returns Also the capacity and the number of slices
Capacity_Slices_Assoc	*capacity_slices_assoc;
	FIN( cb_min_capacity_slices_get(*capacity, *slices) );
	// since the vector is sorted from the minimum capacity to the maximum, get the first element
	capacity_slices_assoc = (Capacity_Slices_Assoc*) prg_vector_access (capacity_slices_map_vptr, 0);
	
	if ( capacity_slices_assoc == PRGC_NIL ) FRET(-1);
	
	// return the number of slices
	*slices = capacity_slices_assoc->num_slices;
	// return the capacity
	*capacity = capacity_slices_assoc->capacity;
	FRET(0);
}


long cb_closer_allowed_capacity_slices_get(int *capacity, int *slices){
//Gets the capacity to be allocated
//Returns the capacity (overwriting the one received) 
//and slices that better fit the requested capacity
Boolean					req_slices_found;
long 					capacity_map_indx,upper_indx,lower_indx,i;
Capacity_Slices_Assoc	*capacity_slices_assoc;
	FIN( cb_closer_allowed_capacity_slices_get(*capacity, *slices) );
	// binary search through the capacity map vector to find the number of slices needed for the requested capacity //
		req_slices_found = OPC_FALSE; 
		upper_indx = prg_vector_size(capacity_slices_map_vptr);
		//if vector is empty or NULL returns an error
		if (upper_indx == 0) FRET(-1)
			
		lower_indx = -1;
		i = upper_indx/2;
		while (upper_indx > lower_indx + 1)  {
			capacity_slices_assoc = (Capacity_Slices_Assoc*) prg_vector_access (capacity_slices_map_vptr, i);
			if (*capacity > capacity_slices_assoc->capacity) {
				lower_indx = i;
				i = i + (long)(upper_indx - i)/2;
			}
			else if (*capacity < capacity_slices_assoc->capacity) {
				upper_indx = i;
				i = i - (long)(i - lower_indx)/2;
			}
			else {
			// we found the exact capacity requested //
				req_slices_found = OPC_TRUE;
				break;
			}
		}
	
		if (req_slices_found == OPC_FALSE) {
			// we cannot find the exact capacity requested //
			// we allocate the least higher that can satisfy the demand //
			capacity_slices_assoc = (Capacity_Slices_Assoc*) prg_vector_access (capacity_slices_map_vptr, upper_indx);
			
			capacity_map_indx = upper_indx;
		} 
		else {
			capacity_map_indx = i;
		}
		// get the requested number of slices //
		*slices = capacity_slices_assoc->num_slices;
		
		// save the capacity in use //
		*capacity = capacity_slices_assoc->capacity;
	FRET(capacity_map_indx);
}


long cb_next_lower_capacity_slices_get(long prev_index, int *capacity, int *slices) {
// Return the next lower capacity and correspective slices 
// with respect to the one pointed by the prev_index
Capacity_Slices_Assoc	*capacity_slices_assoc;
	FIN( cb_next_lower_capacity_slices_get(prev_index, *capacity, *slices) );
	if (prev_index > 0) {
		prev_index--;
		capacity_slices_assoc = (Capacity_Slices_Assoc*) prg_vector_access (capacity_slices_map_vptr, prev_index);
		// get the requested number of slices //
		*slices = capacity_slices_assoc->num_slices;
		// save the capacity  //
		*capacity = capacity_slices_assoc->capacity;
		FRET(prev_index);
	}
	else if (prev_index == 0) {
		FRET(-1);
	}
	else {
	//TODO binary search next lower capacity 
	//based on the capacity received as parameter
		printf("TODO binary search next lower capacity based on the capacity received as parameter\n");
		FRET(-2);
	}
}


int cb_capacity_to_num_carriers(int capacity) {
	FIN(cb_capacity_to_num_carriers(capacity));
		switch(capacity) {
			case 1000: FRET(10);
			case 400: FRET(4);
			case 100: FRET(1);
		}
	FRET(1);
}


int cb_smaller_capacity_get(int capacity) {
	FIN(cb_smaller_capacity_get(capacity));
		switch(capacity) {
			case 1000: FRET(400);
			case 400: FRET(100);
		}
	FRET(-1);
}

int cb_closer_higher_capacity_get(int capacity) {
//TODO questa funzione fa pena
	int capacities[10];
	int best,i,size;
	double dif, min_dif;
	FIN(cb_closer_higher_capacity_get(capacity));
		capacities[0]=1000;
		capacities[1]=400;
		capacities[2]=100;
		size = 3;

		best = 0;
		i=1;
		min_dif = capacities[0]-capacity;
		while (min_dif<0 && i<size) {
			min_dif = capacities[i]-capacity;
			best =i;
			++i;
		}

		for (; i<size; ++i) {
			dif = capacities[i]-capacity;
			if (dif < 0) continue;
			if (dif<min_dif) {
				min_dif=dif;
				best = i;
			}
		}
	// errore se non esiste capacit� maggiore di quella data
	FRET(capacities[best]);
}


MD_Compcode cb_slices_from_capacity_get(int capacity, Transponder *transponder_ptr, int *slices_ptr){
	FIN( cb_slices_from_capacity_get(capacity, transponder_ptr, slices_ptr) );
		switch (capacity) {
			case 1000:
					*slices_ptr = 23;
					FRET(MD_Compcode_Success);

			case 400:
					switch(transponder_type_get(transponder_ptr)) {
						case SBVT:
							*slices_ptr = 9;
							FRET(MD_Compcode_Success);
						case MW_SBVT_FIX:
						case MW_SBVT_VAR:
							*slices_ptr = 8;
							FRET(MD_Compcode_Success);
					}

			case 200:
					*slices_ptr = 8;
					FRET(MD_Compcode_Success);
					
			case 100:
					*slices_ptr = 3;
					FRET(MD_Compcode_Success);
			
		}
	FRET(MD_Compcode_Failure);
}
