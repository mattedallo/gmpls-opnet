#ifndef MD_pcep_api_h
#define MD_pcep_api_h

#include <opnet.h>
#include "MD_path_computation_src.h"
#include "MD_pcep_src.h"
#include "MD_rsvp_msg_util.h"
#include "MD_rsvp.h"

/* Interrupt codes sent to PCEP module*/
/*#define PCEPC_LSP_ACTIVE        0
#define PCEPC_LSP_GOING_UP      1
#define PCEPC_LSP_GOING_DOWN    2
#define PCEPC_LSP_DOWN          3*/
#define PCEPC_PATH_COMP_REQ     4
#define PCEPC_REPORT            6

// Interrupt codes sent by the PCEP module //
#define PCEPC_PATH_COMP_REP     5


/* Structure of an interface handle 	*/
/* which an application can use to		*/
/* communicate with PCEP.				*/
typedef struct ApiT_Pcep_App_Handle {
    Objid   		app_mod_id;
    Objid   		pcep_mod_id;
} ApiT_Pcep_App_Handle;

typedef struct ApiT_Pcep_PC_Request {
    Session_object          *lsp_id;
    Path_Comp_Params        *pc_params;
    int                     capacity;
} ApiT_Pcep_PC_Request;

typedef struct ApiT_Pcep_LSP {
    Session_object          *lsp_id;
    ERO_object              *ero_object_ptr;
    Sender_tspec_object     *sender_tspec_ptr;
	Suggested_label_object  *sugg_label_ptr;
    int                     capacity;
    PrgT_Vector             *carriers_vptr; // list of carriers to be used (not in rfc)
    OpT_uInt32              *sugg_ingress_tx_trans_id_ptr; //transponder to be used as transmitter at the ingress(not in rfc)
    OpT_uInt32              *sugg_ingress_rx_trans_id_ptr; //transponder to be used as receiver at the ingress(not in rfc)
    OpT_uInt32              *sugg_egress_tx_trans_id_ptr; //transponder to be used as transmitter at the egress(not in rfc)
    OpT_uInt32              *sugg_egress_rx_trans_id_ptr; //transponder to be used as receiver at the egress(not in rfc)
    PrgT_Vector       *ingress_tx_carrier_ids_vptr; // list of carrier ids to be used at the TX ingress (not in rfc)
    PrgT_Vector       *ingress_rx_carrier_ids_vptr; // list of carrier ids to be used at the RX ingress (not in rfc)
    PrgT_Vector       *egress_tx_carrier_ids_vptr; // list of carrier ids to be used at the TX egress (not in rfc)
    PrgT_Vector       *egress_rx_carrier_ids_vptr; // list of carrier ids to be used at the RX egress (not in rfc)

} ApiT_Pcep_LSP;

typedef struct ApiT_Pcep_PC_Reply {
    PrgT_Vector  *lsps_vptr; //vector of ApiT_Pcep_LSP
    Lsp_event    result_code;
} ApiT_Pcep_PC_Reply;

typedef struct ApiT_Pcep_PC_Report {
    Session_object          *lsp_id;
    LSP_Operational         state;
    ERO_object              *ero_ptr;
    Sender_tspec_object     *sender_tspec;
    int 			        upper_freq_indx;
	int 			        lower_freq_indx;
    int                     capacity;
} ApiT_Pcep_PC_Report;

// public functions manifest //
ApiT_Pcep_App_Handle pcep_app_register (Objid app_module_id);
ApiT_Pcep_PC_Reply* api_pc_reply_create(Pcep_PcRep_Msg_Body const* pcrep_msg_body_ptr) ;
void api_pc_reply_destroy(ApiT_Pcep_PC_Reply *pc_reply);
void pcep_path_computation_command_send(ApiT_Pcep_App_Handle *pcep_handler, Session_object *lsp_session_ptr,int capacity, Path_Comp_Params *params, int request_id);
void pcep_lsp_status_changed_notify(ApiT_Pcep_App_Handle *pcep_handler, Session_object *lsp_session_ptr, ERO_object *ero_ptr, Sender_tspec_object *sender_tspec,int upper_freq_indx,int lower_freq_indx, int capacity, LSP_Operational state);
#define pcep_lsp_going_up_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity)  \
		pcep_lsp_status_changed_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity, LSP_OP_GOING_UP)
#define pcep_lsp_active_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity)  \
		pcep_lsp_status_changed_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity, LSP_OP_ACTIVE)
#define pcep_lsp_up_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity)  \
		pcep_lsp_status_changed_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity, LSP_OP_UP)
#define pcep_lsp_going_down_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity)  \
		pcep_lsp_status_changed_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity, LSP_OP_GOING_DOWN)
#define pcep_lsp_down_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity)  \
		pcep_lsp_status_changed_notify(pcep_handler, lsp_session_ptr, ero_ptr, sender_tspec, upper_freq_indx, lower_freq_indx, capacity, LSP_OP_DOWN)

void pcep_api_pc_report_destroy(ApiT_Pcep_PC_Report *report);

#endif
