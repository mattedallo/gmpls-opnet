#include 	"MD_ip_src.h"

/*________________________________________________________________________________________________________*/
//Retreive the destination ip address from the output stream index
InetT_Address* forwarding_table_reverse_lookup(PrgT_Bin_Hash_Table *forwarding_table_ptr,int outstream_indx){
	InetT_Address* 		address=OPC_NIL;
	Forwarding_Entry	*forward_entry;
	PrgT_List 			*entries_lptr;
	PrgT_List_Cell 		*cell_ptr;
	int					*stream_indx;
	
	FIN(forwarding_table_reverse_lookup(forwarding_table_ptr,  outstream_indx));
	
	// get the list of entries of the forwarding table //
	entries_lptr = prg_bin_hash_table_item_list_get (forwarding_table_ptr);
	cell_ptr = prg_list_head_cell_get (entries_lptr);
	
	// iterate the list of opaque lsas and flood each one//
	while (cell_ptr != PRGC_NIL) {
		forward_entry = (Forwarding_Entry *) prg_list_cell_data_get (cell_ptr);
		
		if (prg_vector_size(forward_entry->interfaces_vptr) != 1) continue;
		stream_indx = prg_vector_access(forward_entry->interfaces_vptr,0);
		if (*stream_indx == outstream_indx) {
			address = &forward_entry->addr;
			break;
		}
		cell_ptr = prg_list_cell_next_get (cell_ptr);
	}
	
	// destroy the list (not the inner elements) //
	prg_list_destroy (entries_lptr, OPC_FALSE);
	
	FRET(address);
}


/*_________________________________________________________________________________________________________*/
PrgT_Vector* forwarding_table_lookup(PrgT_Bin_Hash_Table *forwarding_table_ptr, const InetT_Address* addr_ptr){
	Forwarding_Entry*	entry;
	FIN(forwarding_table_lookup(forwarding_table_ptr, addr_ptr));
	entry = (Forwarding_Entry*)prg_bin_hash_table_item_get(forwarding_table_ptr, inet_address_addr_ptr_get_const(addr_ptr) );
	if (entry == PRGC_NIL) FRET(PRGC_NIL);
	FRET(entry->interfaces_vptr)
}


OpT_uInt32* forwarding_table_first_entry_lookup(PrgT_Bin_Hash_Table *forwarding_table_ptr, const InetT_Address* addr_ptr){
	PrgT_Vector* interfaces_indexes_vptr;
	
	FIN(forwarding_table_first_entry_lookup(forwarding_table_ptr, addr_ptr));
	
	interfaces_indexes_vptr = forwarding_table_lookup(forwarding_table_ptr, addr_ptr);
	
	if (interfaces_indexes_vptr == PRGC_NIL) FRET(PRGC_NIL);
	
	FRET(prg_vector_access(interfaces_indexes_vptr,0))
}

/*
//-----------------------------SEND PACKET TO IP LAYER---------------------------------//
void pkt_send_to_ip_layer (Packet* pkptr, const InetT_Address* dest_addr) {

Ici	*iciptr = OPC_NIL;

FIN(pkt_send_to_ip_layer(pkptr,dest_addr));

iciptr = op_ici_create ("inet_encap_req");

// place the IP address of the source node into the ICI //
op_ici_attr_set_ptr(iciptr, "dest_addr", dest_addr );
	
// Call the interrupt coupled with the ICI//
op_ici_install (iciptr);

// send the packet to the IP module //
op_pk_send(pkptr,0);

// Deinstall the ICI to prevent it from being associated with other interrupts scheduled by this process//
op_ici_install (OPC_NIL);

FOUT;
}
*/
