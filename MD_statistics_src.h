#ifndef	_MD_STATISTICS_H_INCLUDED_
	#define _MD_STATISTICS_H_INCLUDED_
	
	#include 	<opnet.h>
    #include	"MD_rsvp.h"
	
  //#define		LINK_FAILURE_DEBUG
  
	/* Local Variables*//*
	Stathandle		routing_block_stat_hndl;
	Stathandle		forward_block_stat_hndl;
	Stathandle		backward_block_stat_hndl;
	Stathandle		total_block_stat_hndl;
	Stathandle		forward_block_bw_lack_stat_hndl;
	Stathandle		forward_block_fragmentation_stat_hndl;
	Stathandle		forward_block_continuity_stat_hndl;
	Stathandle		spectrum_utilization_stat_hndl;
  //Stathandle    used_paths_stat_hndl;
	*/
  
  /* recall global variables */
	extern unsigned long gbl_total_busy_slices;
	extern unsigned long gbl_total_slices;

	/*	Function Prototypes					*/
	void register_statistics();
	void update_blocking_statistics(Lsp_event event, unsigned int sub_lsp_capacity, unsigned int total_lsp_capacity);
	void update_links_statistics(Lsp_event event, int num_slices);
  void update_used_path_statistics(unsigned short	number_of_paths);
  void stat_notify_failed_lsps (unsigned int failed_lsps);
  void stat_decrease_capacity_to_recover(unsigned int capacity);
  void stat_increase_capacity_to_recover(unsigned int capacity);
  void stat_recovery_start();
  void update_recovery_blocking_statistics(Lsp_event event,unsigned int sub_lsp_capacity, unsigned int total_lsp_capacity);
  
  void update_recovery_400G(int restored_cap);
  void stat_path_computation_time_write(double pc_time);
  void stat_recovery_time_write(double recovery_time);
  void stat_provisioning_time_write(double provisioning_time);
  void stat_path_computation_roundtrip_time_write(double time);
void recovery_end();
#endif
