#ifndef MD_oxc_api_h
#define MD_oxc_api_h

#include <opnet.h>

/* Interrupt codes */
#define OXC_CONFIGURED_CODE					0
#define OXC_CONFIGURATION_REQUEST_CODE		1


typedef struct ApiT_Oxc_Interface_Handle {
	Objid	client_objid;
	Objid	oxc_objid;
}	ApiT_Oxc_Interface_Handle;



// INTERFACE
ApiT_Oxc_Interface_Handle oxc_client_register (Objid client_mod_id);
void oxc_configuration_request(ApiT_Oxc_Interface_Handle const * const oxc_intf_hndlptr, OpT_Int32 request_id);
Objid oxc_objid_get(ApiT_Oxc_Interface_Handle const * const oxc_intf_hndlptr);
OpT_Int32 oxc_interrupt_request_id_get(ApiT_Oxc_Interface_Handle const * const oxc_intf_hndlptr);

#endif
