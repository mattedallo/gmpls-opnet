#ifndef	_MD_PCEP_ROOT_CHILDS_H_INCLUDED_
	#define _MD_PCEP_ROOT_CHILDS_H_INCLUDED_
    
#include    <ip_addr_v4.h>
#include	"tcp_api_v3.h"
#include	"MD_rsvp.h"
#include    "MD_ospf_src.h"
#include	"MD_spectrum_assignment_src.h"
    
typedef struct LSP_DB_Entry {
  // The information stored in the LSP State Database
    PrgT_Vector       *edges_vptr; //vector of edges
    ERO_object        *ero_object_ptr;
    Session_object 	  *session_ptr;
    int 			  upper_freq_indx;
	int 			  lower_freq_indx;
    PrgT_Vector       *carriers_vptr; // list of carriers to be used (not in rfc)
    PrgT_Vector       *ingress_tx_carrier_ids_vptr; // list of carrier ids to be used at the TX ingress (not in rfc)
    PrgT_Vector       *ingress_rx_carrier_ids_vptr; // list of carrier ids to be used at the RX ingress (not in rfc)
    PrgT_Vector       *egress_tx_carrier_ids_vptr; // list of carrier ids to be used at the TX egress (not in rfc)
    PrgT_Vector       *egress_rx_carrier_ids_vptr; // list of carrier ids to be used at the RX egress (not in rfc)
    OpT_uInt32        *sugg_ingress_tx_trans_id_ptr; //transponder to be used as transmitter at the ingress(not in rfc)
    OpT_uInt32        *sugg_ingress_rx_trans_id_ptr; //transponder to be used as receiver at the ingress(not in rfc)
    OpT_uInt32        *sugg_egress_tx_trans_id_ptr; //transponder to be used as transmitter at the egress(not in rfc)
    OpT_uInt32        *sugg_egress_rx_trans_id_ptr; //transponder to be used as receiver at the egress(not in rfc)
    LSP_Operational   state;
} LSP_DB_Entry;
  
typedef struct Pcep_Process_Shared_Memory {
// the structure shared between root process and childs (peers)
    short                   keepalive;
    short                   deadtimer;
    unsigned int            session_id;
    Ospf_Topology           *ospf_topology; //Topology structure defined in ospf
    PrgT_Bin_Hash_Table     *lsp_state_database_htptr; //Hash table that keeps track of all the lsps (in case of stateful PCE)
    int                     pc_additional_hops; // the number of additional hops to the shortest path
    Path_Comp_Params        provisioning;
    Path_Comp_Params        recovery;
    Boolean                 pce_stateful; // if true the PCE is stateful
    double                  last_path_comp_end_time;
    InetT_Address			node_addr;
    PrgT_Bin_Hash_Table		*pending_pc_requests_htptr;
} Pcep_Process_Shared_Memory;

typedef struct Pcep_Conn_Info {
	// ip address of the remote host
	InetT_Address			neighbor_ip_address;
	// unique id of the tcp connection assigned by tcp module
	int						tcp_connection_id;
	// the pcep peer process handle
	Prohandle				pcep_peer_prohandle;
	// handler of the tcp connection
	ApiT_Tcp_App_Handle		tcp_handle;
    // a queue of PCReq packets 
    PrgT_Vector             *pcreq_queue_ptr;
    // a queue of PCRpt packets 
    PrgT_Vector             *pcrpt_queue_ptr;
    // a flag to indicate when the PCEP session is UP
    Boolean                 pcep_session_up;
    // a flag to indicate if the connection is between stateful PCE
    Boolean                 pce_stateful; 
    Boolean                 pce_peer;
} Pcep_Conn_Info;

typedef struct Pending_PC_Requests {
    Objid       module_objid; //the process that issued the Path Computation
    int         request_id; //identifies the PC
} Pending_PC_Requests;
#endif