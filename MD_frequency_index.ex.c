#include "MD_frequency_index.h"
#include "opnet.h"
#include <math.h>

struct Frequency_Index {
	int		index;
	double	zero_freq;
	double	spacing;
};


Frequency_Index* frequency_index_create(double frequency, double zero_freq, double spacing) {
	Frequency_Index	*fi_ptr;
	FIN( frequency_index_copy(fi_ptr) );
	fi_ptr = prg_mem_alloc(sizeof(Frequency_Index));
	fi_ptr->index = floor( (frequency - zero_freq)/spacing + 0.5);
	fi_ptr->zero_freq = zero_freq;
	fi_ptr->spacing = spacing;
	FRET(fi_ptr);
}

Frequency_Index* frequency_index_copy(Frequency_Index const * const fi_ptr) {
	Frequency_Index	*fi_copy_ptr;
	FIN( frequency_index_copy(fi_ptr) );
	fi_copy_ptr = prg_mem_alloc(sizeof(Frequency_Index));
	fi_copy_ptr->index = fi_ptr->index;
	fi_copy_ptr->zero_freq = fi_ptr->zero_freq;
	fi_copy_ptr->spacing = fi_ptr->spacing;
	FRET(fi_copy_ptr);
}


void frequency_index_destroy(Frequency_Index  *fi_ptr) {
	FIN( frequency_index_destroy(fi_ptr) );
	prg_mem_free(fi_ptr);
	FOUT;
}



Boolean frequency_index_is_equal(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr) {
	double f_a,f_b;
	FIN( frequency_index_is_equal(fi_a_ptr,fi_b_ptr) );
	f_a = fi_a_ptr->zero_freq + (fi_a_ptr->index*fi_a_ptr->spacing);
	f_b = fi_b_ptr->zero_freq + (fi_b_ptr->index*fi_b_ptr->spacing);
	if (f_a == f_b) {
		FRET(OPC_TRUE);
	}
	else {
		FRET(OPC_FALSE);
	}
}

	
Boolean frequency_index_is_lower(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr) {
	double f_a,f_b;
	FIN( frequency_index_is_lower(fi_a_ptr,fi_b_ptr) );
	f_a = fi_a_ptr->zero_freq + (fi_a_ptr->index*fi_a_ptr->spacing);
	f_b = fi_b_ptr->zero_freq + (fi_b_ptr->index*fi_b_ptr->spacing);
	if (f_a < f_b) {
		FRET(OPC_TRUE);
	}
	else {
		FRET(OPC_FALSE);
	}
}


Boolean frequency_index_is_lower_equal(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr) {
	double f_a,f_b;
	FIN( frequency_index_is_lower_equal(fi_a_ptr,fi_b_ptr) );
	f_a = fi_a_ptr->zero_freq + (fi_a_ptr->index*fi_a_ptr->spacing);
	f_b = fi_b_ptr->zero_freq + (fi_b_ptr->index*fi_b_ptr->spacing);
	if (f_a <= f_b) {
		FRET(OPC_TRUE);
	}
	else {
		FRET(OPC_FALSE);
	}
}


Boolean frequency_index_is_higher(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr) {
	double f_a,f_b;
	FIN( frequency_index_is_higher(fi_a_ptr,fi_b_ptr) );
	f_a = fi_a_ptr->zero_freq + (fi_a_ptr->index*fi_a_ptr->spacing);
	f_b = fi_b_ptr->zero_freq + (fi_b_ptr->index*fi_b_ptr->spacing);
	if (f_a > f_b) {
		FRET(OPC_TRUE);
	}
	else {
		FRET(OPC_FALSE);
	}
}

Boolean frequency_index_is_higher_equal(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr) {
	double f_a,f_b;
	FIN( frequency_index_is_higher_equal(fi_a_ptr,fi_b_ptr) );
	f_a = fi_a_ptr->zero_freq + (fi_a_ptr->index*fi_a_ptr->spacing);
	f_b = fi_b_ptr->zero_freq + (fi_b_ptr->index*fi_b_ptr->spacing);
	if (f_a >= f_b) {
		FRET(OPC_TRUE);
	}
	else {
		FRET(OPC_FALSE);
	}
}


short frequency_index_compare(Frequency_Index const * const fi_a_ptr, Frequency_Index const * const fi_b_ptr) {
	double f_a,f_b;
	FIN( frequency_index_compare(fi_a_ptr,fi_b_ptr) );
	f_a = fi_a_ptr->zero_freq + (fi_a_ptr->index*fi_a_ptr->spacing);
	f_b = fi_b_ptr->zero_freq + (fi_b_ptr->index*fi_b_ptr->spacing);
	if (f_a > f_b) {
		FRET(FREQUENCY_INDEXC_HIGHER);
	}
	else if (f_a == f_b) {
		FRET(FREQUENCY_INDEXC_EQUAL);
	}
	else {
		FRET(FREQUENCY_INDEXC_LOWER);
	}
}


double frequency_index_hertz_get(Frequency_Index const * const fi_ptr) {
/**
	Returns the frequency in GHz of the freq index.
**/
	double f;
	FIN( frequency_index_hertz_get(fi_ptr) );
	f = fi_ptr->zero_freq + (fi_ptr->index*fi_ptr->spacing);
	FRET(f);
}

double frequency_index_zero_frequency_get(Frequency_Index const * const fi_ptr) {
/**
	Returns the frequency with zero index (in GHz).
**/
	FIN( frequency_index_hertz_get(fi_ptr) );
	FRET(fi_ptr->zero_freq);
}

double frequency_index_spacing_get(Frequency_Index const * const fi_ptr) {
/**
	Returns the spacing between indexes (in GHz).
**/
	FIN( frequency_index_spacing_get(fi_ptr) );
	FRET(fi_ptr->spacing);
}


void frequency_index_print(Frequency_Index const * const fi_ptr) {
/**
	Print the frequency index.
**/
	FIN( frequency_index_print(fi_ptr) );
	printf("%d",fi_ptr->index);
	FOUT;
}



int frequency_to_index_ceil(double frequency) {
	FIN( frequency_to_index_ceil(frequency) );
	frequency-=FREQUENCY_INDEXC_ZERO_INDEX_DEFAULT;
	FRET(ceil(frequency/FREQUENCY_INDEXC_SPACING_DEFAULT));
}

int frequency_to_index_floor(double frequency) {
	FIN( frequency_to_index_floor(frequency) );
	frequency-=FREQUENCY_INDEXC_ZERO_INDEX_DEFAULT;
	FRET(floor(frequency/FREQUENCY_INDEXC_SPACING_DEFAULT));
}

double index_to_frequency(int frequency_index){
	double frequency;
	FIN( index_to_frequency(frequency_index) );
	frequency = FREQUENCY_INDEXC_ZERO_INDEX_DEFAULT +
							frequency_index*FREQUENCY_INDEXC_SPACING_DEFAULT;
	FRET(frequency);
}
