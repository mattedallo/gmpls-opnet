#include 	<opnet.h>
#include 	<oms_pr.h>
#include 	"MD_oxc_api.h"


ApiT_Oxc_Interface_Handle oxc_client_register (Objid client_objid) {
	ApiT_Oxc_Interface_Handle	oxc_intf_handle;
	List						proc_record_handle_list;
	OmsT_Pr_Handle				process_record_handle;
	Objid						node_objid;

	/** The interface handle is used for communication between the 	**/
	/** client and the OXC module. The members of the interface  	**/
	/** handle data structure are filled in this function and 	 	**/
	/** returned to the client. The client will use this handle 	**/
	/** for future calls to the OXC module.							**/
	FIN (oxc_client_register (client_objid));

	/* Make the client module Object ID as a part of this interface	*/
	oxc_intf_handle.client_objid = client_objid; 

	/* Initialize the BV-OXC module ID to INVALID. It will contain a*/
	/* valid value after discovering the BV-OXC module ID.			*/
	oxc_intf_handle.oxc_objid = OPC_OBJID_INVALID;

	/* Node ID from the application module ID. This value is used 	*/
	/* for discovery of the OXC module.								*/
	node_objid = op_topo_parent (client_objid);

	/* Use process registry to find the TCP module ID present in 	*/
	/* the same node as the applciation.							*/
    oms_pr_process_discover (OPC_OBJID_INVALID, &proc_record_handle_list,
                    "node objid",   OMSC_PR_OBJID,   node_objid,
                    "protocol", OMSC_PR_STRING, "bv-oxc", OPC_NIL);

	if (op_prg_list_size (&proc_record_handle_list))
		{
		/* Assume that there is only one OXC module.				*/

		/* Get a handle to the OXC process handle.					*/
		process_record_handle = (OmsT_Pr_Handle) op_prg_list_remove (&proc_record_handle_list, 
																	 OPC_LISTPOS_HEAD);

		/* Obtain the object ID of the module running TCP.			*/
		oms_pr_attr_get (process_record_handle, "module objid", OMSC_PR_OBJID, &(oxc_intf_handle.oxc_objid));
		}
	else
		{
		/* There is no TCP module. Return an invalid TCP handle.	*/
		FRET (oxc_intf_handle);
		}
 
	FRET (oxc_intf_handle);
}


Objid oxc_objid_get(ApiT_Oxc_Interface_Handle const * const oxc_intf_hndlptr) {
	/** return the object ID of the oxc module **/
	FIN( oxc_objid_get(oxc_intf_hndlptr) );
	FRET(oxc_intf_hndlptr->oxc_objid);
}


void oxc_configuration_request(ApiT_Oxc_Interface_Handle const * const oxc_intf_hndlptr, OpT_Int32 request_id) {
	Ici *ici_ptr;
	/** Function used by the client to invoke the OXC module **/
	/** asking for a configuration request **/
	FIN( oxc_configuration_request(oxc_intf_hndlptr) );
	
	// if the oxc mudule does not exist return.
	if (OPC_OBJID_INVALID == oxc_intf_hndlptr->oxc_objid) {
		printf("OXC API ERROR: OXC module not found, OXC configuration cannot be requested\n");
		FOUT;
	}
	
	ici_ptr = op_ici_create("MD_oxc_ici");
	op_ici_attr_set_int32(ici_ptr, "request id", request_id);
	op_ici_install(ici_ptr);
	op_intrpt_schedule_remote(op_sim_time(), OXC_CONFIGURATION_REQUEST_CODE, oxc_intf_hndlptr->oxc_objid);
	op_ici_install(OPC_NIL);
	
	FOUT;
}

OpT_Int32 oxc_interrupt_request_id_get(ApiT_Oxc_Interface_Handle const * const oxc_intf_hndlptr) {
	Ici *ici_ptr;
	OpT_Int32 request_id;
	/** Function used by the client to get the request id **/
	/** received with the interrupt **/
	FIN( oxc_interrupt_request_id_get(oxc_intf_hndlptr) );
	
	if (OPC_OBJID_INVALID == oxc_intf_hndlptr->oxc_objid) {
		printf("OXC API ERROR: OXC module not found, interrupt request id is not available\n");
		FRET(-1);
	}
	
	if (op_intrpt_source() != oxc_intf_hndlptr->oxc_objid) {
		printf("OXC API ERROR: the interrupt is not associated to the OXC, request id cannot be found\n");
		FRET(-2);
	}
	
	ici_ptr = op_intrpt_ici();
	if ( OPC_NIL == ici_ptr ) {
		printf("OXC API ERROR: no ici received, request id cannot be found\n");
		FRET(-2);
	}
	
	/* read the attributes */
	op_ici_attr_get_int32(ici_ptr, "request id", &request_id);
	// destroy the ici //
	op_ici_destroy(ici_ptr);
	
	FRET(request_id);
}
