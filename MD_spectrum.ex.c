#include	"MD_spectrum.h"
#include	"MD_frequency_slot.h"
#include	"MD_frequency_index.h"
#include	"opnet.h"



struct Spectrum {
	Frequency_Index	*lowest_freq_index;
	Frequency_Index	*highest_freq_index;
	PrgT_List 		*frequency_slots_lptr; //list of occupied frequency slot
	//short			*map;
};




Spectrum* spectrum_create(Frequency_Index const* const lowest_fi, Frequency_Index const* const highest_fi) {
	Spectrum* spectrum_ptr;
	FIN( spectrum_create(lowest_fi,highest_fi) );
	if (frequency_index_hertz_get(highest_fi) <= frequency_index_hertz_get(lowest_fi))
		FRET(PRGC_NIL);
	spectrum_ptr = prg_mem_alloc(sizeof(Spectrum));
	spectrum_ptr->lowest_freq_index = frequency_index_copy(lowest_fi);
	spectrum_ptr->highest_freq_index = frequency_index_copy(highest_fi);
	spectrum_ptr->frequency_slots_lptr = prg_list_create();
	FRET(spectrum_ptr);
}

static void frequency_slot_list_destroy(PrgT_List *frequency_slots_lptr) {
	FIN( frequency_slot_list_destroy(frequency_slots_lptr) );
	prg_list_clear(frequency_slots_lptr, frequency_slot_destroy);
	prg_mem_free(frequency_slots_lptr);
	FOUT;
}


void spectrum_destroy(Spectrum* spectrum_ptr) {
	FIN( spectrum_destroy(Spectrum* spectrum_ptr) );
	spectrum_ptr->lowest_freq_index;
	spectrum_ptr->highest_freq_index;
	frequency_slot_list_destroy( spectrum_ptr->frequency_slots_lptr );
	prg_mem_free(spectrum_ptr);
	FOUT;
}



static int spectrum_frequency_slot_list_compare(const Frequency_Slot* slot1_ptr, const Frequency_Slot* slot2_ptr) {
	FIN(spectrum_frequency_slot_list_compare);
	switch (frequency_slot_compare(slot1_ptr,slot2_ptr)) {
		case SLOTC_HIGHER:
			//slot_1 is higer than slot_2
			//thus it should stay closer to the list tail
			//than slot_2
			FRET(-1);
			break;
		case SLOTC_LOWER:
			FRET(1);
			break;
		default :
			op_sim_end("spectrum.c","spectrum_frequency_slot_list_compare","two spectrum slots intersects","");
			FRET(0);
			break;
	}
}



static void spectrum_frequency_slot_insert(Spectrum* spectrum_ptr, Frequency_Slot *slot_ptr) {
	FIN( spectrum_frequency_slot_insert(spectrum_ptr,slot_ptr) );
	prg_list_insert_sorted(spectrum_ptr->frequency_slots_lptr, slot_ptr, spectrum_frequency_slot_list_compare);
	FOUT;
}



void spectrum_union(Spectrum * spectrum_a_ptr, Spectrum const* const spectrum_b_ptr) {
/**
	The union of two spectrum, takes the union of occupied frequency slots
	and extends the spectrum to the least stringent bounds. The returned spectrum is spectrum_a.
**/
	Frequency_Slot	*slot_b,*slot_a,*next_slot_a;
	PrgT_List_Cell	*cell_b,*cell_a,*next_cell_a,*app_cell;
	short comparison;
	Boolean breakloop;
	
	FIN( spectrum_union(spectrum_a_ptr,spectrum_b_ptr) );
	
	if (spectrum_a_ptr == PRGC_NIL || spectrum_b_ptr == PRGC_NIL) {
		op_sim_end("spectrum.c","spectrum_union","Passed NIL pointers instead of pointer to spectrum","");
		FOUT;
	}
	
	if ( OPC_FALSE == frequency_index_is_equal(spectrum_b_ptr->lowest_freq_index,spectrum_a_ptr->lowest_freq_index) ) {
		if ( OPC_TRUE == frequency_index_is_lower(spectrum_b_ptr->lowest_freq_index,spectrum_a_ptr->lowest_freq_index) ) {
			slot_a = frequency_slot_create(frequency_index_copy(spectrum_b_ptr->lowest_freq_index), spectrum_a_ptr->lowest_freq_index);
			spectrum_frequency_slot_insert(spectrum_a_ptr,slot_a);
			spectrum_a_ptr->lowest_freq_index = frequency_index_copy(spectrum_b_ptr->lowest_freq_index);
		}
		else if ( OPC_TRUE == frequency_index_is_lower(spectrum_a_ptr->lowest_freq_index,spectrum_b_ptr->lowest_freq_index) ) {
			slot_b = frequency_slot_create(frequency_index_copy(spectrum_a_ptr->lowest_freq_index), frequency_index_copy(spectrum_b_ptr->lowest_freq_index));
			slot_a = prg_list_access(spectrum_a_ptr->frequency_slots_lptr,PRGC_LISTPOS_HEAD);
			breakloop = OPC_FALSE;
			while ( PRGC_NIL != slot_a) {
				switch ( frequency_slot_compare(slot_b,slot_a) ) {
					case SLOTC_INCLUDE:
						prg_list_remove (spectrum_a_ptr->frequency_slots_lptr, PRGC_LISTPOS_HEAD);
						frequency_slot_destroy(slot_a);
						slot_a = prg_list_access(spectrum_a_ptr->frequency_slots_lptr,PRGC_LISTPOS_HEAD);
						break;
					case SLOTC_LOWER_INTERSECT:
					case SLOTC_LOWER_ADJACENT:
						frequency_slot_lowest_frequency_set( slot_a, frequency_index_copy( frequency_slot_lowest_frequency_access(slot_b) ) );
						frequency_slot_destroy(slot_b);
						slot_b = PRGC_NIL;
						breakloop = OPC_TRUE;
						break;
					case SLOTC_IS_INCLUDED:
						frequency_slot_destroy(slot_b);
						slot_b = PRGC_NIL;
						breakloop = OPC_TRUE;
						break;
				}
				if (OPC_TRUE == breakloop) break;
			}
			if ( PRGC_NIL != slot_b)
				spectrum_frequency_slot_insert(spectrum_a_ptr,slot_b);
		}
	}
	
	if ( OPC_FALSE == frequency_index_is_equal(spectrum_b_ptr->highest_freq_index,spectrum_a_ptr->highest_freq_index) ) {
		if ( OPC_TRUE == frequency_index_is_higher(spectrum_b_ptr->highest_freq_index,spectrum_a_ptr->highest_freq_index) ) {
			slot_a = frequency_slot_create(spectrum_a_ptr->highest_freq_index, frequency_index_copy(spectrum_b_ptr->highest_freq_index));
			spectrum_frequency_slot_insert(spectrum_a_ptr,slot_a);
			spectrum_a_ptr->highest_freq_index = frequency_index_copy(spectrum_b_ptr->highest_freq_index);
		}
		else if ( OPC_TRUE == frequency_index_is_higher(spectrum_a_ptr->highest_freq_index,spectrum_b_ptr->highest_freq_index) ) {
			slot_b = frequency_slot_create(frequency_index_copy(spectrum_b_ptr->highest_freq_index), frequency_index_copy(spectrum_a_ptr->highest_freq_index));
			slot_a = prg_list_access(spectrum_a_ptr->frequency_slots_lptr,PRGC_LISTPOS_TAIL);
			breakloop = OPC_FALSE;
			while ( PRGC_NIL != slot_a) {
				switch ( frequency_slot_compare(slot_b,slot_a) ) {
					case SLOTC_INCLUDE:
						prg_list_remove (spectrum_a_ptr->frequency_slots_lptr, PRGC_LISTPOS_TAIL);
						frequency_slot_destroy(slot_a);
						slot_a = prg_list_access(spectrum_a_ptr->frequency_slots_lptr,PRGC_LISTPOS_TAIL);
						break;
					case SLOTC_HIGHER_INTERSECT:
					case SLOTC_HIGHER_ADJACENT:
						frequency_slot_highest_frequency_set( slot_a, frequency_index_copy( frequency_slot_highest_frequency_access(slot_b) ) );
						frequency_slot_destroy(slot_b);
						slot_b = PRGC_NIL;
						breakloop = OPC_TRUE;
						break;
					case SLOTC_IS_INCLUDED:
						frequency_slot_destroy(slot_b);
						slot_b = PRGC_NIL;
						breakloop = OPC_TRUE;
						break;
				}
				if (OPC_TRUE == breakloop) break;
			}
			if ( PRGC_NIL != slot_b)
				spectrum_frequency_slot_insert(spectrum_a_ptr,slot_b);
		}
	}
	
	cell_a = prg_list_head_cell_get(spectrum_a_ptr->frequency_slots_lptr);	
	cell_b = prg_list_head_cell_get(spectrum_b_ptr->frequency_slots_lptr);
	while (PRGC_NIL != cell_b) {
		slot_b = prg_list_cell_data_get(cell_b);
		if (PRGC_NIL == cell_a) {
			spectrum_frequency_slot_insert(spectrum_a_ptr,frequency_slot_copy(slot_b));
		}
		else {
			slot_a = prg_list_cell_data_get(cell_a);
			
			switch (frequency_slot_compare(slot_a,slot_b)) {
				case SLOTC_HIGHER:
					//slot_a is higer than slot_b
					spectrum_frequency_slot_insert(spectrum_a_ptr,frequency_slot_copy(slot_b));
					cell_b = prg_list_cell_next_get(cell_b);
					break;
				case SLOTC_LOWER:
					//slot_a is lower than slot_b
					cell_a = prg_list_cell_next_get(cell_a);
					break;
				case SLOTC_INCLUDE:
					//slot_a include slot_b
				case SLOTC_EQUAL:
					//slot_a is equal to slot_b
					cell_b = prg_list_cell_next_get(cell_b);
					break;
				case SLOTC_IS_INCLUDED:
					//slot_a is included in slot_b
				case SLOTC_LOWER_ADJACENT:
					//slot_a is lower adjacent to slot_b (e.g. [-5,-1] and [-1,7])
				case SLOTC_HIGHER_ADJACENT:
					//slot_a is higher adjacent to slot_b (e.g. [-5,-1] and [-10,-5])
				case SLOTC_HIGHER_INTERSECT:
				case SLOTC_LOWER_INTERSECT:
					//slot_a intersects with slot_b
					frequency_slot_union(slot_a,slot_b);
					cell_b = prg_list_cell_next_get(cell_b);
					
					//check if slot_a now overlaps with successive slots
					next_cell_a = prg_list_cell_next_get(cell_a);
					if (PRGC_NIL == next_cell_a) break;
					next_slot_a = prg_list_cell_data_get(next_cell_a);
					comparison = frequency_slot_compare(slot_a,next_slot_a);
					while ( SLOTC_INCLUDE == comparison || 
							SLOTC_LOWER_ADJACENT == comparison || 
							SLOTC_LOWER_INTERSECT == comparison ) {
						if (SLOTC_INCLUDE == comparison) {
							app_cell = next_cell_a;
							next_cell_a = prg_list_cell_next_get(next_cell_a);
							prg_list_cell_remove (spectrum_a_ptr->frequency_slots_lptr, app_cell);
							frequency_slot_destroy(next_slot_a);
							if (PRGC_NIL == next_cell_a) break;
							next_slot_a = prg_list_cell_data_get(next_cell_a);
							comparison = frequency_slot_compare(slot_a,next_slot_a);
						}
						else {
							frequency_slot_union(slot_a,next_slot_a);
							prg_list_cell_remove (spectrum_a_ptr->frequency_slots_lptr, next_cell_a);
							frequency_slot_destroy(next_slot_a);
							break;
						}
					}
					break;
			}
		}
	}
	
	/*
	// shrink the spectrum according to the higher and lower bounds
	slot_a = prg_list_access(spectrum_a_ptr->frequency_slots_lptr,PRGC_LISTPOS_HEAD);
	breakloop = OPC_FALSE;
	while ( PRGC_NIL != slot_a) {
		switch (frequency_slot_frequency_index_compare(slot_a,spectrum_a_ptr->lowest_freq_index)) {
			case SLOTC_LOWER:
				//the slot is at lower frequencies
			case SLOTC_LOWER_ADJACENT:
				//the slot is adjacent
				prg_list_remove (spectrum_a_ptr->frequency_slots_lptr, PRGC_LISTPOS_HEAD);
				frequency_slot_destroy(slot_a);
				break;
			case SLOTC_INCLUDE:
				//the slot includes the frequency index
				frequency_slot_lowest_frequency_set(slot_a,frequency_index_copy(spectrum_a_ptr->lowest_freq_index));
				breakloop = OPC_TRUE;
				break;
			default:
				breakloop = OPC_TRUE;
				break;
		}
		if (OPC_TRUE == breakloop) break;
		slot_a = prg_list_access(spectrum_a_ptr->frequency_slots_lptr,PRGC_LISTPOS_HEAD);
	}
	slot_a = prg_list_access(spectrum_a_ptr->frequency_slots_lptr,PRGC_LISTPOS_TAIL);
	breakloop = OPC_FALSE;
	while ( PRGC_NIL != slot_a) {
		switch (frequency_slot_frequency_index_compare(slot_a,spectrum_a_ptr->highest_freq_index)) {
			case SLOTC_HIGHER:
				//the slot is at higher frequencies
			case SLOTC_HIGHER_ADJACENT:
				//the slot is adjacent
				prg_list_remove (spectrum_a_ptr->frequency_slots_lptr, PRGC_LISTPOS_TAIL);
				frequency_slot_destroy(slot_a);
				break;
			case SLOTC_INCLUDE:
				//the slot includes the frequency index
				frequency_slot_highest_frequency_set(slot_a,frequency_index_copy(spectrum_a_ptr->highest_freq_index));
				breakloop = OPC_TRUE;
				break;
			default:
				breakloop = OPC_TRUE;
				break;
		}
		if (OPC_TRUE == breakloop) break;
		slot_a = prg_list_access(spectrum_a_ptr->frequency_slots_lptr,PRGC_LISTPOS_TAIL);
	}*/
	
	FOUT;
}




Spectrum* spectrum_inversion(Spectrum const* const spectrum_ptr) {
/**
	Returns a inverted spectrum (busy slots becomes free slots).
	e.g. [-5,-2][2,3][3,4][5,10] -> [-10,-5][-2,2][4,5].
**/
	Spectrum			  * inverted_spectrum_ptr;
	Frequency_Index	const * prev_freq_index;
	Frequency_Index	const * freq_index;
	Frequency_Slot	const * slot_ptr;
	Frequency_Slot		  * new_slot_ptr;
	PrgT_List_Cell		  * cell;
	
	FIN( spectrum_inversion(spectrum_ptr) );
	if (spectrum_ptr == PRGC_NIL) FRET(PRGC_NIL);
	inverted_spectrum_ptr = spectrum_create(spectrum_ptr->lowest_freq_index,spectrum_ptr->highest_freq_index);
	prev_freq_index = spectrum_ptr->lowest_freq_index;
	cell = prg_list_head_cell_get(spectrum_ptr->frequency_slots_lptr);
	while (PRGC_NIL != cell) {
		slot_ptr = prg_list_cell_data_get(cell);
		freq_index = frequency_slot_lowest_frequency_access(slot_ptr);
		if ( PRGC_TRUE == frequency_index_is_higher(freq_index,prev_freq_index) ) {
			new_slot_ptr = frequency_slot_create( frequency_index_copy(prev_freq_index), frequency_index_copy(freq_index));
			prg_list_insert(inverted_spectrum_ptr->frequency_slots_lptr, new_slot_ptr, PRGC_LISTPOS_TAIL);
		}
		prev_freq_index = frequency_slot_highest_frequency_access(slot_ptr);
		cell = prg_list_cell_next_get(cell);
	}
	if ( PRGC_TRUE == frequency_index_is_higher(spectrum_ptr->highest_freq_index, prev_freq_index) ) {
		new_slot_ptr = frequency_slot_create( frequency_index_copy(prev_freq_index), frequency_index_copy(spectrum_ptr->highest_freq_index));
		prg_list_insert(inverted_spectrum_ptr->frequency_slots_lptr, new_slot_ptr, PRGC_LISTPOS_TAIL);
	}
	
	prg_list_sorted_set(inverted_spectrum_ptr->frequency_slots_lptr);
	
	FRET(inverted_spectrum_ptr);
}





void spectrum_intersection(Spectrum *spectrum_a_ptr,Spectrum const* const spectrum_b_ptr) {
/**
	Intersect two spectrum. Meaning that available portion of spectrum will
	be reflected to the resulting spectrum.
**/
	Spectrum	*inverse_spectrum_a_ptr, *inverse_spectrum_b_ptr;
	
	FIN( spectrum_intersection(spectrum_a_ptr, spectrum_b_ptr) );
	inverse_spectrum_a_ptr = spectrum_inversion(spectrum_a_ptr);
	inverse_spectrum_b_ptr = spectrum_inversion(spectrum_b_ptr);
	spectrum_destroy(spectrum_a_ptr);
	spectrum_union(inverse_spectrum_a_ptr,inverse_spectrum_b_ptr);
	spectrum_a_ptr = spectrum_inversion(inverse_spectrum_a_ptr);
	spectrum_destroy(inverse_spectrum_a_ptr);
	spectrum_destroy(inverse_spectrum_b_ptr);
	FOUT;
}



Boolean spectrum_frequency_slot_is_available(Spectrum const* const spectrum_ptr, Frequency_Slot const * const slot_ptr) {
	Frequency_Slot	const *read_slot_ptr;
	PrgT_List_Cell	*cell;
	short comp;
	
	FIN( spectrum_frequency_slot_is_available(spectrum_ptr, slot_ptr) );
	cell = prg_list_head_cell_get(spectrum_ptr->frequency_slots_lptr);
	while (PRGC_NIL != cell) {
		read_slot_ptr = prg_list_cell_data_get(cell);
		comp = frequency_slot_compare(slot_ptr,read_slot_ptr);
		if (comp == SLOTC_LOWER || comp == SLOTC_LOWER_ADJACENT) {
			cell = prg_list_cell_next_get(cell);
			continue;
		}
		else if (comp == SLOTC_HIGHER || comp == SLOTC_HIGHER_ADJACENT) {
			FRET(OPC_TRUE);
		}
		else {
			FRET(OPC_FALSE);
		}
	}
	FRET(OPC_TRUE);
}



short spectrum_frequency_slot_allocate(Spectrum *spectrum_ptr, Frequency_Slot const * const slot_ptr) {
	FIN( spectrum_frequency_slot_allocate(spectrum_ptr, slot_ptr) );
	if ( OPC_TRUE == spectrum_frequency_slot_is_available(spectrum_ptr,slot_ptr) ) {
		spectrum_frequency_slot_insert(spectrum_ptr,frequency_slot_copy(slot_ptr));
		FRET(SPECTRUMC_SUCCESS);
	}
	else {
		FRET(SPECTRUMC_SLOT_BUSY);
	}
}



short spectrum_frequency_slot_exact_release(Spectrum *spectrum_ptr, Frequency_Slot const * const slot_ptr) {
/**
	Release a frequency slot from the spectrum
	only if it is found with the exact boundaries 
**/
	Frequency_Slot	*read_slot_ptr;
	PrgT_List_Cell	*cell;
	short comp;
	
	FIN( spectrum_frequency_slot_exact_release(spectrum_ptr, slot_ptr) );
	cell = prg_list_head_cell_get(spectrum_ptr->frequency_slots_lptr);
	while (PRGC_NIL != cell) {
		read_slot_ptr = prg_list_cell_data_get(cell);
		comp = frequency_slot_compare(slot_ptr,read_slot_ptr);
		if (comp == SLOTC_EQUAL) {
			prg_list_cell_remove (spectrum_ptr->frequency_slots_lptr, cell);
			frequency_slot_destroy(read_slot_ptr);
			FRET(SPECTRUMC_SUCCESS);
		}
		else if (comp == SLOTC_LOWER || comp == SLOTC_LOWER_ADJACENT) {
			cell = prg_list_cell_next_get(cell);
			continue;
		}
		else {
			FRET(SPECTRUMC_FAIL);
		}
	}
	FRET(SPECTRUMC_FAIL);
}



short spectrum_frequency_slot_release(Spectrum *spectrum_ptr, Frequency_Slot const * const slot_ptr) {
/**
	Release a frequency slot from the spectrum
	resizing other busy slots if needed
**/
	Frequency_Slot	*read_slot_ptr;
	PrgT_List_Cell	*cell;
	short comp;
	
	FIN( spectrum_frequency_slot_release(spectrum_ptr, slot_ptr) );
	cell = prg_list_head_cell_get(spectrum_ptr->frequency_slots_lptr);
	while (PRGC_NIL != cell) {
		read_slot_ptr = prg_list_cell_data_get(cell);
		comp = frequency_slot_compare(slot_ptr,read_slot_ptr);
		if (comp == SLOTC_EQUAL) {
			prg_list_cell_remove (spectrum_ptr->frequency_slots_lptr, cell);
			frequency_slot_destroy(read_slot_ptr);
			FRET(SPECTRUMC_SUCCESS);
		}
		else if (comp == SLOTC_LOWER || comp == SLOTC_LOWER_ADJACENT) {
			cell = prg_list_cell_next_get(cell);
			continue;
		}
		else if (comp == SLOTC_LOWER_INTERSECT || comp == SLOTC_HIGHER_INTERSECT) {
			//subtract the intersection
			frequency_slot_subtract(read_slot_ptr,slot_ptr);
		}
		else if (comp == SLOTC_HIGHER || comp == SLOTC_HIGHER_ADJACENT) {
			FRET(SPECTRUMC_SUCCESS);
		}
	}
	FRET(SPECTRUMC_SUCCESS);
}



double spectrum_available_bandwidth_get(Spectrum const* const spectrum_ptr) {
	Frequency_Slot	*read_slot_ptr;
	PrgT_List_Cell	*cell;
	double 			total_available_bandwidth;
	
	FIN( spectrum_available_bandwidth_get(Spectrum *spectrum_ptr) );
	total_available_bandwidth = frequency_index_hertz_get(spectrum_ptr->highest_freq_index) - frequency_index_hertz_get(spectrum_ptr->lowest_freq_index); 
	cell = prg_list_head_cell_get(spectrum_ptr->frequency_slots_lptr);
	while (PRGC_NIL != cell) {
		read_slot_ptr = prg_list_cell_data_get(cell);
		total_available_bandwidth -= frequency_slot_bandwidth(read_slot_ptr);
		cell = prg_list_cell_next_get(cell);
	}
	if (total_available_bandwidth < 0) 
		op_sim_end("spectrum.c","spectrum_available_bandwidth_get","negative available bandwidth","");
	FRET(total_available_bandwidth);
}



static PrgT_List* spectrum_available_frequency_slots_get(Spectrum const* const spectrum_ptr) {
	PrgT_List		*available_slots_lptr;
	Frequency_Slot	*slot_ptr,*new_slot_ptr;
	Frequency_Index	const*	prev_freq_index;
	PrgT_List_Cell	*cell;
	short comp;
	
	FIN( spectrum_available_frequency_slots_get(spectrum_ptr) );
	
	available_slots_lptr = prg_list_create();
	cell = prg_list_head_cell_get(spectrum_ptr->frequency_slots_lptr);
	if (PRGC_NIL == cell) {
		new_slot_ptr = frequency_slot_create(frequency_index_copy(spectrum_ptr->lowest_freq_index), frequency_index_copy(spectrum_ptr->highest_freq_index));
		prg_list_insert(available_slots_lptr, new_slot_ptr, PRGC_LISTPOS_HEAD);
		prg_list_sorted_set(available_slots_lptr);
		FRET(available_slots_lptr);
	}
	else {
		slot_ptr = prg_list_cell_data_get(cell);
		comp = frequency_slot_frequency_index_compare(slot_ptr,spectrum_ptr->lowest_freq_index);
		if ( SLOTC_LOWER == comp ) {
			new_slot_ptr = frequency_slot_create(frequency_index_copy(spectrum_ptr->lowest_freq_index), frequency_index_copy(frequency_slot_lowest_frequency_access(slot_ptr)));
			prg_list_insert(available_slots_lptr, new_slot_ptr, PRGC_LISTPOS_HEAD);
			prg_list_sorted_set(available_slots_lptr);
		}
		prev_freq_index = frequency_slot_highest_frequency_access(slot_ptr);
	}
	cell = prg_list_cell_next_get(cell);
	while ( PRGC_NIL != cell ) {
		slot_ptr = prg_list_cell_data_get(cell);
		if ( frequency_index_is_higher(frequency_slot_lowest_frequency_access(slot_ptr), prev_freq_index) ) {
			new_slot_ptr = frequency_slot_create(frequency_index_copy(prev_freq_index), frequency_index_copy(frequency_slot_lowest_frequency_access(slot_ptr)));
			prg_list_insert(available_slots_lptr, new_slot_ptr, PRGC_LISTPOS_TAIL);
			prg_list_sorted_set(available_slots_lptr);
		}
		prev_freq_index = frequency_slot_highest_frequency_access(slot_ptr);
		cell = prg_list_cell_next_get(cell);
	}
	
	if ( frequency_index_is_higher(spectrum_ptr->highest_freq_index, prev_freq_index) ) {
			new_slot_ptr = frequency_slot_create(frequency_index_copy(prev_freq_index), frequency_index_copy(spectrum_ptr->highest_freq_index));
			prg_list_insert(available_slots_lptr, new_slot_ptr, PRGC_LISTPOS_TAIL);
			prg_list_sorted_set(available_slots_lptr);
	}
	
	FRET(available_slots_lptr);
}



PrgT_List* spectrum_bandwidth_allocable_indexes_get(Spectrum const* const spectrum_ptr, double bandwidth_demand, Frequency_Index const* const fi_sample_ptr) {
/**
	Gets: A spectrum pointer, the bandwidth demand (in GHz), a sample of frequency index
	Returns a list of Frequency indexes with the caracteristics
	of the frequency index sample passed as parameter.
	Those indexes are available to be used as central frequency
	to accomodate the bandwidth demand
**/
	PrgT_List		*available_slots_lptr,*indexes_lptr,*temp_indexes_lptr;
	Frequency_Slot	*slot_ptr;
	PrgT_List_Cell	*cell;
	
	FIN( spectrum_bandwidth_allocable_indexes_get(spectrum_ptr, bandwidth_demand, fi_sample_ptr) );
	
	if ( PRGC_NIL == spectrum_ptr ) FRET(PRGC_NIL);
	available_slots_lptr = spectrum_available_frequency_slots_get(spectrum_ptr);
	
	indexes_lptr = prg_list_create();
	
	cell = prg_list_head_cell_get(available_slots_lptr);
	while ( PRGC_NIL != cell) {
		slot_ptr = prg_list_cell_data_get(cell);
		temp_indexes_lptr = frequency_slot_bandwidth_allocable_indexes_get(slot_ptr,bandwidth_demand,fi_sample_ptr);
		if ( prg_list_join(indexes_lptr, temp_indexes_lptr) == PrgC_Compcode_Failure ) {
			op_sim_end("spectrum.c","spectrum_bandwidth_allocable_indexes_get","list error","");
		}
		prg_list_sorted_set(indexes_lptr);
		prg_mem_free(temp_indexes_lptr);
		cell = prg_list_cell_next_get(cell);
	}
	
	frequency_slot_list_destroy( available_slots_lptr );
	FRET(indexes_lptr);
}



int spectrum_bandwidth_allocable_indexes_number_get(Spectrum const* const spectrum_ptr, double bandwidth_demand, Frequency_Index const* const fi_sample_ptr, Boolean non_overlapping) {
/**
	Gets: A spectrum pointer, the bandwidth demand (in GHz), a sample of frequency index
	Returns the number of frequency indexes that can be used to accomodate the demand.
	If non_overlapping is TRUE count only the non overlapping indexes
**/
	PrgT_List		*available_slots_lptr;
	Frequency_Slot	*slot_ptr;
	PrgT_List_Cell	*cell;
	int				num_indexes,tot_num_indexes;
	
	FIN( spectrum_bandwidth_allocable_indexes_number_get(spectrum_ptr, bandwidth_demand, fi_sample_ptr,non_overlapping) );
	
	if ( PRGC_NIL == spectrum_ptr ) FRET(-1);
	available_slots_lptr = spectrum_available_frequency_slots_get(spectrum_ptr);
	
	cell = prg_list_head_cell_get(available_slots_lptr);
	tot_num_indexes = 0;
	while ( PRGC_NIL != cell) {
		slot_ptr = prg_list_cell_data_get(cell);
		num_indexes = frequency_slot_bandwidth_allocable_indexes_number_get(slot_ptr,bandwidth_demand,fi_sample_ptr,non_overlapping);
		if (num_indexes > 0) {
			tot_num_indexes += num_indexes;
		}
		cell = prg_list_cell_next_get(cell);
	}
	
	frequency_slot_list_destroy( available_slots_lptr );
	FRET(tot_num_indexes);
}


void spectrum_print(Spectrum const* const spectrum_ptr) {
/**
	Print the spectrum in terms of frequency slots ( eg. [-1,7][12,15][34,40] ).
**/
	Frequency_Slot	*slot_ptr;
	PrgT_List_Cell	*cell;
	
	FIN( spectrum_print(spectrum_ptr) );
	cell = prg_list_head_cell_get(spectrum_ptr->frequency_slots_lptr);
	while (PRGC_NIL != cell) {
		slot_ptr = prg_list_cell_data_get(cell);
		frequency_slot_print(slot_ptr);
		cell = prg_list_cell_next_get(cell);
	}
	FOUT;
}


//----------------------------------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------------------------//


#include "MD_spectrum_assignment_src.h"
#define MAP_FREQ_INDEX_FREE 0
#define MAP_FREQ_INDEX_BUSY 1
#define MAP_FREQ_INDEX_IS_FREE(index) (MAP_FREQ_INDEX_FREE == index)
#define MAP_FREQ_INDEX_IS_BUSY(index) (MAP_FREQ_INDEX_BUSY == index)

// A spectrum Map is basically an array of 0 or 1. Each element of the array represent
// a frequency index of the spectrum. Frequency indexes are spaced by 6.25GHz according to the flexgrid.
// A 0 means that the frequency index is free
//	 1 means that the frequency index is occupied (it could also correspond to the end of a frequency slot)
struct Spectrum_Map {
	int           *map; //a map (array of int) of the spectrum
    int           lower_frequency_index; //the frequency index corresponding to the lower bound
    int           size; //the size of the spectrum map array
};






Spectrum_Map* spectrum_map_create(int lower_frequency_index, int upper_frequency_index) {
	// This function creates a spectrum map from the lower frequency index
	// to the upper frequency index passed as input (extremes included).
	// The map is initialized to 0 (done automatically by prg_mem_alloc)
	Spectrum_Map* spectrum_map_ptr;
	
	FIN( spectrum_map_create(lower_frequency_index,upper_frequency_index) );
	if (upper_frequency_index < lower_frequency_index)
		FRET(PRGC_NIL);
	spectrum_map_ptr = prg_mem_alloc(sizeof(Spectrum_Map));
	spectrum_map_ptr->lower_frequency_index = lower_frequency_index;
	spectrum_map_ptr->size = upper_frequency_index-lower_frequency_index+1;
	spectrum_map_ptr->map = prg_mem_alloc(spectrum_map_ptr->size*sizeof(int));
	FRET(spectrum_map_ptr);
}



Spectrum_Map* spectrum_map_copy(Spectrum_Map const* spectrum_map_ptr) {
	Spectrum_Map* spectrum_map_cpy_ptr;
	int i;
	
	FIN( spectrum_map_copy(spectrum_map_ptr) );
	if (spectrum_map_ptr == PRGC_NIL) FRET(PRGC_NIL);
	spectrum_map_cpy_ptr = prg_mem_alloc(sizeof(Spectrum_Map));
	spectrum_map_cpy_ptr->lower_frequency_index = spectrum_map_ptr->lower_frequency_index;
	spectrum_map_cpy_ptr->size = spectrum_map_ptr->size;
	spectrum_map_cpy_ptr->map = prg_mem_alloc(spectrum_map_cpy_ptr->size*sizeof(int));
	for (i=0; i<spectrum_map_cpy_ptr->size; ++i) {
		spectrum_map_cpy_ptr->map[i] = spectrum_map_ptr->map[i];
	}
	FRET(spectrum_map_cpy_ptr);
}



void spectrum_map_destroy(Spectrum_Map* spectrum_map_ptr) {
	FIN( spectrum_map_destroy(spectrum_map_ptr) );
	if (PRGC_NIL == spectrum_map_ptr) FOUT;
	if (PRGC_NIL != spectrum_map_ptr->map)
		prg_mem_free(spectrum_map_ptr->map);
	prg_mem_free(spectrum_map_ptr);
	FOUT;
}



int spectrum_map_lower_frequency_index_get(Spectrum_Map const* spectrum_map_ptr) {
	FIN( spectrum_map_lower_frequency_index_get(spectrum_map_ptr) );
	FRET(spectrum_map_ptr->lower_frequency_index);
}



int spectrum_map_upper_frequency_index_get(Spectrum_Map const* spectrum_map_ptr) {
	FIN( spectrum_map_upper_frequency_index_get(spectrum_map_ptr) );
	FRET(spectrum_map_ptr->lower_frequency_index + spectrum_map_ptr->size-1);
}


void spectrum_map_fully_busy_set(Spectrum_Map* spectrum_map_ptr) {
	int i;
	FIN( spectrum_map_fully_busy_set(spectrum_map_ptr) );
	for (i=0; i<spectrum_map_ptr->size; ++i) {
		spectrum_map_ptr->map[i] = MAP_FREQ_INDEX_BUSY;
	}
	FOUT;
}



static int frequency_index_to_map_index(Spectrum_Map const* spectrum_map_ptr, int freq_index) {
	// returns the equivalent index in the spectrum map array
	// for the given frequency index.
	// Or a Negative number in case the freq. index is not in the map
	FIN( frequency_index_to_map_index(spectrum_map_ptr,freq_index) );
	if (spectrum_map_ptr == PRGC_NIL) FRET(-1);
	if (freq_index <= spectrum_map_upper_frequency_index_get(spectrum_map_ptr) && 
		freq_index >= spectrum_map_ptr->lower_frequency_index) {
		FRET(freq_index - spectrum_map_ptr->lower_frequency_index);
	}
	FRET(-1);
}


static MD_Compcode map_index_to_frequency_index(Spectrum_Map const* spectrum_map_ptr, int map_index, int* freq_index_ptr) {
	FIN( map_index_to_frequency_index(spectrum_map_ptr,map_index,freq_index_ptr) );
	if (map_index < 0 || map_index >= spectrum_map_ptr->size) 
		FRET(MD_Compcode_Failure);
	*freq_index_ptr = spectrum_map_ptr->lower_frequency_index+map_index;
	FRET(MD_Compcode_Success);
}


MD_Compcode spectrum_map_free_set(Spectrum_Map* spectrum_map_ptr,int freq_index) {
	int map_index;
	FIN( spectrum_map_free_set(spectrum_map_ptr,freq_index) );
	if (spectrum_map_ptr == PRGC_NIL) FRET(MD_Compcode_Failure);
	map_index = frequency_index_to_map_index(spectrum_map_ptr,freq_index);
	if (map_index>=0) {
		spectrum_map_ptr->map[map_index] = MAP_FREQ_INDEX_FREE;
		FRET(MD_Compcode_Success);
	}
	FRET(MD_Compcode_Failure);
}


MD_Compcode spectrum_map_busy_set(Spectrum_Map* spectrum_map_ptr,int freq_index) {
	int map_index;
	FIN( spectrum_map_busy_set(spectrum_map_ptr,freq_index) );
	if (spectrum_map_ptr == PRGC_NIL) FRET(MD_Compcode_Failure);
	map_index = frequency_index_to_map_index(spectrum_map_ptr,freq_index);
	if (map_index>=0) {
		spectrum_map_ptr->map[map_index] = MAP_FREQ_INDEX_BUSY;
		FRET(MD_Compcode_Success);
	}
	FRET(MD_Compcode_Failure);
}



MD_Compcode spectrum_map_busy_range_set(Spectrum_Map* spectrum_map_ptr,
																				int lower_freq_index,
																				int higher_freq_index) {
	// This function set as busy (1) all the range of frequencies 
	// between lower_freq_index and higher_freq_index (extremes included)
	int lower_index,higher_index, lower_map_findex, upper_map_findex;
	FIN( spectrum_map_busy_range_set(spectrum_map_ptr,lower_freq_index,higher_freq_index ) );

	// get the extremes of the spectrum map
	lower_map_findex = spectrum_map_lower_frequency_index_get(spectrum_map_ptr);
	upper_map_findex = spectrum_map_upper_frequency_index_get(spectrum_map_ptr);

	// Get the intersection of the range and the spectrum map
	lower_freq_index = MAX(lower_freq_index, lower_map_findex);
	higher_freq_index = MIN(higher_freq_index, upper_map_findex);

	// if no intersection, return
	if (lower_freq_index > higher_freq_index) 
		FRET(MD_Compcode_Failure);

	lower_index = frequency_index_to_map_index(spectrum_map_ptr,lower_freq_index);
	higher_index = frequency_index_to_map_index(spectrum_map_ptr,higher_freq_index);

	if (lower_index >= 0 && higher_index>=0) {
		for (;lower_index<=higher_index; ++lower_index) {
			spectrum_map_ptr->map[lower_index] = MAP_FREQ_INDEX_BUSY;
		}
		FRET(MD_Compcode_Success);
	}
	FRET(MD_Compcode_Failure);
}



MD_Compcode spectrum_map_free_range_set(Spectrum_Map* spectrum_map_ptr,
																				int lower_freq_index,
																				int higher_freq_index) {
	// This function set as free (0) all the range of frequencies 
	// between lower_freq_index and higher_freq_index (extremes included)
	int lower_index, higher_index, lower_map_findex, upper_map_findex;
	FIN( spectrum_map_busy_range_set(spectrum_map_ptr,lower_freq_index,higher_freq_index ) );

	// get the extremes of the spectrum map
	lower_map_findex = spectrum_map_lower_frequency_index_get(spectrum_map_ptr);
	upper_map_findex = spectrum_map_upper_frequency_index_get(spectrum_map_ptr);

	// Get the intersection of the range and the spectrum map
	lower_freq_index = MAX(lower_freq_index, lower_map_findex);
	higher_freq_index = MIN(higher_freq_index, upper_map_findex);
	
	// if no intersection, return
	if (lower_freq_index > higher_freq_index) 
		FRET(MD_Compcode_Failure);

	lower_index = frequency_index_to_map_index(spectrum_map_ptr,lower_freq_index);
	higher_index = frequency_index_to_map_index(spectrum_map_ptr,higher_freq_index);
	if (lower_index < 0 || higher_index<0)
		FRET(MD_Compcode_Failure);
	for (;lower_index<=higher_index; ++lower_index) {
			spectrum_map_ptr->map[lower_index] = MAP_FREQ_INDEX_FREE;
	}
	FRET(MD_Compcode_Success);
}





MD_Compcode spectrum_maps_sum(Spectrum_Map** spectrum_map_a_pptr,
															Spectrum_Map* spectrum_map_b_ptr) {
	// Return the sum of the two spectrum into spectrum a.
	// e.g considering the spectrum a [0,0,0,1,0,1] from freq. index -2 to 3
	// 			   and the spectrum b       [1,1,1,0] from freq. index 1 to 4
	// the resulting spectrum will be [0,0,0,1,1,1,0] from freq. index -2 to 4.
	// If in another example spectrum b goes from 0 to 3, in that case the 
	// resulting spectrum would be [0,0] from -2 to -1.
	// If no intersection between the two spectrum spectrum_map_a_ptr becomes PRGC_NIL,
	// also if the spectrum is fully occupied
	
	Spectrum_Map* new_spectrum_map_ptr;
	int imap_a,imap_b,imap_new,i,lower_index,upper_index;
	
	FIN( spectrum_maps_sum( spectrum_map_a_ptr,spectrum_map_b_ptr) );
	if (PRGC_NIL == *spectrum_map_a_pptr) FRET(MD_Compcode_Success);
	if (PRGC_NIL == spectrum_map_b_ptr) {
		spectrum_map_destroy(*spectrum_map_a_pptr);
		*spectrum_map_a_pptr = PRGC_NIL;
		FRET(MD_Compcode_Success);
	}
	if ((*spectrum_map_a_pptr)->lower_frequency_index >
			spectrum_map_b_ptr->lower_frequency_index) {
		lower_index = (*spectrum_map_a_pptr)->lower_frequency_index;
	}
	else {
		lower_index = spectrum_map_b_ptr->lower_frequency_index;
	}
	if (spectrum_map_upper_frequency_index_get(*spectrum_map_a_pptr) < 
			spectrum_map_upper_frequency_index_get(spectrum_map_b_ptr)) {
		upper_index = spectrum_map_upper_frequency_index_get(*spectrum_map_a_pptr);
	}
	else {
		upper_index = spectrum_map_upper_frequency_index_get(spectrum_map_b_ptr);
	}
	if (lower_index > upper_index) {
		// no any intersection between the two spectrum
		spectrum_map_destroy(*spectrum_map_a_pptr);
		*spectrum_map_a_pptr = PRGC_NIL;
		FRET(MD_Compcode_Success);
	}
	
	// restrict the spectrum in case the bounds are occupied 
	imap_a = frequency_index_to_map_index(*spectrum_map_a_pptr,lower_index);
	imap_b = frequency_index_to_map_index(spectrum_map_b_ptr,lower_index);
	if (imap_a<0 || imap_b<0) op_sim_end("banane1","","","");
	for (i=lower_index; i<=upper_index; ++i) {
		if (MAP_FREQ_INDEX_IS_BUSY((*spectrum_map_a_pptr)->map[imap_a]) || 
				MAP_FREQ_INDEX_IS_BUSY(spectrum_map_b_ptr->map[imap_b]) ) {
			++lower_index;
			++imap_a;++imap_b;
		}
		else {
			break;
		}
	}
	imap_a = frequency_index_to_map_index((*spectrum_map_a_pptr),upper_index);
	imap_b = frequency_index_to_map_index(spectrum_map_b_ptr,upper_index);
	if (imap_a<0 || imap_b<0) op_sim_end("banane2","","","");
	for (i=upper_index; i>=lower_index; --i) {
		if (MAP_FREQ_INDEX_IS_BUSY((*spectrum_map_a_pptr)->map[imap_a]) || 
				MAP_FREQ_INDEX_IS_BUSY(spectrum_map_b_ptr->map[imap_b]) ) {
			--upper_index;
			--imap_a;--imap_b;
		}
		else {
			break;
		}
	}
	
	if (lower_index > upper_index) {
		//spectrum fully occupied
		spectrum_map_destroy(*spectrum_map_a_pptr);
		*spectrum_map_a_pptr = PRGC_NIL;
		FRET(MD_Compcode_Success);
	}
	
	new_spectrum_map_ptr = spectrum_map_create(lower_index,upper_index);
	imap_a = frequency_index_to_map_index(*spectrum_map_a_pptr,lower_index);
	imap_b = frequency_index_to_map_index(spectrum_map_b_ptr,lower_index);
	imap_new = frequency_index_to_map_index(new_spectrum_map_ptr,lower_index);
	if (imap_a<0 || imap_b<0 || imap_new<0) op_sim_end("banane3","","","");
	
	for (i=lower_index; i<=upper_index; ++i) {
		if (MAP_FREQ_INDEX_IS_BUSY( (*spectrum_map_a_pptr)->map[imap_a] ) || 
				MAP_FREQ_INDEX_IS_BUSY( spectrum_map_b_ptr->map[imap_b]) ) {
			new_spectrum_map_ptr->map[imap_new] = MAP_FREQ_INDEX_BUSY;
		}
		++imap_a;++imap_b;
		++imap_new;
	}
	spectrum_map_destroy(*spectrum_map_a_pptr);
	*spectrum_map_a_pptr = new_spectrum_map_ptr;
	FRET(MD_Compcode_Success);
}






MD_Compcode spectrum_maps_union(Spectrum_Map** spectrum_map_a_pptr,
															Spectrum_Map* spectrum_map_b_ptr) {

	Spectrum_Map* new_spectrum_map_ptr;
	int imap_a,imap_b,imap_new,lower_index,upper_index;
	
	FIN( spectrum_maps_sum( spectrum_map_a_ptr,spectrum_map_b_ptr) );
	if (PRGC_NIL == *spectrum_map_a_pptr) {
		*spectrum_map_a_pptr = spectrum_map_copy(spectrum_map_b_ptr);
		FRET(MD_Compcode_Success);
	}
	if (PRGC_NIL == spectrum_map_b_ptr) {
		FRET(MD_Compcode_Success);
	}
	
	
	if ((*spectrum_map_a_pptr)->lower_frequency_index != spectrum_map_b_ptr->lower_frequency_index  ||
			spectrum_map_upper_frequency_index_get(*spectrum_map_a_pptr) != spectrum_map_upper_frequency_index_get(spectrum_map_b_ptr) ) {

		if (spectrum_map_lower_frequency_index_get(*spectrum_map_a_pptr) <
		spectrum_map_lower_frequency_index_get(spectrum_map_b_ptr) ) {
			lower_index = (*spectrum_map_a_pptr)->lower_frequency_index;
		}
		else {
			lower_index = spectrum_map_b_ptr->lower_frequency_index;
		}

		if (spectrum_map_upper_frequency_index_get(*spectrum_map_a_pptr) > 
		spectrum_map_upper_frequency_index_get(spectrum_map_b_ptr)) {
			upper_index = spectrum_map_upper_frequency_index_get(*spectrum_map_a_pptr);
		}
		else {
			upper_index = spectrum_map_upper_frequency_index_get(spectrum_map_b_ptr);
		}

		new_spectrum_map_ptr = spectrum_map_create(lower_index,upper_index);
		spectrum_map_fully_busy_set(new_spectrum_map_ptr);

		imap_a = frequency_index_to_map_index((*spectrum_map_a_pptr), (*spectrum_map_a_pptr)->lower_frequency_index);
		imap_b = frequency_index_to_map_index((*spectrum_map_a_pptr), spectrum_map_upper_frequency_index_get(*spectrum_map_a_pptr));
		imap_new = frequency_index_to_map_index(new_spectrum_map_ptr, (*spectrum_map_a_pptr)->lower_frequency_index);
		for (; imap_a<=imap_b; ++imap_a, ++imap_new) {
			if ( MAP_FREQ_INDEX_IS_FREE((*spectrum_map_a_pptr)->map[imap_a]) ) {
				new_spectrum_map_ptr->map[imap_new] = MAP_FREQ_INDEX_FREE;
			}
		}
		spectrum_map_destroy(*spectrum_map_a_pptr);
		*spectrum_map_a_pptr = new_spectrum_map_ptr;
	} 
	
	imap_a = frequency_index_to_map_index(spectrum_map_b_ptr, spectrum_map_b_ptr->lower_frequency_index);
	imap_b = frequency_index_to_map_index(spectrum_map_b_ptr, spectrum_map_upper_frequency_index_get(spectrum_map_b_ptr));
	imap_new = frequency_index_to_map_index((*spectrum_map_a_pptr), spectrum_map_b_ptr->lower_frequency_index);
	for (; imap_a<=imap_b; ++imap_a, ++imap_new) {
		if ( MAP_FREQ_INDEX_IS_FREE( spectrum_map_b_ptr->map[imap_a]) ) {
			(*spectrum_map_a_pptr)->map[imap_new] = MAP_FREQ_INDEX_FREE;
		}
	}
	
	FRET(MD_Compcode_Success);
}






MD_Compcode spectrum_map_first_free_frequency_get(Spectrum_Map const* spectrum_map_ptr,
																									int* freq_index_ptr) {
	// This function returns MD_Compcode_Success if it find a free (=0) index
	// in the map, otherwise MD_Compcode_Failure. 
	// The frequency index is stored into the index variable passed by reference
	int i;
	FIN(spectrum_map_first_free_frequency_get(spectrum_map_ptr, freq_index_ptr));
	for (i=0; i<spectrum_map_ptr->size; ++i) {
		if ( MAP_FREQ_INDEX_IS_FREE(spectrum_map_ptr->map[i]) )
			FRET( map_index_to_frequency_index(spectrum_map_ptr, i, freq_index_ptr) );
	}
	FRET(MD_Compcode_Failure);
}





MD_Compcode spectrum_map_free_frequency_get(Spectrum_Map const* spectrum_map_ptr,
																						Allocation_policy policy,
																						int* freq_index_ptr) {
	// This function returns MD_Compcode_Success if it find a free index
	// in the map, otherwise MD_Compcode_Failure. 
	// The frequency index is stored into the index variable passed by reference
	int i, count, tot_free_freq, nth_free_freq;
	Distribution*	uniform_dist;

	FIN(spectrum_map_free_frequency_get(spectrum_map_ptr, policy, freq_index_ptr));
	switch (policy) {
		case RANDOM:
			tot_free_freq = spectrum_map_free_num(spectrum_map_ptr);
			if (0 == tot_free_freq)	FRET(MD_Compcode_Failure);
			uniform_dist = op_dist_load("uniform_int", 1, tot_free_freq);
			nth_free_freq = op_dist_outcome(uniform_dist);
			op_dist_unload(uniform_dist);
			break;

		case FIRST_FIT:
		default: //first fit by default
				nth_free_freq = 1;
			break;
	}

	count = 0;
	for (i=0; i<spectrum_map_ptr->size; ++i) {
		if ( MAP_FREQ_INDEX_IS_FREE(spectrum_map_ptr->map[i]) ) {
			++count;
			if (count == nth_free_freq) {
				FRET( map_index_to_frequency_index(spectrum_map_ptr, i, freq_index_ptr) );
			}
		}
	}

	FRET(MD_Compcode_Failure);
}




// returns the number of free indexes
OpT_uInt32 spectrum_map_free_num(Spectrum_Map const* spectrum_map_ptr) {
	int i;
	OpT_uInt32 count_free_indexes;
	
	FIN( spectrum_map_free_num(spectrum_map_ptr) );
	count_free_indexes = 0;
	for (i=0; i<spectrum_map_ptr->size; ++i) {
		if ( MAP_FREQ_INDEX_IS_FREE(spectrum_map_ptr->map[i]) )
			++count_free_indexes;
	}
	FRET(count_free_indexes);
}


unsigned int spectrum_map_free_index_min_spacing_num(Spectrum_Map const* spectrum_map_ptr,
																										 unsigned int min_spacing) {
	int i;
	unsigned int count_free_indexes;
	
	FIN( spectrum_map_free_num(spectrum_map_ptr) );
	count_free_indexes = 0;
	for (i=0; i<spectrum_map_ptr->size; ) {
		if ( MAP_FREQ_INDEX_IS_FREE(spectrum_map_ptr->map[i]) ) {
			++count_free_indexes;
			i+=min_spacing;
		} else {
			++i;
		}
		
	}
	FRET(count_free_indexes);
}



Boolean spectrum_is_frequency_index_free(Spectrum_Map const* spectrum_map_ptr,
																				 int freq_index) {
    // This function returns OPC_TRUE if the frequency index given in input
    // is free in the map spectrum.
    int map_index;
    FIN(spectrum_is_frequency_index_free(spectrum_map_ptr, freq_index));

    map_index = frequency_index_to_map_index(spectrum_map_ptr, freq_index);
    if (map_index < 0) {
			FRET(OPC_FALSE);
		}

    if ( MAP_FREQ_INDEX_IS_FREE(spectrum_map_ptr->map[map_index]) ) {
			FRET(OPC_TRUE);
		}
		
    FRET(OPC_FALSE);
}



void spectrum_map_print(Spectrum_Map const* spectrum_map_ptr) {
	unsigned int i;
	FIN( spectrum_map_print(spectrum_map_ptr) );
	if (PRGC_NIL == spectrum_map_ptr) {
		printf("spectrum map range {none,none} : []\n");
		FOUT;
	}
	printf("spectrum map range {%d,%d} : [",
		   spectrum_map_ptr->lower_frequency_index,
		   spectrum_map_upper_frequency_index_get(spectrum_map_ptr));
	for (i=0; i<spectrum_map_ptr->size; ++i) {
		printf("%d,", spectrum_map_ptr->map[i]);
	}
	printf("]\n");
	FOUT;
}
