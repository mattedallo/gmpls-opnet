#ifndef	_MD_IP_API_H_INCLUDED_
	#define _MD_IP_API_H_INCLUDED_
	
	#include <opnet.h>
	#include <ip_addr_v4.h>
		
	typedef struct Ip_Api_Interface Ip_Api_Interface;
	
	Ip_Api_Interface* ip_api_register (Objid module_id);

	Ip_Api_Interface* ip_api_interface_copy(Ip_Api_Interface* ip_api_intf_ptr);

	void ip_api_interface_destroy(Ip_Api_Interface* ip_api_intf_ptr);
	
	void ip_api_pk_send(Ip_Api_Interface* ip_api_intf, Packet* pkptr, InetT_Address const* dest_addr);

#endif
