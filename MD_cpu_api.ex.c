#include 	<opnet.h>
#include 	<oms_pr.h>
#include 	"MD_cpu_api.h"


Api_Cpu_Interface_Handle cpu_client_register (Objid client_objid) {
	Api_Cpu_Interface_Handle	cpu_intf_handle;
	List						proc_record_handle_list;
	OmsT_Pr_Handle				process_record_handle;
	Objid						node_objid;

	/** The interface handle is used for communication between the 	**/
	/** client and the CPU module. The members of the interface  	**/
	/** handle data structure are filled in this function and 	 	**/
	/** returned to the client. The client will use this handle 	**/
	/** for future calls to the CPU module.							**/
	FIN (cpu_client_register (client_objid));

	/* Make the client module Object ID as a part of this interface	*/
	cpu_intf_handle.client_objid = client_objid; 

	/* Initialize the CPU module ID to INVALID. It will contain a	*/
	/* valid value after discovering the CPU module ID.				*/
	cpu_intf_handle.cpu_objid = OPC_OBJID_INVALID;

	/* Node ID from the application module ID. This value is used 	*/
	/* for discovery of the CPU module.								*/
	node_objid = op_topo_parent (client_objid);

	/* Use process registry to find the CPU module ID present in 	*/
	/* the same node as the applciation.							*/
    oms_pr_process_discover (OPC_OBJID_INVALID, &proc_record_handle_list,
                    "node objid",   OMSC_PR_OBJID,   node_objid,
                    "hardware", OMSC_PR_STRING, "MD_cpu", OPC_NIL);

	if (op_prg_list_size (&proc_record_handle_list))
		{
		/* Get a handle to the CPU process handle.					*/
		process_record_handle = (OmsT_Pr_Handle) op_prg_list_remove (&proc_record_handle_list, 
																	 OPC_LISTPOS_HEAD);

		/* Obtain the object ID of the CPU.			*/
		oms_pr_attr_get (process_record_handle, "module objid", OMSC_PR_OBJID, &(cpu_intf_handle.cpu_objid));
		}
	else
		{
		FRET (cpu_intf_handle);
		}
 
	FRET (cpu_intf_handle);
}


Objid cpu_objid_get(Api_Cpu_Interface_Handle const * const cpu_intf_hndlptr) {
	/** return the object ID of the CPU module **/
	FIN( cpu_objid_get(cpu_intf_hndlptr) );
	FRET(cpu_intf_hndlptr->cpu_objid);
}


void cpu_computation_request(Api_Cpu_Interface_Handle const * const cpu_intf_hndlptr, OpT_Int32 request_id) {
	Ici *ici_ptr;
	/** Function used by the client to invoke the CPU module **/
	/** asking for a computation							 **/
	FIN( cpu_computation_request(cpu_intf_hndlptr) );
	
	// if the oxc mudule does not exist return.
	if (OPC_OBJID_INVALID == cpu_intf_hndlptr->cpu_objid) {
		printf("CPU API ERROR: CPU module not found, CPU computation cannot be requested\n");
		FOUT;
	}
	
	ici_ptr = op_ici_create("MD_cpu_ici");
	op_ici_attr_set_int32(ici_ptr, "request id", request_id);
	op_ici_install(ici_ptr);
	op_intrpt_schedule_remote(op_sim_time(), CPU_COMPUTATION_REQUEST_CODE, cpu_intf_hndlptr->cpu_objid);
	op_ici_install(OPC_NIL);
	
	FOUT;
}

OpT_Int32 cpu_interrupt_request_id_get(Api_Cpu_Interface_Handle const * const cpu_intf_hndlptr) {
	Ici *ici_ptr;
	OpT_Int32 request_id;
	/** Function used by the client to get the request id **/
	/** received with the interrupt **/
	FIN( oxc_interrupt_request_id_get(cpu_intf_hndlptr) );
	
	if (OPC_OBJID_INVALID == cpu_intf_hndlptr->cpu_objid) {
		printf("CPU API ERROR: CPU module not found, interrupt request id is not available\n");
		FRET(-1);
	}
	
	if (op_intrpt_source() != cpu_intf_hndlptr->cpu_objid) {
		printf("CPU API ERROR: the interrupt is not associated to the CPU, request id cannot be found\n");
		FRET(-2);
	}
	
	ici_ptr = op_intrpt_ici();
	if ( OPC_NIL == ici_ptr ) {
		printf("CPU API ERROR: no ici received, request id cannot be found\n");
		FRET(-2);
	}
	
	/* read the attributes */
	op_ici_attr_get_int32(ici_ptr, "request id", &request_id);
	// destroy the ici //
	op_ici_destroy(ici_ptr);
	
	FRET(request_id);
}
