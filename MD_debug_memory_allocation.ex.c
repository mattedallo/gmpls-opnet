#include 	<opnet.h>

void* alloca(int s, int line, char* file, char* func ) {
    void *ciao =  prg_mem_alloc(s);
    printf("allocato %x di %d bytes in %s linea %d\n",ciao, s, func, line);
    return ciao;
  }
  

void libera(void* s, int line, char* file, char* func) {
    prg_mem_free(s);
    printf("deallocato %x in %s linea %d\n",s, func, line);
}

