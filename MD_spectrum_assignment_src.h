#ifndef	_MD_SPECTRUM_ASSIGNMENT_H_INCLUDED_
	#define _MD_SPECTRUM_ASSIGNMENT_H_INCLUDED_
	
	#include	"MD_rsvp_msg_util.h"
	
	/* Allocation algorithms used to select the label from the label set*/
	typedef enum Allocation_policy {
			FIRST_FIT=0,
			LAST_FIT,
			RANDOM,
			EXACT_FIT,
			FIRST_PROPER_FIT
		} Allocation_policy;

  typedef struct Excluded_Frequency_Range {
  /* describe a range of frequency that must be */
  /*  excluded during the label selection */
    int 			excluded_upper_freq_indx;
	int 			excluded_lower_freq_indx;
  } Excluded_Frequency_Range;
  
	/* Function prototypes*/
	int* sa_select_label_from_set ( Label_set_object const* label_set_ptr, Allocation_policy a_policy,int requested_slices, PrgT_List *excluded_freq_ranges_lptr);
		
#endif
