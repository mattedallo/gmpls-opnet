#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <opnet.h>

#include "csv_parser.h"

#ifdef __cplusplus
extern "C" {
#endif

CsvParser *CsvParser_new(const char *filePath, const char *delimiter, int firstLineIsHeader) {
    CsvParser *csvParser;
    FIN(CsvParser_new());

    csvParser = (CsvParser*)prg_mem_alloc(sizeof(CsvParser));
    if (filePath == NULL) {
        csvParser->filePath_ = NULL;
    } else {
    int filePathLen;
        filePathLen = strlen(filePath);
        csvParser->filePath_ = (char*)prg_mem_alloc((filePathLen + 1));
        strcpy(csvParser->filePath_, filePath);
    }
    csvParser->firstLineIsHeader_ = firstLineIsHeader;
    csvParser->errMsg_ = NULL;
    if (delimiter == NULL) {
        csvParser->delimiter_ = ',';
    } else if (_CsvParser_delimiterIsAccepted(delimiter)) {
        csvParser->delimiter_ = *delimiter;
    } else {
        csvParser->delimiter_ = '\0';
    }
    csvParser->header_ = NULL;
    csvParser->fileHandler_ = NULL;
    csvParser->fromString_ = 0;
    csvParser->csvString_ = NULL;
    csvParser->csvStringIter_ = 0;

    FRET(csvParser);
}

CsvParser *CsvParser_new_from_string(const char *csvString, const char *delimiter, int firstLineIsHeader) {
  CsvParser *csvParser;
  FIN(CsvParser_new_from_string());

  csvParser = CsvParser_new(NULL, delimiter, firstLineIsHeader);
  csvParser->fromString_ = 1; 
  if (csvString != NULL) {
    int csvStringLen;
    csvStringLen = strlen(csvString);
    csvParser->csvString_ = (char*)prg_mem_alloc(csvStringLen + 1);
    strcpy(csvParser->csvString_, csvString);
  } 
  FRET(csvParser);
}

void CsvParser_destroy(CsvParser *csvParser) {
    FIN(CsvParser_destroy());

    if (csvParser == NULL) {
        FOUT;
    }
    if (csvParser->filePath_ != NULL) {
        prg_mem_free(csvParser->filePath_);
    }
    if (csvParser->errMsg_ != NULL) {
        prg_mem_free(csvParser->errMsg_);
    }
    if (csvParser->fileHandler_ != NULL) {
        fclose(csvParser->fileHandler_);
    }
    if (csvParser->header_ != NULL) {
        CsvParser_destroy_row(csvParser->header_);
    }
    if (csvParser->csvString_ != NULL) {
        prg_mem_free(csvParser->csvString_);
    }
    prg_mem_free(csvParser);
    FOUT;
}

void CsvParser_destroy_row(CsvRow *csvRow) {
    int i;
    FIN(CsvParser_destroy_row());
    for (i = 0 ; i < csvRow->numOfFields_ ; i++) {
        prg_mem_free(csvRow->fields_[i]);
    }
    prg_mem_free(csvRow);
    FOUT;
}

CsvRow *CsvParser_getHeader(CsvParser *csvParser) {
    FIN(CsvParser_getHeader());

    if (! csvParser->firstLineIsHeader_) {
        _CsvParser_setErrorMessage(csvParser, "Cannot supply header, as current CsvParser object does not support header");
        FRET(NULL);
    }
    if (csvParser->header_ == NULL) {
        csvParser->header_ = _CsvParser_getRow(csvParser);
    }

    FRET(csvParser->header_);
}

CsvRow *CsvParser_getRow(CsvParser *csvParser) {
    FIN(CsvParser_getRow());
    if (csvParser->firstLineIsHeader_ && csvParser->header_ == NULL) {
        csvParser->header_ = _CsvParser_getRow(csvParser);
    }
    FRET(_CsvParser_getRow(csvParser));
}

int CsvParser_getNumFields(CsvRow *csvRow) {
    FIN(CsvParser_getNumFields());
    FRET(csvRow->numOfFields_);
}

char **CsvParser_getFields(CsvRow *csvRow) {
    FIN(CsvParser_getFields());
    FRET(csvRow->fields_);
}

CsvRow *_CsvParser_getRow(CsvParser *csvParser) {
    int accceptedFields = 64;
    int acceptedCharsInField = 64;
    CsvRow *csvRow;
    char *currField;
    int fieldIter = 0;
    int inside_complex_field = 0;
    int currFieldCharIter = 0;
    int seriesOfQuotesLength = 0;
    int lastCharIsQuote = 0;
    int isEndOfFile = 0;

    FIN(_CsvParser_getRow());
  
    if (csvParser->filePath_ == NULL && (! csvParser->fromString_)) {
        _CsvParser_setErrorMessage(csvParser, "Supplied CSV file path is NULL");
        FRET(NULL);
    }
    if (csvParser->csvString_ == NULL && csvParser->fromString_) {
        _CsvParser_setErrorMessage(csvParser, "Supplied CSV string is NULL");
        FRET(NULL);
    }
    if (csvParser->delimiter_ == '\0') {
        _CsvParser_setErrorMessage(csvParser, "Supplied delimiter is not supported");
        FRET(NULL);
    }
	
    if (! csvParser->fromString_) {
        if (csvParser->fileHandler_ == NULL) {
          csvParser->fileHandler_ = fopen(csvParser->filePath_, "r");
          if (csvParser->fileHandler_ == NULL) {
            int errorNum;
            const char *errStr;
            char *errMsg;
      errorNum = errno;
      errStr = strerror(errorNum);
      errMsg = (char*)prg_mem_alloc(1024 + strlen(errStr));
            strcpy(errMsg, "");
            sprintf(errMsg, "Error opening CSV file for reading: %s : %s", csvParser->filePath_, errStr);
            _CsvParser_setErrorMessage(csvParser, errMsg);
            prg_mem_free(errMsg);
            FRET(NULL);
          }
        }
    }
  
    csvRow = (CsvRow*)prg_mem_alloc(sizeof(CsvRow));
    csvRow->fields_ = (char**)prg_mem_alloc(accceptedFields * sizeof(char*));
    csvRow->numOfFields_ = 0;
    
    currField = (char*)prg_mem_alloc(acceptedCharsInField);

    while (1) {
        int endOfFileIndicator;
        char currChar = (csvParser->fromString_) ? csvParser->csvString_[csvParser->csvStringIter_] : fgetc(csvParser->fileHandler_);
        
        csvParser->csvStringIter_++;
        if (csvParser->fromString_) {
            endOfFileIndicator = (currChar == '\0');
        } else {
            endOfFileIndicator = feof(csvParser->fileHandler_);
        }
        if (endOfFileIndicator) {
            if (currFieldCharIter == 0 && fieldIter == 0) {
                _CsvParser_setErrorMessage(csvParser, "Reached EOF");
                FRET(NULL);
            }
            currChar = '\n';
            isEndOfFile = 1;
        }
        if (currChar == '\r') {
            continue;
        }
        if (currFieldCharIter == 0  && ! lastCharIsQuote) {
            if (currChar == '\"') {
                inside_complex_field = 1;
                lastCharIsQuote = 1;
                continue;
            }
        } else if (currChar == '\"') {
            seriesOfQuotesLength++;
            inside_complex_field = (seriesOfQuotesLength % 2 == 0);
            if (inside_complex_field) {
                currFieldCharIter--;
            }
        } else {
            seriesOfQuotesLength = 0;
        }
        if (isEndOfFile || ((currChar == csvParser->delimiter_ || currChar == '\n') && ! inside_complex_field) ){
            currField[lastCharIsQuote ? currFieldCharIter - 1 : currFieldCharIter] = '\0';
            csvRow->fields_[fieldIter] = (char*)prg_mem_alloc(currFieldCharIter + 1);
            strcpy(csvRow->fields_[fieldIter], currField);
            prg_mem_free(currField);
            csvRow->numOfFields_++;
            if (currChar == '\n') {
                FRET(csvRow);
            }
            acceptedCharsInField = 64;
            currField = (char*)prg_mem_alloc(acceptedCharsInField);
            currFieldCharIter = 0;
            fieldIter++;
            inside_complex_field = 0;
        } else {
            currField[currFieldCharIter] = currChar;
            currFieldCharIter++;
            if (currFieldCharIter == acceptedCharsInField - 1) {
                acceptedCharsInField *= 2;
                currField = (char*)realloc(currField, acceptedCharsInField);
            }
        }
        lastCharIsQuote = (currChar == '\"') ? 1 : 0;
    }
}

int _CsvParser_delimiterIsAccepted(const char *delimiter) {
  char actualDelimiter;
    FIN(_CsvParser_delimiterIsAccepted());
  
  actualDelimiter = *delimiter;
    if (actualDelimiter == '\n' || actualDelimiter == '\r' || actualDelimiter == '\0' ||
            actualDelimiter == '\"') {
        FRET(0);
    }
    FRET(1);
}

void _CsvParser_setErrorMessage(CsvParser *csvParser, const char *errorMessage) {
  int errMsgLen;
    FIN(_CsvParser_setErrorMessage());
    if (csvParser->errMsg_ != NULL) {
        prg_mem_free(csvParser->errMsg_);
    }
    errMsgLen = strlen(errorMessage);
    csvParser->errMsg_ = (char*)prg_mem_alloc(errMsgLen + 1);
    strcpy(csvParser->errMsg_, errorMessage);
    FOUT;
}

const char *CsvParser_getErrorMessage(CsvParser *csvParser) {
    FIN(CsvParser_getErrorMessage());
    FRET(csvParser->errMsg_);
}

#ifdef __cplusplus
}
#endif
