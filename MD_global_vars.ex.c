#include 	<opnet.h>
#include	<prg_bin_hash.h>

////////////////////////////////////////////////////////////

/*PrgT_Bin_Hash_Table	*memory_htable_ptr = OPC_NIL;
PrgT_Bin_Hash_Table	*vettori_htable_ptr = OPC_NIL;
PrgT_Bin_Hash_Table	*liste_htable_ptr = OPC_NIL;
PrgT_Bin_Hash_Table	*pacchetti_htable_ptr = OPC_NIL;

  void stampa_memoria() {
  FILE *ofp;
  PrgT_List *   list_ptr;
  PrgT_List_Cell 			*cell_ptr;
    
    ofp = fopen("D:\\gmpls_memory_leak.txt", "w");
    list_ptr = prg_bin_hash_table_item_list_get (memory_htable_ptr);
		cell_ptr = prg_list_head_cell_get (list_ptr);
    while (cell_ptr != PRGC_NIL) {
						fprintf(ofp,"%s\n", prg_list_cell_data_get (cell_ptr));
						cell_ptr = prg_list_cell_next_get (cell_ptr);
		}
		prg_list_destroy (list_ptr, OPC_TRUE);
    fclose(ofp);
    ofp = fopen("D:\\gmpls_vettori_leak.txt", "w");
    list_ptr = prg_bin_hash_table_item_list_get (vettori_htable_ptr);
		cell_ptr = prg_list_head_cell_get (list_ptr);
    while (cell_ptr != PRGC_NIL) {
						fprintf(ofp,"%s\n", prg_list_cell_data_get (cell_ptr));
						cell_ptr = prg_list_cell_next_get (cell_ptr);
		}
		prg_list_destroy (list_ptr, OPC_TRUE);
    fclose(ofp);
    ofp = fopen("D:\\gmpls_liste_leak.txt", "w");
    list_ptr = prg_bin_hash_table_item_list_get (liste_htable_ptr);
		cell_ptr = prg_list_head_cell_get (list_ptr);
    while (cell_ptr != PRGC_NIL) {
						fprintf(ofp,"%s\n", prg_list_cell_data_get (cell_ptr));
						cell_ptr = prg_list_cell_next_get (cell_ptr);
		}
		prg_list_destroy (list_ptr, OPC_TRUE);
    fclose(ofp);
    ofp = fopen("D:\\gmpls_pacchetti_leak.txt", "w");
    list_ptr = prg_bin_hash_table_item_list_get (pacchetti_htable_ptr);
		cell_ptr = prg_list_head_cell_get (list_ptr);
    while (cell_ptr != PRGC_NIL) {
						fprintf(ofp,"%s\n", prg_list_cell_data_get (cell_ptr));
						cell_ptr = prg_list_cell_next_get (cell_ptr);
		}
		prg_list_destroy (list_ptr, OPC_TRUE);
    fclose(ofp);
  }
  
void* alloca(int s, int line, char* file, char* func ) {
    void *ciao =  prg_mem_alloc(s);
    char *descr = prg_mem_alloc(128);
    if (memory_htable_ptr == OPC_NIL) { 
      memory_htable_ptr =  prg_bin_hash_table_create (10, sizeof(void*));
    }
    sprintf(descr, "%f %s %d bytes linea %d",op_sim_time(),func,s, line);
    prg_bin_hash_table_item_insert(memory_htable_ptr, &ciao, descr, PRGC_NIL);
    
    return ciao;
  }
  
  void libera(void* s, int line, char* file, char* func) {
    char *descr;
    descr = (char *)prg_bin_hash_table_item_get (memory_htable_ptr, &s);
    prg_bin_hash_table_item_remove (memory_htable_ptr, &s);
    prg_mem_free(descr);
    prg_mem_free(s);
    //printf("deallocato %x in %s linea %d\n",s, func, line);
  }
  
  PrgT_Vector * crea_vettore(unsigned long initial_size,PrgT_Vector_Destroy_Proc destroy_proc,PrgT_Vector_Copy_Proc copy_proc,int line, char* file, char* func){
    PrgT_Vector *vettore = prg_vector_create(initial_size, destroy_proc, copy_proc);
    char *descr = prg_mem_alloc(128);
    if (vettori_htable_ptr == OPC_NIL) { 
      vettori_htable_ptr =  prg_bin_hash_table_create (10, sizeof(void*));
    }
    sprintf(descr, "%f %s %d bytes linea %d",op_sim_time(),func, sizeof(PrgT_Vector), line);
    prg_bin_hash_table_item_insert(vettori_htable_ptr, &vettore, descr, PRGC_NIL);
    return vettore;
  }
  
  PrgT_Compcode distruggi_vettore (PrgT_Vector * vector_ptr,int dealloc_client_state) {
    char *descr;
    descr =(char *)prg_bin_hash_table_item_get (vettori_htable_ptr, &vector_ptr);
    prg_bin_hash_table_item_remove (vettori_htable_ptr, &vector_ptr);
    prg_mem_free(descr);
    return prg_vector_destroy(vector_ptr,dealloc_client_state);
  }
  
  PrgT_List * crea_lista_da_htable (PrgT_Bin_Hash_Table * hash_ptr,int line, char* file, char* func) {
    PrgT_List * lista = prg_bin_hash_table_item_list_get (hash_ptr);
    char *descr = prg_mem_alloc(128);
    if (liste_htable_ptr == OPC_NIL) { 
      liste_htable_ptr =  prg_bin_hash_table_create (10, sizeof(void*));
    }
    sprintf(descr, "%f %s %d bytes linea %d",op_sim_time(),func,sizeof(PrgT_List), line);
    prg_bin_hash_table_item_insert(liste_htable_ptr, &lista, descr, PRGC_NIL);
    return lista;
  }
  
  PrgT_List * crea_lista (int line, char* file, char* func){
  PrgT_List * lista = prg_list_create();
    char *descr = prg_mem_alloc(128);
    if (liste_htable_ptr == OPC_NIL) { 
      liste_htable_ptr =  prg_bin_hash_table_create (10, sizeof(void*));
    }
    sprintf(descr, "%f %s %d bytes linea %d",op_sim_time(),func,sizeof(PrgT_List),  line);
    prg_bin_hash_table_item_insert(liste_htable_ptr, &lista, descr, PRGC_NIL);
    return lista;
  }
  
  void  distruggi_lista(PrgT_List * list_ptr,int dealloc_elements) {
  char *descr;
    descr =(char *)prg_bin_hash_table_item_get (liste_htable_ptr, &list_ptr);
    prg_mem_free(descr);
    prg_bin_hash_table_item_remove (liste_htable_ptr, &list_ptr);
    prg_list_destroy(list_ptr, dealloc_elements);
  }
  
  Packet* crea_pacchetto(char* format_name,int line, char* file, char* func){
  Packet* pacchetto=op_pk_create_fmt("MD_RSVP_Packet");
  char *descr = prg_mem_alloc(128);
    if (pacchetti_htable_ptr == OPC_NIL) { 
      pacchetti_htable_ptr =  prg_bin_hash_table_create (10, sizeof(void*));
    }
    sprintf(descr, "%f %s %d bytes linea %d",op_sim_time(),func,sizeof(pacchetto), line);
    prg_bin_hash_table_item_insert(pacchetti_htable_ptr, &pacchetto, descr, PRGC_NIL);
    return pacchetto;
  }
  void distruggi_pacchetto(Packet* pkptr){
  char *descr;
    descr =(char *)prg_bin_hash_table_item_get (pacchetti_htable_ptr, &pkptr);
   prg_mem_free(descr);
   prg_bin_hash_table_item_remove (pacchetti_htable_ptr, &pkptr);
   op_pk_destroy(pkptr);
  }
*/
 ///////////////////////////////////////////////////////////////////////////////
  
  
#include 	<prg_graph.h>
#include	"MD_ospf_src.h"
#include	"MD_spectrum_assignment_src.h"

/* PCE GLOBAL VARS*/
/*PrgT_Graph_State_ID 		gbl_topology_graph_state_ID;
PrgT_Bin_Hash_Table			*gbl_topology_vertex_htable_ptr = OPC_NIL;
PrgT_Bin_Hash_Table			*gbl_topology_paths_htable_ptr = OPC_NIL;
PrgT_Graph 					*gbl_topology_graph_ptr = OPC_NIL;*/
//PrgT_Vector					*gbl_topology_node_address_vptr = OPC_NIL;

/*
Routing_Strategy		pce_routing_strategy = LEAST_CONGESTED;
int						pce_additional_hops = 2;
int						pce_path_pool_limit = 99999;
Boolean					pce_provisioning_suggested_label_status = OPC_TRUE;
Allocation_polocy		pce_provisioning_suggested_label_policy = FIRST_FIT;
Boolean					pce_recovery_suggested_label_status = OPC_TRUE;
Allocation_polocy		pce_recovery_suggested_label_policy = FIRST_FIT;

Splitting_Type			pce_splitting_type = BALANCED;
Splitting_Strategy		pce_splitting_stretegy = NO_SPLITTING;
Splitting_Strategy		pce_recovery_splitting_stretegy = ADAPTIVE_SPLITTING;
int						pce_max_sub_lightpath = 99999;
PrgT_Vector				*pce_capacity_slices_map_vptr = PRGC_NIL;
*/
