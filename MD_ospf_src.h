#ifndef _MD_OSPF_H_INCLUDED_
  #define _MD_OSPF_H_INCLUDED_
  #include  <prg_vector.h>
  #include  <opnet.h>
  #include  <prg_graph.h>
  #include    <ip_addr_v4.h>
  #include  "MD_rsvp.h"
  #include  "MD_transponder.h"

  /* Interrupt code values for self interrupt type    */
  #define   LINK_STATUS_CHANGE_CODE   1
  #define   LSU_FLOODING_CODE         2
  #define   REQUEST_ADDRESS_CODE        7
  
  typedef enum Ospf_Message_Type{
    OSPF_HELLO_MSG = 1,
    OSPF_DB_DESC_MSG,
    OSPF_LSREQ_MSG,
    OSPF_LSU_MSG,
    OSPF_LSACK_MSG
  } Ospf_Message_Type;
  
  typedef enum Ospf_Lsa_Type {
    OSPF_ROUTER_LSA = 1,
    OSPF_NETWORK_LSA,
    OSPF_SUMMARY_LSA,
    OSPF_ASBR_SUMMARY_LSA,
    OSPF_EXTERNAL_LSA,
    OSPF_GROUP_MEMBERSHIP_LSA,
    OSPF_NSSA_EXT_LSA,
    OSPF_EXTERNAL_ATTR_LSA,
    OSPF_LINK_LOCAL_OPAQUE_LSA,
    OSPF_AREA_LOCAL_OPAQUE_LSA,
    OSPF_AS_OPAQUE_LSA,
    OSPF_NODE_ARCHITECTURE_LSA // not in rfc, used for Transponder information exchange
  } Ospf_Lsa_Type;
  
  /*typedef struct Ospf_Label_Set{
    OpT_uInt32    adv_router; //the loopback interface address
    PrgT_Vector   *link_vector; //a vector of Ospf_Link
  } Ospf_Label_Set;
  */
  
  typedef struct Ospf_Topology {
    PrgT_Graph*           graph_ptr;
    PrgT_Graph_State_ID   graph_state_handler;
    PrgT_Bin_Hash_Table*  vertices_htable_ptr;
    PrgT_Bin_Hash_Table*  fake_vertices_htptr;
    PrgT_Bin_Hash_Table*  paths_htable_ptr;
    PrgT_Vector*          node_address_vptr;
  } Ospf_Topology;
  
  typedef struct Ospf_Label_Range {
    /* describe a slot of available labels (frequency indexes)*/
    int     upper_freq_indx;
    int     lower_freq_indx;
  } Ospf_Label_Range;
  
  typedef struct Ospf_Opaque_Link{
    /*  rfc3630 
      http://tools.ietf.org/html/draft-ietf-ccamp-gmpls-general-constraints-ospf-te-05
      http://tools.ietf.org/html/draft-ietf-ccamp-general-constraint-encode-11
      http://tools.ietf.org/html/draft-zhang-ccamp-flexible-grid-ospf-ext-02
    */
    //Link_type //always point to point (1)
    InetT_Address dest_addr; //Link_ID For point-to-point links is the Router ID of the neighbor
    //local_interface_IP_addr
    //remote_interface_IP_addr
    //Traffic Engineering Metric
    //Maximum bandwidth
    //Maximum reservable bandwidth
    //Unreserved bandwidth
    //Administrative group
    PrgT_Vector   *available_slots_vptr; //an array of Ospf_Label_Range
  } Ospf_Opaque_Link;
  
  typedef struct Ospf_Link{
    /*rfc2328*/
    //int link_type; //always point to point (1)
    InetT_Address dest_addr; //link_id
    //OpT_uInt32  link_data;
    double  metric;
  } Ospf_Link;
  
  typedef struct Ospf_Opaque_Lsa_Body{
  //Type 10 - Area-local "opaque" LSA  (defined in OSPF-TE) rfc3630
    Ospf_Opaque_Link    *opaque_link; //only one link description per opaque lsa
    //Router_TLV    *router_tlv;
  } Ospf_Opaque_Lsa_Body;
  
  typedef struct Ospf_Router_Lsa_Body{
    PrgT_Vector       *link_vector; //a vector of Ospf_Link
  } Ospf_Router_Lsa_Body;
  

  typedef struct Ospf_Node_Architecture_Lsa_Body {
  // introduced to exchange informations of 
  // the Transponders on the node
    Transponder *transponder_ptr;
  } Ospf_Node_Architecture_Lsa_Body;
 

  typedef struct Ospf_Lsa_Header {
    //int           ls_age;
    Ospf_Lsa_Type   lsa_type;
    InetT_Address   ls_id;
    InetT_Address   adv_router; //the loopback interface address
    OpT_uInt32      ls_seq_number;
    //int           ls_checksum;
    //int           length;
  } Ospf_Lsa_Header;
  
  typedef struct Ospf_Lsa{
    Ospf_Lsa_Header   *lsa_header;
    void              *lsa_body; // depends on the type of lsa
  } Ospf_Lsa;
  
  typedef struct Ospf_LSU_Msg{ // Link State Update message
    // Authentication
    //OpT_uInt32    num_lsa; //the number of LSA inside the LSU
    PrgT_Vector   *lsa_vptr; //a vector of Ospf_Lsa
  } Ospf_LSU_Msg;
  
  
    /* structure used to attach informations to a vertex*/
  typedef struct TE_vertex_state{
    InetT_Address     vertex_node_addr;
    Boolean           visited;
        PrgT_Bin_Hash_Table *transponders_htptr; //hash table of transponders
  } TE_vertex_state;
  
  /* structure used to attach informations to an edge*/
  typedef struct TE_edge_state{ 
    PrgT_Vector   *available_slots_vptr; //an array of Ospf_Label_Range
    int       tot_available_freq_slices;
    Spectrum_Map  *spectrum_map_ptr;
    Boolean       failure;
    double        metric;
  } TE_edge_state;
  
    /* enum used to specify the metric of the graph's edges*/
  typedef enum  Edge_metric{
    UNDEFINED = 0,
    UNITY_BASED,
    SLICES_BASED
  } Edge_metric;
  
  /*  Function Prototypes         */

  /*Ospf_Link*          ospf_link_create (OpT_uInt32 dest_addr);
  Ospf_Opaque_Link*   ospf_opaque_link_create (OpT_uInt32 dest_addr, Rsvp_Link *rsvp_link);
  PrgT_Vector*        ospf_opaque_link_detach_available_slots_vector (Ospf_Opaque_Link* link);
  OpT_uInt32          ospf_link_get_dest_addr (Ospf_Link* link);
  OpT_uInt32          ospf_opaque_link_get_dest_addr (Ospf_Opaque_Link* link);
  int                 ospf_link_get_tot_available_freq_slices (Ospf_Link* link);
  
  Ospf_Router_Lsa*    ospf_router_lsa_create(OpT_uInt32 adv_router);
  Ospf_Opaque_Lsa*    ospf_opaque_lsa_create(OpT_uInt32 adv_router, Ospf_Opaque_Link *opaque_link);
  void                ospf_router_lsa_destroy(Ospf_Router_Lsa *router_lsa);
  void                ospf_opaque_lsa_destroy(Ospf_Opaque_Lsa *opaque_lsa);
  void                ospf_router_lsa_add_link(Ospf_Router_Lsa *router_lsa, Ospf_Link *link);
  OpT_uInt32          ospf_router_lsa_get_adv_router(Ospf_Router_Lsa *router_lsa);
  OpT_uInt32          ospf_opaque_lsa_get_adv_router(Ospf_Opaque_Lsa *opaque_lsa);
  int                 ospf_router_lsa_get_num_link(Ospf_Router_Lsa *router_lsa);
  Ospf_Link*          ospf_router_lsa_get_link(Ospf_Router_Lsa *router_lsa, unsigned long index);
  Ospf_Opaque_Link*   ospf_opaque_lsa_get_link(Ospf_Opaque_Lsa* opaque_lsa);
  */
#endif
