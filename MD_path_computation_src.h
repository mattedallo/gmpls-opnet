#ifndef _MD_PATH_COMPUTATION_H_INCLUDED_
  #define _MD_PATH_COMPUTATION_H_INCLUDED_
  
  #define NO_DEBUG_OUTPUT
  #ifndef NO_DEBUG_OUTPUT
    #define   PCE_DEBUG
  #endif
  
  #include  <prg_vector.h>
  #include  <prg_graph.h>
  #include  <prg_djk.h>
  #include  <prg_bin_hash.h>
  #include  <ip_addr_v4.h>
  #include  "MD_ospf_src.h"
  #include  "MD_rsvp.h"   
  #include  "MD_spectrum_assignment_src.h"
  #include  "MD_rsvp_msg_util.h"
    
    //#include  "MD_pcep_src.h"
    
    
typedef enum Routing_Strategy{
  SHORTEST_PATH = 0,
  LEAST_CONGESTED =1
} Routing_Strategy;
  
/*
typedef enum Splitting_Type {
    BALANCED = 0,
    UNBALANCED = 1
} Splitting_Type;*/
  
typedef enum Splitting_Strategy {
  NO_SPLIT = 0,
  MAX_SPLIT = 1,
  ADAPTIVE_SPLIT = 2
} Splitting_Strategy;

typedef struct Metric {
  int   type; //Routing_Strategy: least congested, shortest path
  int additional_hops; // additional hops in case of least congested
  int path_pool_limit;
} Metric;

typedef struct Suggested_Label_Params {
  Boolean             flag; //if true, suggested label is enabled
  int                 allocation_policy; //First Fit, Last Fit...
} Suggested_Label_Params;

typedef struct Slice_Ability_Params {
  int                 strategy; //Splitting_Strategy: No split, adaptive, max
  int                 split_limit;
  Boolean             allow_partial_solution; //if true partial solution of path computation allowed
} Slice_Ability_Params;

typedef struct Path_Comp_Params {
  Metric   *metric_ptr;
  Slice_Ability_Params *slice_ability_ptr;
  Suggested_Label_Params *suggested_label_ptr;
} Path_Comp_Params;
    
typedef struct TE_path {
  PrgT_Vector   *edges_vptr; //vector of edges
  int           *spectrum_map; //a map (array of int) of the spectrum obtained from the intersection of the links
  int           start_freq_indx; //the frequency index corresponding to the first element of the spectrum array
  int           map_size; //the size of the spectrum map array
} TE_path;
  

/* contains the minimum number of hops and all the possible shortest path (+n) between two nodes*/
typedef struct Paths_htable_tuple {
  int           min_hops_num;
  PrgT_Vector   *te_paths_vptr; //vector of TE_path
} Paths_htable_tuple;


typedef struct Eligible_TE_Path {
  Label_set_object     *label_set_ptr; //pointer to a label set for a specific slices demand
  TE_path              *te_path_ptr;   //pointer to a TE path
  int                  tot_av_labels;  //total available labels for a specific slices demand
  int                  tot_av_non_overlap_labels;  //total available non overlapping labels for a specific demand
} Eligible_TE_Path;
  

// The structure returned after path computation
typedef struct Computed_LSP {
  
  PrgT_Vector             *edges_vptr; //vector of edges
  ERO_object              *ero_object_ptr;
  Sender_tspec_object     *sender_tspec_ptr;
  Suggested_label_object  *sugg_label_ptr;
  int                     capacity;
  PrgT_Vector             *carriers_vptr; // list of carriers to be used (not in rfc)
  OpT_uInt32              *sugg_ingress_tx_trans_id_ptr; //transponder to be used as transmitter at the ingress(not in rfc)
  OpT_uInt32              *sugg_ingress_rx_trans_id_ptr; //transponder to be used as receiver at the ingress(not in rfc)
  OpT_uInt32              *sugg_egress_tx_trans_id_ptr; //transponder to be used as transmitter at the egress(not in rfc)
  OpT_uInt32              *sugg_egress_rx_trans_id_ptr; //transponder to be used as receiver at the egress(not in rfc)
} Computed_LSP;


/* the reply returned by the PCE after a path computation request */
typedef struct PCReply {
  PrgT_Vector   *computed_lsps_vptr; //vector of Computed_LSP
  Lsp_event     result_code;
} PCReply;


  /*  Function Prototypes         */
  void computed_lsp_destroy(Computed_LSP * comp_lsp_ptr);

  void pc_reply_destroy(PCReply *pc_reply);

  Computed_LSP* pc_reply_computed_lsp_access(PCReply *pc_reply, unsigned long i);

  unsigned long pc_reply_size(PCReply *pc_reply);

  PrgT_Compcode pc_reply_computed_lsp_append(PCReply *pc_reply,Computed_LSP* lsp_desc);

  InetT_Address pce_request_random_destination(Ospf_Topology *topology, InetT_Address excluded);
                            
  void pc_path_comp_params_init(Path_Comp_Params* pc_params_ptr);

  void pc_computed_paths_destroy(Ospf_Topology *topology);

  InetT_Address pce_get_next_shortest_path_hop(InetT_Address source_addr,InetT_Address destination_addr);
  
  Metric* pc_metric_copy(Metric* metric_ptr);

  void pc_metric_destroy(Metric* metric_ptr);

  Slice_Ability_Params* pc_slice_ability_copy(Slice_Ability_Params* slice_ability_ptr);

  void pc_slice_ability_destroy(Slice_Ability_Params* slice_ability_ptr);

  Suggested_Label_Params* pc_suggested_label_copy(Suggested_Label_Params* suggested_label_ptr);

  void pc_suggested_label_destroy(Suggested_Label_Params* suggested_label_ptr);
  
  Path_Comp_Params* pc_path_comp_params_copy(Path_Comp_Params* pc_params_ptr);
  
  void pc_path_comp_params_destroy(Path_Comp_Params* pc_params_ptr);
  
  PCReply* path_computation ( Ospf_Topology *topology_ptr, 
                            InetT_Address const* source_addr_ptr,
                            InetT_Address const* destination_addr_ptr,
                            int requested_capacity, 
                            Path_Comp_Params const * const params,
                            Transponder *tx_transponder_to_use_ptr,
                            Transponder *rx_transponder_to_use_ptr );


  PCReply* bulk_path_computation ( Ospf_Topology *topology_ptr, 
                            InetT_Address const* source_addr_ptr,
                            InetT_Address const* destination_addr_ptr,
                            int requested_capacity,
                            int num_requests,
                            Path_Comp_Params const * const params,
                            Transponder *tx_transponder_to_use_ptr,
                            Transponder *rx_transponder_to_use_ptr );

#endif
