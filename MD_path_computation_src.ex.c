#include  <math.h>
#include  "MD_path_computation_src.h"
#include  "MD_statistics_src.h"
#include  "MD_capacity_bandwidth_lib.h"
#include  "MD_utils.h"
#include  "MD_TED.h"
#include  "MD_spectrum.h"

typedef struct Spectrum_Request_Assoc {
  Spectrum_Map *spectrum_map_ptr;
  int num_carriers;
  double carrier_spacing;
} Spectrum_Request_Assoc;

typedef struct Transponder_Spectrum_Assoc {
  Transponder *transponder_ptr;
  PrgT_List   *spectrum_map_request_assoc_lptr; // list of spectrum Request Assoc
} Transponder_Spectrum_Assoc;


typedef struct Path_Spectrum_Assoc {
  Spectrum_Map *spectrum_map_ptr;
  PrgT_Vector *path_vptr;
} Path_Spectrum_Assoc;


//#define PC_DEBUG

Metric* pc_metric_copy(Metric* metric_ptr) {
  FIN(pc_metric_copy(metric_ptr) );
  if (metric_ptr == OPC_NIL) FRET(OPC_NIL);
  FRET( prg_mem_copy_create( metric_ptr, sizeof(Metric)) );
}

void pc_metric_destroy(Metric* metric_ptr) {
  FIN( pc_metric_destroy(metric_ptr) );
  if (metric_ptr == OPC_NIL) FOUT;
  prg_mem_free (metric_ptr);
  FOUT;
}

Slice_Ability_Params* pc_slice_ability_copy(Slice_Ability_Params* slice_ability_ptr) {
  FIN(pc_slice_ability_copy(slice_ability_ptr) );
  if (slice_ability_ptr == OPC_NIL) FRET(OPC_NIL);
  FRET(prg_mem_copy_create( slice_ability_ptr, sizeof(Slice_Ability_Params)));
}

void pc_slice_ability_destroy(Slice_Ability_Params* slice_ability_ptr) {
  FIN( pc_slice_ability_destroy(slice_ability_ptr) );
  if (slice_ability_ptr == OPC_NIL) FOUT;
  prg_mem_free (slice_ability_ptr);
  FOUT;
}

Suggested_Label_Params* pc_suggested_label_copy(Suggested_Label_Params* suggested_label_ptr) {
  FIN(pc_suggested_label_copy(suggested_label_ptr) );
  if (suggested_label_ptr == OPC_NIL) FRET(OPC_NIL);
  FRET(prg_mem_copy_create( suggested_label_ptr, sizeof(Suggested_Label_Params)));
}

void pc_suggested_label_destroy(Suggested_Label_Params* suggested_label_ptr) {
  FIN( pc_suggested_label_destroy(suggested_label_ptr) );
  if (suggested_label_ptr == OPC_NIL) FOUT;
  prg_mem_free (suggested_label_ptr);
  FOUT;
}


void pc_path_comp_params_init(Path_Comp_Params* pc_params_ptr) {  
  FIN(pc_path_comp_params_init() );
  if (pc_params_ptr == OPC_NIL) FOUT;
  pc_params_ptr->metric_ptr = (Metric *) prg_mem_alloc (sizeof (Metric));
  pc_params_ptr->slice_ability_ptr = (Slice_Ability_Params *) prg_mem_alloc (sizeof (Slice_Ability_Params));
  pc_params_ptr->suggested_label_ptr = (Suggested_Label_Params *) prg_mem_alloc (sizeof (Suggested_Label_Params));
  FOUT;
}

Path_Comp_Params* pc_path_comp_params_copy(Path_Comp_Params* pc_params_ptr) {
  Path_Comp_Params *copied_obj;
  
  FIN(pc_path_comp_params_copy(pc_params_ptr) );
  if (pc_params_ptr == OPC_NIL) FRET(OPC_NIL);
  copied_obj = (Path_Comp_Params *) prg_mem_alloc (sizeof (Path_Comp_Params));
  copied_obj->metric_ptr = pc_metric_copy(pc_params_ptr->metric_ptr);
  copied_obj->slice_ability_ptr = pc_slice_ability_copy(pc_params_ptr->slice_ability_ptr);
  copied_obj->suggested_label_ptr = pc_suggested_label_copy(pc_params_ptr->suggested_label_ptr);
  FRET(copied_obj);
}

void pc_path_comp_params_destroy(Path_Comp_Params* pc_params_ptr) {
  FIN( pc_path_comp_params_destroy(pc_params_ptr) );
  if (pc_params_ptr == OPC_NIL) FOUT;
  pc_metric_destroy(pc_params_ptr->metric_ptr);
  pc_slice_ability_destroy(pc_params_ptr->slice_ability_ptr);
  pc_suggested_label_destroy(pc_params_ptr->suggested_label_ptr);
  prg_mem_free (pc_params_ptr);
  FOUT;
}


void computed_lsp_destroy(Computed_LSP * comp_lsp_ptr) {
  FIN( computed_lsp_destroy(Computed_LSP * comp_lsp_ptr)  );
    ERO_object_destroy (comp_lsp_ptr->ero_object_ptr);
    sender_tspec_object_destroy(comp_lsp_ptr->sender_tspec_ptr);
    suggested_label_object_destroy(comp_lsp_ptr->sugg_label_ptr);
    // The contained edge is owned by the graph, so do not destroy elements.
    prg_vector_destroy(comp_lsp_ptr->edges_vptr,PRGC_FALSE);
    prg_vector_destroy(comp_lsp_ptr->carriers_vptr,PRGC_TRUE);
    prg_mem_free (comp_lsp_ptr->sugg_ingress_tx_trans_id_ptr);
    prg_mem_free (comp_lsp_ptr->sugg_ingress_rx_trans_id_ptr);
    prg_mem_free (comp_lsp_ptr->sugg_egress_tx_trans_id_ptr);
    prg_mem_free (comp_lsp_ptr->sugg_egress_rx_trans_id_ptr);
    prg_mem_free (comp_lsp_ptr);
  FOUT;
}


static PrgT_Compcode computed_lsps_vector_destroy(Computed_LSP * comp_lsp_ptr)  {
// function to destroy the lsp descriptor stored inside the PCP-REP
  FIN (computed_lsps_vector_destroy(comp_lsp_ptr));
    if (comp_lsp_ptr == OPC_NIL) FRET (PrgC_Compcode_Success);
    computed_lsp_destroy(comp_lsp_ptr);
  FRET (PrgC_Compcode_Success);
}


static PCReply* pc_reply_create(){
//returns a new path computation reply object
PCReply *pc_reply;
  FIN(pc_reply_create());
    pc_reply = (PCReply *) prg_mem_alloc (sizeof (PCReply));
    pc_reply->computed_lsps_vptr = PRGC_NIL;
    pc_reply->result_code = COMPUTATION_OK;
  FRET(pc_reply);
}

void pc_reply_destroy(PCReply *pc_reply) {
// Aim: destroy the pc_reply
  FIN(pc_reply_destroy(pc_reply));
  if (pc_reply == OPC_NIL) FOUT;
  if (pc_reply->computed_lsps_vptr != PRGC_NIL)
    prg_vector_destroy(pc_reply->computed_lsps_vptr,PRGC_TRUE);
  prg_mem_free(pc_reply);
  FOUT;
}


Computed_LSP* pc_reply_computed_lsp_access(PCReply *pc_reply, unsigned long i){
// Aim: access an lsp inside the PCReply
  FIN(pc_reply_computed_lsp_access(pc_reply,i));
  FRET( prg_vector_access(pc_reply->computed_lsps_vptr,i) );
}


unsigned long pc_reply_size(PCReply *pc_reply){
// Aim: Get the number of lsp descriptors inside the PCReply
  FIN(pc_reply_size(pc_reply));
  FRET( prg_vector_size(pc_reply->computed_lsps_vptr) );
}


PrgT_Compcode pc_reply_computed_lsp_append(PCReply *pc_reply,Computed_LSP* lsp_desc) {
// Aim: Append a new lsp to the pc reply
  FIN(pc_reply_computed_lsp_append(pc_reply,lsp_desc));
  if (pc_reply->computed_lsps_vptr == PRGC_NIL)
     pc_reply->computed_lsps_vptr = prg_vector_create (1, computed_lsps_vector_destroy, OPC_NIL);
  FRET(prg_vector_append (pc_reply->computed_lsps_vptr,lsp_desc));
}


/*--------------------------------FIND THE MINIMUM NUMBER OF HOPS BETWEEN TWO NODES---------------------------------*/
static int pce_find_minimum_hops_number(Ospf_Topology *topology, PrgT_Graph_Vertex *vertex_a_ptr, PrgT_Graph_Vertex *vertex_b_ptr) {
  TE_vertex_state     *vertex_state_ptr;
  PrgT_Vector       *adj_vertices_vptr, *vertices_vptr;
  int           i,vertex_queue_size,hops_count,vector_size;
  PrgT_List         *discovery_vertex_queue, *supp_queue;
  Boolean         found;
  PrgT_List_Cell      *cell_ptr;
  PrgT_Graph_Vertex     *vertex_ptr,*cur_vertex_ptr;
  PrgT_Graph_Edge     *edge_ab_ptr;
  TE_edge_state     *edge_state_ptr;

  FIN( pce_find_minimum_hops_number(graph_ptr, vertex_a_ptr, vertex_b_ptr) );
    
    // initialize all the nodes of the graph as not yet visited //
    vertices_vptr=prg_graph_vertex_all_get(topology->graph_ptr);
    vector_size = prg_vector_size(vertices_vptr);
    for (i=0; i<vector_size ;i++) {
      // read the vertex from the vector of vertices //
      vertex_ptr = prg_vector_access (vertices_vptr, i);
      // get the state information of the vertex //
      vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get(vertex_ptr, topology->graph_state_handler);
      if (PRGC_NIL != vertex_state_ptr) {
        // set the vertex as not visited //
        vertex_state_ptr->visited = OPC_FALSE;
      }
      
    }
  
    // deallocate the vector memory //
    prg_vector_destroy (vertices_vptr, PRGC_FALSE);

    discovery_vertex_queue = prg_list_create ();
    // at the beginning the discovery queue is just formed by the source vertex //
    prg_list_insert (discovery_vertex_queue, vertex_a_ptr, PRGC_LISTPOS_HEAD);
    // get the state information of the vertex //
    vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get(vertex_a_ptr, topology->graph_state_handler);
    // set the starting vertex as already visited //
    vertex_state_ptr->visited = OPC_TRUE;
  
    vertex_queue_size = 1;
    found = OPC_FALSE;
    hops_count = 0;

    // iterate until either we reach the destination or no more vertex can be visited//
    while ((found == OPC_FALSE) && (vertex_queue_size > 0)) {
      hops_count++;
      vertex_queue_size = 0;
      supp_queue = prg_list_create ();
      
      // get the first vertex of the discovery queue//
      cell_ptr = prg_list_head_cell_get (discovery_vertex_queue);
      // inspect entirely the discovery queue and create the next to be inspected//
      while ((cell_ptr != PRGC_NIL) && (found == OPC_FALSE)) {
        cur_vertex_ptr = (PrgT_Graph_Vertex *) prg_list_cell_data_get (cell_ptr);
        
        // get all the adjacent vertices connected to the current vertex//
        adj_vertices_vptr = prg_vertex_adjacent_vertices_get (cur_vertex_ptr, PrgC_Graph_Element_Set_Outgoing);
        vector_size = prg_vector_size(adj_vertices_vptr);
        
        // inspect the list of adjacent vertices of the current vertex//
        for (i=0; i<vector_size ;i++) {
          // read the vertex from the vector of adjacent vertices //
          vertex_ptr = prg_vector_access (adj_vertices_vptr, i);
          
          // check if the edge connecting the two vertices is not failed, //
          // otherwise the two links are considered not adjacent //
          edge_ab_ptr = prg_graph_edge_exists (topology->graph_ptr, cur_vertex_ptr, vertex_ptr, PrgC_Graph_Edge_Simplex);
          edge_state_ptr = prg_edge_client_state_get (edge_ab_ptr, topology->graph_state_handler);
          if (edge_state_ptr != PRGC_NIL) {
            if (edge_state_ptr->failure == OPC_TRUE) {
/*     
vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get(cur_vertex_ptr, topology->graph_state_handler);
asd=vertex_state_ptr->vertex_node_addr;
vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get(vertex_ptr, topology->graph_state_handler);
printf("found failed link between nodes %d and %d\n",asd,vertex_state_ptr->vertex_node_addr);
*/
              continue; // skip the vertex because it is considered not adjacent
            }
          }
          
          // get the state information of the vertex //
          vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get(vertex_ptr, topology->graph_state_handler);
        
          // check if the vertex has been already visited //
          if ( vertex_state_ptr->visited == OPC_TRUE) {
            continue;
          }
          
          if (vertex_ptr == vertex_b_ptr) {
            found = OPC_TRUE;
            break;
          }
          
          // set the vertex as already visited //
          vertex_state_ptr->visited = OPC_TRUE;
          
          // insert the vertex in the supplementary queue that will be inspected in the next iteration //
          prg_list_insert (supp_queue, vertex_ptr, PRGC_LISTPOS_HEAD);
          
          // increment the size of the queue that will be inspected in the next iteration//
          vertex_queue_size++;
          
        }
        
        // destroy the vector of adjacet vertices without deallocating them from the graph //
        prg_vector_destroy (adj_vertices_vptr, PRGC_FALSE);
        
        // get the next vertex of the discovery queue //
        cell_ptr = prg_list_cell_next_get (cell_ptr);
        
        // remove the vertex already analized//
        prg_list_remove (discovery_vertex_queue, PRGC_LISTPOS_HEAD);
      }
            
      // destroy the list without deallocating the vertices of the graph //
      prg_list_destroy (discovery_vertex_queue, PRGC_FALSE);
      
      // the new list of vertex to inspect now is the supp_queue//
      discovery_vertex_queue = supp_queue;
    }
    
    // destroy the list without deallocating the vertices of the graph //
    prg_list_destroy (discovery_vertex_queue, PRGC_FALSE);
  
  if (found == OPC_TRUE) {
    FRET(hops_count);
  }
  else {
    FRET(-1);
  }
}



/*--------------------------------CREATE ERO OBJECT FOROM VECTOR OF EDGES (PATH VECTOR)---------------------------*/
static ERO_object*  pc_ero_from_edge_vector_create( Ospf_Topology *topology, PrgT_Vector *path_vptr) {
  PrgT_Graph_Vertex   *vertex_ptr;
  PrgT_Graph_Edge     *edge_ptr;
  ERO_object        *ero_obj;
  ERO_IPv4_subobject    *ero_subobj;
  TE_vertex_state     *vertex_state_ptr;
  int           i, vsize;
  #ifdef PC_DEBUG
  char          addr_str[INETC_ADDR_STR_LEN];
  #endif
  
  FIN( pc_ero_from_edge_vector_create(topology, path_vptr) );
  // instantiate memory for ero_obj //
  ero_obj = rsvp_ero_object_create();
  
  #ifdef PC_DEBUG
  printf ("Path Computation: Created ERO ( ", addr_str);
  #endif
  
  vsize = prg_vector_size (path_vptr);
  for (i=0; i< vsize; i++) {
    // get the edge from the path vector //
    edge_ptr = (PrgT_Graph_Edge *) prg_vector_access (path_vptr, i);
    
    // get the next vertex //
    vertex_ptr = prg_edge_vertex_b_get (edge_ptr);
    
    // get the state information of the vertex //
    vertex_state_ptr = (TE_vertex_state*) prg_vertex_client_state_get(vertex_ptr, topology->graph_state_handler);
    if (vertex_state_ptr == PRGC_NIL) {
      printf ("Path Computation ERROR: No address attribute found for a vertex\n");
    }
    
    //create the ipv4 subobject
    ero_subobj = rsvp_ero_ipv4_subobject_create(vertex_state_ptr->vertex_node_addr);
    
    //append the subobject to the ero object
    rsvp_ero_subobject_add(ero_obj,ero_subobj);
    
    #ifdef PC_DEBUG
    inet_address_ptr_print(addr_str, &(vertex_state_ptr->vertex_node_addr));
    printf ("%s, ", addr_str);
    #endif
  }
  #ifdef PC_DEBUG
  printf (")\n", addr_str);
  #endif
  
  FRET(ero_obj);
}


static PrgT_Vector* pc_copy_edges_vector( PrgT_Vector *edges_vector) {
// This function is used to copy the vector containing the pointers to the graph edges.
// In particular it copies only the references. do not create a copy of the edges
PrgT_Vector   *new_edges_vector;
PrgT_Graph_Edge *edge_ptr;
unsigned int  vsize,i;

  FIN( pc_copy_edges_vector(edges_vector) );
  if ( edges_vector == OPC_NIL) FRET(OPC_NIL);
  vsize = prg_vector_size (edges_vector);
  new_edges_vector = prg_vector_create (vsize, PRGC_NIL, PRGC_NIL);
  for (i=0; i< vsize; i++) {
    edge_ptr = (PrgT_Graph_Edge *) prg_vector_access (edges_vector, i);
    prg_vector_append(new_edges_vector,edge_ptr);
  }
  FRET(new_edges_vector);
}



static int transponders_utilization_compare(Transponder_Spectrum_Assoc const* transponder_spectrum_1ptr,
                                            Transponder_Spectrum_Assoc const* transponder_spectrum_2ptr) {
  //1 if transponder_1_ptr should be closer to the vector head than transponder_2_ptr
  //0 if transponder_1_ptr and transponder_2_ptr are equal in rank
  //-1 if transponder_2_ptr should be closer to the vector head than transponder_1_ptr
  double utilization1,utilization2;
  
  FIN( transponders_utilization_compare(transponder_spectrum_1ptr,transponder_spectrum_2ptr) );

  utilization1 = transponder_utilization(transponder_spectrum_1ptr->transponder_ptr);
  utilization2 = transponder_utilization(transponder_spectrum_2ptr->transponder_ptr);
  if (utilization1>utilization2) {
      FRET(1);
  } else if (utilization1<utilization2) {
      FRET(-1);
  } else {
      FRET(0);
  }
}


static void spectrum_request_assoc_destroy(Spectrum_Request_Assoc *spectrum_request_assoc_ptr) {
  FIN(spectrum_request_assoc_destroy(transponder_smap_ptr));
  spectrum_map_destroy(spectrum_request_assoc_ptr->spectrum_map_ptr);
  prg_mem_free(spectrum_request_assoc_ptr);
  FOUT;
}

static void transponder_spectrum_assoc_destroy(Transponder_Spectrum_Assoc *transponder_smap_assoc_ptr) {
  FIN(transponder_spectrum_assoc_destroy(transponder_smap_ptr));
  prg_list_clear( transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr,
                  spectrum_request_assoc_destroy);
  prg_list_destroy( transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr,
                    OPC_TRUE);
  prg_mem_free(transponder_smap_assoc_ptr);
  FOUT;
}

static void path_spectrum_assoc_destroy(Path_Spectrum_Assoc *path_spectrum_assoc_ptr) {
  FIN(path_spectrum_assoc_destroy(path_spectrum_assoc_ptr));
  spectrum_map_destroy(path_spectrum_assoc_ptr->spectrum_map_ptr);
  prg_mem_free(path_spectrum_assoc_ptr);
  FOUT;
}

static void path_spectrum_maps_htable_destroy(PrgT_List *path_spectrum_assoc_lptr) {
  FIN(path_spectrum_maps_htable_destroy(path_spectrum_assoc_lptr));
  prg_list_clear(path_spectrum_assoc_lptr, path_spectrum_assoc_destroy);
  prg_list_destroy(path_spectrum_assoc_lptr,OPC_TRUE);
  FOUT;
}


static int paths_spectrum_map_compare(Spectrum_Map const* spectrum_map1_ptr,
                                      Spectrum_Map const* spectrum_map2_ptr) {
  // insert the most available spectrum map closer to the head of the vector
  //1 if spectrum_map1_ptr should be closer to the vector head than spectrum_map2_ptr
  //0 if spectrum_map1_ptr and spectrum_map2_ptr are equal in rank
  //-1 if spectrum_map2_ptr should be closer to the vector head than spectrum_map1_ptr
  OpT_uInt32 num_free_indexes1,num_free_indexes2;
  
  FIN( paths_spectrum_map_compare(spectrum_map1_ptr,spectrum_map2_ptr) );

  num_free_indexes1 = spectrum_map_free_num(spectrum_map1_ptr);
  num_free_indexes2 = spectrum_map_free_num(spectrum_map2_ptr);
  if (num_free_indexes1>num_free_indexes2) {
      FRET(1);
  } else if (num_free_indexes1<num_free_indexes2) {
      FRET(-1);
  } else {
      FRET(0);
  }
}

static int paths_spectrum_assoc_compare(Path_Spectrum_Assoc const* spectrum_assoc1_ptr,
                                        Path_Spectrum_Assoc const* spectrum_assoc2_ptr) {
  FIN(paths_spectrum_assoc_compare(spectrum_assoc1_ptr,spectrum_assoc2_ptr));
  FRET( paths_spectrum_map_compare(spectrum_assoc1_ptr->spectrum_map_ptr,
                                   spectrum_assoc2_ptr->spectrum_map_ptr) );
}

static PrgT_Vector* precomputed_paths(Ospf_Topology *topology_ptr, 
                                      InetT_Address const* source_addr_ptr,
                                      InetT_Address const* destination_addr_ptr) {
  PrgT_Vector *precomputed_paths_vptr;
  OpT_uInt32  hash_key[2];

  FIN(precomputed_paths(topology_ptr, source_addr_ptr, destination_addr_ptr));

  hash_key[0] = *((OpT_uInt32 *) inet_address_addr_ptr_get_const(source_addr_ptr));
  hash_key[1] = *((OpT_uInt32 *) inet_address_addr_ptr_get_const(destination_addr_ptr));
  
  precomputed_paths_vptr = prg_bin_hash_table_item_get(topology_ptr->paths_htable_ptr,hash_key);
  FRET(precomputed_paths_vptr);
}




static void precomputed_paths_save(Ospf_Topology *topology_ptr,
                                   PrgT_Vector *precomputed_paths_vptr,
                                   InetT_Address const* source_addr_ptr,
                                   InetT_Address const* destination_addr_ptr) {
  OpT_uInt32 hash_key[2], num_nodes;
  FIN(precomputed_paths_save(topology_ptr,
                 precomputed_paths_vptr,
                             source_addr_ptr,
                             destination_addr_ptr));
  // check if the hash table with the paths exists, if not create//
  if (topology_ptr->paths_htable_ptr == OPC_NIL) {
    num_nodes = op_topo_object_count (OPC_OBJTYPE_NODE_FIX);
    topology_ptr->paths_htable_ptr = prg_bin_hash_table_create(log(num_nodes*(num_nodes+1))/log(2), 2*sizeof(OpT_uInt32));
  }
  hash_key[0] = *((OpT_uInt32 *) inet_address_addr_ptr_get_const(source_addr_ptr));
  hash_key[1] = *((OpT_uInt32 *) inet_address_addr_ptr_get_const(destination_addr_ptr));
  prg_bin_hash_table_item_insert(topology_ptr->paths_htable_ptr, hash_key, precomputed_paths_vptr, PRGC_NIL);
  FOUT;
}


/*
// from a list of transponders returns a list of Transponder_Spectrum_Assoc
// structure with spectrum map considering only those transponders
// which has # free carriers >= num_carriers
static PrgT_List* transponder_spectrum_maps_list_get(PrgT_List const* transponders_lptr,
                                                    int num_carriers,
                                                    double bandwidth,
                                                    Boolean sorted) {
  PrgT_List *transponder_spectrum_lptr;
  PrgT_List_Cell *cell_ptr;
  Transponder *transponder_ptr;
  Spectrum_Map *spectrum_map_ptr;
  Transponder_Spectrum_Assoc *transponder_smap_ptr;
  
  FIN( transponder_spectrum_maps_list_get(transponders_lptr, num_carriers, bandwidth, sorted) );

  transponder_spectrum_lptr = prg_list_create();

  cell_ptr = prg_list_head_cell_get(transponders_lptr);
  while (PRGC_NIL != cell_ptr) {
    transponder_ptr = prg_list_cell_data_get(cell_ptr);
    cb_closer_allowed_capacity_slices_get(&requested_capacity, 
                                          transponder_ptr,
                                          &required_slices);

    // get the number of carriers required
    num_carriers = cb_capacity_to_num_carriers(requested_capacity);

    if (transponder_num_free_carriers(transponder_ptr) < num_carriers) {
      // the transponder cannot satisfy the demand, skip
      cell_ptr = prg_list_cell_next_get(cell_ptr);
      continue;
    }

    bandwidth = required_slices*FREQUENCY_INDEXC_SLICE_SIZE;

    // compute the spectrum map for the transponder
    spectrum_map_ptr = transponder_spectrum_map_by_demand_get(transponder_ptr,num_carriers,bandwidth);
    if (PRGC_NIL == spectrum_map_ptr) {
      cell_ptr = prg_list_cell_next_get(cell_ptr);
      continue;
    }

    transponder_smap_ptr = (Transponder_Spectrum_Assoc *) prg_mem_alloc (sizeof (Transponder_Spectrum_Assoc));
    transponder_smap_ptr->spectrum_map_ptr = spectrum_map_ptr;
    transponder_smap_ptr->transponder_ptr = transponder_ptr;
    transponder_smap_ptr->bandwidth = bandwidth;
    transponder_smap_ptr->num_carriers = num_carriers;
    transponder_smap_ptr->carrier_bw = bandwidth/num_carriers;
    if (num_carriers > 1) {
        transponder_smap_ptr->carrier_spacing = carrier_bw; // spacing in GHertz
    } else {
        transponder_smap_ptr->carrier_spacing = 0;
    }

    if (OPC_TRUE == sorted) {
      prg_list_insert_sorted(transponder_spectrum_lptr, transponder_smap_ptr, transponders_utilization_compare);
    } else {
      prg_list_insert(transponder_spectrum_lptr, transponder_smap_ptr, PRGC_LISTPOS_TAIL);
    }

    cell_ptr = prg_list_cell_next_get(cell_ptr);
  }

  if (prg_list_size(transponder_spectrum_lptr) == 0) {
    prg_list_destroy(transponder_spectrum_lptr,PRGC_TRUE);
    FRET(PRGC_NIL);
  }

  FRET(transponder_spectrum_lptr);
}
*/





/** 
  Structure useful during the path computation
**/
typedef struct Tx_Rx_Couple{ 
  Transponder *tx_transponder_ptr;
  Transponder *rx_transponder_ptr;
  Spectrum_Map *tx_smap_ptr;
  Spectrum_Map *rx_smap_ptr;
  double bandwidth;
  int num_carriers;
  double weight;
} Tx_Rx_Couple;


static int transponder_couples_compare(Tx_Rx_Couple const* tx_rx_couple1_ptr,
                                       Tx_Rx_Couple const* tx_rx_couple2_ptr) {
  //1 if tx_rx_couple1_ptr should be closer to the vector head than tx_rx_couple2_ptr
  //0 if tx_rx_couple1_ptr and tx_rx_couple2_ptr are equal in rank
  //-1 if tx_rx_couple2_ptr should be closer to the vector head than tx_rx_couple1_ptr 
  Transponder *least_performant_transponder1_ptr, *least_performant_transponder2_ptr;
  Transponder *most_performant_transponder1_ptr, *most_performant_transponder2_ptr;
  
  FIN( transponder_couples_compare(tx_rx_couple1_ptr,tx_rx_couple1_ptr) );
  if (tx_rx_couple1_ptr->bandwidth < tx_rx_couple2_ptr->bandwidth) {
    FRET(1);
  } else if (tx_rx_couple1_ptr->bandwidth == tx_rx_couple2_ptr->bandwidth) {
    //return the least performant or, if equal performant return the most used
    least_performant_transponder1_ptr = transponder_less_performant(tx_rx_couple1_ptr->tx_transponder_ptr,
                                                                    tx_rx_couple1_ptr->rx_transponder_ptr);
    least_performant_transponder2_ptr = transponder_less_performant(tx_rx_couple2_ptr->tx_transponder_ptr,
                                                                    tx_rx_couple2_ptr->rx_transponder_ptr);
    switch (transponder_performance_compare(least_performant_transponder1_ptr, least_performant_transponder2_ptr)) {
      case 1: //least perrformance transponder 1 is more performant than transponder 2
          FRET(-1);
      case -1: //least perrformance transponder 1 is less performant than transponder 2
          FRET(1);
      case 0: //transponders are equally performant
          most_performant_transponder1_ptr = transponder_most_performant(tx_rx_couple1_ptr->tx_transponder_ptr,
                                                                         tx_rx_couple1_ptr->rx_transponder_ptr);
          most_performant_transponder2_ptr = transponder_most_performant(tx_rx_couple2_ptr->tx_transponder_ptr,
                                                                         tx_rx_couple2_ptr->rx_transponder_ptr);
          switch (transponder_performance_compare(most_performant_transponder1_ptr, most_performant_transponder2_ptr)) {
            case 1: //least perrformance transponder 1 is more performant than transponder 2
                FRET(-1);
            case -1: //least perrformance transponder 1 is less performant than transponder 2
                FRET(1);
            case 0: //transponders are equally performant
                if (tx_rx_couple1_ptr->weight > tx_rx_couple2_ptr->weight) {
                  FRET(1);
                } else {
                  FRET(-1);
                }
            default:
                FRET(0);
          }
    default:
      FRET(0);
    }
  }
  else {
    FRET(-1);
  }
}

static void tx_rx_couple_destroy(Tx_Rx_Couple *tx_rx_couple_ptr) {
  FIN(tx_rx_couple_destroy(tx_rx_couple_ptr));
  prg_mem_free(tx_rx_couple_ptr);
  FOUT;
}




static PrgT_List * path_spectrum_maps_assoc_get(Ospf_Topology *topology_ptr, 
                                                InetT_Address const* source_addr_ptr,
                                                InetT_Address const* destination_addr_ptr,
                                                int required_slices,
                                                int path_pool_limit,
                                                int additional_hops) {

  PrgT_Vector *computed_paths_vptr, *edges_vptr;
  PrgT_Graph_Vertex *source_vertex_ptr, *dest_vertex_ptr;
  int min_num_hops;
  long size,i,j;
  PrgT_List *path_spectrum_assoc_lptr;
  Spectrum_Map *path_spectrum_map_ptr;
  Path_Spectrum_Assoc *path_spectrum_assoc_ptr;
  char source_addr_str[INETC_ADDR_STR_LEN], dest_addr_str[INETC_ADDR_STR_LEN];

  FIN(path_spectrum_maps_assoc_get());

  // get the source and the destination vertices
  source_vertex_ptr = topology_vertex_by_id_get(topology_ptr,source_addr_ptr);
  dest_vertex_ptr = topology_vertex_by_id_get(topology_ptr,destination_addr_ptr);
  
  if (PRGC_NIL == source_vertex_ptr || PRGC_NIL == dest_vertex_ptr) {
    op_sim_error(OPC_SIM_ERROR_ABORT,"source or destination vertex not found","");
  }

  // compute the minimum number of hops needed
  min_num_hops = pce_find_minimum_hops_number(topology_ptr, source_vertex_ptr, dest_vertex_ptr); 
  if (-1 == min_num_hops) {
    op_sim_error(OPC_SIM_ERROR_ABORT,"source and destination are not connected","");
  }

  // get the precomputed paths if existing
  computed_paths_vptr = precomputed_paths(topology_ptr, source_addr_ptr, destination_addr_ptr);

  // if not present compute pool of paths
  // and save them
  if (PRGC_NIL==computed_paths_vptr) {
    // compute the shortest+k paths
    computed_paths_vptr = prg_djk_k_paths_compute_hop_limit(source_vertex_ptr,
                                                            dest_vertex_ptr,
                                                            path_pool_limit,
                                                            min_num_hops + additional_hops);
    precomputed_paths_save(topology_ptr,
                           computed_paths_vptr,
                           source_addr_ptr,
                           destination_addr_ptr);
    //#ifdef PC_DEBUG
    inet_address_ptr_print(source_addr_str, source_addr_ptr);
    inet_address_ptr_print(dest_addr_str, destination_addr_ptr);
    printf ("Path Computation: Found %d paths in the transponder graph between nodes %s and %s\n", 
            prg_vector_size(computed_paths_vptr), source_addr_str, dest_addr_str);
    // #endif
  }
  
  // loop the sorted paths vector
  size = prg_vector_size(computed_paths_vptr);
  
  if (size == 0) {
    op_sim_error(OPC_SIM_ERROR_ABORT,"source and destination are not connected","");
  }

  path_spectrum_assoc_lptr = prg_list_create();
  // compute the spectrum map sum for each path
  for (i=0; i<size; ++i) {
    edges_vptr = prg_vector_access(computed_paths_vptr,i);
    for (j=0; j<prg_vector_size(edges_vptr);++j) {
      // update the spectrum map for the edges 
      te_edge_spectrum_map_compute(topology_ptr,
                                   prg_vector_access(edges_vptr,j),
                                   required_slices);
    }

    // compute the spectrum sum of all the edges
    path_spectrum_map_ptr = te_edges_spectrum_sum(topology_ptr,edges_vptr);
    if (PRGC_NIL == path_spectrum_map_ptr ) continue;

    path_spectrum_assoc_ptr = (Path_Spectrum_Assoc *) prg_mem_alloc (sizeof (Path_Spectrum_Assoc));
    path_spectrum_assoc_ptr->spectrum_map_ptr = path_spectrum_map_ptr;
    path_spectrum_assoc_ptr->path_vptr = edges_vptr;
    prg_list_insert_sorted (path_spectrum_assoc_lptr, path_spectrum_assoc_ptr, paths_spectrum_assoc_compare);
  }

  FRET(path_spectrum_assoc_lptr);
}



























PCReply* path_computation ( Ospf_Topology *topology_ptr, 
                            InetT_Address const* source_addr_ptr,
                            InetT_Address const* destination_addr_ptr,
                            int requested_capacity, 
                            Path_Comp_Params const * const params,
                            Transponder *tx_transponder_to_use_ptr,
                            Transponder *rx_transponder_to_use_ptr ) {
    
  PCReply *pc_reply;
  Computed_LSP *computed_lsp;
  int num_carriers, required_slices, selected_required_slices;
  int central_frequency_index, selected_central_frequency_index;
  unsigned long i;
  PrgT_Vector *selected_path_vptr;
  PrgT_Bin_Hash_Table *transponders_hptr;
  PrgT_List_Cell  *tx_cell_ptr, *rx_cell_ptr, *tx_rx_cell_ptr, *path_cell_ptr;
  PrgT_List *tx_spectrum_maps_assoc_lptr, *rx_spectrum_maps_assoc_lptr;
  PrgT_List *path_spectrum_assoc_lptr, *tx_rx_couple_lptr, *tx_transponders_lptr, *rx_transponders_lptr;
  Spectrum_Map *overall_spectrum_map_ptr, *tx_rx_spectrum_map_ptr, *tx_transponder_smap_ptr, *rx_transponder_smap_ptr;
  OpT_uInt32 transponder_id;
  Path_Spectrum_Assoc *path_spectrum_assoc_ptr;
  Transponder_Spectrum_Assoc *tx_transponder_smap_assoc_ptr, *rx_transponder_smap_assoc_ptr;
  Boolean rsa_done, first_fit_overall,break_loop, tx_rx_match, tx_tran_block, rx_tran_block, path_spectrum_block;
  double carrier_spacing, carrier_freq, *carrier_freq_ptr, bandwidth, carrier_bw, offset;
  double tx_utilization, rx_utilization, selected_bandwidth;
  Transponder *selected_tx_transponder_ptr, *selected_rx_transponder_ptr, *transponder_ptr;
  Tx_Rx_Couple *tx_rx_couple_ptr;
  PrgT_Bin_Hash_Table *path_smaps_htable_ptr;
  int num_used_transponders;



  FIN( path_computation(topology_ptr,source_addr_ptr,destination_addr_ptr,requested_capacity) );
  
  pc_reply = pc_reply_create();
  
  // get the number of carriers required
  num_carriers = cb_capacity_to_num_carriers(requested_capacity);

  tx_spectrum_maps_assoc_lptr = prg_list_create();

  if (tx_transponder_to_use_ptr == PRGC_NIL) {
    // get the list of TX transponder associated to the node
    transponders_hptr = ted_node_transponders_get(topology_ptr, source_addr_ptr);
    tx_transponders_lptr = prg_bin_hash_table_item_list_get(transponders_hptr);

    tx_cell_ptr = prg_list_head_cell_get(tx_transponders_lptr);
    while (PRGC_NIL != tx_cell_ptr) {
      transponder_ptr = (Transponder *) prg_list_cell_data_get(tx_cell_ptr);

      if (transponder_tx_num_free_carriers(transponder_ptr) < num_carriers) {
        // the transponder cannot satisfy the demand, skip
        tx_cell_ptr = prg_list_cell_next_get(tx_cell_ptr);
        continue;
      }

      tx_transponder_smap_assoc_ptr = prg_mem_alloc(sizeof(Transponder_Spectrum_Assoc));
      tx_transponder_smap_assoc_ptr->transponder_ptr = transponder_ptr;
      tx_transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr = prg_list_create();
      prg_list_insert(tx_spectrum_maps_assoc_lptr, tx_transponder_smap_assoc_ptr, PRGC_LISTPOS_HEAD);
      tx_cell_ptr = prg_list_cell_next_get(tx_cell_ptr);
    }

    prg_list_destroy (tx_transponders_lptr, PRGC_FALSE);

  } else {
    tx_transponder_smap_assoc_ptr = prg_mem_alloc(sizeof(Transponder_Spectrum_Assoc));
    tx_transponder_smap_assoc_ptr->transponder_ptr = tx_transponder_to_use_ptr;
    tx_transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr = prg_list_create();
    prg_list_insert(tx_spectrum_maps_assoc_lptr, tx_transponder_smap_assoc_ptr, PRGC_LISTPOS_HEAD);
  }



  rx_spectrum_maps_assoc_lptr = prg_list_create();

  if (rx_transponder_to_use_ptr == PRGC_NIL) {
    // get the list of RX transponder associated to the node
    transponders_hptr = ted_node_transponders_get(topology_ptr, destination_addr_ptr);
    rx_transponders_lptr = prg_bin_hash_table_item_list_get(transponders_hptr);

    rx_cell_ptr = prg_list_head_cell_get(rx_transponders_lptr);

    while (PRGC_NIL != rx_cell_ptr) {
      transponder_ptr = (Transponder *) prg_list_cell_data_get(rx_cell_ptr);

      if (transponder_rx_num_free_carriers(transponder_ptr) < num_carriers) {
        // the transponder cannot satisfy the demand, skip
        rx_cell_ptr = prg_list_cell_next_get(rx_cell_ptr);
        continue;
      }

      rx_transponder_smap_assoc_ptr = prg_mem_alloc(sizeof(Transponder_Spectrum_Assoc));
      rx_transponder_smap_assoc_ptr->transponder_ptr = transponder_ptr;
      rx_transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr = prg_list_create();
      prg_list_insert(rx_spectrum_maps_assoc_lptr, rx_transponder_smap_assoc_ptr, PRGC_LISTPOS_HEAD);
      rx_cell_ptr = prg_list_cell_next_get(rx_cell_ptr);
    }
    
    prg_list_destroy (rx_transponders_lptr, PRGC_FALSE);

  } else {
    rx_transponder_smap_assoc_ptr = prg_mem_alloc(sizeof(Transponder_Spectrum_Assoc));
    rx_transponder_smap_assoc_ptr->transponder_ptr = rx_transponder_to_use_ptr;
    rx_transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr = prg_list_create();
    prg_list_insert(rx_spectrum_maps_assoc_lptr, rx_transponder_smap_assoc_ptr, PRGC_LISTPOS_HEAD);
  }

  /*{
    char source_addr_str[INETC_ADDR_STR_LEN], dest_addr_str[INETC_ADDR_STR_LEN];
    inet_address_ptr_print(source_addr_str, source_addr_ptr);
    inet_address_ptr_print(dest_addr_str, destination_addr_ptr);
    printf("%s TX %d, %s RX %d, num_carriers %d\n",
            source_addr_str,
            prg_list_size(tx_spectrum_maps_assoc_lptr),
            dest_addr_str,
            prg_list_size(rx_spectrum_maps_assoc_lptr),
            num_carriers);
  }*/

  tx_tran_block = OPC_TRUE;
  rx_tran_block = OPC_TRUE;

  // create the list of tx rx couple
  tx_rx_couple_lptr = prg_list_create();
  
  // Couple all the TX with all RX and order based on the least used couple.
  tx_cell_ptr = prg_list_head_cell_get(tx_spectrum_maps_assoc_lptr);
  while (PRGC_NIL != tx_cell_ptr) {
    int tx_required_slices;

    tx_transponder_smap_assoc_ptr = (Transponder_Spectrum_Assoc *) prg_list_cell_data_get(tx_cell_ptr);

    cb_slices_from_capacity_get(requested_capacity, 
                                tx_transponder_smap_assoc_ptr->transponder_ptr,
                                &tx_required_slices);

    tx_utilization = transponder_utilization(tx_transponder_smap_assoc_ptr->transponder_ptr);

    // normalize the tx utilization between 0 and 1
    tx_utilization = tx_utilization/TRANSPONDER_C_MAX_UTILIZATION;

    rx_cell_ptr = prg_list_head_cell_get(rx_spectrum_maps_assoc_lptr);
    while (PRGC_NIL != rx_cell_ptr) {
        int rx_required_slices;
        Boolean found;
        PrgT_List_Cell *cell_ptr;
        Spectrum_Request_Assoc *spectrum_request_assoc_ptr;

        rx_transponder_smap_assoc_ptr = (Transponder_Spectrum_Assoc *) prg_list_cell_data_get(rx_cell_ptr);

        // get the number of slices to allocate
        cb_slices_from_capacity_get(requested_capacity, 
                                    rx_transponder_smap_assoc_ptr->transponder_ptr,
                                    &rx_required_slices);

        required_slices = (tx_required_slices > rx_required_slices) ? tx_required_slices : rx_required_slices;

        bandwidth = required_slices*FREQUENCY_INDEXC_SLICE_SIZE;

        carrier_bw = bandwidth/num_carriers;

        if (num_carriers > 1) {
            carrier_spacing = carrier_bw; // spacing in GHertz
        } else {
            carrier_spacing = 0;
        }

        
        found = OPC_FALSE;
        cell_ptr = prg_list_head_cell_get(rx_transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr);
        while (PRGC_NIL != cell_ptr) {
          spectrum_request_assoc_ptr = (Spectrum_Request_Assoc *) prg_list_cell_data_get(cell_ptr);
          if (spectrum_request_assoc_ptr->num_carriers == num_carriers &&
              spectrum_request_assoc_ptr->carrier_spacing == carrier_spacing) {
            found = OPC_TRUE;
            rx_transponder_smap_ptr = spectrum_request_assoc_ptr->spectrum_map_ptr;
            break;
          }
          cell_ptr = prg_list_cell_next_get(cell_ptr);
        }


        if (OPC_FALSE == found) {
          // compute the spectrum map for the transponder
          rx_transponder_smap_ptr = transponder_rx_spectrum_map_by_demand_get(rx_transponder_smap_assoc_ptr->transponder_ptr,
                                                                              num_carriers,
                                                                              bandwidth);

          spectrum_request_assoc_ptr = prg_mem_alloc(sizeof(Spectrum_Request_Assoc));
          spectrum_request_assoc_ptr->num_carriers = num_carriers;
          spectrum_request_assoc_ptr->carrier_spacing = carrier_spacing;
          spectrum_request_assoc_ptr->spectrum_map_ptr = rx_transponder_smap_ptr;
          
          prg_list_insert(rx_transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr,
                          spectrum_request_assoc_ptr,
                          PRGC_LISTPOS_HEAD);
        }

        if (PRGC_NIL == rx_transponder_smap_ptr) {
          rx_cell_ptr = prg_list_cell_next_get(rx_cell_ptr);
          continue;
        }

        rx_tran_block = OPC_FALSE;


        found = OPC_FALSE;
        cell_ptr = prg_list_head_cell_get(tx_transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr);
        while (PRGC_NIL != cell_ptr) {
          spectrum_request_assoc_ptr = (Spectrum_Request_Assoc *) prg_list_cell_data_get(cell_ptr);
          if (spectrum_request_assoc_ptr->num_carriers == num_carriers &&
              spectrum_request_assoc_ptr->carrier_spacing == carrier_spacing) {
            found = OPC_TRUE;
            tx_transponder_smap_ptr = spectrum_request_assoc_ptr->spectrum_map_ptr;
            break;
          }
          cell_ptr = prg_list_cell_next_get(cell_ptr);
        }

        if (OPC_FALSE == found) {
          // compute the spectrum map for the transponder
          tx_transponder_smap_ptr = transponder_tx_spectrum_map_by_demand_get(tx_transponder_smap_assoc_ptr->transponder_ptr,
                                                                              num_carriers,
                                                                              bandwidth);

          spectrum_request_assoc_ptr = prg_mem_alloc(sizeof(Spectrum_Request_Assoc));
          spectrum_request_assoc_ptr->num_carriers = num_carriers;
          spectrum_request_assoc_ptr->carrier_spacing = carrier_spacing;
          spectrum_request_assoc_ptr->spectrum_map_ptr = tx_transponder_smap_ptr;

          prg_list_insert(tx_transponder_smap_assoc_ptr->spectrum_map_request_assoc_lptr,
                          spectrum_request_assoc_ptr,
                          PRGC_LISTPOS_HEAD);
        }

        if (PRGC_NIL == tx_transponder_smap_ptr) {
          rx_cell_ptr = prg_list_cell_next_get(rx_cell_ptr);
          continue;
        }

        tx_tran_block = OPC_FALSE;


        rx_utilization = transponder_utilization(rx_transponder_smap_assoc_ptr->transponder_ptr);
        // normalize the rx utilization between 0 and 1
        rx_utilization = rx_utilization/TRANSPONDER_C_MAX_UTILIZATION;

        tx_rx_couple_ptr = prg_mem_alloc(sizeof(Tx_Rx_Couple));
        tx_rx_couple_ptr->tx_smap_ptr = tx_transponder_smap_ptr;
        tx_rx_couple_ptr->rx_smap_ptr = rx_transponder_smap_ptr;
        tx_rx_couple_ptr->tx_transponder_ptr = tx_transponder_smap_assoc_ptr->transponder_ptr;
        tx_rx_couple_ptr->rx_transponder_ptr = rx_transponder_smap_assoc_ptr->transponder_ptr;
        tx_rx_couple_ptr->weight = 1000000*tx_utilization*rx_utilization + (tx_utilization + rx_utilization);
        tx_rx_couple_ptr->bandwidth = bandwidth;
        tx_rx_couple_ptr->num_carriers = num_carriers;

        // insert the couple of TX/RX transponder into a list
        // sorted by the most used couple
        prg_list_insert_sorted (tx_rx_couple_lptr, tx_rx_couple_ptr, transponder_couples_compare);

        rx_cell_ptr = prg_list_cell_next_get(rx_cell_ptr);
    }

    tx_cell_ptr = prg_list_cell_next_get(tx_cell_ptr);
  }


  if (OPC_TRUE == rx_tran_block) {

      prg_list_destroy(tx_rx_couple_lptr,OPC_TRUE);

      prg_list_clear(tx_spectrum_maps_assoc_lptr, transponder_spectrum_assoc_destroy);
      prg_list_destroy(tx_spectrum_maps_assoc_lptr, OPC_TRUE);

      prg_list_clear(rx_spectrum_maps_assoc_lptr, transponder_spectrum_assoc_destroy);
      prg_list_destroy(rx_spectrum_maps_assoc_lptr, OPC_TRUE);

      if ( OPC_TRUE == tx_tran_block ) {
        pc_reply->result_code = INGRESS_NODE_TX_TRANSPONDER_BLOCKING;
      } else {
        pc_reply->result_code = EGRESS_NODE_RX_TRANSPONDER_BLOCKING;
      }

      FRET(pc_reply);
  }


  path_smaps_htable_ptr = prg_bin_hash_table_create(4, sizeof(int));

  rsa_done = OPC_FALSE;
  break_loop = OPC_FALSE;
  first_fit_overall = OPC_FALSE;
  tx_rx_match = OPC_FALSE;
  path_spectrum_block = OPC_TRUE;
  num_used_transponders = -1;

  // iterate the tx rx couple
  tx_rx_cell_ptr = prg_list_head_cell_get(tx_rx_couple_lptr);
  while (PRGC_NIL != tx_rx_cell_ptr) {
    tx_rx_couple_ptr = (Tx_Rx_Couple *) prg_list_cell_data_get(tx_rx_cell_ptr);

    // Rather than switching ON an unused transponder,
    // favors the first fit on overall spectrum (even if is not the real first fit)
    if ( OPC_TRUE == first_fit_overall ) {
      int temp_num_used_transp=0;
      // if the previous TX/RX couple was using at least one transponder,
      // use the previous couple.
      if (0 == tx_rx_couple_ptr->weight && num_used_transponders>0) {
        break;
      }
      if (!transponder_is_totally_free(tx_rx_couple_ptr->tx_transponder_ptr)) {
        ++temp_num_used_transp;
      }
      if (!transponder_is_totally_free(tx_rx_couple_ptr->rx_transponder_ptr)) {
        ++temp_num_used_transp;
      }
      if (temp_num_used_transp<num_used_transponders) {
        break;
      }
    } 

    // sum the spectrum of TX and RX
    tx_rx_spectrum_map_ptr = spectrum_map_copy(tx_rx_couple_ptr->tx_smap_ptr);
    // match the spectrum maps of the TX and RX transponders
    spectrum_maps_sum(&tx_rx_spectrum_map_ptr, tx_rx_couple_ptr->rx_smap_ptr);

    // if no match between the TX RX, read the next RX
    if (PRGC_NIL == tx_rx_spectrum_map_ptr) {
      tx_rx_cell_ptr = prg_list_cell_next_get(tx_rx_cell_ptr);
      continue;
    }

    // at least one TX RX match
    tx_rx_match = OPC_TRUE;

    required_slices = tx_rx_couple_ptr->bandwidth/FREQUENCY_INDEXC_SLICE_SIZE;      

    path_spectrum_assoc_lptr = (PrgT_List *) prg_bin_hash_table_item_get( path_smaps_htable_ptr,
                                                                          &required_slices);

    // if path list is NIL calculate and save it
    if (PRGC_NIL == path_spectrum_assoc_lptr) {
      path_spectrum_assoc_lptr = path_spectrum_maps_assoc_get(topology_ptr, 
                                                              source_addr_ptr,
                                                              destination_addr_ptr,
                                                              required_slices,
                                                              params->metric_ptr->path_pool_limit,
                                                              params->metric_ptr->additional_hops);

      prg_bin_hash_table_item_insert( path_smaps_htable_ptr,
                                      &required_slices,
                                      path_spectrum_assoc_lptr,
                                      PRGC_NIL);
    }

    if (prg_list_size(path_spectrum_assoc_lptr) != 0) {
      path_spectrum_block = OPC_FALSE;
    }

    // iterate the paths
    path_cell_ptr = prg_list_head_cell_get(path_spectrum_assoc_lptr);
    while(PRGC_NIL != path_cell_ptr) {
      path_spectrum_assoc_ptr = (Path_Spectrum_Assoc *) prg_list_cell_data_get(path_cell_ptr);

      overall_spectrum_map_ptr = spectrum_map_copy(tx_rx_spectrum_map_ptr);

      // match the spectrum map of the transponders with the spectrum map of the path
      spectrum_maps_sum(&overall_spectrum_map_ptr, path_spectrum_assoc_ptr->spectrum_map_ptr);

      if (PRGC_NIL == overall_spectrum_map_ptr) {
        path_cell_ptr = prg_list_cell_next_get(path_cell_ptr);
        continue;
      }

      if ( spectrum_map_first_free_frequency_get(path_spectrum_assoc_ptr->spectrum_map_ptr, &central_frequency_index) == MD_Compcode_Failure ) {
        spectrum_map_destroy(overall_spectrum_map_ptr);
        path_cell_ptr = prg_list_cell_next_get(path_cell_ptr);
        continue;
      }

      if ( spectrum_is_frequency_index_free(overall_spectrum_map_ptr, central_frequency_index) == OPC_FALSE ) {
        // if the first fit of the path cannot be allocated by the transponder
        // save a fisrt fit of the overall spectrum match
        if (first_fit_overall==OPC_FALSE &&
        spectrum_map_first_free_frequency_get(overall_spectrum_map_ptr,&central_frequency_index) == MD_Compcode_Success) {
          // we have selected a first fit base on the overall spectrum matching
          first_fit_overall = OPC_TRUE;
          // set routing and spectrum assignment as true
          rsa_done = OPC_TRUE;
          // save selected freqeuncy index, path and transponders
          selected_central_frequency_index = central_frequency_index;
          selected_path_vptr = path_spectrum_assoc_ptr->path_vptr;
          selected_tx_transponder_ptr = tx_rx_couple_ptr->tx_transponder_ptr;
          selected_rx_transponder_ptr = tx_rx_couple_ptr->rx_transponder_ptr;
          selected_bandwidth = tx_rx_couple_ptr->bandwidth;
          selected_required_slices = selected_bandwidth/FREQUENCY_INDEXC_SLICE_SIZE;
          carrier_bw = (selected_bandwidth)/(tx_rx_couple_ptr->num_carriers);
          // distance between the fist carrier and the central frequency
          offset = (selected_bandwidth - carrier_bw)/2;
          if (tx_rx_couple_ptr->num_carriers > 1) {
            carrier_spacing = carrier_bw; // spacing in GHertz
          } else {
            carrier_spacing = 0;
          }

          num_used_transponders = 0;
          if (!transponder_is_totally_free(tx_rx_couple_ptr->tx_transponder_ptr)) {
            ++num_used_transponders;
          }
          if (!transponder_is_totally_free(tx_rx_couple_ptr->rx_transponder_ptr)) {
            ++num_used_transponders;
          }
        }

        spectrum_map_destroy(overall_spectrum_map_ptr);

      } else {
        // fisrt fit based on the path spectrum succeded,
        // set routing and spectrum assignment as true
        rsa_done = OPC_TRUE;

        // if the first fit made on the overall spectrum
        // uses less BW than the first fit on the path, favors it.
        if (first_fit_overall == OPC_TRUE &&
        selected_bandwidth<tx_rx_couple_ptr->bandwidth) {
          spectrum_map_destroy(overall_spectrum_map_ptr);
          // break the loop cascade
          break_loop = OPC_TRUE;
          break;
        }

        // set false the first fit based on the overall spectrum
        // because it is overwritten
        first_fit_overall = OPC_FALSE;
        // save selected freqeuncy index, path and transponders
        selected_central_frequency_index = central_frequency_index;
        selected_path_vptr = path_spectrum_assoc_ptr->path_vptr;
        selected_tx_transponder_ptr = tx_rx_couple_ptr->tx_transponder_ptr;
        selected_rx_transponder_ptr = tx_rx_couple_ptr->rx_transponder_ptr;
        selected_bandwidth = tx_rx_couple_ptr->bandwidth;
        selected_required_slices = selected_bandwidth/FREQUENCY_INDEXC_SLICE_SIZE;

        carrier_bw = (selected_bandwidth)/(tx_rx_couple_ptr->num_carriers);
        // distance between the fist carrier and the central frequency
        offset = (selected_bandwidth - carrier_bw)/2;
        if (tx_rx_couple_ptr->num_carriers > 1) {
          carrier_spacing = carrier_bw;
        } else {
          carrier_spacing = 0;
        }

        spectrum_map_destroy(overall_spectrum_map_ptr);

        // break the loop cascade
        break_loop = OPC_TRUE;
        break;
      }
      
      path_cell_ptr = prg_list_cell_next_get(path_cell_ptr);
    }
    
    spectrum_map_destroy(tx_rx_spectrum_map_ptr);

    if (break_loop) break;

    tx_rx_cell_ptr = prg_list_cell_next_get(tx_rx_cell_ptr);
  }



  if (rsa_done) {
    pc_reply->result_code = COMPUTATION_OK;

    // add the selected central_frequency_index to the path comp reply
    // create the computed LSP object that will be returned in a list //
    computed_lsp = (Computed_LSP *) prg_mem_alloc(sizeof(Computed_LSP));

    computed_lsp->sender_tspec_ptr = (Sender_tspec_object *) prg_mem_alloc(sizeof(Sender_tspec_object));
    computed_lsp->sender_tspec_ptr->number_of_slices = selected_required_slices;
    computed_lsp->sugg_label_ptr = (Suggested_label_object *) prg_mem_alloc(sizeof(Suggested_label_object));
    computed_lsp->sugg_label_ptr->central_freq_indx = selected_central_frequency_index;
    computed_lsp->capacity = requested_capacity;
    computed_lsp->edges_vptr = pc_copy_edges_vector(selected_path_vptr);

    // create the ero from the vector of edges
    computed_lsp->ero_object_ptr = pc_ero_from_edge_vector_create(topology_ptr, computed_lsp->edges_vptr );

    // get the suggested ingress TX (/RX) transponder from the path
    transponder_id = transponder_id_get(selected_tx_transponder_ptr);
    computed_lsp->sugg_ingress_tx_trans_id_ptr = prg_mem_alloc(sizeof(OpT_uInt32));
    *(computed_lsp->sugg_ingress_tx_trans_id_ptr) = transponder_id;
    computed_lsp->sugg_ingress_rx_trans_id_ptr = prg_mem_alloc(sizeof(OpT_uInt32));
    *(computed_lsp->sugg_ingress_rx_trans_id_ptr) = transponder_id;

    // get the suggested egress RX (/TX) transponder from the path
    transponder_id = transponder_id_get(selected_rx_transponder_ptr);
    computed_lsp->sugg_egress_rx_trans_id_ptr = prg_mem_alloc(sizeof(OpT_uInt32));
    *(computed_lsp->sugg_egress_rx_trans_id_ptr) = transponder_id;
    computed_lsp->sugg_egress_tx_trans_id_ptr = prg_mem_alloc(sizeof(OpT_uInt32));
    *(computed_lsp->sugg_egress_tx_trans_id_ptr) = transponder_id;

    computed_lsp->carriers_vptr = prg_vector_create(num_carriers,prg_vector_mem_free,double_mem_copy);

    // compute the first carrier
    carrier_freq_ptr = prg_mem_alloc(sizeof(double));
    *carrier_freq_ptr = index_to_frequency(selected_central_frequency_index) - offset;
    prg_vector_append(computed_lsp->carriers_vptr, carrier_freq_ptr);
    for (i=1; i<num_carriers; ++i) {
      carrier_freq = *carrier_freq_ptr + carrier_spacing;
      carrier_freq_ptr = prg_mem_alloc(sizeof(double));
      *carrier_freq_ptr = carrier_freq;
      prg_vector_append(computed_lsp->carriers_vptr,carrier_freq_ptr);
    }

    // append the computed lsp to the pc reply
    pc_reply_computed_lsp_append (pc_reply,computed_lsp);

  } else {
      if (tx_rx_match == OPC_FALSE) {
        pc_reply->result_code = TRANSPONDER_MATCHING_BLOCKING;
      } else if (path_spectrum_block == OPC_TRUE) {
        pc_reply->result_code = PATH_SPECTRUM_BLOCKING;
      } else {
        pc_reply->result_code = TRANSPONDERS_PATHS_MATCHING_BLOCKING;
      }
  }

  // clear the memory of the temporary lists used.
  prg_list_clear(tx_rx_couple_lptr, tx_rx_couple_destroy);
  prg_list_destroy(tx_rx_couple_lptr,OPC_TRUE);

  prg_list_clear(tx_spectrum_maps_assoc_lptr, transponder_spectrum_assoc_destroy);
  prg_list_destroy(tx_spectrum_maps_assoc_lptr, OPC_TRUE);

  prg_list_clear(rx_spectrum_maps_assoc_lptr, transponder_spectrum_assoc_destroy);
  prg_list_destroy(rx_spectrum_maps_assoc_lptr, OPC_TRUE);

  prg_bin_hash_table_destroy(path_smaps_htable_ptr,path_spectrum_maps_htable_destroy);
  

  // return the path comp reply
  // it should contain: computed path, the suggested frequency, carriers and transponders
  FRET(pc_reply);
}

























typedef struct Transponder_Spectrum_Assoc2 {
  Spectrum_Map *spectrum_map_ptr;
  Transponder *transponder_ptr;
} Transponder_Spectrum_Assoc2;



static void transponder_spectrum_assoc2_destroy(Transponder_Spectrum_Assoc2 *transponder_smap_ptr) {
  FIN(transponder_spectrum_assoc2_destroy(transponder_smap_ptr));
  spectrum_map_destroy(transponder_smap_ptr->spectrum_map_ptr);
  prg_mem_free(transponder_smap_ptr);
  FOUT;
}


  typedef struct Tx_Rx_Path{ 
  Transponder *tx_ptr;
  Transponder *rx_ptr;
  Path_Spectrum_Assoc *path_spectrum_assoc_ptr;
  Spectrum_Map *overall_smap_ptr;
  int required_slices;
  } Tx_Rx_Path;


static int transponders_least_used(Transponder_Spectrum_Assoc2 const* transponder_spectrum_1ptr,
                                  Transponder_Spectrum_Assoc2 const* transponder_spectrum_2ptr) {
  double utilization1,utilization2;
  
  FIN( transponders_least_used(transponder_spectrum_1ptr,transponder_spectrum_2ptr) );

  utilization1 = transponder_utilization(transponder_spectrum_1ptr->transponder_ptr);
  utilization2 = transponder_utilization(transponder_spectrum_2ptr->transponder_ptr);
  if (utilization1<utilization2) {
      FRET(1);
  } else if (utilization1>utilization2) {
      FRET(-1);
  } else {
      FRET(0);
  }
}

static int tx_rx_path_order(Tx_Rx_Path const* tx_rx_path_1ptr,
                            Tx_Rx_Path const* tx_rx_path_2ptr) {
  unsigned int num1,num2;
  Transponder *least_performant_transponder1_ptr, *least_performant_transponder2_ptr;
  Transponder *most_performant_transponder1_ptr, *most_performant_transponder2_ptr;

  FIN( tx_rx_path_order(tx_rx_path_1ptr,tx_rx_path_2ptr) );
  
  if (tx_rx_path_1ptr->required_slices < tx_rx_path_2ptr->required_slices) {
    FRET(1);
  } else if (tx_rx_path_1ptr->required_slices == tx_rx_path_2ptr->required_slices) {
    //return the least performant or, if equal performant return the most used
    least_performant_transponder1_ptr = transponder_less_performant(tx_rx_path_1ptr->tx_ptr,
                                                                    tx_rx_path_1ptr->rx_ptr);
    least_performant_transponder2_ptr = transponder_less_performant(tx_rx_path_2ptr->tx_ptr,
                                                                    tx_rx_path_2ptr->rx_ptr);
    switch (transponder_performance_compare(least_performant_transponder1_ptr, least_performant_transponder2_ptr)) {
      case 1: //least perrformance transponder 1 is more performant than transponder 2
          FRET(-1);
      case -1: //least perrformance transponder 1 is less performant than transponder 2
          FRET(1);
      case 0: //transponders are equally performant
          most_performant_transponder1_ptr = transponder_most_performant(tx_rx_path_1ptr->tx_ptr,
                                                                         tx_rx_path_1ptr->rx_ptr);
          most_performant_transponder2_ptr = transponder_most_performant(tx_rx_path_2ptr->tx_ptr,
                                                                         tx_rx_path_2ptr->rx_ptr);

          switch (transponder_performance_compare(most_performant_transponder1_ptr, most_performant_transponder2_ptr)) {
            case 1: //most perrformance transponder 1 is more performant than transponder 2
                FRET(-1);
            case -1: //most perrformance transponder 1 is less performant than transponder 2
                FRET(1);
            case 0: // the two couple of transponders are composed of same type of transponders
                num1 = spectrum_map_free_index_min_spacing_num(tx_rx_path_1ptr->overall_smap_ptr,tx_rx_path_1ptr->required_slices);
                num2 = spectrum_map_free_index_min_spacing_num(tx_rx_path_2ptr->overall_smap_ptr,tx_rx_path_2ptr->required_slices);
                if (num1>num2) {
                    FRET(1);
                } else if (num1<num2) {
                    FRET(-1);
                } else {
                    FRET(0);
                }
            default:
                FRET(0);
          }
      default:
          FRET(0);
    }
  }
  else {
    FRET(-1);
  }

}

static void tx_rx_path_destroy(Tx_Rx_Path *tx_rx_path_ptr) {
  FIN(tx_rx_path_destroy(tx_rx_path_ptr));
  spectrum_map_destroy(tx_rx_path_ptr->overall_smap_ptr);
  prg_mem_free(tx_rx_path_ptr);
  FOUT;
}



static PrgT_List* transponder_spectrum_maps_list_get2(PrgT_List const* transponders_lptr,
                                                    int num_carriers,
                                                    double bandwidth,
                                                    Boolean tx_case) {
  PrgT_List *transponder_spectrum_lptr;
  PrgT_List_Cell *cell_ptr;
  Transponder *transponder_ptr;
  Spectrum_Map *spectrum_map_ptr;
  Transponder_Spectrum_Assoc2 *transponder_smap_ptr;
  int (*f_transponder_num_free_carriers)(Transponder const * const);
  Spectrum_Map* (*f_transponder_spectrum_map_by_demand_get)(Transponder const* const,int,double);
  
  FIN( transponder_spectrum_maps_list_get2(transponders_lptr, num_carriers, bandwidth) );

  if (tx_case) {
    f_transponder_num_free_carriers = &transponder_tx_num_free_carriers;
    f_transponder_spectrum_map_by_demand_get = &transponder_tx_spectrum_map_by_demand_get;
  } else {
    f_transponder_num_free_carriers = &transponder_rx_num_free_carriers;
    f_transponder_spectrum_map_by_demand_get = &transponder_rx_spectrum_map_by_demand_get;
  }

  transponder_spectrum_lptr = prg_list_create();

  cell_ptr = prg_list_head_cell_get(transponders_lptr);
  while (PRGC_NIL != cell_ptr) {
    transponder_ptr = prg_list_cell_data_get(cell_ptr);
    if (f_transponder_num_free_carriers(transponder_ptr) < num_carriers) {
      // the transponder cannot satisfy the demand, skip
      cell_ptr = prg_list_cell_next_get(cell_ptr);
      continue;
    }
    // compute the spectrum map for the transponder
    spectrum_map_ptr = f_transponder_spectrum_map_by_demand_get(transponder_ptr,num_carriers,bandwidth);
    if (PRGC_NIL == spectrum_map_ptr) {
      cell_ptr = prg_list_cell_next_get(cell_ptr);
      continue;
    }

    transponder_smap_ptr = (Transponder_Spectrum_Assoc2 *) prg_mem_alloc (sizeof (Transponder_Spectrum_Assoc2));
    transponder_smap_ptr->spectrum_map_ptr = spectrum_map_ptr;
    transponder_smap_ptr->transponder_ptr = transponder_ptr;
    
    prg_list_insert_sorted(transponder_spectrum_lptr, transponder_smap_ptr, transponders_least_used);

    cell_ptr = prg_list_cell_next_get(cell_ptr);
  }

  if (prg_list_size(transponder_spectrum_lptr) == 0) {
    prg_list_destroy(transponder_spectrum_lptr,PRGC_TRUE);
    FRET(PRGC_NIL);
  }

  FRET(transponder_spectrum_lptr);
}




static PrgT_List *recursive_allocation(Spectrum_Map *smap_ptr, int * allocated_requests_ptr,
                                Transponder *tx_copy_ptr, Transponder *rx_copy_ptr,
                                int num_carriers, double carrier_bw, double carrier_spacing, 
                                double bandwidth, double offset, int num_requests) {
  int central_frequency_index, selected_central_frequency_index;
  int *carrier_freq_index_ptr;
  int j;
  PrgT_List *selected_frequencies_lptr, *frequencies_lptr;

  FIN( recursive_allocation(smap_ptr, allocated_requests_ptr,tx_copy_ptr,rx_copy_ptr,
                           num_carriers,carrier_bw, carrier_spacing, 
                           bandwidth, offset) );

  selected_frequencies_lptr = prg_list_create();

  if ( PRGC_NIL == smap_ptr ) {
    FRET(selected_frequencies_lptr);
  }

  if (spectrum_map_first_free_frequency_get(smap_ptr, &central_frequency_index) == MD_Compcode_Success) {
    ++(*allocated_requests_ptr);
    selected_central_frequency_index = central_frequency_index;
    if (*allocated_requests_ptr == num_requests) {
      carrier_freq_index_ptr = prg_mem_alloc(sizeof(int));
      *carrier_freq_index_ptr = central_frequency_index;
      prg_list_insert(selected_frequencies_lptr, 
                      carrier_freq_index_ptr,
                      PRGC_LISTPOS_HEAD);
      FRET(selected_frequencies_lptr);
    }
  } else {
    FRET(selected_frequencies_lptr);
  }
  
  
  do {
    Spectrum_Map *spectrum_map_ptr, *smap_copy_ptr;
    Transponder *tx_copy_copy_ptr, *rx_copy_copy_ptr;

    double freq = index_to_frequency(central_frequency_index) - offset;

    tx_copy_copy_ptr = transponder_copy(tx_copy_ptr);
    rx_copy_copy_ptr = transponder_copy(rx_copy_ptr);
    transponder_tx_free_carrier_use(tx_copy_copy_ptr,
                                    freq,
                                    carrier_bw,
                                    PRGC_NIL,
                                    PRGC_NIL);
    transponder_rx_free_carrier_use(rx_copy_copy_ptr,
                                    freq,
                                    carrier_bw,
                                    PRGC_NIL,
                                    PRGC_NIL);
    for (j=1; j<num_carriers; ++j) {
      freq += carrier_spacing;
      transponder_tx_free_carrier_use(tx_copy_copy_ptr,
                                      freq,
                                      carrier_bw,
                                      PRGC_NIL,
                                      PRGC_NIL);
      transponder_rx_free_carrier_use(rx_copy_copy_ptr,
                                      freq,
                                      carrier_bw,
                                      PRGC_NIL,
                                      PRGC_NIL);
    }
    smap_copy_ptr = spectrum_map_copy(smap_ptr);
    spectrum_map_ptr = transponder_tx_spectrum_map_by_demand_get(tx_copy_copy_ptr, num_carriers, bandwidth);
    spectrum_maps_sum(&(smap_copy_ptr),spectrum_map_ptr);
    spectrum_map_destroy(spectrum_map_ptr);
    spectrum_map_ptr = transponder_rx_spectrum_map_by_demand_get(rx_copy_copy_ptr, num_carriers, bandwidth);
    spectrum_maps_sum(&(smap_copy_ptr),spectrum_map_ptr);
    spectrum_map_destroy(spectrum_map_ptr);

    frequencies_lptr = recursive_allocation(smap_copy_ptr,allocated_requests_ptr,
                                            tx_copy_copy_ptr, rx_copy_copy_ptr,
                                            num_carriers, carrier_bw, 
                                            carrier_spacing, bandwidth,
                                            offset,num_requests);
    spectrum_map_destroy(smap_copy_ptr);
    transponder_destroy(tx_copy_copy_ptr);
    transponder_destroy(rx_copy_copy_ptr);

    if ( prg_list_size(frequencies_lptr) > prg_list_size(selected_frequencies_lptr) ) {
      prg_list_clear(selected_frequencies_lptr, prg_mem_free);
      prg_list_destroy(selected_frequencies_lptr,OPC_TRUE);
      selected_frequencies_lptr = frequencies_lptr;
      selected_central_frequency_index = central_frequency_index;
    } else {
      prg_list_clear(frequencies_lptr, prg_mem_free);
      prg_list_destroy(frequencies_lptr,OPC_TRUE);
    }

    if ( *allocated_requests_ptr == num_requests) {
      carrier_freq_index_ptr = prg_mem_alloc(sizeof(int));
      *carrier_freq_index_ptr = selected_central_frequency_index;
      prg_list_insert(selected_frequencies_lptr,carrier_freq_index_ptr,PRGC_LISTPOS_HEAD);
      FRET(selected_frequencies_lptr);
    }

    spectrum_map_busy_set(smap_ptr, central_frequency_index);

  } while (spectrum_map_first_free_frequency_get(smap_ptr, &central_frequency_index) == MD_Compcode_Success);

  carrier_freq_index_ptr = prg_mem_alloc(sizeof(int));
  *carrier_freq_index_ptr = selected_central_frequency_index;
  prg_list_insert(selected_frequencies_lptr,carrier_freq_index_ptr,PRGC_LISTPOS_HEAD);
  FRET(selected_frequencies_lptr);
}



















PCReply* bulk_path_computation ( Ospf_Topology *topology_ptr, 
                            InetT_Address const* source_addr_ptr,
                            InetT_Address const* destination_addr_ptr,
                            int requested_capacity,
                            int num_requests,
                            Path_Comp_Params const * const params,
                            Transponder *tx_transponder_to_use_ptr,
                            Transponder *rx_transponder_to_use_ptr ) {
    
  PCReply *pc_reply;
  Computed_LSP *computed_lsp;
  int num_carriers, min_num_hops, required_slices;
  int selected_central_frequency_index;
  unsigned long size, i, j;
  PrgT_Vector *computed_paths_vptr, *edges_vptr, *selected_path_vptr;
  PrgT_Graph_Vertex *source_vertex_ptr, *dest_vertex_ptr;
  PrgT_Bin_Hash_Table *transponders_hptr;
  PrgT_List_Cell  *tx_cell_ptr, *rx_cell_ptr, *path_cell_ptr;
  PrgT_List *rx_spectrum_maps_lptr, *tx_spectrum_maps_lptr;
  PrgT_List *transponders_lptr, *path_spectrum_assoc_lptr;
  Spectrum_Map *path_spectrum_map_ptr, *overall_spectrum_map_ptr;
  OpT_uInt32 transponder_id;
  Path_Spectrum_Assoc *path_spectrum_assoc_ptr;
  Transponder_Spectrum_Assoc2 *tx_transponder_smap_ptr, *rx_transponder_smap_ptr;
  Boolean rsa_done, tx_rx_match;
  char source_addr_str[INETC_ADDR_STR_LEN], dest_addr_str[INETC_ADDR_STR_LEN];
  double carrier_spacing, carrier_freq, *carrier_freq_ptr, bandwidth, carrier_bw, offset;
  Transponder *selected_tx_transponder_ptr, *selected_rx_transponder_ptr;
  int tot_num_carriers,allocated_requests, best_allocated_requests;
  Spectrum_Map *overall_path_spectrum_map_union_ptr;
  PrgT_List *tx_rx_path_lptr, *selected_frequencies_lptr, *best_selected_frequencies_lptr;
  Tx_Rx_Path *tx_rx_path_ptr;
  PrgT_List_Cell *cell_ptr;
  Transponder *tx_copy_ptr, *rx_copy_ptr;
  int *carrier_freq_index_ptr;

  FIN( bulk_path_computation(topology_ptr,source_addr_ptr,destination_addr_ptr,requested_capacity) );
  

  pc_reply = pc_reply_create();
  
  // check if the source node has at least one available TX transponder.
  if (ted_node_free_carriers_num_get(topology_ptr,source_addr_ptr) == 0) {
    pc_reply->result_code = INGRESS_NODE_TX_TRANSPONDER_LACK_BLOCKING;
    FRET(pc_reply);
  }

  // check if the destination node has at least one available RX transponder.
  if (ted_node_free_carriers_num_get(topology_ptr,destination_addr_ptr) == 0) {
    pc_reply->result_code = EGRESS_NODE_RX_TRANSPONDER_LACK_BLOCKING;
    FRET(pc_reply);
  }

  // get the source and the destination vertices
  source_vertex_ptr = topology_vertex_by_id_get(topology_ptr,source_addr_ptr);
  dest_vertex_ptr = topology_vertex_by_id_get(topology_ptr,destination_addr_ptr);
  
  if (PRGC_NIL == source_vertex_ptr || PRGC_NIL == dest_vertex_ptr) {
    pc_reply->result_code = UNREACHABLE_DESTINATION;
    op_sim_error(OPC_SIM_ERROR_ABORT,"source or destination vertex not found","");
    FRET(pc_reply);
  }

  // compute the minimum number of hops needed
  min_num_hops = pce_find_minimum_hops_number(topology_ptr, source_vertex_ptr, dest_vertex_ptr); 
  if (-1 == min_num_hops) {
    pc_reply->result_code = UNREACHABLE_DESTINATION;
    op_sim_error(OPC_SIM_ERROR_ABORT,"source and destination are not connected","");
    FRET(pc_reply);
  }
  
  // get the number of slices to allocate
  cb_closer_allowed_capacity_slices_get(&requested_capacity, &required_slices);

  // get the number of carriers required
  num_carriers = cb_capacity_to_num_carriers(requested_capacity);

  tot_num_carriers = num_carriers*num_requests;

  bandwidth = required_slices*FREQUENCY_INDEXC_SLICE_SIZE;

  carrier_bw = bandwidth/num_carriers;

  if (num_carriers > 1) {
      carrier_spacing = carrier_bw; // spacing in GHertz
  } else {
      carrier_spacing = 0;
  }

  // distance between the fist carrier and the central frequency
  offset = (bandwidth-carrier_bw)/2; 


  if (tx_transponder_to_use_ptr == PRGC_NIL) {
    // get the list of TX transponder associated to the node
    transponders_hptr = ted_node_transponders_get(topology_ptr, source_addr_ptr);
    transponders_lptr = prg_bin_hash_table_item_list_get(transponders_hptr);
  } else {
    transponders_lptr = prg_list_create();
    prg_list_insert (transponders_lptr, tx_transponder_to_use_ptr, PRGC_LISTPOS_HEAD);
  }

  if (params->slice_ability_ptr->allow_partial_solution == OPC_TRUE) {
    // Resturn the list of transponder's spectrum maps
    // sorted by transponder utilization (from least to the most used)
    tx_spectrum_maps_lptr = transponder_spectrum_maps_list_get2( transponders_lptr,
                                                                num_carriers,
                                                                bandwidth,
                                                                OPC_TRUE);
  } else {
    tx_spectrum_maps_lptr = transponder_spectrum_maps_list_get2( transponders_lptr,
                                                                tot_num_carriers,
                                                                bandwidth,
                                                                OPC_TRUE);
  }
  // destroy the temporary list of transponder
  prg_list_destroy (transponders_lptr, PRGC_FALSE);

  if ( PRGC_NIL == tx_spectrum_maps_lptr ) {
    pc_reply->result_code = INGRESS_NODE_TX_TRANSPONDER_BLOCKING;
    FRET(pc_reply);
  }

  if (rx_transponder_to_use_ptr == PRGC_NIL) {
    // get the list of RX transponder associated to the node
    transponders_hptr = ted_node_transponders_get(topology_ptr, destination_addr_ptr);
    transponders_lptr = prg_bin_hash_table_item_list_get(transponders_hptr);
  } else {
    transponders_lptr = prg_list_create();
    prg_list_insert (transponders_lptr, rx_transponder_to_use_ptr, PRGC_LISTPOS_HEAD);
  }

  if (params->slice_ability_ptr->allow_partial_solution == OPC_TRUE) {
    rx_spectrum_maps_lptr = transponder_spectrum_maps_list_get2(transponders_lptr,
                                                                num_carriers,
                                                                bandwidth,
                                                                OPC_FALSE);
  } else {
    rx_spectrum_maps_lptr = transponder_spectrum_maps_list_get2(transponders_lptr,
                                                                tot_num_carriers,
                                                                bandwidth,
                                                                OPC_FALSE);
  }
  // destroy the temporary list of transponder
  prg_list_destroy (transponders_lptr, PRGC_FALSE);
  
  if ( PRGC_NIL == rx_spectrum_maps_lptr ) {
    prg_list_clear(tx_spectrum_maps_lptr, transponder_spectrum_assoc2_destroy);
    prg_list_destroy(tx_spectrum_maps_lptr,OPC_TRUE);
    pc_reply->result_code = EGRESS_NODE_RX_TRANSPONDER_BLOCKING;
    FRET(pc_reply);
  }


  // get the precomputed paths if existing
  computed_paths_vptr = precomputed_paths(topology_ptr, source_addr_ptr, destination_addr_ptr);

  // if not present compute pool of paths
  // and save them
  if (PRGC_NIL==computed_paths_vptr) {
    // compute the shortest+k paths
    computed_paths_vptr = prg_djk_k_paths_compute_hop_limit(source_vertex_ptr,
                                                            dest_vertex_ptr,
                                                            params->metric_ptr->path_pool_limit,
                                                            min_num_hops + params->metric_ptr->additional_hops);
    precomputed_paths_save(topology_ptr,
                           computed_paths_vptr,
                           source_addr_ptr,
                           destination_addr_ptr);
    //#ifdef PC_DEBUG
    inet_address_ptr_print(source_addr_str, source_addr_ptr);
    inet_address_ptr_print(dest_addr_str, destination_addr_ptr);
    printf ("Path Computation: Found %d paths in the transponder graph between nodes %s and %s\n", 
            prg_vector_size(computed_paths_vptr), source_addr_str, dest_addr_str);
    // #endif
  }
  
  // loop the sorted paths vector
  size = prg_vector_size(computed_paths_vptr);
  
  if (size == 0) {
    pc_reply->result_code = UNREACHABLE_DESTINATION;
    op_sim_error(OPC_SIM_ERROR_ABORT,"source and destination are not connected","");
    FRET(pc_reply);
  }

  overall_path_spectrum_map_union_ptr = PRGC_NIL;
  path_spectrum_assoc_lptr = prg_list_create();
  // compute the spectrum map sum for each path
  for (i=0; i<size; ++i) {
    edges_vptr = prg_vector_access(computed_paths_vptr,i);
    for (j=0; j<prg_vector_size(edges_vptr);++j) {
      // update the spectrum map for the edges (excluding the fake ones)
      te_edge_spectrum_map_compute(topology_ptr,
                                   prg_vector_access(edges_vptr,j),
                                   required_slices);
    }

    // compute the spectrum sum of all the edges
    path_spectrum_map_ptr = te_edges_spectrum_sum(topology_ptr,edges_vptr);
    if (PRGC_NIL == path_spectrum_map_ptr ) continue;

    path_spectrum_assoc_ptr = (Path_Spectrum_Assoc *) prg_mem_alloc (sizeof (Path_Spectrum_Assoc));
    path_spectrum_assoc_ptr->spectrum_map_ptr = path_spectrum_map_ptr;
    path_spectrum_assoc_ptr->path_vptr = edges_vptr;
    prg_list_insert_sorted (path_spectrum_assoc_lptr, path_spectrum_assoc_ptr, paths_spectrum_assoc_compare);

    spectrum_maps_union(&overall_path_spectrum_map_union_ptr, path_spectrum_map_ptr);
  }

  if (prg_list_size(path_spectrum_assoc_lptr) == 0) {
    prg_list_clear(rx_spectrum_maps_lptr, transponder_spectrum_assoc2_destroy);
    prg_list_destroy(rx_spectrum_maps_lptr,OPC_TRUE);
    prg_list_clear(tx_spectrum_maps_lptr, transponder_spectrum_assoc2_destroy);
    prg_list_destroy(tx_spectrum_maps_lptr,OPC_TRUE);
    prg_list_destroy(path_spectrum_assoc_lptr,OPC_TRUE);
    pc_reply->result_code = PATH_SPECTRUM_BLOCKING;
    FRET(pc_reply);
  }


  // create the list of tx rx couple
  tx_rx_path_lptr = prg_list_create();

  tx_rx_match = OPC_FALSE;

  // Couple all the TX with all RX and order based on the least used couple.
  tx_cell_ptr = prg_list_head_cell_get(tx_spectrum_maps_lptr);
  while (PRGC_NIL != tx_cell_ptr) {
    tx_transponder_smap_ptr = (Transponder_Spectrum_Assoc2 *) prg_list_cell_data_get(tx_cell_ptr);

    rx_cell_ptr = prg_list_head_cell_get(rx_spectrum_maps_lptr);
    while (PRGC_NIL != rx_cell_ptr) {
        rx_transponder_smap_ptr = (Transponder_Spectrum_Assoc2 *) prg_list_cell_data_get(rx_cell_ptr);

        overall_spectrum_map_ptr = spectrum_map_copy(tx_transponder_smap_ptr->spectrum_map_ptr);

        spectrum_maps_sum(&overall_spectrum_map_ptr, rx_transponder_smap_ptr->spectrum_map_ptr);

        if (PRGC_NIL == overall_spectrum_map_ptr) {
          rx_cell_ptr = prg_list_cell_next_get(rx_cell_ptr);
          spectrum_map_destroy(overall_spectrum_map_ptr);
          continue;
        }
        tx_rx_match = OPC_TRUE;

        // match the spectrum map of the transponders with the spectrum map of the union of paths
        spectrum_maps_sum(&overall_spectrum_map_ptr, overall_path_spectrum_map_union_ptr);

        if (PRGC_NIL != overall_spectrum_map_ptr) {
            tx_rx_path_ptr = (Tx_Rx_Path *) prg_mem_alloc (sizeof (Tx_Rx_Path));
            tx_rx_path_ptr->tx_ptr = tx_transponder_smap_ptr->transponder_ptr;
            tx_rx_path_ptr->rx_ptr = rx_transponder_smap_ptr->transponder_ptr;
            tx_rx_path_ptr->path_spectrum_assoc_ptr = OPC_NIL;
            tx_rx_path_ptr->overall_smap_ptr = overall_spectrum_map_ptr;
            tx_rx_path_ptr->required_slices = required_slices;

            prg_list_insert_sorted(tx_rx_path_lptr, tx_rx_path_ptr, tx_rx_path_order);
        }

        rx_cell_ptr = prg_list_cell_next_get(rx_cell_ptr);
    }

    tx_cell_ptr = prg_list_cell_next_get(tx_cell_ptr);
  }

  if (prg_list_size(tx_rx_path_lptr) == 0) {
      prg_list_destroy(tx_rx_path_lptr,OPC_TRUE);
      prg_list_clear(rx_spectrum_maps_lptr, transponder_spectrum_assoc2_destroy);
      prg_list_destroy(rx_spectrum_maps_lptr,OPC_TRUE);
      prg_list_clear(tx_spectrum_maps_lptr, transponder_spectrum_assoc2_destroy);
      prg_list_destroy(tx_spectrum_maps_lptr,OPC_TRUE);
      spectrum_map_destroy(overall_path_spectrum_map_union_ptr);
      if (!tx_rx_match) {
        pc_reply->result_code = TRANSPONDER_MATCHING_BLOCKING;
      } else {
        pc_reply->result_code = TRANSPONDERS_PATHS_MATCHING_BLOCKING;
      }
      FRET(pc_reply);
  }



  rsa_done = OPC_FALSE;
  best_allocated_requests = 0;
  selected_frequencies_lptr = PRGC_NIL;
  best_selected_frequencies_lptr = PRGC_NIL;

  cell_ptr = prg_list_head_cell_get(tx_rx_path_lptr);
  while (PRGC_NIL != cell_ptr) {
    tx_rx_path_ptr = (Tx_Rx_Path *) prg_list_cell_data_get(cell_ptr);

    tx_copy_ptr = transponder_copy(tx_rx_path_ptr->tx_ptr);
    rx_copy_ptr = transponder_copy(tx_rx_path_ptr->rx_ptr);

    allocated_requests = 0;
    selected_frequencies_lptr = recursive_allocation( tx_rx_path_ptr->overall_smap_ptr, &allocated_requests,
                                                      tx_copy_ptr, rx_copy_ptr,
                                                      num_carriers, carrier_bw, 
                                                      carrier_spacing, bandwidth,
                                                      offset, num_requests);
    
    transponder_destroy(tx_copy_ptr);
    transponder_destroy(rx_copy_ptr);

    if (allocated_requests > best_allocated_requests) {
      rsa_done = OPC_TRUE;
      best_allocated_requests = allocated_requests;

      selected_tx_transponder_ptr = tx_rx_path_ptr->tx_ptr;
      selected_rx_transponder_ptr = tx_rx_path_ptr->rx_ptr;
      if (best_selected_frequencies_lptr != PRGC_NIL) {
        prg_list_clear(best_selected_frequencies_lptr, prg_mem_free);
        prg_list_destroy(best_selected_frequencies_lptr,OPC_TRUE);
      }
      best_selected_frequencies_lptr = selected_frequencies_lptr;

      if ( best_allocated_requests == num_requests) {
        break;
      }
    }

    cell_ptr = prg_list_cell_next_get(cell_ptr);
  }



  if (rsa_done &&
      !(params->slice_ability_ptr->allow_partial_solution == OPC_FALSE && best_allocated_requests!=num_requests) ) {

    pc_reply->result_code = COMPUTATION_OK;

    cell_ptr = prg_list_head_cell_get(best_selected_frequencies_lptr);
    while (cell_ptr!=PRGC_NIL) {
      carrier_freq_index_ptr = (int *) prg_list_cell_data_get(cell_ptr);

      selected_central_frequency_index = *carrier_freq_index_ptr;

      // search a path that can allocate the demand
      path_cell_ptr = prg_list_head_cell_get(path_spectrum_assoc_lptr);
      while (path_cell_ptr != PRGC_NIL) {
        path_spectrum_assoc_ptr = (Path_Spectrum_Assoc *) prg_list_cell_data_get(path_cell_ptr);
        if (OPC_TRUE == spectrum_is_frequency_index_free(path_spectrum_assoc_ptr->spectrum_map_ptr, selected_central_frequency_index)) {
          selected_path_vptr = path_spectrum_assoc_ptr->path_vptr;
          break;
        }
        path_cell_ptr = prg_list_cell_next_get(path_cell_ptr);
      }
      

      if (path_cell_ptr==PRGC_NIL) op_sim_error(OPC_SIM_ERROR_ABORT,"ERRORE PATH COMPUTATION","1");

      // add the selected central_frequency_index to the path comp reply
      // create the computed LSP object that will be returned in a list //
      computed_lsp = (Computed_LSP *) prg_mem_alloc(sizeof(Computed_LSP));

      computed_lsp->sender_tspec_ptr = (Sender_tspec_object *) prg_mem_alloc(sizeof(Sender_tspec_object));
      computed_lsp->sender_tspec_ptr->number_of_slices = required_slices;
      computed_lsp->sugg_label_ptr = (Suggested_label_object *) prg_mem_alloc(sizeof(Suggested_label_object));
      computed_lsp->sugg_label_ptr->central_freq_indx = selected_central_frequency_index;
      computed_lsp->capacity = requested_capacity;
      computed_lsp->edges_vptr = pc_copy_edges_vector(selected_path_vptr);

      // create the ero from the vector of edges
      computed_lsp->ero_object_ptr = pc_ero_from_edge_vector_create(topology_ptr, computed_lsp->edges_vptr );

      // get the suggested ingress TX (/RX) transponder from the path
      transponder_id = transponder_id_get(selected_tx_transponder_ptr);
      computed_lsp->sugg_ingress_tx_trans_id_ptr = prg_mem_alloc(sizeof(OpT_uInt32));
      *(computed_lsp->sugg_ingress_tx_trans_id_ptr) = transponder_id;
      computed_lsp->sugg_ingress_rx_trans_id_ptr = prg_mem_alloc(sizeof(OpT_uInt32));
      *(computed_lsp->sugg_ingress_rx_trans_id_ptr) = transponder_id;

      // get the suggested egress RX (/TX) transponder from the path
      transponder_id = transponder_id_get(selected_rx_transponder_ptr);
      computed_lsp->sugg_egress_rx_trans_id_ptr = prg_mem_alloc(sizeof(OpT_uInt32));
      *(computed_lsp->sugg_egress_rx_trans_id_ptr) = transponder_id;
      computed_lsp->sugg_egress_tx_trans_id_ptr = prg_mem_alloc(sizeof(OpT_uInt32));
      *(computed_lsp->sugg_egress_tx_trans_id_ptr) = transponder_id;

      computed_lsp->carriers_vptr = prg_vector_create(num_carriers,prg_vector_mem_free,double_mem_copy);
      // compute the first carrier
      carrier_freq_ptr = prg_mem_alloc(sizeof(double));
      *carrier_freq_ptr = index_to_frequency(selected_central_frequency_index) - offset;
      prg_vector_append(computed_lsp->carriers_vptr, carrier_freq_ptr);
      for (i=1; i<num_carriers; ++i) {
        carrier_freq = *carrier_freq_ptr + carrier_spacing;
        carrier_freq_ptr = prg_mem_alloc(sizeof(double));
        *carrier_freq_ptr = carrier_freq;
        prg_vector_append(computed_lsp->carriers_vptr,carrier_freq_ptr);
      }

      // append the computed lsp to the pc reply
      pc_reply_computed_lsp_append (pc_reply,computed_lsp);

      cell_ptr = prg_list_cell_next_get(cell_ptr);
    }
  } 

  // clear the memory of the temporary lists used
  if (best_selected_frequencies_lptr != PRGC_NIL) {
    prg_list_clear(best_selected_frequencies_lptr, prg_mem_free);
    prg_list_destroy(best_selected_frequencies_lptr,OPC_TRUE);
  }
  prg_list_clear(tx_rx_path_lptr, tx_rx_path_destroy);
  prg_list_destroy(tx_rx_path_lptr,OPC_TRUE);
  prg_list_clear(rx_spectrum_maps_lptr, transponder_spectrum_assoc2_destroy);
  prg_list_destroy(rx_spectrum_maps_lptr,OPC_TRUE);
  prg_list_clear(tx_spectrum_maps_lptr, transponder_spectrum_assoc2_destroy);
  prg_list_destroy(tx_spectrum_maps_lptr,OPC_TRUE);
  prg_list_clear(path_spectrum_assoc_lptr, path_spectrum_assoc_destroy);
  prg_list_destroy(path_spectrum_assoc_lptr,OPC_TRUE);
  spectrum_map_destroy(overall_path_spectrum_map_union_ptr);

  // return the path comp reply
  // it should contain: computed path, the suggested frequency, carriers and transponders
  FRET(pc_reply);
}
