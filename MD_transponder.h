#ifndef _MD_TRANSPONDER_H_INCLUDED_
  #define _MD_TRANSPONDER_H_INCLUDED_
  
  #include <opnet.h>
  #include "MD_utils.h"
  #include "MD_spectrum.h"
  
  #define TRANSPONDER_C_MAX_UTILIZATION 1
  
  typedef struct Carrier Carrier;
  typedef struct Laser Laser;
  typedef struct Transponder Transponder;

  typedef enum  Transponder_Type{ 
  // NOTE: This list must be ordered from least to most performant transponder!
  //        If the order cannot be found, rewrite the transponder comparison function
    SBVT = 0, // Multiple laser generating a single carrier 
    MW_SBVT_FIX = 1, // 1 Multi-Wavelength Laser with fixed spacing between carriers
    MW_SBVT_VAR = 2 // 1 MW Laser with variable spacing between carriers
  } Transponder_Type;
  

 
  // create a transponder
  // if the spacing parameter given is 0 and type is SBVT_FIX, 
  // the spacing is assigned automatically when a second LSP is associated to the SBVT
  Transponder* transponder_create(OpT_uInt32 id,
                                  Transponder_Type type,
                                  int max_num_carriers,
                                  double min_spacing,
                                  double max_spacing,
                                  double min_freq,
                                  double max_freq);

  Transponder* transponder_copy(Transponder const* const transponder_ptr);

  void transponder_destroy(Transponder  *transponder_ptr);

  Transponder_Type transponder_type_get(Transponder const* const transponder_ptr);






  MD_Compcode transponder_tx_free_carrier_use(Transponder  *transponder_ptr,
                                              double carrier_frequency,
                                              double bandwidth,
                                              OpT_uInt32 * const o_laser_id_ptr,
                                              OpT_uInt32 * const o_carrier_id_ptr);

  MD_Compcode transponder_rx_free_carrier_use(Transponder  *transponder_ptr,
                                              double carrier_frequency,
                                              double bandwidth,
                                              OpT_uInt32 * const o_laser_id_ptr,
                                              OpT_uInt32 * const o_carrier_id_ptr);

  MD_Compcode transponder_tx_carriers_use(Transponder  * const transponder_ptr,
                                          PrgT_Vector const * const use_carriers_vptr,
                                          double bandwidth,
                                          PrgT_Vector * const o_used_lasers_ids_vptr,
                                          PrgT_Vector * const o_used_carrier_ids_vptr);
  MD_Compcode transponder_rx_carriers_use(Transponder  * const transponder_ptr,
                                          PrgT_Vector const * const use_carriers_vptr,
                                          double bandwidth,
                                          PrgT_Vector * const o_used_lasers_ids_vptr,
                                          PrgT_Vector * const o_used_carrier_ids_vptr);


  MD_Compcode transponder_tx_carriers_use_by_id(Transponder  *transponder_ptr,
                                                PrgT_Vector const *use_carriers_vptr,
                                                double bandwidth,
                                                PrgT_Vector const *use_carrier_ids_vptr);
  MD_Compcode transponder_rx_carriers_use_by_id(Transponder  *transponder_ptr,
                                                PrgT_Vector const *use_carriers_vptr,
                                                double bandwidth,
                                                PrgT_Vector const *use_carrier_ids_vptr);


  MD_Compcode transponder_tx_carriers_release(Transponder  *transponder_ptr, 
                                              PrgT_Vector const *carrier_ids_vptr);

  MD_Compcode transponder_rx_carriers_release(Transponder  *transponder_ptr, 
                                              PrgT_Vector const *carrier_ids_vptr);




  OpT_uInt32 transponder_id_get(Transponder const * const transponder_ptr);







  int transponder_tx_num_free_carriers(Transponder const* const transponder_ptr);

  int transponder_rx_num_free_carriers(Transponder const* const transponder_ptr);

  int transponder_num_free_carriers(Transponder const* const transponder_ptr);


  Boolean transponder_is_totally_free(Transponder const* const transponder_ptr);

  Boolean transponder_tx_is_totally_free(Transponder const* const transponder_ptr);

  Boolean transponder_rx_is_totally_free(Transponder const* const transponder_ptr);




double transponder_tx_utilization(Transponder const*const transponder_ptr);
double transponder_rx_utilization(Transponder const*const transponder_ptr);

// this function returns a value between 0 and TRANSPONDER_C_MAX_UTILIZATION corresponding
// to the utilization of the transponder carriers (0 if totally free, TRANSPONDER_C_MAX_UTILIZATION if full)
double transponder_utilization(Transponder const*const transponder_ptr);


  #define transponder_utilization_percentage(transponder_ptr) ((double)(100.0*transponder_utilization(transponder_ptr))/TRANSPONDER_C_MAX_UTILIZATION)





  OpT_uInt32* transponder_id_copy(OpT_uInt32 const * const transponder_id_ptr);

  void transponder_id_destroy(OpT_uInt32 *transponder_id_ptr);

  double transponder_minimum_spacing_get(Transponder const* const transponder_ptr);
  
  double transponder_maximum_spacing_get(Transponder const* const transponder_ptr);



  Spectrum_Map* transponder_tx_spectrum_map_by_demand_get(Transponder const* const transponder_ptr,
                                                          int num_consecutive_carriers,
                                                          double bandwidth);
  Spectrum_Map* transponder_rx_spectrum_map_by_demand_get(Transponder const* const transponder_ptr,
                                                          int num_consecutive_carriers,
                                                          double bandwidth);






  void transponder_print(Transponder const * const transponder_ptr);

  void frequency_vector_print(PrgT_Vector const * const frequencies_vptr);

  // return the least available transponder which has at least
  // a number of free carriers equal to the suggested available carriers.
  // if none of the transponders has # free carriers >= the suggested_available_carriers 
  // returns the one having the highest number of free carriers and it is least available.
  /*Transponder* transponders_least_available_transponder_get(PrgT_List const* const transponders_lptr,
                                                            int suggested_available_carriers);
*/

  //Returns the less performant transponder (if equal return transponder2_ptr).
  Transponder* transponder_less_performant( Transponder * transponder1_ptr,
                                            Transponder * transponder2_ptr);

  //Returns the most performant transponder (if equal return transponder1_ptr).
  Transponder* transponder_most_performant( Transponder * transponder1_ptr,
                                            Transponder * transponder2_ptr);


  // Returns: 1 if transponder1 is more performant than transponder2
  //         -1 if transponder1 is less performant than transponder2
  //          0 if transponder1 is equal performant than transponder2
  short transponder_performance_compare(Transponder * transponder1_ptr,
                                      Transponder * transponder2_ptr);
  


  PrgT_Compcode carrier_id_destroy(OpT_uInt32* carrier_id_ptr);

  OpT_uInt32* carrier_id_copy(OpT_uInt32 * carrier_id_ptr);

  PrgT_Vector* carrier_ids_vector_create(unsigned long size);
  void carrier_ids_vector_print(PrgT_Vector const* carrier_ids_vptr);
  #define carriers_vector_print(carriers_vptr) frequency_vector_print(carriers_vptr)

  #define transponder_is_used(transponder_ptr) (!transponder_is_totally_free(transponder_ptr))

#endif
