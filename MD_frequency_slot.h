#ifndef	_MD_FREQUENCY_SLOT_H_INCLUDED_
	#define _MD_FREQUENCY_SLOT_H_INCLUDED_

#include <opnet.h>
#include "MD_frequency_index.h"
	
#define SLOTC_SUCCESS			0
#define SLOTC_FAIL				1
#define SLOTC_HIGHER			2
#define SLOTC_LOWER				3
#define SLOTC_LOWER_ADJACENT	4
#define SLOTC_HIGHER_ADJACENT	5
#define SLOTC_INCLUDE			6
#define SLOTC_IS_INCLUDED		7
#define SLOTC_LOWER_INTERSECT	8
#define SLOTC_HIGHER_INTERSECT	9
#define SLOTC_EQUAL				10

typedef struct Frequency_Slot Frequency_Slot;

Frequency_Slot* frequency_slot_create(Frequency_Index* lowest_fi_ptr, Frequency_Index* highest_fi_ptr);
void frequency_slot_destroy(Frequency_Slot* slot_ptr);
Frequency_Slot* frequency_slot_copy(Frequency_Slot const* const slot_ptr);
short frequency_slot_resize(Frequency_Slot* slot_ptr, Frequency_Index* lowest_fi_ptr, Frequency_Index* highest_fi_ptr);

short frequency_slot_lowest_frequency_set(Frequency_Slot* slot_ptr, Frequency_Index* fi_ptr);
short frequency_slot_highest_frequency_set(Frequency_Slot* slot_ptr, Frequency_Index* fi_ptr);
Frequency_Index const* frequency_slot_highest_frequency_access(Frequency_Slot const* const slot_ptr);
Frequency_Index const* frequency_slot_lowest_frequency_access(Frequency_Slot const* const slot_ptr);

short frequency_slot_union(Frequency_Slot* slot_a_ptr,Frequency_Slot const* const slot_b_ptr);
short frequency_slot_subtract(Frequency_Slot* slot_a_ptr,Frequency_Slot const* const slot_b_ptr);

short frequency_slot_compare(Frequency_Slot const* const slot_a_ptr,Frequency_Slot const* const slot_b_ptr);
short frequency_slot_frequency_index_compare(Frequency_Slot const* const slot_a_ptr, Frequency_Index const* const fi_ptr);

PrgT_List* frequency_slot_bandwidth_allocable_indexes_get(Frequency_Slot const* const slot_ptr, double bandwidth_demand, Frequency_Index const* const fi_sample_ptr);
int frequency_slot_bandwidth_allocable_indexes_number_get(Frequency_Slot const* const slot_ptr, double bandwidth_demand, Frequency_Index const* const fi_sample_ptr, Boolean non_overlapping);
double frequency_slot_bandwidth(Frequency_Slot const* const slot_ptr);

void frequency_slot_print(Frequency_Slot const* const slot_ptr);

#endif
