#include <opnet.h>
#include <oms_pr.h>
#include "MD_ip_api.h"

/***
	This API is used to communicate with the IP layer.
***/

struct Ip_Api_Interface {
	Objid			app_mod_id;
	Objid			ip_mod_id;
	int				ip_stream_index;
	Ici				*ip_ici_ptr;
	InetT_Address	dest_addr;
};


Ip_Api_Interface* ip_api_register (Objid module_id) {
	Ip_Api_Interface 	*ip_api_intf_ptr;
	List				proc_record_handle_list;
	OmsT_Pr_Handle		process_record_handle;
	Objid				node_objid;
	Objid				ip_module_id;
	Objid				stream_id;

	/** The interface handle is used to comunicate with the IP  	**/
	/** module. The members of the interface handle 				**/
	/** data structure are filled in this function and returned to 	**/
	/** the application. The application will include this handle 	**/
	/** for future calls to the IP layer.							**/
	FIN (ip_api_register(module_id));

	ip_api_intf_ptr = prg_mem_alloc(sizeof(Ip_Api_Interface));
	
	/* Make the app module ID as a part of this interface			*/
	ip_api_intf_ptr->app_mod_id = module_id; 
	
	/* Initialize the IP module ID to INVALID. It will contain a	*/
	/* valid value after discovering the IP module ID.				*/
	ip_api_intf_ptr->ip_mod_id = OPC_OBJID_INVALID;

	/* Node ID from the application module ID. This value is used 	*/
	/* for discovery of the IP layer.								*/
	node_objid = op_topo_parent (module_id);

	/* Use process registry to find the IP module ID present in 	*/
	/* the same node as the applciation.							*/
    oms_pr_process_discover (OPC_OBJID_INVALID, &proc_record_handle_list,
                    "node objid",   OMSC_PR_OBJID,   node_objid,
                    "protocol", OMSC_PR_STRING, "MD_ip", OPC_NIL);

	/* There should be only one IP module.							*/
	if (op_prg_list_size (&proc_record_handle_list))
		{
		/* Assume that there is only one IP module.				*/

		/* Get a handle to the IP process handle.					*/
		process_record_handle = (OmsT_Pr_Handle) op_prg_list_remove (&proc_record_handle_list, 
																	 OPC_LISTPOS_HEAD);

		/* Obtain the object ID of the module running IP.			*/
		oms_pr_attr_get (process_record_handle, "module objid", OMSC_PR_OBJID, &ip_module_id);
		}
	else
		{
		/* There is no IP module. Return an invalid IP handle.	*/
		FRET (PRGC_NIL);
		}

	/* Hold the value of the IP module ID in interface handle.		*/
	ip_api_intf_ptr->ip_mod_id = ip_module_id;
		
	/* Find the index of the stream connecting application and IP.	*/
	stream_id = op_topo_connect (module_id, ip_module_id, OPC_OBJTYPE_STRM, 0);
	op_ima_obj_attr_get (stream_id, "src stream", &(ip_api_intf_ptr->ip_stream_index));
	
	// create the ici once
	ip_api_intf_ptr->ip_ici_ptr = op_ici_create ("inet_encap_req");
	
	// place the IP address of the source node into the ICI //
	op_ici_attr_set_ptr(ip_api_intf_ptr->ip_ici_ptr, "dest_addr", &(ip_api_intf_ptr->dest_addr) );
	
	FRET (ip_api_intf_ptr);
}



Ip_Api_Interface* ip_api_interface_copy(Ip_Api_Interface* ip_api_intf_ptr) {
/**
	Copy the interface. Useful in case of child processes.
**/
	Ip_Api_Interface 	*ip_api_intf_copy_ptr;
	
	FIN(ip_api_interface_copy(ip_api_intf_ptr));
	if (PRGC_NIL == ip_api_intf_ptr) FRET(PRGC_NIL);
	ip_api_intf_copy_ptr = prg_mem_alloc(sizeof(Ip_Api_Interface));
	ip_api_intf_copy_ptr->app_mod_id = ip_api_intf_ptr->app_mod_id;
	ip_api_intf_copy_ptr->ip_mod_id = ip_api_intf_ptr->ip_mod_id;
	ip_api_intf_copy_ptr->ip_stream_index = ip_api_intf_ptr->ip_stream_index;
	ip_api_intf_copy_ptr->dest_addr = inet_address_copy(ip_api_intf_ptr->dest_addr);
	ip_api_intf_copy_ptr->ip_ici_ptr = op_ici_create ("inet_encap_req");
	op_ici_attr_set_ptr(ip_api_intf_copy_ptr->ip_ici_ptr, "dest_addr", &(ip_api_intf_copy_ptr->dest_addr) );
	FRET(ip_api_intf_copy_ptr);
}


void ip_api_interface_destroy(Ip_Api_Interface* ip_api_intf_ptr) {
/**
	Destroy the interface with IP.
**/
	FIN( ip_api_interface_destroy(Ip_Api_Interface* ip_api_intf_ptr) );
	if (PRGC_NIL == ip_api_intf_ptr) FOUT;
	inet_address_destroy(ip_api_intf_ptr->dest_addr);
	op_ici_destroy(ip_api_intf_ptr->ip_ici_ptr);
	prg_mem_free(ip_api_intf_ptr);
	FOUT;
}


/*-----------------------------SEND PACKET TO IP LAYER---------------------------------*/
void ip_api_pk_send(Ip_Api_Interface* ip_api_intf, Packet* pkptr, InetT_Address const* dest_addr) {
/**
	note: dest_addr will not be destroyed by the IP module
**/
	Ici	*iciptr = OPC_NIL;

	FIN(pkt_send_to_ip_layer(pkptr,dest_addr));

	// the destination address has been already created (during registration)
	inet_address_destroy(ip_api_intf->dest_addr);
	inet_address_copy_const(ip_api_intf->dest_addr, *dest_addr);
	
	op_ici_install (ip_api_intf->ip_ici_ptr);

	// send the packet to the IP module //
	op_pk_send_forced(pkptr,ip_api_intf->ip_stream_index);

	// Deinstall the ICI to prevent it from being associated with other interrupts scheduled by this process//
	op_ici_install (OPC_NIL);

	FOUT;
}
