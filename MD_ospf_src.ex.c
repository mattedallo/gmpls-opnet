/*#include	"MD_ospf_src.h"
#include	<math.h>
#include 	<prg_graph.h>

// SINGLETON OSPF-TE VARS //
PrgT_Graph_State_ID 		*gbl_topology_graph_state_ID_ptr = OPC_NIL;
PrgT_Graph 					*gbl_topology_graph_ptr = OPC_NIL; // the topology graph
PrgT_Bin_Hash_Table			*gbl_topology_vertex_htable_ptr = OPC_NIL; // a collection of all the nodes of the network
PrgT_Bin_Hash_Table			*gbl_topology_paths_htable_ptr = OPC_NIL;  // a collection of all possible paths, given the S/D nodes

void ospf_topology_graph_init() {
int num_nodes;
	FIN(ospf_topology_graph_init());
	num_nodes = op_topo_object_count(OPC_OBJTYPE_NODE_FIX);
	gbl_topology_paths_htable_ptr = prg_bin_hash_table_create (log(num_nodes*(num_nodes+1))/log(2), 2*sizeof(OpT_uInt32)); //can manage up to num_nodes nodes
	
	// Registers a client state handler for a given namespace //
	gbl_topology_graph_state_ID_ptr = prg_mem_alloc(sizeof(PrgT_Graph_State_ID));
	*gbl_topology_graph_state_ID_ptr = prg_graph_state_handler_register ("OSPF namespace", "routing", PRGC_NIL, PRGC_NIL);
	if (*gbl_topology_graph_state_ID_ptr == PRGC_GRAPH_INVALID_ID) printf ("ERROR_OSPF: Invalid graph state handler\n");
		
	// create the graph//
	gbl_topology_graph_ptr = prg_graph_create("OSPF namespace");
	#ifdef OSPF_TE_DEBUG
		printf ("OSPF-TE: Created graph\n");
	#endif
	FOUT;
}
*/
