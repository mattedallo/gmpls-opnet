#ifndef	_MD_GLOBAL_MANAGER_H_INCLUDED_
	#define _MD_GLOBAL_MANAGER_H_INCLUDED_

#include	"MD_lsp.h"
#include	"MD_utils.h"

/***
	The LSPs Global Manager stores a global collection of pointer to LSPs.
	This makes the collection available at every node.
	
	It is implemented as a singleton.
***/

typedef struct Global_Manager Global_Manager;

Global_Manager* global_manager_get();

//MD_Compcode global_manager_lsp_insert(Global_Manager* gbl_manager_ptr, LSP* lsp_ptr);

//LSP* global_manager_lsp_get(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr);

//LSP* global_manager_lsp_remove(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr);

//global_manager_lsp_failed(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr);

//MD_Compcode global_manager_lsp_update(Global_Manager* gbl_manager_ptr, LSP const* lsp_ptr);

//void global_manager_lsp_update_or_insert(Global_Manager* gbl_manager_ptr, LSP const* lsp_ptr);

//void lsps_collector_destroy(Lsps_Collector* lsps_collector_ptr);

void global_manager_lsp_provisioning_start(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr);
void global_manager_lsp_provisioning_end(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr);
void global_manager_lsp_life_end(Global_Manager* gbl_manager_ptr, Session_object const * const session_ptr);

void global_manager_notification_sent(Global_Manager* gbl_manager_ptr);
void global_manager_increase_capacity_to_recover(Global_Manager* gbl_manager_ptr, unsigned int capacity);
void global_manager_increase_recovered_capacity(Global_Manager* gbl_manager_ptr, unsigned int capacity);
void global_manager_increase_unrecovered_capacity(Global_Manager* gbl_manager_ptr, unsigned int capacity);
void global_manager_notifications_elaborated(Global_Manager* gbl_manager_ptr, int number);

double global_manager_failure_begin_get(Global_Manager* gbl_manager_ptr);
#endif
